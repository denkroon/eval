var defaults = {
    // Where to find the different sources
    paths: {
        build: './build',
        tmp: './tmp',
        scripts: './app',
        styles: './styles'
    },

    // path to index.html
    rootFile: 'index.html',

    // Ugilfy options used on compiled typescript code
    uglify: {
        mangle: true,
        compress: true
    },

    // Typescript options
    ts: {
        reportErrors: false
    },

    // Use minimized versions of 3rd party libraries?
    minify: false,

    // Whether to combine the Javascript files or not
    combine: true,

    // extra libraries not provided by bower
    libs: {
        // will be added to vendor.js
        js: [
            'lib/inspinia/js/inspinia.js',
            'lib/inspinia/js/module.js',
            'lib/inspinia/js/directives.js',
            'lib/angular-lodash-module/angular-lodash-module.js',
            'lib/angular-sanitize/angular-sanitize.js'
        ],
        // will be added to vendor.css
        css: [
        ]
    },

    // passed to the sass compiler so we can easily @import from those paths
    sassIncludePaths: [
        'bower_components/bootstrap-sass/assets/stylesheets',
        'lib/inspinia/scss',
        'bower_components/font-awesome/scss'
    ],

    // minification options for angular templates
    htmlMin: {
        collapseWhitespace: true,
        conservativeCollapse: true,
        removeComments: true,
        collapseBooleanAttributes: true,
        removeAttributeQuotes: true,
        removeRedundantAttributes: true,
        removeEmptyAttributes: true
    },

    // browserSync options
    browserSync: {
        ghostMode: false,
        open: false,
        reloadOnRestart: true,
        browser: ['chrome']
    },

    backends: {
        default: {
            target: 'http://localhost'
        },
        testServer: {
            target: 'http://test.eval-wa.org',
            pathRewrite: {
                '^/StateEvalWeb': '/StateEval_WebAPI_Test'
            }
        }
    }
}

var fs = require('fs');
var _ = require('lodash');

var localConfig = {};
try {
    localConfig = JSON.parse(fs.readFileSync('config.local.json') || '');
}
catch (e) { }

module.exports = _.merge(defaults, localConfig);
