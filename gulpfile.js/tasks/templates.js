'use strict';

var gulp = require('gulp');

gulp.task('templates', function () {
    var config = require('../config.js');
    var newer = require('gulp-newer');
    var htmlhint = require('gulp-htmlhint');
    var htmlmin = require('gulp-htmlmin');
    var templateCache = require('gulp-angular-templatecache');
    var uglify = require('gulp-uglify');
    var gutil = require('gulp-util');
    var path = require('path');

    return gulp.src(config.paths.scripts + '/**/*.html')
        .pipe(newer(config.paths.build + '/js/templates.js'))
        .pipe(htmlhint('.htmlhintrc'))
        .pipe(htmlhint.reporter())
        .pipe(htmlhint.failReporter())
        .pipe(htmlmin(config.htmlMin))
        .pipe(templateCache({standalone: true, base: path.resolve('.') }))
        .pipe(config.minify ? uglify(config.uglify) : gutil.noop())
        .pipe(gulp.dest(config.paths.build + '/js'));
});
