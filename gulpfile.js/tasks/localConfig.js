var gulp = require('gulp');
var fs = require('fs');
var config = require('../config.js');

gulp.task('localConfig', function(cb) {
  fs.writeFile('config.local.json', JSON.stringify(config, null, 4), cb);
});
