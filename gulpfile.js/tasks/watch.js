var gulp = require('gulp');
var config = require('../config.js');
var _ = require('lodash');

function watch(proxyOptions) {
  var watch = require('gulp-watch');
  var browserSync = require('browser-sync').create();
  var proxyMiddleware = require('http-proxy-middleware');
  var proxy = proxyMiddleware('/StateEvalWeb', _.merge(proxyOptions, { ws: true }));

  watch(config.rootFile, function () { gulp.start('root'); });
  watch(config.paths.styles + '/**/*.scss', function () { gulp.start('sass'); });
  watch(config.paths.scripts + '/**/*.ts', function () { gulp.start('typescript'); });
  watch(config.paths.scripts + '/**/*.html', function () { gulp.start('templates'); });
  watch('images/**/*', function () { gulp.start('images'); });
  watch('bower.json', function () { gulp.start('vendor'); });

  if (config.combine) {
    watch(['!' + config.paths.build + '/**/*.map', config.paths.build + '/**/*'], browserSync.reload);
  }

  browserSync.init(_.merge(config.browserSync, {
    server: {
      baseDir: config.paths.build,
      routes: { // Needed to work with source maps
        '/js/app': 'app',
        '/css/styles': '.'
      },
      middleware: [proxy]
    }})
  );
}

_.forEach(config.backends, (proxyOptions, name) => {
    gulp.task(name, ['build'], () => {
        watch(proxyOptions);
    });
})