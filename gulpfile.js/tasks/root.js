'use strict';

var gulp = require('gulp');

gulp.task('root', function() {
  var newer = require('gulp-newer');
  var config = require('../config.js');

  return gulp.src(config.rootFile)
    .pipe(newer(config.paths.build))
    .pipe(gulp.dest(config.paths.build));
});
