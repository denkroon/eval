'use strict';

var gulp = require('gulp');
var ts = require('gulp-typescript');
var config = require('../config.js');

var tsProject = ts.createProject(config.paths.scripts + '/tsconfig.json', { sortOutput: true });
var testProject = ts.createProject(config.paths.scripts + '/tsconfig.json', { sortOutput: true });

gulp.task('typescript', ['root'], function () {
    var sourcemaps = require('gulp-sourcemaps');
    var concat = require('gulp-concat');
    var annotate = require('gulp-ng-annotate');
    var uglify = require('gulp-uglify');
    var gutil = require('gulp-util');
    var flatten = require('gulp-flatten');
    var inject = require('gulp-inject');

    var reporter = config.ts.reportErrors ? ts.reporter.defaultReporter() : ts.reporter.nullReporter();

    var tsResult = gulp.src([
        '!' + config.paths.scripts + '/**/*.spec.ts',
        '!' + config.paths.scripts + '/specs/**/*.ts',
        config.paths.scripts + '/**/*.ts'
    ], { base: '.' })
        .pipe(sourcemaps.init())
        .pipe(ts(tsProject, {}, reporter));

    var scripts = tsResult.js
        .pipe(annotate())
        .pipe(config.combine ? concat('app.js') : flatten())
        .pipe(config.minify ? uglify(config.uglify) : gutil.noop())
        .pipe(sourcemaps.write('.', { includeContent: false }))
        .pipe(gulp.dest(config.paths.build + '/js'));

    return gulp.src(config.paths.build + '/index.html')
        .pipe(inject(scripts, { name: 'app', relative: true }))
        .pipe(gulp.dest(config.paths.build));
});

gulp.task('typescript:spec', function () {
    var reporter = config.ts.reportErrors ? ts.reporter.defaultReporter() : ts.reporter.nullReporter();
    var tsResult = gulp.src(config.paths.scripts + '/**/*.ts')
        .pipe(ts(testProject, {}, reporter));
    return tsResult.js
        .pipe(gulp.dest(config.paths.tmp + '/js'));
});