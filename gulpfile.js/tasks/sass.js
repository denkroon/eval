'use strict';

var gulp = require('gulp');

gulp.task('sass', function () {
    var config = require('../config.js');
    var flatten = require('gulp-flatten');
    var sourcemaps = require('gulp-sourcemaps');
    var sass = require('gulp-sass');
    var postcss = require('gulp-postcss');
    var autoprefixer = require('autoprefixer');
    var mqpacker = require('css-mqpacker');
    var csswring = require('csswring');

    gulp.src(config.paths.styles + '/main.scss', { base: '.' })
        .pipe(sourcemaps.init({ debug: true }))
        .pipe(sass({
            precision: 10,
            includePaths: config.sassIncludePaths
        }).on('error', sass.logError))
        .pipe(sourcemaps.write({ includeContent: false }))
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(postcss([
            autoprefixer({ browsers: ['last 2 versions'] }),
            mqpacker({ sort: true }),
            csswring({ removeAllComments: true })
        ]))
        .pipe(flatten())
        .pipe(sourcemaps.write('.', { includeContent:false }))
        .pipe(gulp.dest(config.paths.build + '/css'));
});
