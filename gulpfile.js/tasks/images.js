var gulp = require('gulp');

gulp.task('images', function() {
  var newer = require('gulp-newer');
  var config = require('../config.js');
  var imagemin = require('gulp-imagemin');

  return gulp.src(['images/**/*', 'lib/inspinia/img/**/*'])
    .pipe(newer(config.paths.build + '/images'))
    .pipe(imagemin())
    .pipe(gulp.dest(config.paths.build + '/images'));
});
