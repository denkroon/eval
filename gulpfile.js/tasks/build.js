'use strict';

var gulp = require('gulp');

// Build everything else
gulp.task('build', ['images', 'sass', 'root', 'typescript', 'vendor', 'templates']);
