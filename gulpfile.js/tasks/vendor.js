'use strict';

var gulp = require('gulp');

// Vendor scripts
gulp.task('vendor:js', ['root', 'typescript'], function () {
    var bowerFiles = require('main-bower-files');
    var newer = require('gulp-newer');
    var sourcemaps = require('gulp-sourcemaps');
    var concat = require('gulp-concat');
    var config = require('../config.js');
    var fs = require('fs');
    var inject = require('gulp-inject');

    // attempting to pick minified files if possible
    var files = bowerFiles('**/*.js');

    if (config.minify) {
        files = files.map((name) => {
            var n = name.endsWith('.min.js') ? name : name.replace(/\.js$/, '.min.js')
            try {
                fs.accessSync(n, fs.F_OK);
            } catch (e) {
                n = name;
            }
            return n;
        });
    }

    if (config.combine) {
        return gulp.src(files.concat(config.libs.js), { base: '.' })
            .pipe(newer(config.paths.build + '/js/vendor.js'))
            .pipe(sourcemaps.init())
            .pipe(concat('vendor.js'))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(config.paths.build + '/js'));
    }
    else {
        var scripts = gulp.src(files.concat(config.libs.js), { base: '.' })
            // .pipe(flatten())
            .pipe(gulp.dest(config.paths.build + '/js'));

        return gulp.src(config.paths.build + '/index.html')
            .pipe(inject(scripts, {name: 'vendor', relative: true}))
            .pipe(gulp.dest(config.paths.build));
    }
});

gulp.task('vendor:spec', () => {
    var bowerFiles = require('main-bower-files');
    var concat = require('gulp-concat');
    var config = require('../config.js');

    var files = bowerFiles('**/*.js');
    return gulp.src(files.concat(config.libs.js).concat(['bower_components/angular-mocks/angular-mocks.js']), { base: '.' })
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(config.paths.tmp + '/js'));
});

gulp.task('vendor:css', function () {
    var bowerFiles = require('main-bower-files');
    var newer = require('gulp-newer');
    var sourcemaps = require('gulp-sourcemaps');
    var concat = require('gulp-concat');
    var postcss = require('gulp-postcss');
    var autoprefixer = require('autoprefixer');
    var mqpacker = require('css-mqpacker');
    var csswring = require('csswring');
    var url = require("postcss-url")
    var config = require('../config.js');

    return gulp.src(bowerFiles('**/*.css').concat(config.libs.css), { base: '.' })
        .pipe(newer(config.paths.build + '/css/vendor.css'))
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.css'))
        .pipe(postcss([
            autoprefixer({ browsers: ['last 2 versions'] }),
            mqpacker({ sort: true }),
            csswring({ removeAllComments: true }),
            url({ url: 'rebase' })
        ], { to: 'css/vendor.css' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.paths.build + '/css'));
});

gulp.task('vendor:fonts', function () {
    var bowerFiles = require('main-bower-files');
    var filter = require('gulp-filter');
    var flatten = require('gulp-flatten');
    var newer = require('gulp-newer');
    var config = require('../config.js');

    return gulp.src(bowerFiles(/(\.eog|\.svg|\.ttf|\.woff|\.woff2)$/), { base: '.' })
        .pipe(filter(function (file) { return /font/.test(file.path); }))
        .pipe(flatten())
        .pipe(newer(config.paths.build + '/fonts'))
        .pipe(gulp.dest(config.paths.build + '/fonts'));
});

gulp.task('vendor', ['vendor:js', 'vendor:css', 'vendor:fonts']);
