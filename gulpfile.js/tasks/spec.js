'use strict';

var gulp = require('gulp');

gulp.task('spec', ['vendor:spec', 'typescript:spec', 'templates'], (done) => {
    var path = require('path');
    var Server = require('karma').Server;

    new Server({
        configFile: path.resolve('karma.conf.js'),
        browsers: ['PhantomJS'],
        singleRun: true
    }, () => { done() }).start();
});

gulp.task('spec:ci', ['vendor:spec', 'typescript:spec', 'templates'], (done) => {
    var path = require('path');
    var Server = require('karma').Server;

    new Server({
        configFile: path.resolve('karma.conf.js'),
		reporters: ['teamcity'],
        browsers: ['PhantomJS'],
        singleRun: true
    }, () => { done() }).start();
});

gulp.task('spec:chrome', ['vendor:spec', 'typescript:spec', 'templates'], (done) => {
    var watch = require('gulp-watch');
    var path = require('path');
    var Server = require('karma').Server;
    var config = require('../config.js');

    watch(config.paths.scripts + '/**/*.ts', function () { gulp.start('typescript:spec'); });
    watch('bower.json', function () { gulp.start('vendor:spec'); });
    watch(config.paths.scripts + '/**/*.html', function () { gulp.start('templates'); });

    new Server({
        configFile: path.resolve('karma.conf.js'),
        reporters: ['progress', 'html'],
        browsers: ['Chrome'],
        singleRun: false
    }, () => { done() }).start();
});