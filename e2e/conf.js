﻿//var ScreenShotReporter = require('protractor-screenshot-reporter');


// __dirname retuns a path of this particular config file
var basePath = __dirname + '/';

exports.config = {
    allScriptsTimeout: 11000,

    suites: {
        workareas: 'specs/work-areas/**/*spec.js',
        observations: 'specs/observations/**/observation-tabs.spec.js',
/*        artifacts: 'specs/artifacts/!**!/!*spec.js'*/
        login: 'specs/login/**/*spec.js',
        settings:'specs/settings/**/*spec.js'
    },

    specs: [
        '*.js'
    ],
    capabilities: {
        'browserName': 'chrome'
    },
    chromeOnly: true,
    //baseUrl: 'http://localhost:3000',
    baseUrl: 'http://test.eval-wa.org/stateeval_test/index.html',
    framework: 'jasmine2',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 2500000,
        showColors: true
    },


    onPrepare: function() {

      /*  var jasmineReporters = require('jasmine-reporters');
        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            savePath: 'testresults',
            filePrefix: 'xmloutput'
        }));*/

        browser.manage().window().maximize();

        global.ctrlA = protractor.Key.chord(protractor.Key.CONTROL, "a");
        global.EC = protractor.ExpectedConditions;

        global.frameworkNames = {
            DanState: 'Danielson State',
            DanInstr: 'Danielson Instructional'
        };

       // global.baseUrl = 'http://localhost:3000';
        global.baseUrl= 'http://test.eval-wa.org/stateeval_test/index.html';


        global.requirePO = function (relativePath) {
            return require(basePath + 'page-objects/' + relativePath + '.po.js');
        };

        global.requireHelper = function (relativePath) {
            return require(basePath + 'helpers/' + relativePath + '.js');
        };

        // require all the pages here and make them globally accessible so we don't
        // have to do it in every spec file.

        var helperPO = requirePO('helpers/helper');
        var loginPO = requirePO('login/login');
        var observationsPO = requirePO('observations/observations');
       // var observationPO = requirePO('observations/observation');
        var notesEditorPO = requirePO('observations/notes-editor');
        var rubricCoderPO = requirePO('observations/rubric-coder');
        var leftNavPO = requirePO('layout/left-nav');
        var rubricHelperPO = requirePO('evidence-collection/rubric-helper');
        var availableEvidencePO = requirePO('evidence-collection/available-evidence');
        var topNavPO = requirePO('layout/top-nav');
        var observationSetupPO = requirePO('observations/observation-setup');
        var observationConfTorPO = requirePO('observations/observation-conf-tor');
        var observationConfTeePO = requirePO('observations/observation-conf-tee');
        var settingsPO = requirePO('settings/settings');

        global.pages = {
            helperPage: new helperPO(),
            loginPage: new loginPO(),
            observationsPage: new observationsPO(),
            //observationPage: new observationPO(),
            observationSetupPage: new observationSetupPO(),
            notesEditorPage: new notesEditorPO(),
            rubricCoderPage: new rubricCoderPO(),
            leftNavPage: new leftNavPO(),
            rubricHelperPage: new rubricHelperPO(),
            availableEvidencePage: new availableEvidencePO(),
            topNavPage: new topNavPO(),
            observationConfTorPage: new observationConfTorPO(),
            observationConfTeePage: new observationConfTeePO(),
            settingsPage : new settingsPO()

        };

        // Add a screenshot reporter and store screenshots to `/tmp/screnshots`:
        //jasmine.getEnv().addReporter(new ScreenShotReporter({
        //    baseDirectory: '/tmp/screenshots', takeScreenShotsOnlyForFailedSpecs: true
        //}));
    }

    //framework: 'jasmine2',
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    //specs: ['spec.js']
}