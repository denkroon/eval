﻿var ObservationsPage = function () {

    this.addNewButton = element(by.id("addNewButton"));
    this.formalButton = element(by.id('formal-btn'));
    this.informalButton=element(by.id('informal-btn'));

    this.title = element(by.model("vm.observation.title"));

    this.setupTab = element(by.linkText('Setup'));
    this.preConferenceTab = element(by.linkText('Pre Conference'));
    this.observeTab = element(by.linkText('Observe'));
    this.postConferenceTab = element(by.linkText('Post Conference'));
    this.alignAndScoreTab = element(by.partialLinkText('Score'));
    this.artifactsTab = element(by.linkText('Artifacts'));
    this.reportTab = element(by.linkText('Report'));


    this.go = function () {
        browser.get(global.baseUrl + "/#/observation-home/observation-list");
    };

    //this.addNewObservation = function(btn) {
    //    browser.wait(global.EC.visibilityOf(btn), 5000);
    //    btn.click();
    //    browser.getLocationAbsUrl();
    //};

    this.clickAddNewObservation = function(){
        this.addNewButton.click();
    };

    this.clickFormalButton = function(){
        browser.wait(global.EC.visibilityOf(this.formalButton), 5000);
        this.formalButton.click();
    };

    this.addNewFormalObservation = function() {
        this.clickAddNewObservation();
        this.clickFormalButton();
    };

    this.clickInFormalButton = function(){
        browser.wait(global.EC.visibilityOf(this.informalButton), 5000);
        this.informalButton.click();
    };

    this.addNewInFormalObservation = function() {
        this.clickAddNewObservation();
        this.clickInFormalButton();
    };

    // todo:
    this.selectNewestObservation = function() {
        var grid = $$('.table tr');
        var lastRow = grid.get(grid.count()-1);
        lastRow.$('button').click();
    }
};

module.exports = ObservationsPage;