﻿var NotesEditorPage = function () {

    this.notesEditor = element(by.css('.note-editable.panel-body'));
    this.removeCodeBtn = this.notesEditor.element(by.css('.remove-code-selection'));

    this.getNodeDescTag = function(rowShortName) {
        return this.notesEditor.element(by.cssContainingText('.node-desc', rowShortName));
    };

    this.getNodeToolbarBtn = function(nodeShortName) {
        return element(by.cssContainingText('.framework_node_toolbar.framework_node', nodeShortName));
    };

    this.clickEditorToolbarBtn = function(rubricShortName) {
        element(by.css('.rubric_toolbar.rubric_' + rubricShortName)).click();
    };

    this.addNotes = function(notes) {
        this.notesEditor.sendKeys(notes);
    };

    this.addNotesAndCodeToRubricRow = function(notes, rubricShortName) {
        this.addNotes(notes);
        this.notesEditor.sendKeys(global.ctrlA);
        this.clickEditorToolbarBtn(rubricShortName);
    };

    this.getNotes = function() {
        return this.notesEditor.getText().then(function(text) {
            return text.trim();
        })
    };

    this.clickRemoveCodeBtn = function() {
        this.removeCodeBtn.click();
        var btn = element(by.css('.modal-open')).element(by.css('.btn-primary'));
        browser.wait(global.EC.visibilityOf(btn), 5000);
        btn.click();
        browser.getLocationAbsUrl();
    };
};

module.exports = NotesEditorPage;