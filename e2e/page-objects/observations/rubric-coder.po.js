﻿var RubricCoderPage = function () {

    this.rubricCoder = element(by.css('.code-area'));
    this.removeCodeBtn = element(by.css('.remove-code-selection'));

    this.getNodeDescTag = function(rowShortName) {
        return element(by.cssContainingText('.node-desc', rowShortName));
    };

    this.getNodeToolbarBtn = function(nodeShortName) {
        return element(by.cssContainingText('.framework_node_toolbar.framework_node', nodeShortName));
    };

    this.clickEditorToolbarBtn = function(rubricShortName) {
        element(by.css('.rubric_toolbar.rubric_' + rubricShortName)).click();
    };

    this.codeToRubricRow = function(rubricShortName) {
        browser.executeScript(function () {
            var range = document.createRange();
            range.selectNode(document.body.querySelector('.code-area').firstChild);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
            return sel;
        });

        this.clickEditorToolbarBtn(rubricShortName);
    };

    this.getResponse = function() {
        return element(by.css('.code-area')).getText().then(function(text) {
            return text.trim();
        })
    };

    this.clickRemoveCodeBtn = function() {
        this.removeCodeBtn.click();
        var btn = element(by.css('.modal-open')).element(by.css('.btn-primary'));
        browser.wait(global.EC.visibilityOf(btn), 5000);
        btn.click();
        browser.getLocationAbsUrl();
    };
};

module.exports = RubricCoderPage;