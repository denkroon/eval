var ObservationConfTeePage = function () {

    this.promptResponseTextArea = element(by.model('userPrompt.response'));
    this.evaluateePreConfNotesTextArea = element(by.model('vm.observation.evaluateePreConNotes'));

    this.shareNotesBtn = element(by.id('share-notes-btn'));
    this.shareResponsesBtn = element(by.id('share-responses-btn'));
    this.shareConfirmBtn = element(by.id('save-true-btn'));

    this.addResponse = function(response) {
        this.promptResponseTextArea.sendKeys(response);
    };

    this.addEvaluateePreConfNotes = function(notes) {
        this.evaluateePreConfNotesTextArea.sendKeys(notes);
    };

    this.shareNotes = function() {
        this.shareNotesBtn.click();
        browser.wait(global.EC.visibilityOf(this.shareConfirmBtn), 5000);
        this.shareConfirmBtn.click();
        browser.getLocationAbsUrl();
    }

    this.shareResponses = function() {
        this.shareResponsesBtn.click();
        browser.wait(global.EC.visibilityOf(this.shareConfirmBtn), 5000);
        this.shareConfirmBtn.click();
        browser.getLocationAbsUrl();
    };
};

module.exports = ObservationConfTeePage;
