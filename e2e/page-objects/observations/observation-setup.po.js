﻿var ObservationSetupPage = function () {

    this.addPreConfBtn = element(by.id('add-pre-btn'));
    this.addPostConfBtn = element(by.id('add-post-btn'));

    this.addConference = function(addBtn) {
        addBtn.click();
        var btn = element(by.id('ok-btn'));
        browser.wait(global.EC.visibilityOf(btn), 5000);
        btn.click();
        browser.getLocationAbsUrl();
    };

    this.addPreConference = function() {
        this.addConference(this.addPreConfBtn);
    };

    this.addPostConference = function() {
        this.addConference(this.addPostConfBtn);
    }
};

module.exports = ObservationSetupPage;