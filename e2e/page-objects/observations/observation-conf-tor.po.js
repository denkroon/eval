var ObservationConfTorPage = function () {

    this.setupTab = element(by.linkText('Prompt Setup'));
    this.responsesTab = element(by.partialLinkText('Responses'));
    this.notesEditor = element(by.css('.note-editable.panel-body'));

    this.addNewPromptBtn = element(by.id('add-new-prompt-btn'));
    this.addNewPromptDoneBtn = element(by.id('add-new-prompt-done-btn'));
    this.sendPromptsBtn = element(by.id('send-prompts-btn'));
    this.addNewPromptTextArea = element(by.id('add-new-prompt-textarea'));
    this.codeAsEvidenceBtn = element(by.buttonText('Code as Evidence'));
    this.codeConfirmBtn = element(by.id('code-confirm-ok-btn'));

    this.clickResponsesTab = function() {
        this.responsesTab.click();
    };

    this.clickSetupTab = function() {
        this.setupTab.click();
    };

    this.clickCodeAsEvidenceBtn = function() {
        this.codeAsEvidenceBtn.click();
        browser.wait(global.EC.visibilityOf(this.codeConfirmBtn), 5000);
        this.codeConfirmBtn.click();
        browser.getLocationAbsUrl();
    }

    this.addNewPrompt = function(prompt, send) {
        this.addNewPromptBtn.click();
        this.addNewPromptTextArea.sendKeys(prompt);
        this.addNewPromptDoneBtn.click();

        if (send) {
            this.sendPrompts();
        }
    };

    this.sendPrompts = function() {
        this.sendPromptsBtn.click();
    }

};

module.exports = ObservationConfTorPage;
