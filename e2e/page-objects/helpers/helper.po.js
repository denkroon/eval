var HelperPage = function () {

    this.initToNewObservation = function(district, partialTorUserName, partialTeeUserName, tab) {

        global.pages.loginPage.loginAs(district, partialTorUserName);
        global.pages.leftNavPage.selectEvaluatee(partialTeeUserName);
        global.pages.leftNavPage.clickObservations();
        global.pages.observationsPage.addNewFormalObservation();
        global.pages.observationPage.clickTab(tab);
    };

    this.initNewObservationWithSharedConferencePromptResponse = function(district, partialTorUserName,
                                                                         partialTeeUserName, tab, prompt, response) {
        this.initToNewObservation(district, partialTorUserName, partialTeeUserName, tab);
        global.pages.observationConfTorPage.addNewPrompt(prompt, true);

        var torUrl = '';
        return browser.getCurrentUrl().then(function (url) {
            global.pages.loginPage.loginAs('ntps', 'T1');
            torUrl = url;
            browser.get(url + 'tee');

            global.pages.observationConfTeePage.addResponse(response);
            global.pages.observationConfTeePage.shareResponses();

            global.pages.loginPage.loginAs('ntps', 'PR1');
            global.pages.leftNavPage.selectEvaluatee('T1');
            browser.get(torUrl);
            global.pages.observationConfTorPage.clickResponsesTab();
        });
    }


    this.initNewObservationWithSharedConferenceNotes = function(district, partialTorUserName,
                                                                         partialTeeUserName, tab, notes) {
        this.initToNewObservation(district, partialTorUserName, partialTeeUserName, tab);

        var torUrl = '';
        return browser.getCurrentUrl().then(function (url) {
            global.pages.loginPage.loginAs('ntps', 'T1');
            torUrl = url;
            browser.get(url + 'tee');

            global.pages.observationConfTeePage.addEvaluateePreConfNotes(notes);
            global.pages.observationConfTeePage.shareNotes();

            global.pages.loginPage.loginAs('ntps', 'PR1');
            global.pages.leftNavPage.selectEvaluatee('T1');
            browser.get(torUrl);
            global.pages.observationConfTorPage.clickResponsesTab();
        });
    }
};

module.exports = HelperPage;