﻿var RubricHelperPage = function () {

    this.getEvidenceCount = function(nodeScoreDiv) {
        return nodeScoreDiv.isPresent().then(function(result) {
            if ( result ) {
                return nodeScoreDiv.getText().then(function(text) {return parseInt(text.trim());});
            } else {
                return 0;
            }
        });
    };

    this.getNodeDiv = function(shortName) {
        var cell1Div = element(by.cssContainingText('.node-name.cell-1', shortName));
        return cell1Div.element(by.xpath('..'));
    };

    this.getRowDiv = function(shortName) {
        var cell1Div = element(by.cssContainingText('.row-name.cell-1', shortName));
        return cell1Div.element(by.xpath('..'));
    };

    this.nodeEvidenceCount = function(shortName) {
        var nodeDiv = this.getNodeDiv(shortName);
        var cell3Div = nodeDiv.element(by.css('.node-score'));
        return this.getEvidenceCount(cell3Div);
    };

    this.expandNodeAndClickRow = function(nodeShortName, rowShortName) {
        var that = this;
        return this.expandNode(nodeShortName).then(function() {
            that.clickRow(rowShortName);
        });
    };

    this.expandNode = function(shortName) {
        var cell2Div;
        var nodeDiv = this.getNodeDiv(shortName);
        var sectionDiv = nodeDiv.element(by.xpath('..'));
        var rowDiv = sectionDiv.element(by.css('.row-name.cell-1'));

        return rowDiv.isDisplayed().then(function(isDisplayed) {
            if (!isDisplayed) {
                cell2Div = nodeDiv.element(by.css('.cell-2'));
                cell2Div.click();
            }
        });
    };

    this.clickRow = function(rowShortName) {
        var rowDiv = this.getRowDiv(rowShortName);
        var cell2Div = rowDiv.element(by.css('.cell-2'));
        cell2Div.click();
    };

    this.rowEvidenceCount = function(shortName) {
        var rowDiv = this.getRowDiv(shortName);
        var cell3Div = rowDiv.element(by.css('.node-score'));
        return this.getEvidenceCount(cell3Div);
    };

};

module.exports = RubricHelperPage;
