﻿var AvailableEvidencePage = function () {

    this.addOtherEvidenceBtn = element(by.id('add-other-evidence-btn'));
    this.otherEvidenceInput = element(by.id('other-evidence-textarea'));
    this.otherEvidenceDoneBtn = element(by.id('other-evidence-done-btn'));
    this.deleteEvidenceBtn = element(by.css('.remove-evidence-btn'));

    this.getObservationNoteElement = function() {
        return element(by.css('p[ng-bind-html="vm.evidence.rubricRowAnnotation.annotation"]'));
    };

    this.getOtherEvidenceElement = function() {
        return element(by.css('p[ng-bind-html="vm.evidence.rubricRowAnnotation.annotation"]'));
    };

    this.getConfNotesTorEvidenceElement = function() {
        return element(by.css('p[ng-bind-html="vm.evidence.rubricRowAnnotation.annotation"]'));
    };

    this.getConfPromptResponseEvidenceElement = function() {
        return element(by.css('p[ng-bind-html="vm.evidence.rubricRowAnnotation.annotation"]'));
    };

    this.addOtherEvidence = function(text) {
        this.addOtherEvidenceBtn.click();
        browser.wait(global.EC.visibilityOf(this.otherEvidenceInput), 5000);
        this.otherEvidenceInput.sendKeys(text);
        this.otherEvidenceDoneBtn.click();
    };

    this.removeEvidence = function() {
        this.deleteEvidenceBtn.click();
        var btn = element(by.id('ok-btn'));
        browser.wait(global.EC.visibilityOf(btn), 5000);
        btn.click();
        browser.getLocationAbsUrl();
    }
};

module.exports = AvailableEvidencePage;
