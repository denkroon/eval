var TopNavPage = function () {

    this.logOutButton=element(by.linkText('Log out'));

    this.changeFramework = function(frameworkName) {
        var select = element(by.model('vm.frameworkId'));
        select.$('[label="' + frameworkName + '"]').click();
    };

    this.logOut = function(){
        this.logOutButton.click();
    };


};

module.exports = TopNavPage;