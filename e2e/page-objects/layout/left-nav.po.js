var LeftNavPage = function () {

    this.evaluateeName = element(by.model('vm.context.evaluatee'));
    this.observationTab = element(by.cssContainingText('.nav-label', 'Observations'));
    this.settingsTab=element(by.cssContainingText('.nav-label', 'Settings'));
    this.selfAssessmentTab = element(by.cssContainingText('.nav-label', 'Self-Assessments'))

    this.selectEvaluatee = function(name) {
        this.evaluateeName.sendKeys(name);
    };

    this.clickObservations = function() {
        this.observationTab.click();
        browser.getLocationAbsUrl();
    };

    this.clickSettings = function() {
        this.settingsTab.click();
    };
};

module.exports = LeftNavPage;