var SettingsPage=function(){

    this.selfAssessmentOptions=element(by.id('cbSelfAssessmentsModuleEnabled'));
    this.saveSettingsButton=element(by.buttonText('Save Settings'));

    this.toastMessage = element(by.id('toast-container'));

    this.unCheckSelfAssessmentOptions = function(){
        this.selfAssessmentOptions.click();
    };

    this.saveSettingsChanges = function(){
      this.saveSettingsButton.click();
    };
};

module.exports=SettingsPage;
