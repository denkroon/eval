﻿var config = require("./../../config");

var ArtifactBuilderPage = function () {

    this.title = element(by.model("vm.artifact.title"));
    this.saveButton = element(by.id('saveButton'));
    this.changeFocusDiv = element(by.id('changeFocusDiv'));

    this.go = function () {
        browser.get(ArtifactBuilderPage.url);
    };

    this.withTitle = function (newTitle) {

        this.title.sendKeys(newTitle);
        this.changeFocusDiv.click();
        return this;
    };

    /*this.getLinkedObservationRepeater function(title) {
        var observations = element.all(by.repeater('category in vm.categories')).then(function(rows) {

        });

        element.all(by.repeater('suggestion_type in searchBoxData.suggestion_types')).then(function(rows) {
            for(var i=0;i<rows.length;i++){
                element.all(by.repeater('row in category.rows')).each(function(elem) {
                    elem.getText().then(function(text) {

                    });
                });
            }
        });
    }*/

    this.save = function() {
        this.saveButton.click();
    }
};

module.exports = ArtifactBuilderPage;