﻿var config = require("../../config");
var ArtifactBuilderPage = require('artifact-builder.po');

var ArtifactsPage = function () {

    this.addNewButton = element(by.id("addNewButton"));

    this.go = function () {
        browser.get(ArtifactsPage.url);
    };

    this.addNewArtifact = function() {
        this.addNewButton.click();
        return new ArtifactBuilderPage();
    }
};

ArtifactsPage.url = config.baseUrl + "/#/artifacts";

module.exports = ArtifactsPage;