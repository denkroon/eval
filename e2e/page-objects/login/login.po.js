﻿
var LoginPage = function () {

    this.norththurston_usernames  = [
        "North Thurston High School AD",
        "North Thurston High School PRH",
        "North Thurston High School PR1",
        "North Thurston High School PR2",
        "North Thurston High School T1",
        "North Thurston High School T2",
        "North Thurston High School LIB",
        "North Thurston Public Schools DA",
        "North Thurston Public Schools DV",
        "North Thurston Public Schools DE1",
        "North Thurston Public Schools DE2",
        "North Thurston Public Schools DTE1",
        "North Thurston Public Schools DTE2",
        "North Thurston Public Schools DRM",
        "TMS - NTHS, SBE",
        "PMS - NTHS, SBE"
    ];

    this.school_district = [
        "Chehalis School District",
        "Conway School District",
        "North Thurston Public Schools",
        "Othello School District"
    ];



    this.loginButton = element(by.id("loginButton"));
    this.districtName = element(by.model('vm.district'));
    this.userName=element(by.model('vm.user'));


    this.go = function () {
        browser.get(global.baseUrl + "#/login");
    };

    this.getFullUserName = function(district, partialUserName) {
        if (district === 'ntps') {
            for (var i=0; i<this.norththurston_usernames.length; ++i) {
                if (this.norththurston_usernames[i].indexOf(partialUserName)>0) {
                    return this.norththurston_usernames[i];
                }
            }

            throw Error('getFullUserName: partial not found');
        }
        else {
            throw Error('getFullUserName: unexpected district');
        }
    };

    this.loginAs = function(district, partialUserName) {
        this.go();
        var select = element(by.model('vm.district'));
        select.$('[value="' + district + '"]').click();
        select.click();

        select = element(by.model('vm.school'));
        var fullUserName = this.getFullUserName(district, partialUserName);
        select.$('[value="' + fullUserName + '"]').click();
        select.click();

        this.login();

    };

    this.loginAsValidUser = function(district, userName) {
        this.go();
        var select = this.districtName;
        select.$('[label="' + district + '"]').click();
        select.click();

        select = this.userName;
        select.$('[label="' + userName + '"]').click();
        select.click();

        this.login();

    };

    this.login = function() {
        this.loginButton.click();
        browser.getLocationAbsUrl();
    };
};

module.exports = LoginPage;