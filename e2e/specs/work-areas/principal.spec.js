/**
 * Created by anne on 4/22/2016.
 */

describe('principal work-areas', function() {
    describe('evaluate teachers', function () {

        beforeAll(function () {
            global.pages.loginPage.loginAs('ntps', 'PR1');
        });

        beforeEach(function () {
            global.pages.leftNavPage.clickObservations();
        });

        it('should start on setup page', function () {
            global.pages.observationsPage.addNewFormalObservation();
            browser.getCurrentUrl().then(function (actualUrl) {
                expect(actualUrl).toContain('setup');
            });
        });

        it('should have setup, pre, observe, post, report tabs for formal evaluator view', function () {
            global.pages.observationsPage.addNewFormalObservation();
            expect(global.pages.observationPage.setupTab.isPresent()).toBeTruthy();
            expect(global.pages.observationPage.preConfTab.isPresent()).toBeTruthy();
            expect(global.pages.observationPage.observeTab.isPresent()).toBeTruthy();
            expect(global.pages.observationPage.postConfTab.isPresent()).toBeTruthy();
            expect(global.pages.observationPage.reportTab.isPresent()).toBeTruthy();
        });

        it('should have setup, pre (no), observe, post (no), report tabs for informal evaluator view', function () {
            global.pages.observationsPage.addNewInFormalObservation();
            expect(global.pages.observationPage.setupTab.isPresent()).toBeTruthy();
            expect(global.pages.observationPage.preConfTab.isPresent()).toBeFalsy();
            expect(global.pages.observationPage.observeTab.isPresent()).toBeTruthy();
            expect(global.pages.observationPage.postConfTab.isPresent()).toBeFalsy();
            expect(global.pages.observationPage.reportTab.isPresent()).toBeTruthy();

        });

        it('should add conference tabs after clicking the add conference buttons', function () {
            global.pages.observationsPage.addNewInFormalObservation();
            expect(global.pages.observationSetupPage.addPreConfBtn.isPresent()).toBeTruthy();
            expect(global.pages.observationSetupPage.addPostConfBtn.isPresent()).toBeTruthy();
            global.pages.observationSetupPage.addPreConference();
            expect(global.pages.observationPage.preConfTab.isPresent()).toBeTruthy();
            expect(global.pages.observationPage.postConfTab.isPresent()).toBeFalsy();
            global.pages.observationSetupPage.addPostConference();
            expect(global.pages.observationPage.preConfTab.isPresent()).toBeTruthy();
            expect(global.pages.observationPage.preConfTab.isPresent()).toBeTruthy();
        });
    });

    describe('observation tabs evaluatee', function () {

        beforeAll(function() {
        });

        beforeEach(function() {
            global.pages.loginPage.loginAs('ntps', 'PR1');
            global.pages.leftNavPage.selectEvaluatee('T1');
            global.pages.leftNavPage.clickObservations();
        });

        it('should have setup, pre, observe (no),  post, report tabs for formal evaluatee view', function() {
            global.pages.observationsPage.addNewFormalObservation();
            browser.getCurrentUrl().then(function(url) {
                global.pages.loginPage.loginAs('ntps', 'T1');
                browser.get(url);

                expect(global.pages.observationPage.setupTab.isPresent()).toBeTruthy();
                expect(global.pages.observationPage.preConfTab.isPresent()).toBeTruthy();
                expect(global.pages.observationPage.observeTab.isPresent()).toBeFalsy();
                expect(global.pages.observationPage.postConfTab.isPresent()).toBeTruthy();
                expect(global.pages.observationPage.reportTab.isPresent()).toBeTruthy();

            });
        });

        it('should have setup, pre (no), observe (no),  post (no), report tabs for informal evaluatee view', function() {
            global.pages.observationsPage.addNewInFormalObservation();
            browser.getCurrentUrl().then(function(url) {
                global.pages.loginPage.loginAs('ntps', 'T1');
                browser.get(url);

                expect(global.pages.observationPage.setupTab.isPresent()).toBeTruthy();
                expect(global.pages.observationPage.preConfTab.isPresent()).toBeFalsy();
                expect(global.pages.observationPage.observeTab.isPresent()).toBeFalsy();
                expect(global.pages.observationPage.postConfTab.isPresent()).toBeFalsy();
                expect(global.pages.observationPage.reportTab.isPresent()).toBeTruthy();

            });
        });
    });
});
