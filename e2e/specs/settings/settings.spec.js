/* As a district administrator i would like to turn off the self-assessments module in eVAL.*/

describe('Settings Tab',function (){

    describe('For Teacher, self-assessments options ',function () {
        beforeAll(function() {
            global.pages.loginPage.loginAsValidUser(global.pages.loginPage.school_district[2],global.pages.loginPage.norththurston_usernames[7] );
        });

        beforeEach(function() {
            global.pages.leftNavPage.clickSettings();
        });

        it('should present at the settings page', function () {

            browser.getCurrentUrl().then(function (actualUrl) {
                expect(actualUrl).toContain('settings');
            });

            expect(global.pages.settingsPage.selfAssessmentOptions.isPresent()).toBeTruthy();
        });

        it('should be turned off by DA', function () {
            global.pages.settingsPage.unCheckSelfAssessmentOptions();
            global.pages.settingsPage.saveSettingsChanges();
            expect( global.pages.settingsPage.toastMessage.isDisplayed()).toBe(true);
            expect(global.pages.settingsPage.toastMessage.getText()).toBe("Your settings have been saved");
            global.pages.settingsPage.toastMessage.click();
            global.pages.topNavPage.logOut();
            global.pages.loginPage.loginAsValidUser(global.pages.loginPage.school_district[2],global.pages.loginPage.norththurston_usernames[4] );
            expect(global.pages.leftNavPage.selfAssessmentTab.isDisplayed()).toBeFalsy();
            global.pages.topNavPage.logOut();
        });
    });

    describe('For Principal, self-assessments options ',function () {
        it('should be turned off by DA', function () {
            global.pages.loginPage.loginAsValidUser(global.pages.loginPage.school_district[2],global.pages.loginPage.norththurston_usernames[2]);
            expect(global.pages.leftNavPage.selfAssessmentTab.isDisplayed()).toBeFalsy();
            global.pages.topNavPage.logOut();
        });
    });
});