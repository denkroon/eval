﻿var _ = require('lodash');
var config = require("../../config");
var LoginPage = require('../../page-objects/login/login.po.js');
var ArtifactsPage = require('../../page-objects/artifacts/artifacts.po.js');
var ArtifactBuilderPage = require('../../page-objects/artifacts/artifact-builder.po.js');
var ObservationsPage = request('../../page-objects/observations.po');

describe('artifact-builder', function () {

    beforeAll(function() {
        var loginPage = new LoginPage();
        loginPage.loginAs('ntps', 'T1');
    });

    beforeEach(function() {
        var artifactsPage = new ArtifactsPage();
        var observationsPage  = new ObservationsPage();
    });

    it('should redirect to artifact-builder on add new artifact', function() {
        artifactsPage.go();
        artifactsPage.addNewArtifact();
        var url = browser.getCurrentUrl();
        expect(url).toContain('artifacts/artifact-builder');
    });

    it ('should add an artifact to observation available evidence after aligned/linked/shared', function() {
        observationsPage.go();
        var observationPage = observationsPage.addNewObservation();
        var title = observationPage.title;
        artifactsPage.go();
        var artifactBuilderPage = artifactsPage.addNewArtifact();
    })
    
});
