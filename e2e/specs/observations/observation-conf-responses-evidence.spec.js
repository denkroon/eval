﻿
describe('observation conference response evidence', function() {

    describe('creating evidence', function () {

        var response, prompt;

        beforeAll(function () {

            response = 'this is a test';
            prompt = 'test prompt';
            global.pages.helperPage.initNewObservationWithSharedConferencePromptResponse('ntps', 'PR1', 'T1',
                global.pages.observationPage.preConfTab, prompt, response).then(function() {
                global.pages.observationConfTorPage.clickCodeAsEvidenceBtn();
                global.pages.rubricCoderPage.codeToRubricRow('1a');
            });
        });

        it('should have coding markup in response html', function () {
            expect(global.pages.rubricCoderPage.getNodeDescTag('1a').isPresent()).toBeTruthy();
        });

        it('should show up in node item view evidence count', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(1);
        });

        it('should show up in row item view evidence count', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function () {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(1);
            });
        });

        it('should show up in evidence view available evidence', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function () {
                global.pages.rubricHelperPage.clickRow('1a');
                var responseElement = global.pages.availableEvidencePage.getConfPromptResponseEvidenceElement();
                expect(responseElement.isPresent()).toBeTruthy();
                expect(responseElement.getText()).toContain(response);
            });
        });

        it('should should have a delete btn on available evidence', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function () {
                global.pages.rubricHelperPage.clickRow('1a');
                expect(global.pages.availableEvidencePage.deleteEvidenceBtn.isPresent()).toBeTruthy();
            });
        });

        it('should still be there on refresh', function () {
            browser.driver.navigate().refresh();
            global.pages.observationPage.clickTab(global.pages.observationPage.preConfTab);
            global.pages.observationConfTorPage.clickResponsesTab();
            global.pages.rubricHelperPage.expandNode("D1").then(function () {
                global.pages.rubricHelperPage.clickRow('1a');
                var noteElement = global.pages.availableEvidencePage.getConfPromptResponseEvidenceElement();
                expect(noteElement.isPresent()).toBeTruthy();
                expect(noteElement.getText()).toContain(response);
            });
        });

    });

    describe('deleting evidence from response', function () {

        var response, prompt;

        beforeAll(function () {

            response = 'this is a test';
            prompt = 'test prompt';
            return global.pages.helperPage.initNewObservationWithSharedConferencePromptResponse('ntps', 'PR1', 'T1',
                global.pages.observationPage.preConfTab, prompt, response).then(function () {
                global.pages.observationConfTorPage.clickCodeAsEvidenceBtn();
                global.pages.rubricCoderPage.codeToRubricRow('1a');
            });
        });

        it('should remove the node tag from the response html', function () {
            global.pages.rubricCoderPage.clickRemoveCodeBtn();
            expect(global.pages.rubricCoderPage.removeCodeBtn.isPresent()).toBeFalsy();
            expect(global.pages.rubricCoderPage.getNodeDescTag('1a').isPresent()).toBeFalsy();
        });

        it('should remove the remove button from the response html', function () {
            var removeCodeBtn = global.pages.rubricCoderPage.removeCodeBtn;
            expect(removeCodeBtn.isPresent()).toBeFalsy();
        });

        it('should leave the response text that was coded', function () {
            expect(global.pages.rubricCoderPage.getResponse()).toEqual(response);
        });

        it('should remove the evidence from the node rubric helper item view', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(0);
        });

        it('should remove the evidence from the row rubric helper item view', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function() {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(0);
            });
        });

        it('should remove the evidence from the evidence view available evidence list', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getConfPromptResponseEvidenceElement();
                expect(noteElement.isPresent()).toBeFalsy();
            });
        });
    });

    describe('deleting evidence from available evidence view', function () {

        var response, prompt;

        beforeAll(function () {

            response = 'this is a test';
            prompt = 'test prompt';
            return global.pages.helperPage.initNewObservationWithSharedConferencePromptResponse('ntps', 'PR1', 'T1',
                global.pages.observationPage.preConfTab, prompt, response).then(function () {
                global.pages.observationConfTorPage.clickCodeAsEvidenceBtn();
                global.pages.rubricCoderPage.codeToRubricRow('1a');
                return global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                    global.pages.availableEvidencePage.removeEvidence();
                    global.pages.observationPage.clickTab(global.pages.observationPage.preConfTab);
                    global.pages.observationConfTorPage.clickResponsesTab();

                    // have to return to code mode to be able to check for the coding markup
                    global.pages.observationConfTorPage.clickCodeAsEvidenceBtn();
                });
            });
        });

        it('should remove the node tag from the response html', function () {
            expect(global.pages.rubricCoderPage.removeCodeBtn.isPresent()).toBeFalsy();
            expect(global.pages.rubricCoderPage.getNodeDescTag('1a').isPresent()).toBeFalsy();
        });

        it('should remove the remove button from the response html', function () {
            var removeCodeBtn = global.pages.rubricCoderPage.removeCodeBtn;
            expect(removeCodeBtn.isPresent()).toBeFalsy();
        });

        it('should leave the response text that was coded', function () {
            expect(global.pages.rubricCoderPage.getResponse()).toEqual(response);
        });

        it('should remove the evidence from the node rubric helper item view', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(0);
        });

        it('should remove the evidence from the row rubric helper item view', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function() {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(0);
            });
        });

        it('should remove the evidence from the evidence view available evidence list', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getConfPromptResponseEvidenceElement();
                expect(noteElement.isPresent()).toBeFalsy();
            });
        });
    });
});



