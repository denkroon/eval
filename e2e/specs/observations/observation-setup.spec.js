it('should add conference tabs after clicking the add conference buttons', function () {
    global.pages.observationsPage.addNewInFormalObservation();
    expect(global.pages.observationSetupPage.addPreConfBtn.isPresent()).toBeTruthy();
    expect(global.pages.observationSetupPage.addPostConfBtn.isPresent()).toBeTruthy();

    global.pages.observationSetupPage.addPreConference();
    expect(global.pages.observationPage.preConfTab.isPresent()).toBeTruthy();
    expect(global.pages.observationPage.postConfTab.isPresent()).toBeFalsy();

    global.pages.observationSetupPage.addPostConference();
    expect(global.pages.observationPage.preConfTab.isPresent()).toBeTruthy();
    expect(global.pages.observationPage.preConfTab.isPresent()).toBeTruthy();
});