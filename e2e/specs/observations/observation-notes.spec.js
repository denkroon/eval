﻿
describe('observation notes editor', function() {
    describe('observation notes editor - evaluator', function () {

        var notes;

        beforeAll(function() {
            notes = 'this is a test';
            global.pages.helperPage.initToNewObservation('ntps', 'PR1', 'T1', global.pages.observationPage.observeTab);
        });

        beforeEach(function() {

        });

        it ('should contain notes editor', function() {
            expect(global.pages.notesEditorPage.notesEditor.isPresent()).toBeTruthy();
        });

        it ('should auto-save notes', function() {
            global.pages.notesEditorPage.addNotes(notes);
            browser.sleep(1000);
            browser.driver.navigate().refresh();
            expect(global.pages.notesEditorPage.getNotes()).toEqual(notes);

        });

        it ('show change toolbar to match selected framework', function() {
            global.pages.topNavPage.changeFramework(global.frameworkNames.DanInstr);
            var btn = global.pages.notesEditorPage.getNodeToolbarBtn('D1');
            expect(btn.isPresent()).toBeTruthy();
            btn = global.pages.notesEditorPage.getNodeToolbarBtn('C1');
            expect(btn.isPresent()).toBeFalsy();
            global.pages.topNavPage.changeFramework(global.frameworkNames.DanState);
            btn = global.pages.notesEditorPage.getNodeToolbarBtn('D1');
            expect(btn.isPresent()).toBeFalsy();
            btn = global.pages.notesEditorPage.getNodeToolbarBtn('C1');
            expect(btn.isPresent()).toBeTruthy();
        });

    });
});


