﻿
describe('observation notes evidence', function() {

    describe('creating evidence', function () {

        var notes;

        beforeAll(function () {

            notes = 'this is a test';
            global.pages.helperPage.initToNewObservation('ntps', 'PR1', 'T1', global.pages.observationPage.observeTab);
            global.pages.notesEditorPage.addNotesAndCodeToRubricRow(notes, '1a');
        });

        beforeEach(function() {
            global.pages.observationPage.clickTab(global.pages.observationPage.observeTab);
        });

        it('should have coding marking up editor html', function () {
            expect(global.pages.notesEditorPage.getNodeDescTag('1a').isPresent()).toBeTruthy();
        });

        it('should show up in node item view evidence count', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(1);
        });

        it('should show up in row item view evidence count', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function() {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(1);
            });
        });

        it('should show up in evidence view available evidence', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getObservationNoteElement();
                expect(noteElement.isPresent()).toBeTruthy();
                expect(noteElement.getText()).toEqual(notes);
            });
        });

        it('should should have a delete btn on available evidence', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                expect(global.pages.availableEvidencePage.deleteEvidenceBtn.isPresent()).toBeTruthy();
            });
        });

        it ('should stil be there on refresh', function() {
            browser.driver.navigate().refresh();
            global.pages.observationPage.clickTab(global.pages.observationPage.observeTab);
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getObservationNoteElement();
                expect(noteElement.isPresent()).toBeTruthy();
                expect(noteElement.getText()).toEqual(notes);
            });
        });
    });

    describe('deleting evidence through notes editor', function () {

        var notes;

        beforeAll(function () {

            notes = 'this is a test';
            global.pages.helperPage.initToNewObservation('ntps', 'PR1', 'T1', global.pages.observationPage.observeTab);
            global.pages.notesEditorPage.addNotesAndCodeToRubricRow(notes, '1a');
            global.pages.notesEditorPage.clickRemoveCodeBtn();
        });

        it('should be removed the node tag from the notes editor html', function () {
            expect(global.pages.notesEditorPage.getNodeDescTag('1a').isPresent()).toBeFalsy();
        });

        it('should be removed the remove button from the notes editor html', function () {
            var removeCodeBtn = global.pages.notesEditorPage.removeCodeBtn;
            expect(removeCodeBtn.isPresent()).toBeFalsy();
        });

        it('should be leave the notes editor text that was coded', function () {
            expect(global.pages.notesEditorPage.getNotes()).toEqual(notes);
        });

        it('should remove the evidence from the node rubric helper item view', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(0);
        });

        it('should remove the evidence from the row rubric helper item view', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function() {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(0);
            });
        });

        it('should remove the evidence from the evidence view available evidence list', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getObservationNoteElement();
                expect(noteElement.isPresent()).toBeFalsy();
            });
        });
    });

    describe('deleting evidence through available evidence', function () {

        var notes;

        beforeAll(function () {

            notes = 'this is a test';
            global.pages.helperPage.initToNewObservation('ntps', 'PR1', 'T1', global.pages.observationPage.observeTab);
            global.pages.notesEditorPage.addNotesAndCodeToRubricRow(notes, '1a');
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                global.pages.availableEvidencePage.removeEvidence();
                global.pages.observationPage.clickTab(global.pages.observationPage.observeTab);
            });
        });

        it('should be removed the node tag from the notes editor html', function () {
            expect(global.pages.notesEditorPage.getNodeDescTag('1a').isPresent()).toBeFalsy();
        });

        it('should be removed the remove button from the notes editor html', function () {
            var removeCodeBtn = global.pages.notesEditorPage.removeCodeBtn;
            expect(removeCodeBtn.isPresent()).toBeFalsy();
        });

        it('should be leave the notes editor text that was coded', function () {
            expect(global.pages.notesEditorPage.getNotes()).toEqual(notes);
        });

        it('should remove the evidence from the node rubric helper item view', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(0);
        });

        it('should remove the evidence from the row rubric helper item view', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function() {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(0);
            });
        });

        it('should remove the evidence from the evidence view available evidence list', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getObservationNoteElement();
                expect(noteElement.isPresent()).toBeFalsy();
            });
        });
    });

});