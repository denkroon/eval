﻿describe('observation evaluatee conference notes evidence', function() {
    describe('creating evidence', function () {

        var notes = 'this is a test';

        beforeAll(function() {

            notes = 'this is a test';
            global.pages.helperPage.initNewObservationWithSharedConferenceNotes('ntps', 'PR1', 'T1',
                global.pages.observationPage.preConfTab, notes).then(function() {
                global.pages.observationConfTorPage.clickCodeAsEvidenceBtn();
                global.pages.rubricCoderPage.codeToRubricRow('1a');
            });
        });

        it('should have coding marking up editor html', function () {
            expect(global.pages.notesEditorPage.getNodeDescTag('1a').isPresent()).toBeTruthy();
        });

        it('should show up in node item view evidence count', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(1);
        });

        it('should show up in row item view evidence count', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function() {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(1);
            });
        });

        it('should show up in evidence view available evidence', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getConfNotesTorEvidenceElement();
                expect(noteElement.isPresent()).toBeTruthy();
                expect(noteElement.getText()).toEqual(notes);
            });
        });

        it('should have a delete btn on available evidence', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1','1a').then(function() {
                expect(global.pages.availableEvidencePage.deleteEvidenceBtn.isPresent()).toBeTruthy();
            });
        });

        it ('should stiil be there on refresh', function() {
            browser.driver.navigate().refresh();
            global.pages.observationPage.clickTab(global.pages.observationPage.preConfTab);
            global.pages.observationConfTorPage.clickResponsesTab();
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getConfNotesTorEvidenceElement();
                expect(noteElement.isPresent()).toBeTruthy();
                expect(noteElement.getText()).toEqual(notes);
            });
        });
    });

    describe('deleting evidence through editor', function () {

        var notes;

        beforeAll(function() {
            notes = 'this is a test';
            global.pages.helperPage.initNewObservationWithSharedConferenceNotes('ntps', 'PR1', 'T1',
                global.pages.observationPage.preConfTab, notes).then(function() {
                global.pages.observationConfTorPage.clickCodeAsEvidenceBtn();
                global.pages.rubricCoderPage.codeToRubricRow('1a');
                global.pages.notesEditorPage.clickRemoveCodeBtn();
            });
        });

        it('should remove the node tag from the notes html', function () {
            expect(global.pages.rubricCoderPage.removeCodeBtn.isPresent()).toBeFalsy();
            expect(global.pages.rubricCoderPage.getNodeDescTag('1a').isPresent()).toBeFalsy();
        });

        it('should remove the remove button from the response html', function () {
            var removeCodeBtn = global.pages.rubricCoderPage.removeCodeBtn;
            expect(removeCodeBtn.isPresent()).toBeFalsy();
        });

        it('should leave the response text that was coded', function () {
            expect(global.pages.rubricCoderPage.getResponse()).toEqual(notes);
        });

        it('should remove the evidence from the node rubric helper item view', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(0);
        });

        it('should remove the evidence from the row rubric helper item view', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function() {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(0);
            });
        });

        it('should remove the evidence from the evidence view available evidence list', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getConfPromptResponseEvidenceElement();
                expect(noteElement.isPresent()).toBeFalsy();
            });
        });
    });

    describe('deleting evidence through available evidence', function () {

        var notes;

        beforeAll(function() {
            notes = 'this is a test';
            global.pages.helperPage.initNewObservationWithSharedConferenceNotes('ntps', 'PR1', 'T1',
                global.pages.observationPage.preConfTab, notes).then(function() {
                global.pages.observationConfTorPage.clickCodeAsEvidenceBtn();
                global.pages.rubricCoderPage.codeToRubricRow('1a');

                global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                    global.pages.rubricHelperPage.clickRow('1a');
                    global.pages.availableEvidencePage.removeEvidence();
                    global.pages.observationPage.clickTab(global.pages.observationPage.preConfTab);
                    global.pages.observationConfTorPage.clickResponsesTab();
                    // have to return to code mode to be able to check for the coding markup
                    global.pages.observationConfTorPage.clickCodeAsEvidenceBtn();
                });
            });
        });

        it('should remove the node tag from the notes html', function () {
            expect(global.pages.rubricCoderPage.removeCodeBtn.isPresent()).toBeFalsy();
            expect(global.pages.rubricCoderPage.getNodeDescTag('1a').isPresent()).toBeFalsy();
        });

        it('should remove the remove button from the response html', function () {
            var removeCodeBtn = global.pages.rubricCoderPage.removeCodeBtn;
            expect(removeCodeBtn.isPresent()).toBeFalsy();
        });

        it('should leave the response text that was coded', function () {
            expect(global.pages.rubricCoderPage.getResponse()).toEqual(notes);
        });

        it('should remove the evidence from the node rubric helper item view', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(0);
        });

        it('should remove the evidence from the row rubric helper item view', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function() {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(0);
            });
        });

        it('should remove the evidence from the evidence view available evidence list', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getConfPromptResponseEvidenceElement();
                expect(noteElement.isPresent()).toBeFalsy();
            });
        });
    });
});
