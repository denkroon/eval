﻿describe('observation tabs', function() {


    describe('observation tabs for evaluator', function () {

        beforeAll(function () {
            global.pages.loginPage.loginAsValidUser(global.pages.loginPage.school_district[2],global.pages.loginPage.norththurston_usernames[2] );
            global.pages.leftNavPage.selectEvaluatee('T1 North Thurston High School');
        });

        beforeEach(function () {
            global.pages.leftNavPage.clickObservations();
        });

        it('should start on obeservation setup page for formal observation', function () {
            global.pages.observationsPage.addNewFormalObservation();
            browser.getCurrentUrl().then(function (actualUrl) {
                expect(actualUrl).toContain('setup');
            });
        });

        it('should start on obeservation setup page for informal observation', function () {
            global.pages.observationsPage.addNewInFormalObservation();
            browser.getCurrentUrl().then(function (actualUrl) {
                expect(actualUrl).toContain('setup');
            });
        });

        it('should have setup, pre conference, observe, post conference, Align & Score, Artifacts, Report tabs for formal evaluator view', function () {
            global.pages.observationsPage.addNewFormalObservation();
            expect(global.pages.observationsPage.setupTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.preConferenceTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.observeTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.postConferenceTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.alignAndScoreTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.artifactsTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.reportTab.isPresent()).toBeTruthy();
        });

        it('should have setup, Observe, Align & Score, Artifacts and Report tabs for informal evaluator view', function () {
            global.pages.observationsPage.addNewInFormalObservation();
            expect(global.pages.observationsPage.setupTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.preConferenceTab.isPresent()).toBeFalsy();
            expect(global.pages.observationsPage.observeTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.postConferenceTab.isPresent()).toBeFalsy();
            expect(global.pages.observationsPage.alignAndScoreTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.artifactsTab.isPresent()).toBeTruthy();
            expect(global.pages.observationsPage.reportTab.isPresent()).toBeTruthy();
        });
    });


    describe('for evaluatee', function () {

        beforeAll(function() {
            global.pages.loginPage.loginAsValidUser(global.pages.loginPage.school_district[2],global.pages.loginPage.norththurston_usernames[4] );

        });

        beforeEach(function() {
            global.pages.leftNavPage.clickObservations();
        });


        it('should show obeservation list', function () {
            browser.getCurrentUrl().then(function (actualUrl) {
                expect(actualUrl).toContain('observation-list');
            });
        });


        it('should not show any tab for evaluatee view', function() {
                expect(global.pages.observationsPage.setupTab.isPresent()).toBeFalsy();
                expect(global.pages.observationsPage.preConferenceTab.isPresent()).toBeFalsy();
                expect(global.pages.observationsPage.observeTab.isPresent()).toBeFalsy();
                expect(global.pages.observationsPage.postConferenceTab.isPresent()).toBeFalsy();
                expect(global.pages.observationsPage.alignAndScoreTab.isPresent()).toBeFalsy();
                expect(global.pages.observationsPage.artifactsTab.isPresent()).toBeTruthy();
                expect(global.pages.observationsPage.reportTab.isPresent()).toBeFalsy();
        });
    });
});
