﻿describe('observation other evidence', function () {

    describe('creating other evidence', function () {

        var notes;

        beforeAll(function () {

            notes = 'other evidence';
            global.pages.helperPage.initToNewObservation('ntps', 'PR1', 'T1', global.pages.observationPage.observeTab);
        });

        beforeEach(function () {
            browser.driver.manage().window().setSize(1280, 1024);
        });

        it('should contain add other evidence btn', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function () {
                expect(global.pages.availableEvidencePage.addOtherEvidenceBtn.isPresent()).toBeTruthy();
            });
        });

        it('should allow user to add new other evidence', function () {
            global.pages.availableEvidencePage.addOtherEvidence(notes);
            var evidence = global.pages.availableEvidencePage.getOtherEvidenceElement();
            expect(evidence.isPresent()).toBeTruthy();
            expect(evidence.getText()).toEqual(notes);
        });

        it('should show up in node item view evidence count', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(1);
        });

        it('should show up in row item view evidence count', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function () {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(1);
            });
        });

        it('should show up in evidence view available evidence', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function () {
                var noteElement = global.pages.availableEvidencePage.getOtherEvidenceElement();
                expect(noteElement.isPresent()).toBeTruthy();
                expect(noteElement.getText()).toEqual(notes);
            });
        });

        it('should should have a delete btn on available evidence', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function () {
                expect(global.pages.availableEvidencePage.deleteEvidenceBtn.isPresent()).toBeTruthy();
            });
        });

        it('should still be there on refresh', function () {
            browser.driver.navigate().refresh();
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function () {
                var evidence = global.pages.availableEvidencePage.getOtherEvidenceElement();
                expect(evidence.isPresent()).toBeTruthy();
                expect(evidence.getText()).toEqual(notes);
            });
        });
    });

    describe('deleting other evidence through available evidence', function () {

        var notes;

        beforeAll(function () {

            notes = 'this is a test';
            global.pages.helperPage.initToNewObservation('ntps', 'PR1', 'T1', global.pages.observationPage.observeTab);
             global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                 global.pages.availableEvidencePage.addOtherEvidence(notes);
                 global.pages.availableEvidencePage.removeEvidence();
                 global.pages.observationPage.clickTab(global.pages.observationPage.observeTab);
            });
        });

        it('should remove the evidence from the node rubric helper item view', function () {
            expect(global.pages.rubricHelperPage.nodeEvidenceCount("D1")).toEqual(0);
        });

        it('should remove the evidence from the row rubric helper item view', function () {
            global.pages.rubricHelperPage.expandNode("D1").then(function() {
                expect(global.pages.rubricHelperPage.rowEvidenceCount("1a")).toEqual(0);
            });
        });

        it('should remove the evidence from the evidence view available evidence list', function () {
            global.pages.rubricHelperPage.expandNodeAndClickRow('D1', '1a').then(function() {
                var noteElement = global.pages.availableEvidencePage.getOtherEvidenceElement();
                expect(noteElement.isPresent()).toBeFalsy();
            });
        });
    });

});
