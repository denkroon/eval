# Dev environment for the front-end

## Installing tools

You'll first need to have Node installed. I recommend using [nvm for windows](https://github.com/coreybutler/nvm-windows/releases) for this.
Once installed, use it to install node:vm

    nvm install 4.2.5
    nvm use 4.2.5

This will make version 4.2.5 the default on your system.

Then you'll want to ensure you have the latest npm version:

    npm i -g npm@3

And finally you should install at least gulp and probably bower globally:

    npm i -g gulp bower

This is a one time setup, you won't need to do that again.

## Using Gulp

From the `src\web` folder, run the following command:

    npm i

This will install all the build tools we need. You won't need to do that again unless you get errors
about missing packages when running gulp.

**
if you get a bunch of errors (and it says something about python) see

 http://stackoverflow.com/questions/21365714/nodejs-error-installing-with-npm

 which gives a link to a python windows installer that will fix up the paths necessary

Also, if you get the above error, bear in mind you should probably get rid of your node_modules
folder and invoke the npm i command again; for me, i got a sass error, which wasn't fixed until
i started over.

Then you have the following commands available:

 * `gulp build`: will build all of the front-end into `src\web\build`
 * `gulp clean`: will clean the content of `src\web\build`
 * `gulp`: will build and start a web server on port 3000 to serve the front-end, using a local API.
 * `gulp testServer`: same as `gulp` but uses the test server for the API.
 * `gulp localConfig`: generates `config.local.json` with default settings so you can override them.

More details on how to use some of these commands can be found further down.

## What gets built?

The build process does the following:

 * compiles all the typescript files into `js\app.js`
     * automatically adds dependency injector annotations.
     * creates source maps
 * compiles all the sass files (including bootstrap, inspinia and font-awesome) into `css\main.css`
     * automatically adds browser prefixes
     * minifies the CSS
     * creates source maps
 * precaches all the angular templates into `js\templates.js`
     * lints the templates and reports any error.
     * minifies the html.
 * processes bower dependencies and other libraries
     * combines all JavaScript in `js\vendor.js`
     * combines all CSS in `css\vendor.css`
     * copies all the fonts in the `fonts` folder
 * copies images into the `images` folder
     * processes them to reduce their size
 * copies `index.html` over

## Typical flow

Just run `gulp` or `gulp testServer` in a console and let it run. As you save files, they will be
processed and your web browser will be automatically refreshed.

By default, you can reach your dev server at http://localhost:3000

In order to speed things up, we try to avoid rebuilding things that haven't changed. Sometimes, for
example after changing the local config settings, the files won't be rebuilt. You'll want to stop
gulp, run `gulp clean` and start gulp again.

## Adding libraries

If you need to install a 3rd party library, see if you can use bower for that as it makes things
easier. In the best case, all you need to do is:

    bower install <library> --save

Gulp should automatically pick up the changes and rebuild the vendor files.

Some libraries don't properly expose the necessary files, you can override their default settings
in the `bower.json` file.

If you need to install libraries that are not available to bower, copy the files in the `lib` folder
and add them to the configuration in `gulpfile.js\config.js`.

## Debugging

By default, the build process will minify and obfuscate the application's code, as well as used
the minified version of the 3rd party libraries whenever they can be found. It also generates
source maps that allow browser development tools to map the original source code to the minified code.
Chrome is very good with that but other browsers are a little trickier particularly when handling
exceptions.

To disable minification, you'll need to override the build settings with a local config. Create a
file called `config.local.json` and set its content to:

    {
        "minify": false
    }

Minification is still important to reduce files size and improve performance in production. So testing
should be done with minified code.

Additionally, it's possibile to disable script combining for both vendor and application files using
anoher setting in the local config:

    "combine": false

This could be useful at times for debugging.

## Unit Tests

### Running Tests

There are a couple of tasks to run unit tests:

    gulp spec

Will run the specs using PhantomJS and report the results on the console, that's what should
be used by a CI server.

    gulp spec:chrome

Will run the specs in Chrome and wait to run them again if files change. To view the test
results in the browser or debug the tests, click the `debug` button and look in the new
tab.

### Writing Tests

Add your tests files alongside the source and call them `.spec.ts` so they can be
filtered out of the regular build.

Place any extra file that will support your tests in the `app\specs` folder.

The following test libraries are currently included:

- jasmine
- chai
- sinon
- sinon-chai
- bard