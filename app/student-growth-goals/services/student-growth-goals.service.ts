/// <reference path="../student-growth-goals.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.student-growth-goals')
        .factory('studentGrowthGoalsService', studentGrowthGoalsService);

    studentGrowthGoalsService.$inject = ['$http', '_', 'config', 'rubricUtils', '$q', '$state',
        'activeUserContextService', '$httpParamSerializerJQLike', 'enums', 'utils', 'artifactService'];

    function studentGrowthGoalsService($http, _, config, rubricUtils, $q, $state,
           activeUserContextService, $httpParamSerializerJQLike, enums, utils, artifactService) {

        var service = {
            getBundleById: getBundleById,
            getBundleForEvaluation: getBundleForEvaluation,
            updateBundleForEvaluation: updateBundleForEvaluation,
            updateBundleForEvaluatee: updateBundleForEvaluatee,
            shareProcess:shareProcess,
            updateGoalForEvaluation: updateGoalForEvaluation,
            newGoal: newGoal,
            syncGoalsWithEvalCycle: syncGoalsWithEvalCycle,
            newShareOptions: newShareOptions,
            updateShareOptions: updateShareOptions,
            saveBundleNotes: saveBundleNotes,
            approveProcess: approveProcess,
            reviseProcess: reviseProcess,
            addArtifactForGoal: addArtifactForGoal,
            getLinkedArtifactsForGoal: getLinkedArtifactsForGoal
        };

        return service;

        function getLinkedArtifactsForGoal(goal, rubricRowId)
        {
            var artifacts = [];
            goal.linkedArtifacts.forEach(function (artifact) {
                var match = _.find(artifact.alignedRubricRows, { id: rubricRowId });
                if (match) {
                    artifacts.push(artifact);
                }
            });

            return artifacts;
        }

        function addArtifactForGoal(goal, rubricRowId)
        {
            var stateNodes = activeUserContextService.getStateFramework().frameworkNodes;
            var artifact = artifactService.newArtifact();
            artifact.isShared = true;
            artifactService.saveArtifact(artifact)
                .then(function() {
                    artifact.alignedRubricRows.push(rubricUtils.getRubricRowById(stateNodes, rubricRowId));
                    return artifactService.saveArtifactAlignment(artifact);
                }).then(function() {
                    artifact.linkedStudentGrowthGoals.push(goal);
                    return artifactService.saveArtifactBundleLinkages(artifact).then(function() {
                        $state.go('artifact-builder', {
                            artifactId: artifact.id,
                            artifactOrigin: $state.current.name,
                            goalId: goal.id
                        });
                    })
            })
        }

        function reviseProcess(bundle) {
            var currentEvaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + currentEvaluationId + '/sggoalbundles/reviseprocess';

            return $http({
                method: 'PUT',
                url: url,
                data: $httpParamSerializerJQLike(bundle),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        }

        function approveProcess(bundle) {
            var currentEvaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + currentEvaluationId + '/sggoalbundles/approveprocess';

            return $http({
                method: 'PUT',
                url: url,
                data: $httpParamSerializerJQLike(bundle),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        }

        function saveBundleNotes(bundleId, note, noteProperty) {
            var url = config.apiUrl + 'sggoalbundle/updatebundlenotesproperty';
            var request = {
                bundleId: bundleId,
                noteProperty: noteProperty,
                notePropertyValue: note
            };
            return $http.post(url, request).then(function (response) {
                return response.data;
            });
        }

        function updateGoalActivation(goal) {
            var url = config.apiUrl  + '/updategoalactivation';
            return $http.put(url, goal);
        }

        function updateGoalIsActive(bundle, fn, active) {
            var goal = _.find(bundle.goals, { 'frameworkNodeId': fn.id });
            if (goal.isActive != active) {
                goal.isActive = active;
                updateGoalActivation(goal);
            }
        }

        function syncGoalsWithEvalCycle(evaluatee) {

            var bundle;
            getBundleWithInactiveGoalssById(evaluatee.evalData.studentGrowthGoalBundleId).then((data) => {
                bundle = data;

                var studentGrowthFrameworkNodes = activeUserContextService.getFrameworkContext().studentGrowthFrameworkNodes;
                var planType = evaluatee.evalData.planType;
                if (planType === enums.EvaluationPlanType.COMPREHENSIVE) {
                    studentGrowthFrameworkNodes.forEach(function(fn) {
                        updateGoalIsActive(bundle, fn, true);
                    });
                }
                else if (planType === enums.EvaluationPlanType.FOCUSED) {
                    var focusNode = utils.getStudentGrowthFocusFrameworkNode(activeUserContextService, evaluatee);
                    if (focusNode) {
                        studentGrowthFrameworkNodes.forEach(function(fn) {
                            if (fn.id === focusNode.id) {
                                updateGoalIsActive(bundle, fn, true);
                            }
                            else {
                                updateGoalIsActive(bundle, fn, false);
                            }
                        });
                    }
                }

                return updateBundleForEvaluatee(evaluatee, bundle);

            });
        }

        function newGoal(bundle, frameworkNode) {
            var sgRows = _.filter(frameworkNode.rubricRows, 'isStudentGrowthAligned');
            var goal = {
                id: 0,
                bundleId: bundle.id,
                creationDateTime: new Date(),
                evaluationId: bundle.evaluationId,
                title: frameworkNode.shortName +" (" + _.map(sgRows, 'shortName').join(', ') + ")",
                goalStatement: '',
                frameworkNodeId: frameworkNode.id,
                resultsRubricRowId: null,
                isActive: true,
                prompts: [],
                linkedArtifacts: []
            };

            if (activeUserContextService.context.workArea().evaluationType === enums.EvaluationType.TEACHER) {
                var processRow = rubricUtils.getStudentGrowthProcessRubricRow(frameworkNode);
                if (processRow) {
                    goal.processRubricRowId = processRow.id;
                }

                var resultsRow = rubricUtils.getStudentGrowthResultsRubricRow(frameworkNode);
                if (resultsRow) {
                    goal.resultsRubricRowId = resultsRow.id;
                }
            }
            else if (activeUserContextService.context.workArea().evaluationType === enums.EvaluationType.PRINCIPAL) {
                var rr;
                for (var i = 0; i < frameworkNode.rubricRows.length; ++i) {
                    rr = frameworkNode.rubricRows[i];
                    if (rr.isStudentGrowthAligned) {
                        goal.resultsRubricRowId = rr.id;
                        break;
                    }
                }
            }

            return goal;
        }

        function getBundleForEvaluation() {
            var currentEvaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var currentUserId = activeUserContextService.user.id;
            var url = config.apiUrl + currentEvaluationId + '/sggoalbundle/' + currentUserId;
            return $http.get(url).then(function(response) {
                return response.data;
            })
        }

        function getBundleWithInactiveGoalssById(id) {
            var url = config.apiUrl + '/sggoalbundleswithinactivegoals/' + id;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function getBundleById(id) {
            var currentUserId = activeUserContextService.user.id;
            var url = config.apiUrl + '/sggoalbundles/' + id + '/' + currentUserId;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function updateGoalForEvaluation(goal) {
            var url = config.apiUrl  + '/sggoals';
            return $http.put(url, goal);
        }

        function updateBundleForEvaluationId(evaluationId, bundle) {
            var url = config.apiUrl + evaluationId + '/sggoalbundles';

            return $http({
                method: 'PUT',
                url: url,
                data: $httpParamSerializerJQLike(bundle),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        }

        function updateBundleForEvaluatee(evaluatee, bundle) {
            var currentEvaluationId = evaluatee.evalData.id;
            return updateBundleForEvaluationId(evaluatee.evalData.id, bundle);
        }
        function updateBundleForEvaluation(bundle) {
            var currentEvaluationId = activeUserContextService.context.evaluatee.evalData.id;
            return updateBundleForEvaluationId(currentEvaluationId, bundle);
        }

        function shareProcess(bundle) {
            var currentEvaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + currentEvaluationId + '/sggoalbundles/shareprocess';

            return $http({
                method: 'PUT',
                url: url,
                data: $httpParamSerializerJQLike(bundle),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        }

        function newShareOptions(bundleId) {
            return {
                bundleId: bundleId,
                evaluatorScoresShared: false,
                notifyEvaluatee: true,
                message: ''
            }
        }

        function updateShareOptions(shareOptions) {
            var url = config.apiUrl + 'sggoalbundles/updateshareoptions';
            return $http.post(url, shareOptions).then(function (response) {
            });
        }

    }
})();
