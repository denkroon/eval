/// <reference path="student-growth-goals.module.ts" />

/**
 * Created by anne on 6/19/2015.
 */
(function(){
angular.module('stateeval.student-growth-goals')
    .config(configureRoutes);

function configureRoutes($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('sg-home', {
            url: '/sg-home',
            parent: 'default-content',
            abstract: true,
        })
        .state('sg-bundle', {
            url: '/sg-bundle/:id',
            parent: 'sg-home',
            abstract: true,
            views: {
                'content@base': {
                    templateUrl: 'app/student-growth-goals/views/sg-bundle.html',
                    controller: 'sgBundleController as vm'
                }
            },
            resolve: {
                evidenceCollection: ['evidenceCollectionService', 'studentGrowthGoalsService', 'enums', 'activeUserContextService',
                    function (evidenceCollectionService, studentGrowthGoalsService, enums, activeUserContextService) {
                        return studentGrowthGoalsService.getBundleForEvaluation()
                            .then(function (bundle) {
                                return evidenceCollectionService.getEvidenceCollection("SG GOAL BUNDLE",
                                    enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS,
                                    bundle.id,
                                    activeUserContextService.user.id);
                            })
                            .then(function (evidenceCollection) {
                                return evidenceCollection;
                            });
                    }]
            }
        })
        .state('sg-bundle-summary', {
            url: '/sg-bundle-summary',
            parent: 'sg-bundle',
            views: {
                'content@sg-bundle': {
                    templateUrl: 'app/student-growth-goals/summary/sg-bundle-summary-page.html',
                    controller: 'sgBundleSummaryPageController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Student Growth Goals'
            },
            data: {
                evaluateeRequired: true
            }
        })
        .state('sg-goal-planning', {
            url: '/sg-goal-planning/:id',
            parent: 'sg-bundle-summary',
            views: {
                'content@sg-bundle': {
                    templateUrl: 'app/student-growth-goals/views/sg-goal-planning.html',
                    controller: 'sgGoalPlanningController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Student Growth Goals'
            }
        })
        .state('sg-planning-conf', {
            url: '/sg-planning-conf',
            parent: 'sg-bundle-summary',
            views: {
                'content@sg-bundle': {
                    templateUrl: 'app/student-growth-goals/views/sg-planning-conf.html',
                    controller: 'sgPlanningConferenceController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Student Growth Goals'
            }
        })
        .state('sg-bundle-evidence-collection', {
            url: '/sg-bundle-evidence-collection',
            parent: 'sg-bundle-summary',
            views: {
                'content@sg-bundle': {
                    templateUrl: 'app/student-growth-goals/views/sg-bundle-evidence-collection.html',
                    controller: 'sgBundleEvidenceCollectionController as vm'
                }
            },
            params: {
                nodeShortName: null,
                rowId: null
            },
            ncyBreadcrumb: {
                label: 'Student Growth Goals'
            }
        })
        .state('sg-goal-results', {
            url: '/sg-goal-results/:id',
            parent: 'sg-bundle-summary',
            views: {
                'content@sg-bundle': {
                    templateUrl: 'app/student-growth-goals/views/sg-goal-results.html',
                    controller: 'sgGoalResultsController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Student Growth Goals'
            }
        })
}

})();