/**
 * Created by anne on 3/29/2016.
 */

/**
 * Created by anne on 3/24/2016.
 */
/// <reference path="../student-growth-goals.module.ts" />

(function () {
    'use strict';

    function sgBundleSummaryDirective() {
        return {
            restrict: 'E',
            scope: {
                evidenceCollection: '='
            },
            templateUrl: 'app/student-growth-goals/summary/sg-bundle-summary.directive.html',
            controller: 'sgBundleSummaryController as vm',
            bindToController: true
        }
    }

    class sgBundleSummaryController {
        evidenceCollection:any;
        bundle: any;
        isAssignedEvaluator: boolean;
        isEvaluatee: boolean;
        evaluateePlanType: string;
        goalEvidence: [any];
        goalScores: [any];
        rubricRowVisitedByRowId: [any];
        rubricRowModifiedByRowId: [any];
        sgNodes: [any];
        currentTeeStep = 1;
        currentTorStep = 1;
        evaluateeTerm: string;
        processRubricList: [any];
        resultsRubricList: [any];
        hasProcess: any;
        showSteps = true;
        processShared: boolean = false;

        userCanSeeProcessScores: boolean;
        userCanSeeResultsScores: boolean;

        constructor(
            private evidenceCollectionService: any,
            private activeUserContextService: any,
            private utils: any,
            public $state: any,
            private enums: any,
            private _: _.LoDashStatic
        ) {
            this.bundle = this.evidenceCollection.studentGrowthGoalBundle;
            this.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();
            this.goalEvidence = [];
            this.rubricRowVisitedByRowId = [];
            this.rubricRowModifiedByRowId = [];
            this.sgNodes =  activeUserContextService.getFrameworkContext().studentGrowthFrameworkNodes;
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermLowerCase();
            this.processRubricList = [];
            this.resultsRubricList = [];
            this.hasProcess = activeUserContextService.context.workArea().evaluationType === this.enums.EvaluationType.TEACHER;

            this.userCanSeeProcessScores = (this.isEvaluatee && this.bundle.wfState === enums.WfState.SGBUNDLE_PROCESS_COMPLETE) ||
                                           (!this.isEvaluatee && this.bundle.wfState >= enums.WfState.SGBUNDLE_PROCESS_SHARED);
            this.userCanSeeResultsScores = (this.isEvaluatee && this.bundle.evaluatorScoresShared) ||
                                           (!this.isEvaluatee && this.bundle.wfState === enums.WfState.SGBUNDLE_PROCESS_COMPLETE);

            if (this.bundle.wfState === enums.WfState.SGBUNDLE_PROCESS_SHARED) {
                this.currentTeeStep = 2;
                this.currentTorStep = 2;
                this.processShared = true;
            }
            if (this.bundle.wfState === enums.WfState.SGBUNDLE_PROCESS_COMPLETE) {
                this.currentTeeStep = 3;
                this.currentTorStep = 3;
            }

            var evaluatee = activeUserContextService.context.evaluatee;
            this.evaluateePlanType = utils.mapEvaluationPlanTypeForUserToString(activeUserContextService, evaluatee);

            evidenceCollectionService.getRubricRowLastVisitsForSGBundle(this.bundle.id)
                .then((lastVisits) => {
                    this.rubricRowVisitedByRowId = _.groupBy(lastVisits, 'rubricRowId');
                    return evidenceCollectionService.getRubricRowLastModifiedDatesForSGBundle(this.bundle.id);
                }).then((modificationDates) =>  {
                    this.rubricRowModifiedByRowId = _.groupBy(modificationDates, 'rubricRowId');
                    this.bundle.goals.forEach((goal) => {

                        if (this.hasProcess) {
                            this.processRubricList.push(this.evidenceCollection.getRowById(goal.processRubricRowId).data.shortName);
                            this.getDataForGoalRubricComponent(goal, goal.processRubricRowId);
                        }
                        if (goal.resultsRubricRowId) {
                            this.resultsRubricList.push(this.evidenceCollection.getRowById(goal.resultsRubricRowId).data.shortName);
                            this.getDataForGoalRubricComponent(goal, goal.resultsRubricRowId);
                        }
                    });
                });
        }

        torStepClass(step: number) {
            if (step < this.currentTorStep) {
                return 'step-complete';
            }
            if (step == this.currentTorStep) {
                return 'step-active';
            }
            return 'step-inactive';
        }

        teeStepClass(step: number) {
            if (step < this.currentTeeStep) {
                return 'step-complete';
            }
            if (step == this.currentTeeStep) {
                return 'step-active';
            }
            return 'step-inactive';
        }


        getDataForGoalRubricComponent(goal, rubricRowId) {
            var list = this._.filter(this.evidenceCollection.availableEvidence, (ae) =>{
                return ((ae.evidenceCollectionType === this.enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) &&
                ae.studentGrowthGoalId === goal.id &&
                ae.rubricRowId == rubricRowId);
            });

            var key = goal.id+'-'+rubricRowId;

            var score = this.evidenceCollection.getRowById(rubricRowId).score;
            if (score.performanceLevel) {
                score = this.enums.PerformanceLevels[score.performanceLevel - 1].substring(0, 3);
            }
            else {
                score = '-';
            }
            this.goalEvidence[key] = {
                avail: list.length,
                used: this._.filter(list, {inUse: true}).length,
                changed:  this.evidenceCollectionService.checkForEvidenceChange(this.rubricRowModifiedByRowId,
                    this.rubricRowVisitedByRowId, this.bundle,
                    this.enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS, rubricRowId),
                score:score
            };

            if (this.goalEvidence[key].changed) {
                this.evidenceCollectionService.state.studentGrowthEvidenceChanged = true;
            }
        }
        gotoGoalPlanning(goal) {
            this.$state.go('sg-goal-planning', {id: goal.id});
        }

        gotoGoalResults(goal) {
            this.$state.go('sg-goal-results', {id: goal.id});
        }
        gotoGoalProcessRow(goal) {
            this.$state.go('sg-bundle-evidence-collection', {rowId: goal.processRubricRowId});
        }
        gotoGoalResultsRow(goal) {
            this.$state.go('sg-bundle-evidence-collection', {rowId: goal.resultsRubricRowId});
        }
    }
    angular
        .module('stateeval.student-growth-goals')
        .controller('sgBundleSummaryController', sgBundleSummaryController)
        .directive('sgBundleSummaryDirective', sgBundleSummaryDirective)
})();



