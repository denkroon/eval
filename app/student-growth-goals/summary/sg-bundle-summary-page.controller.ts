/**
 * Created by anne on 3/24/2016.
 */
/// <reference path="../student-growth-goals.module.ts" />

(function () {
    'use strict';
    class sgBundleSummaryPageController {

        bundle: any;
        isAssignedEvaluator: boolean;
        isEvaluatee: boolean;
        evaluateePlanType: string;
        enums: any;
        goalTerm: string;
        hasProcess: boolean;
        currentStep = 1;

        constructor(
            private evidenceCollection: any,
            private activeUserContextService: any,
            private utils: any,
            public enums: any,
            private $scope: any
        ) {
            this.enums = enums;
            this.hasProcess = true;
            this.bundle = evidenceCollection.studentGrowthGoalBundle;
            this.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();
            this.evaluateePlanType = utils.mapEvaluationPlanTypeForUserToString(activeUserContextService,
                activeUserContextService.context.evaluatee);

            if (this.activeUserContextService.context.evaluatee.evalData.evaluationType === this.enums.EvaluationType.PRINCIPAL) {
                this.hasProcess = false;
            }

            if (this.bundle.goals.length===1) {
                this.goalTerm = "Goal";
            }
            else {
                this.goalTerm = "Goals"
            }

            $scope.$watch('vm.bundle.wfState === enums.WfState.SGBUNDLE_STARTED', (val: boolean) => {
                if (val) this.currentStep = 1;
            });
            $scope.$watch('vm.bundle.wfState === enums.WfState.SGBUNDLE_PROCESS_SHARED', (val: boolean) => {
                if (val) this.currentStep = 2;
            });
            $scope.$watch('vm.bundle.wfState === enums.WfState.SGBUNDLE_PROCESS_COMPLETE', (val: boolean) => {
                if (val) this.currentStep = 3;
            });
        }


        stepClass(step: number) {
            if (step < this.currentStep) {
                return 'step-complete';
            }
            if (step == this.currentStep) {
                return 'step-active';
            }
            return 'step-inactive';
        }
    }
    angular
        .module('stateeval.student-growth-goals')
        .controller('sgBundleSummaryPageController', sgBundleSummaryPageController);
})();


