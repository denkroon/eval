/**
 * Created by anne on 3/24/2016.
 */
/// <reference path="../student-growth-goals.module.ts" />

(function () {
    'use strict';
    class sgBundleEvidenceCollectionController {

        evidenceCollection: any;
        bundle: any;
        isAssignedEvaluator: boolean;

        constructor(
            private evidenceCollection: any,
            private activeUserContextService: any,
            public enums: any
        ) {

            this.evidenceCollection = evidenceCollection;
            this.bundle = evidenceCollection.studentGrowthGoalBundle;
            this.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();
        }
    }
    angular
        .module('stateeval.student-growth-goals')
        .controller('sgBundleEvidenceCollectionController', sgBundleEvidenceCollectionController);
})();


