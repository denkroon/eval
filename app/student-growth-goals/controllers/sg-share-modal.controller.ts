/// <reference path="../student-growth-goals.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.student-growth-goals')
        .controller('sgShareModalController', sgShareModalController);

    sgShareModalController.$inject = ['$uibModalInstance', 'activeUserContextService', 'type'];

    /* @ngInject */
    function sgShareModalController($uibModalInstance,  activeUserContextService, type) {

        var vm = this;
        vm.type = type;

        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermLowerCase();
        vm.comment = '';

        vm.save = save;
        vm.cancel = cancel;

        /////////////////////////////////

        function save() {

            $uibModalInstance.close({
                comment: vm.comment
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

