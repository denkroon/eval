/// <reference path="../student-growth-goals.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.student-growth-goals')
        .controller('sgApproveGoalsModalController', sgApproveGoalsModalController);

    sgApproveGoalsModalController.$inject = ['$uibModalInstance', 'evidenceCollection'];

    /* @ngInject */
    function sgApproveGoalsModalController($uibModalInstance, evidenceCollection) {

        var vm = this;
        vm.bundle = evidenceCollection.studentGrowthGoalBundle;
        vm.evidenceCollection = evidenceCollection;
        vm.missingRows = '';
        vm.comment;
        vm.ready = true;

        vm.cancel = cancel;
        vm.save = save;

        activate();

        function activate() {

            var missingRows = [];
            var resultsRows = [];

            vm.bundle.goals.forEach((goal) => {

                var score = vm.evidenceCollection.getRowById(goal.processRubricRowId).score;
                if (!score.performanceLevel) {
                    missingRows.push(vm.evidenceCollection.getRowById(goal.processRubricRowId).data.shortName);
                }
            });

            if (missingRows.length>0) {
                vm.ready = false;
                vm.missingRows = missingRows.join(', ');
            }
        }

        function save() {
            $uibModalInstance.close({
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

