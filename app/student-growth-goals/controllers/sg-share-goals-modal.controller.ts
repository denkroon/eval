/// <reference path="../student-growth-goals.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.student-growth-goals')
        .controller('sgShareGoalsModalController', sgShareGoalsModalController);

    sgShareGoalsModalController.$inject = ['$uibModalInstance', 'bundle', 'rubricUtils', 'activeUserContextService'];

    /* @ngInject */
    function sgShareGoalsModalController($uibModalInstance, bundle, rubricUtils, activeUserContextService) {

        var vm = this;
        vm.bundle = bundle;
        vm.comment = '';
        vm.complete = true;
        vm.missingGoals = [];
        vm.resultsRows = [];

        vm.save = save;
        vm.cancel = cancel;

        activate();

        function activate() {
            var studentGrowthFrameworkNodes = activeUserContextService.getFrameworkContext().studentGrowthFrameworkNodes;

            vm.bundle.goals.forEach(function(goal) {
                if (goal.resultsRubricRowId) {
                    vm.resultsRows.push(
                        rubricUtils.getRubricRowById(studentGrowthFrameworkNodes, goal.resultsRubricRowId).shortName);
                }
                if (!goal.goalStatement) {
                    vm.missingGoals.push(
                        rubricUtils.getRubricRowById(studentGrowthFrameworkNodes, goal.processRubricRowId).shortName);
                    vm.complete = false;
                }
            })
        }

        function save() {

            $uibModalInstance.close({
                comment: vm.comment
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

