/// <reference path="../student-growth-goals.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.student-growth-goals')
        .controller('shareBundleModalController', shareBundleModalController);

    shareBundleModalController.$inject = ['$uibModalInstance', 'activeUserContextService',
        'studentGrowthGoalsService', 'bundle'];

    /* @ngInject */
    function shareBundleModalController($uibModalInstance, activeUserContextService,
                                        studentGrowthGoalsService, bundle) {

        var vm = this;
        vm.bundle = bundle;

        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();

        vm.notes = '';
        vm.notify = true;
        vm.shareOptions = studentGrowthGoalsService.newShareOptions(vm.bundle.id);
        vm.shareOptions.evaluatorScoresShared = vm.bundle.evaluatorScoresShared;
        vm.shareOptions.notifyEvaluatee = false;
        vm.shareOptions.message = '';

        vm.save = save;
        vm.cancel = cancel;

        /////////////////////////////////

        function save() {

            studentGrowthGoalsService.updateShareOptions(vm.shareOptions).then(function() {
                vm.bundle.evaluatorScoresShared = vm.shareOptions.evaluatorScoresShared;
                $uibModalInstance.close({ });
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

