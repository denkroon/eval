/// <reference path="../student-growth-goals.module.ts" />

/**
 * Created by anne on 11/8/2015.
 */

(function() {
    'use strict';

    angular
        .module('stateeval.student-growth-goals')
        .controller('sgGoalPlanningController', sgGoalPlanningController);

    sgGoalPlanningController.$inject = ['enums', 'userPromptService', 'utils', '$stateParams', 'artifactService',
        'studentGrowthGoalsService', '$timeout', '$state', 'activeUserContextService',
        'evidenceCollection', '_', '$confirm', 'evidenceCollectionService'];

    /* @ngInject */
    function sgGoalPlanningController(enums, userPromptService, utils, $stateParams, artifactService,
        studentGrowthGoalsService, $timeout, $state, activeUserContextService,
        evidenceCollection, _, $confirm, evidenceCollectionService) {

        var vm = this;

        vm.utils = utils;
        vm.enums = enums;
        vm.userPromptResponses = [];
        vm.artifacts = [];
        vm.evidenceCollection = evidenceCollection;
        vm.bundle = evidenceCollection.studentGrowthGoalBundle;
        vm.readOnly = activeUserContextService.context.goalBundleIsReadOnly(vm.bundle) ||
                      vm.bundle.wfState >= enums.WfState.SGBUNDLE_PROCESS_SHARED;

        vm.savePrompt = savePrompt;
        vm.saveGoal = saveGoal;
        vm.addArtifact = addArtifact;
        vm.removeArtifact = removeArtifact;
        vm.editArtifact = editArtifact;

        activate();

        function activate() {
            vm.goal = _.find(vm.bundle.goals, {id: parseInt($stateParams.id)});
            vm.row = evidenceCollection.getRowById(vm.goal.processRubricRowId);
            vm.artifacts = studentGrowthGoalsService.getLinkedArtifactsForGoal(vm.goal, vm.goal.processRubricRowId );
            evidenceCollectionService.recordLastVisit(vm.evidenceCollection, vm.goal.processRubricRowId);
            userPromptService.getStudentGrowthUserPromptResponses(vm.goal).then(function(userPromptResponses) {
                vm.userPromptResponses = userPromptResponses;
           })
        }

        var goalSaveTimer = null;

        function saveGoal() {
            vm.bundle.saveNotice = 'Saving...';

            if (goalSaveTimer) {
                $timeout.cancel(goalSaveTimer);
            }

            goalSaveTimer = $timeout(function () {
                return studentGrowthGoalsService.updateGoalForEvaluation(vm.goal).then(function() {
                    vm.bundle.saveNotice = 'All changes saved';
                });
            }, 1000);
        }

        var promptSaveTimer = null;

        function savePrompt(prompt) {
            vm.bundle.saveNotice = 'Saving...';

            if (promptSaveTimer) {
                $timeout.cancel(promptSaveTimer);
            }

            promptSaveTimer = $timeout(function () {
                return userPromptService.saveStudentGrowthUserPromptResponse(prompt).then(function() {
                    vm.bundle.saveNotice = 'All changes saved';
                });
            }, 1000);
        }

        function addArtifact()
        {
            studentGrowthGoalsService.addArtifactForGoal(vm.goal, vm.goal.processRubricRowId);
        }

        function editArtifact(artifact) {
            $state.go('artifact-builder', {artifactId: artifact.id, artifactOrigin: $state.current.name, goalId: vm.goal.id});
        }

        function removeArtifact(artifact) {
            $confirm({}, { templateUrl: 'app/artifacts/views/artifacts-confirm-delete.html' })
                .then(() => {
                    artifactService.deleteArtifact(artifact).then(() => {
                        vm.artifacts = _.reject(vm.artifacts, {id: artifact.id});
                    })
                });
        }
    }

})();

