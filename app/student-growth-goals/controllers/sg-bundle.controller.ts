/// <reference path="../student-growth-goals.module.ts" />
(function() {
    'use strict';

    angular
        .module('stateeval.student-growth-goals')
        .controller('sgBundleController', sgBundleController);

    sgBundleController.$inject = ['activeUserContextService', 'enums', 'rubricUtils', 'evidenceCollection',
        '$state', '$uibModal', 'utils', '$stateParams', 'studentGrowthGoalsService', '$confirm'];

    /* @ngInject */
    function sgBundleController(activeUserContextService, enums, rubricUtils, evidenceCollection,
         $state, $uibModal, utils, $stateParams, studentGrowthGoalsService, $confirm) {

        var vm = this;
        vm.enums = enums;
        vm.$state = $state;
        vm.$stateParams = $stateParams;
        vm.bundle = evidenceCollection.studentGrowthGoalBundle;
        vm.isEvaluatee = activeUserContextService.context.isEvaluatee();
        vm.isEvaluator = activeUserContextService.context.isAssignedEvaluator();
        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
        vm.evalType = activeUserContextService.context.workArea().evaluationType;
        vm.evidenceCollection = evidenceCollection;
        vm.hasProcess = true;

        vm.tabs = [];

        vm.shareGoals = shareGoals;
        vm.reviseGoals = reviseGoals;
        vm.approveGoals = approveGoals;
        vm.navigate = navigate;
        vm.openConfigureSharingModal = openConfigureSharingModal;

        vm.tabs.push(new utils.Link("Summary", 'sg-bundle-summary'));
        var studentGrowthFrameworkNodes = activeUserContextService.getFrameworkContext().studentGrowthFrameworkNodes;

        // Teachers have process and results components.
        // Principals only have results.
        if (vm.evalType === vm.enums.EvaluationType.TEACHER) {

            if (vm.isEvaluatee) {
                studentGrowthFrameworkNodes.forEach(function (fn) {
                    var goal = _.find(vm.bundle.goals, {frameworkNodeId: fn.id});
                    if (goal) {
                        var processRow = rubricUtils.getRubricRowById(studentGrowthFrameworkNodes, goal.processRubricRowId);
                        vm.tabs.push(new utils.Link(processRow.shortName, 'sg-goal-planning', {id: goal.id}));
                    }
                });

                if (vm.bundle.wfState >= enums.WfState.SGBUNDLE_PROCESS_COMPLETE) {

                    studentGrowthFrameworkNodes.forEach(function (fn) {
                        var goal = _.find(vm.bundle.goals, {frameworkNodeId: fn.id});
                        if (goal) {
                            var resultsRow = goal.resultsRubricRowId ?
                                rubricUtils.getRubricRowById(studentGrowthFrameworkNodes, goal.resultsRubricRowId) : null;
                            if (resultsRow) {
                                vm.tabs.push(new utils.Link(resultsRow.shortName, 'sg-goal-results', {id: goal.id}));
                            }
                        }
                    });
                }

            }
            if (vm.isEvaluatee && vm.bundle.evaluatorScoresShared) {
                vm.tabs.push(new utils.Link("Evaluator Input", 'sg-bundle-evidence-collection', {nodeShortName: vm.bundle.goals[0].frameworkNodeShortName}));
            }
            else if (!vm.isEvaluatee && vm.bundle.wfState>= enums.WfState.SGBUNDLE_PROCESS_SHARED) {
                vm.tabs.push(new utils.Link("Align & Score", 'sg-bundle-evidence-collection', {nodeShortName: vm.bundle.goals[0].frameworkNodeShortName}));
            }
        }
        else if (vm.evalType === vm.enums.EvaluationType.PRINCIPAL) {
            vm.hasProcess = false;
            if (vm.isEvaluatee) {
                studentGrowthFrameworkNodes.forEach(function (fn) {
                    var goal = _.find(vm.bundle.goals, {frameworkNodeId: fn.id});
                    if (goal) {
                        var resultsRow = goal.resultsRubricRowId ?
                            rubricUtils.getRubricRowById(studentGrowthFrameworkNodes, goal.resultsRubricRowId) : null;
                        if (resultsRow) {
                            vm.tabs.push(new utils.Link(resultsRow.shortName, 'sg-goal-results', {id: goal.id}));
                        }
                    }
                });
            }

            if (vm.isEvaluatee && vm.bundle.evaluatorScoresShared) {
                vm.tabs.push(new utils.Link("Evaluator Input", 'sg-bundle-evidence-collection', {nodeShortName: vm.bundle.goals[0].frameworkNodeShortName}));
            }
            else if (!vm.isEvaluatee && vm.bundle.wfState>= enums.WfState.SGBUNDLE_PROCESS_SHARED) {
                vm.tabs.push(new utils.Link("Align & Score", 'sg-bundle-evidence-collection', {nodeShortName: vm.bundle.goals[0].frameworkNodeShortName}));
            }
        }


        function navigate(link) {
            $state.go(link.state, link.params);
        }

        function shareGoalsInner(comment) {
            studentGrowthGoalsService.shareProcess(vm.bundle).then(function() {
                $state.go('sg-bundle-summary', {}, {reload:true});
            });
        }

        function shareScores()
        {

        }
        function approveGoals() {

            var modalInstance = $uibModal.open({
                templateUrl: 'app/student-growth-goals/views/sg-approve-goals-modal.html',
                controller: 'sgApproveGoalsModalController as vm',
                resolve: {
                    evidenceCollection: vm.evidenceCollection
                }
            });

            modalInstance.result.then(function () {
                studentGrowthGoalsService.approveProcess(vm.bundle).then(function() {
                    $state.go('sg-bundle-summary', {}, {reload:true});
                });
            });
        }

        function shareGoals() {

            var modalInstance = $uibModal.open({
                templateUrl: 'app/student-growth-goals/views/sg-share-goals-modal.html',
                controller: 'sgShareGoalsModalController as vm',
                resolve: {
                    bundle: vm.bundle
                }
            });

            modalInstance.result.then(function (result) {
                shareGoalsInner(result.comment);
            });
        }

        function reviseGoals() {

            $confirm(
                {
                    ok: "Yes, I want to revise my goal(s)", cancel: "Cancel"
                }, {templateUrl: 'app/student-growth-goals/views/_revise-bundle-modal.html'})
                .then(() => {
                    studentGrowthGoalsService.reviseProcess(vm.bundle).then(function() {
                        $state.go($state.current, {}, {reload:true});
                    });
                });
        }

        function openConfigureSharingModal() {

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'app/student-growth-goals/views/_share-bundle-modal.html',
                controller: 'shareBundleModalController as vm',
                windowClass: 'modal-sharing',
                resolve: {
                    bundle: vm.bundle
                }
            });

            modalInstance.result.then(function (result) {
            });
        }
    }
})();

