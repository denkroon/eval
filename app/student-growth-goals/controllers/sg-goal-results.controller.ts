/// <reference path="../student-growth-goals.module.ts" />
(function () {
    'use strict';
    angular
        .module('stateeval.student-growth-goals')
        .controller('sgGoalResultsController', sgGoalResultsController);

    sgGoalResultsController.$inject = ['enums', 'artifactService', '_',
        '$state', '$stateParams', 'evidenceCollection', '$confirm',
        'evidenceCollectionService', 'studentGrowthGoalsService'];

    function sgGoalResultsController(enums, artifactService, _,
         $state, $stateParams, evidenceCollection, $confirm,
        evidenceCollectionService, studentGrowthGoalsService) {
        var vm = this;
        vm.enums = enums;
        vm.artifacts = [];
        vm.row = null;
        vm.bundle = evidenceCollection.studentGrowthGoalBundle;

        vm.addArtifact = addArtifact;
        vm.removeArtifact = removeArtifact;
        vm.editArtifact = editArtifact;

        activate();

        function activate() {

            vm.goal = _.find(vm.bundle.goals, {id: parseInt($stateParams.id)});
            vm.row = evidenceCollection.getRowById(vm.goal.resultsRubricRowId);
            vm.artifacts = studentGrowthGoalsService.getLinkedArtifactsForGoal(vm.goal, vm.goal.resultsRubricRowId );
            evidenceCollectionService.recordLastVisit(evidenceCollection, vm.goal.resultsRubricRowId);
        }

        function addArtifact() {
            studentGrowthGoalsService.addArtifactForGoal(vm.goal, vm.goal.resultsRubricRowId);
        }

        function editArtifact(artifact) {
            $state.go('artifact-builder', {artifactId: artifact.id, artifactOrigin: $state.current.name, goalId: vm.goal.id});
        }

        function removeArtifact(artifact) {
            $confirm({}, { templateUrl: 'app/artifacts/views/artifacts-confirm-delete.html' })
                .then(() => {
                    artifactService.deleteArtifact(artifact).then(() => {
                        vm.artifacts = _.reject(vm.artifacts, {id: artifact.id});
                    })
                });
        }
    }
})();
