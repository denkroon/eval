/// <reference path="../core/core.module.ts" />

/**
 * Created by anne on 11/3/2015.
 */
(function () {
    'use strict';

    angular.module('stateeval.summative-eval', ['stateeval.core']);
})();

