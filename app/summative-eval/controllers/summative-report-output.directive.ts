(function () {
    'use strict';

    function summativeReportOutputDirective() {
        return {
            restrict: 'E',
            scope: {
                evidenceCollection: '=',
                reportFormat: '=',
                preview: '='
            },
            templateUrl: 'app/summative-eval/views/summative-report-output.directive.html',
            controller: 'summativeReportOutputController as vm',
            bindToController: true
        }
    }

    class SummativeReportOutputController {
        planTypeDisplayName: string;
        planType: string;
        evaluatee: any;
        evaluatorDisplayName: string;
        evalFocused: boolean;
        schoolYear: string;
        district: string;
        school: string;
        summativeScore: string;
        evidenceCollection: any;
        evaluateeTermUC: string;
        evaluateeTermLC: string;
        isEvaluatee: boolean;
        isAssignedEvaluator: boolean;
        stateFramework: any;
        instructionalFramework: any
        reportTitle: string;
        preview: boolean;
        summativeFinalOptionReportFormats: [any];
        reportFormat: any;
        reportOptions: [any];

        constructor(private activeUserContextService: any,
                    private utils: any,
                    private rubricUtils: any,
                    public enums: any,
                    private reportService: any,
                    private _: _.LoDashStatic,
                    $scope: ng.IScope
        ) {

            this.evaluatee = activeUserContextService.context.evaluatee;

            var title = activeUserContextService.context.frameworkContext.districtConfiguration.finalReportTitle;
            // todo: get report title from da settings
            if (this.preview) {
                this.reportTitle = title + " (Preview)";
            }
            else {
                if (this.evaluatee.evalData.wfState <= this.enums.WfState.EVAL_READY_FOR_CONFERENCE) {
                    this.reportTitle = title + " (Preview)";
                }
                else if (this.evaluatee.evalData.wfState === this.enums.WfState.EVAL_READY_FOR_FORMAL_RECEIPT) {
                    this.reportTitle = title + " (Preview - Final Acknowledgement)";
                }
                else {
                    this.reportTitle = title;
                }
            }

            this.evaluateeTermUC = activeUserContextService.getEvaluateeTermUpperCase();
            this.evaluateeTermLC = activeUserContextService.getEvaluateeTermLowerCase();
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();
            this.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();

            this.planType = activeUserContextService.context.evaluatee.evalData.planType;
            this.evaluatee = activeUserContextService.context.evaluatee;

            this.planTypeDisplayName = utils.mapEvaluationPlanTypeForUserToString(activeUserContextService, this.evaluatee, false);
            this.evaluatorDisplayName = activeUserContextService.getEvaluatorDisplayName();
            this.evalFocused = this.planType === enums.EvaluationPlanType.FOCUSED;

            this.schoolYear =  utils.getSchoolYear(activeUserContextService.context);
            this.district = activeUserContextService.context.orientation.districtName;
            // todo: if this is a dte then we have to figure out the school from the dte's request
            this.school = activeUserContextService.context.orientation.schoolName;
            this.summativeScore = rubricUtils.mapPerformanceLevelToDisplayString(this.evidenceCollection.score.performanceLevel);

            $scope.$watch('vm.reportFormat', (val: any) => {
                this.buildReport();
            });
        }

        setReportOption(optionName) {
            this.reportOptions[optionName] =  this.reportOptionEnabled(optionName);
        }

        buildReport() {
            this.reportService.getReportOptionReportFormats(this.enums.ReportTypeEnum.SUMMATIVE, this.reportFormat)
                .then((summativeFinalOptionReportFormats) => {
                    this.summativeFinalOptionReportFormats = summativeFinalOptionReportFormats;
                    this.reportOptions = [];

                    this.setReportOption(this.enums.SummativeReportOptions.INSTRUCTIONAL_VIEW);
                    this.setReportOption(this.enums.SummativeReportOptions.STATE_VIEW);

                    this.stateFramework = this.activeUserContextService.context.frameworkContext.stateFramework;

                    if (this.reportOptions[this.enums.SummativeReportOptions.INSTRUCTIONAL_VIEW]) {
                        this.instructionalFramework = this.activeUserContextService.context.frameworkContext.instructionalFramework;
                    }

                    this.setReportOption(this.enums.SummativeReportOptions.PRE_CONFERENCE);
                    this.setReportOption(this.enums.SummativeReportOptions.POST_CONFERENCE);
                    this.setReportOption(this.enums.SummativeReportOptions.OBSERVATION_NOTES);
                    this.setReportOption(this.enums.SummativeReportOptions.AVAILABLE_EVIDENCE);
                    this.setReportOption(this.enums.SummativeReportOptions.ALIGNED_EVIDENCE);
                });
        }

        reportOptionEnabled(name) {
            var match = _.find(this.summativeFinalOptionReportFormats, (optionFormat) => {
                return (optionFormat.reportOption.optionName === name && optionFormat.enabled);
            });

            return match != null;
        }

    }

    angular.module('stateeval.summative-eval')
        .directive('summativeReportOutput', summativeReportOutputDirective)
        .controller('summativeReportOutputController', SummativeReportOutputController);

})();
