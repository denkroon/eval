/// <reference path="../summative-eval.module.ts" />
(function() {
    'use strict';

    class summativeEvalEvidenceCollectionController {

        constructor(
            public evidenceCollection: any
        ) {

        }
    }

    angular.module('stateeval.summative-eval')
    .controller('summativeEvalEvidenceCollectionController', summativeEvalEvidenceCollectionController );

})();