// <reference path="../summative-eval.module.ts" />
 (function() {

    'use strict';

    class summativeEvalReportController {

        finalReportFormatType: boolean;

        constructor(private enums: any,
                    public evidenceCollection: any) {

            this.finalReportFormatType = enums.ReportFormatTypeEnum.FINAL;
        }
    }
    angular.module('stateeval.summative-eval')
    .controller('summativeEvalReportController', summativeEvalReportController);
})();
