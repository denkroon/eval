/// <reference path="../summative-eval.module.ts" />

(function () {
    'use strict';

    class summativeEvalController {

        summativeEvaluationEnabled: boolean;
        evaluationTabs: [any];

        constructor(
            private activeUserContextService: any,
            private utils: any,
            public evidenceCollection: any
        ) {
            this.summativeEvaluationEnabled =
                activeUserContextService.context.frameworkContext.districtConfiguration.summativeEvaluationEnabled;

            this.evaluationTabs = [
                new utils.Link('Evidence/Scoring', 'summative-eval-ec'),
                new utils.Link('Report', 'summative-eval-report')
            ];
        }
    }

    angular.module('stateeval.summative-eval')
        .controller('summativeEvalController', summativeEvalController);
})();


