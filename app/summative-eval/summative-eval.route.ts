/// <reference path="summative-eval.module.ts" />

/**
 * Created by anne on 11/3/2015.
 */
(function(){
angular.module('stateeval.summative-eval')
    .config(configureRoutes);

function configureRoutes($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state('summative-eval', {
            url: '/summative-eval',
            parent: 'default-content',
            views: {
                'content@base': {
                    controller: 'summativeEvalController as vm',
                    templateUrl: 'app/summative-eval/views/summative-eval.html'
                }
            },
            resolve: {
                evidenceCollection: ['evidenceCollectionService', 'enums', 'activeUserContextService',
                    function (evidenceCollectionService, enums, activeUserContextService) {
                     var enabled = activeUserContextService.context.frameworkContext.districtConfiguration.summativeEvaluationEnabled;
                        if (enabled) {
                            return evidenceCollectionService.getEvidenceCollection("SUMMATIVE",
                                    enums.EvidenceCollectionType.SUMMATIVE, null,
                                    activeUserContextService.user.id)
                                .then(function (evidenceCollection) {
                                    return evidenceCollection;
                                })
                        }
                        else {
                            return null;
                        }
                }]
            },
            data: {
                evaluateeRequired: true
            },
            ncyBreadcrumb: {
                label: 'Summative Evaluation'
            }
        })
        .state('summative-eval-report', {
                url: '/summative-eval-report',
                parent: 'summative-eval',
                views: {
                    'content@summative-eval': {
                        controller: 'summativeEvalReportController as vm',
                        templateUrl: 'app/summative-eval/views/summative-eval-report.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Summative Evaluation Report'
                }
            })
        .state('summative-eval-ec', {
            url: '/summative-eval-ec',
            parent: 'summative-eval',
            views: {
                'content@summative-eval': {
                    controller: 'summativeEvalEvidenceCollectionController as vm',
                    templateUrl: 'app/summative-eval/views/summative-eval-ec.html'
                }
            },
            ncyBreadcrumb: {
                label: 'Summative Evaluation Evidence'
            }
        });

}
})();