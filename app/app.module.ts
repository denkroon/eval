/**
 * Created by anne on 6/19/2015.
 */
(function () {
    'use strict';

    angular.module('stateeval', [
        'stateeval.core',
        'stateeval.assignments',
        'stateeval.observation',
        'stateeval.login',
        'stateeval.artifact',
        'stateeval.student-growth-goals',
        'stateeval.district-admin',
        'stateeval.super-admin',
        'stateeval.prompt',
        'stateeval.reports',
        'stateeval.resources',
        'stateeval.layout',
        'stateeval.summative-eval',
        'stateeval.configuration',
        'stateeval.self-assessment',
        'stateeval.training',
        'stateeval.ytd-evidence',
        'stateeval.start-of-year'
    ]);

})();

