/// <reference path="../training.module.ts" />

namespace StateEval.Training {

    interface ISegment {
        alignedRubricRows?: any[];
        anchorId?: number;
        annotation?: string;
        endTime?: number;
        id?: number;
        protocolId?: number;
        startTime?: number;
        trainingProtocol?: any;
        $$isNew?: boolean;
        $$editing?: boolean;
        $$range?: any;
        $$originalValues?: {
            annotation: string,
            startTime: number,
            endTime: number,
            alignedRubricRows: any[]
        }
    }

    interface IAnchor {
        creationDateTime: string;
        districtCode: string;
        evidenceSegments: ISegment[];
        id: number;
        trainingProtocol: any;
        trainingProtocolId: number;
    }

    class TrainingProtocolAnchorController {

        API: Videogular.API;
        config: {};

        constructor(
            private trainingService: any,
            public anchor: IAnchor,
            private _: _.LoDashStatic,
            private $sce: ng.ISCEService
        ) {
            this.config = {
                sources: [
                    { src: this.$sce.trustAsResourceUrl(this.anchor.trainingProtocol.videoSrc), type: "video/mp4" }
                ],
                plugins: {
                    poster: this.anchor.trainingProtocol.videoPoster
                }
            };
        }

        onPlayerReady(API) {
            this.API = API;
        };

        startPercentage(segment: ISegment) {
            return segment.startTime * 100000.0 / this.API.totalTime;
        }

        lengthPercentage(segment: ISegment) {
            return (Math.max(segment.endTime, segment.startTime) - segment.startTime) * 100000.0 / this.API.totalTime;
        }

        addSegment() {
            this.API.pause();

            var s: ISegment = {
                startTime: 0,
                alignedRubricRows: [],
                anchorId: this.anchor.id,
                protocolId: this.anchor.trainingProtocolId,
                $$isNew: true,
                $$editing: true,
                $$range: {
                    start: [
                        this.API.currentTime / 1000,
                        this.API.totalTime / 1000
                    ],
                    connect: true,
                    step: 1,
                    range: {
                        min: 0,
                        max: this.API.totalTime / 1000
                    }
                }
            };

            this.anchor.evidenceSegments.push(s);
        }

        edit(segment: ISegment) {
            console.log(segment)
            segment.$$editing = true;
            segment.$$range = {
                val: [segment.startTime, segment.endTime || segment.startTime]
            };
            segment.$$originalValues = {
                annotation: segment.annotation,
                startTime: segment.startTime,
                endTime: segment.endTime,
                alignedRubricRows: _.clone(segment.alignedRubricRows)
            }

            segment.$$range = {
                start: [segment.startTime, segment.endTime],
                connect: true,
                step: 1,
                range: {
                    min: 0,
                    max: this.API.totalTime / 1000
                }
            }
        }

        play(segment: ISegment) {
            this.API.seekTime(segment.startTime);
            this.API.play();
        }

        cancel(segment: ISegment) {
            if (segment.$$isNew) {
                _.remove(this.anchor.evidenceSegments, segment);
            }
            else {
                segment.annotation = segment.$$originalValues.annotation;
                segment.startTime = segment.$$originalValues.startTime;
                segment.endTime = segment.$$originalValues.endTime;
                segment.alignedRubricRows = _.clone(segment.$$originalValues.alignedRubricRows);
                segment.$$editing = false;
            }
        }

        delete(segment: ISegment) {
            this.trainingService.deleteTrainingProtocolAnchorEvidenceSegment(segment).then(() => {
                this.anchor.evidenceSegments = _.reject(this.anchor.evidenceSegments, { id: segment.id });
            });
        }

        alignmentDone(segment, alignedRubricRows) {
            segment.alignedRubricRows = alignedRubricRows;
        }

        save(segment: ISegment) {
            segment.$$editing = false;

            segment.startTime = Math.round(segment.$$range.start[0]);
            segment.endTime = Math.round(segment.$$range.start[1]);

            if (segment.$$isNew) {
                this.trainingService.addTrainingProtocolAnchorEvidenceSegment(segment).then(() => {
                    this.trainingService.saveEvidenceSegmentAlignment(segment);
                });
            }
            else {
                this.trainingService.updateTrainingProtocolAnchorEvidenceSegment(segment).then(() => {
                    this.trainingService.saveEvidenceSegmentAlignment(segment);
                })
            }
        }
    }

    angular
        .module('stateeval.training')
        .controller('trainingProtocolAnchorController', TrainingProtocolAnchorController);

}