/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('practiceSessionController', practiceSessionController);

 practiceSessionController.$inject = ['session', 'evidenceCollection', 'evidenceCollectionService',
     'activeUserContextService', 'enums', 'trainingService'];

/* @ngInject */
function practiceSessionController (session, evidenceCollection, evidenceCollectionService,
        activeUserContextService, enums, trainingService) {
    /* jshint validthis: true */
    var vm = this;
    vm.enums = enums;

    vm.evidenceCollection = evidenceCollection;
    vm.session = session;
    vm.classrooms = [];
    vm.switch = 1;
    vm.startClosed = true;

    vm.framework = activeUserContextService.context.framework;

    activate();

    ////////////////

    function activate() {

        if (vm.session.sessionType === enums.PracticeSessionTypeEnum.LEARNING_WALK) {
            trainingService.getLearningWalkClassroomsForPracticeSession(vm.session.id).then(function(classrooms) {
                vm.classrooms = classrooms;
                vm.selectedClassroom = vm.classrooms[0];
            })
        }
    }


}
}());