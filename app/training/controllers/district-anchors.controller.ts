/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('districtAnchorsController', districtAnchorsController);

districtAnchorsController.$inject = ['trainingService', 'utils', '_', '$state'];

/* @ngInject */
function districtAnchorsController(trainingService, utils, _, $state) {
    /* jshint validthis: true */
    var vm = this;

    vm.sessions = [];

    vm.activate = activate;
    vm.editSession = editSession;

    activate();

    ////////////////

    function activate() {

        trainingService.getAnchorSessionsForDistrict()
        .then(function(sessions) {
            vm.sessions = sessions;
            })
    }

    function editSession(session) {
        $state.go('anchor-session', {id: session.id});
    }
}
})();