/// <reference path="../training.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.training')
        .controller('joinSessionModalController', joinSessionModalController);

    joinSessionModalController.$inject = ['$uibModalInstance', 'enums', 'service'];

    /* @ngInject */
    function joinSessionModalController($uibModalInstance, enums, service) {
        var vm = this;

        vm.sessionKey = '';
        vm.notFound = false;

        vm.join = join;
        vm.cancel = cancel;

        /////////////////////////////////

        activate();

        function activate() {
        }

        function join() {

            vm.notFound = false;
            service.joinSession(vm.sessionKey)
                .then(function(session) {
                    if (session != null) {
                        $uibModalInstance.close({
                            session: session
                        });
                    }
                    else {
                        vm.notFound = true;
                    }
                });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

