/// <reference path="../training.module.ts" />

(function() {
    'use strict';

    angular
        .module('stateeval.training')
        .controller('learningWalkSetupController', learningWalkSetupController);

    learningWalkSetupController.$inject = ['$state', 'learningWalkService', 'learningWalk'];

    /* @ngInject */
    function learningWalkSetupController($state, learningWalkService, learningWalk) {

        var vm = this;
        vm.learningWalk = learningWalk;
        vm.showAddParticipantsArea = false;
        vm.showAddClassroomArea = false;
        vm.classroomName = '';
        vm.classroomTags = '';
        vm.classaroom = null;

        vm.deleteLearningWalk = deleteLearningWalk;
        vm.saveLearningWalk = saveLearningWalk;
        vm.startAddParticipants = startAddParticipants;
        vm.inviteParticipants = inviteParticipants;
        vm.cancelAddParticipants = cancelAddParticipants;

        vm.startAddClassroom = startAddClassroom;
        vm.saveClassroom = saveClassroom;
        vm.cancelAddClassroom = cancelAddClassroom;

        vm.editClassroom = editClassroom;
        vm.deleteClassroom = deleteClassroom;

        vm.autoSave = function() {
            saveLearningWalk();
        }

        ////////////////////////////////////////

        activate();

        function activate() {
        }

        function deleteClassroom(classroom) {
            vm.learningWalk.classrooms = _.filter(vm.learningWalk.classrooms, {id: classroom.id});
            learningWalkService.saveLearningWalkClassrooms(vm.learningWalk);
        }

        function editClassroom(classroom) {
            vm.classroom = classroom;
            vm.classroomName = classroom.name;
            vm.classroomTags = classroom.tags;
            vm.showAddClassroomArea = true;
        }

        function startAddClassroom() {
            vm.classroom = null;
            vm.classroomName = '';
            vm.classroomTags = '';
            vm.showAddClassroomArea = true;
        }

        function saveClassroom() {
            vm.showAddClassroomArea = false;
            if (vm.classroom) {
                vm.classroom.name = vm.classroomName;
                vm.classroom.tags = vm.classroomTags;
            }
            else {
                vm.learningWalk.classrooms.push(learningWalkService.newClassroom(vm.learningWalk, vm.classroomName, vm.classroomTags));
            }

            learningWalkService.saveLearningWalkClassrooms(vm.learningWalk);
        }

        function cancelAddClassroom() {
            vm.showAddClassroomArea = false;
        }

        function startAddParticipants() {
            vm.showAddParticipantsArea = true;
        }

        function inviteParticipants() {
            vm.showAddParticipantsArea = false;
        }

        function cancelAddParticipants() {
            vm.showAddParticipantsArea = false;
        }

        function saveLearningWalk() {
            return learningWalkService.saveLearningWalk(vm.learningWalk).then(function () {
            });
        }

        function deleteLearningWalk() {
            learningWalkService.deleteLearningWalk(vm.learningWalk).then(function () {
                $state.go("learning-walk-list");
            });
        }
    }

})();