/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('trainingProtocolListController', trainingProtocolListController);

trainingProtocolListController.$inject = ['trainingService', 'practiceSessionService', 'activeUserContextService',
    'learningWalkService', '_', '$state', '$uibModal'];

/* @ngInject */
function trainingProtocolListController(trainingService, practiceSessionService, activeUserContextService,
        learningWalkService, _, $state, $uibModal) {

    /* jshint validthis: true */
    var vm = this;

    vm.districtAnchors = [];
    vm.protocols = [];
    vm.labelGroups = [];
    vm.selectedLabels = [];
    vm.titleSearch = '';
    vm.searchResults = [];
    vm.playlistFilter = false;

    vm.criteria = [];
    vm.practice = [];
    vm.subjects = [];
    vm.gradeLevels = [];
    vm.providedBy = [];
    vm.playlist = [];

    vm.activate = activate;
    vm.search = search;
    vm.addToPlaylist = addToPlaylist;
    vm.createPracticeSession = createPracticeSession;
    vm.createDistrictAnchorSession = createDistrictAnchorSession;
    vm.createLearningWalk = createLearningWalk;
	vm.openModalVideo = openModalVideo;
    vm.gotoDetail = gotoDetail;
    vm.editAnchor = editAnchor;
    vm.createAnchor = createAnchor;

    activate();

    ////////////////

    function activate() {

        trainingService.getTrainingProtocols()
        .then(function(protocols) {
            vm.protocols = protocols;
            vm.searchResults = vm.protocols;
            loadCriteriaAlignment();
            loadHighLeveragePracticesAlignment();
            return trainingService.getTrainingProtocolLabelGroups();
        }).then(function(labelGroups) {
            vm.labelGroups = labelGroups;
            loadSubjects();
            loadProvidedBy();
            loadGradeLevels();
            return trainingService.getPlaylist();
        }).then(function(playlist) {
            loadPlaylist(playlist);
            return trainingService.getDistrictAnchors();
        }).then(function(anchors) {
            vm.districtAnchors = anchors;
            loadAnchors();
        })
    }

    function createLearningWalk() {
        learningWalkService.createLearningWalk().then(function(learningWalk) {
            $state.go('learning-walk', {id: learningWalk.id});
        })
    }

    function createPracticeSession(protocol) {
        practiceSessionService.createVideoPracticeSession(protocol).then(function(practiceSession) {
                $state.go('practice-session-setup', {id: practiceSession.id});
        })
    }

    function createDistrictAnchorSession(protocol) {
        trainingService.createDistrictAnchorSession(protocol).then(function(practiceSession) {
            $state.go('anchor-session', {id: practiceSession.id});
        })
    }

    function createAnchor(protocol) {
        var anchor = trainingService.newTrainingProtocolAnchor(protocol.id);
        trainingService.saveTrainingProtocolAnchor(anchor).then(function() {
            $state.go('training-protocol-anchor', {id: anchor.id});
        })
    }

    function editAnchor(protocol) {
         $state.go('training-protocol-anchor', {id: protocol.anchor.id});
    }

    function loadAnchors() {
        vm.protocols.forEach(function(protocol) {
            var anchor = _.find(vm.districtAnchors, {trainingProtocolId: protocol.id, districtCode: activeUserContextService.context.orientation.districtCode});
            protocol.anchor = anchor;
        })
    }

    function addToPlaylist(protocol) {
        trainingService.addToPlaylist(protocol).then(function() {
            vm.playlist[protocol.id] = true;
        })
    }

    function addToSearchResults(protocol) {
        if (vm.playlistFilter && vm.titleSearch!="") {
            if (vm.playlist[protocol.id] && protocol.title.toUpperCase().indexOf(vm.titleSearch.toUpperCase()) >= 0) {
                vm.searchResults.push(protocol);
            }
        }
        else if (vm.playlistFilter) {
            if (vm.playlist[protocol.id]) {
                vm.searchResults.push(protocol);
            }
        }
        else if (vm.titleSearch!="") {
            if (protocol.title.toUpperCase().indexOf(vm.titleSearch.toUpperCase()) >= 0) {
                vm.searchResults.push(protocol);
            }
        }
        else {
            vm.searchResults.push(protocol);
        }
    }

    function search()
    {
        vm.searchResults = [];
        if (vm.selectedLabels.length === 0 && !vm.playlistFilter && vm.titleSearch==="") {
            vm.searchResults = vm.protocols;
        }
        else {
            vm.protocols.forEach(function (protocol) {
                var added = false;
                protocol.labels.forEach(function (label) {
                    if (_.find(vm.selectedLabels, {id: label.id})) {
                        added = true;
                        if (!_.find(vm.searchResults, {id: protocol.id}))
                        {
                            addToSearchResults(protocol);
                        }
                    }
                });
                if (!added && vm.selectedLabels.length==0 && (vm.titleSearch !== "" || vm.playlistFilter)) {
                    addToSearchResults(protocol);
                }
            })
        }
    }

    function loadPlaylist(playlist) {
        vm.protocols.forEach(function(protocol) {
            vm.playlist[protocol.id] = _.find(playlist, {protocolId: protocol.id})?true:false;
        })
    }

    function loadProvidedBy() {
        vm.protocols.forEach(function(protocol) {
            vm.providedBy[protocol.id] = trainingService.getProvidedByForProtocol(protocol, vm.labelGroups);
        })
    }

    function loadGradeLevels() {
        vm.protocols.forEach(function(protocol) {
            vm.gradeLevels[protocol.id] = trainingService.getGradeLevelsForProtocol(protocol, vm.labelGroups);
        })
    }

    function loadSubjects() {
        vm.protocols.forEach(function(protocol) {
            vm.subjects[protocol.id] = trainingService.getSubjectsForProtocol(protocol,vm.labelGroups);
        })
    }

    function loadCriteriaAlignment() {
        vm.protocols.forEach(function(protocol) {
            vm.criteria[protocol.id] = trainingService.getCriteriaAlignmentForProtocol(protocol);
        })
    }

    function loadHighLeveragePracticesAlignment() {
        vm.protocols.forEach(function(protocol) {
            vm.practice[protocol.id] = trainingService.getHighLeverageAlignmentForProtocol(protocol);
        })
    }

	function openModalVideo(protocol) {
		$uibModal.open({
                        templateUrl: 'app/training/views/training-protocol-modal.html',
                        controller: 'trainingProtocolModalController as vm',
                        resolve: {
                            libItem: function () {
                                return protocol;
                            }
                        }
         });
	}

    function gotoDetail(protocol) {
        $state.go('training-protocol', {id: protocol.id});
    }
}
})();