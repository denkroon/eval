/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('practiceSessionNotesController', practiceSessionNotesController);

 practiceSessionNotesController.$inject = ['evidenceCollection','activeUserContextService', 'enums', 'session',
    'trainingService', '$sce', 'practiceSessionService'];

/* @ngInject */
function practiceSessionNotesController (evidenceCollection, activeUserContextService, enums, session,
    trainingService, $sce, practiceSessionService) {
    /* jshint validthis: true */
    var vm = this;
    vm.enums = enums;

    vm.evidenceCollection = evidenceCollection;
    vm.session = session;
    vm.sessionObservation = evidenceCollection.practiceSessionObservation;
    vm.framework = activeUserContextService.context.framework;
    vm.showNewAnntationArea = false;

    vm.API = null;

    vm.subject = '';
    vm.content = '';

    vm.onPlayerReady = function(API) {
        vm.API = API;
    };
    vm.addAnnotation = addAnnotation;
    vm.cancelAnnotation = cancelAnnotation;
    vm.saveAnnotation = saveAnnotation;
    vm.goToAnnotation = goToAnnotation;

    activate();

    ////////////////

    function activate() {

        trainingService.getTrainingProtocolById(vm.session.trainingProtocolId).then(function(protocol) {
            vm.protocol = protocol;
            vm.config = {
                sources: [
                    {src: $sce.trustAsResourceUrl(vm.protocol.videoSrc), type: "video/mp4"}
                ],
                tracks: [
                    {
                        src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                        kind: "subtitles",
                        srclang: "en",
                        label: "English",
                        default: ""
                    }
                ],
                theme: "styles/videogular/videogular.css",
                plugins: {
                    poster: vm.protocol.videoPoster
                }
            };
        })

    }

    function goToAnnotation(annotation) {
        vm.API.seekTime(annotation.seconds);
        vm.API.play();
    }

    function addAnnotation() {
        vm.API.pause();
        vm.showNewAnnotationArea = true;
    }

    function cancelAnnotation() {
        vm.showNewAnnotationArea = false;
    }

    function saveAnnotation() {
        vm.showNewAnntationArea = false;
        var annotation = {
            createdByUserId: activeUserContextService.user.id,
            subject: vm.subject,
            content: vm.content,
            seconds:parseInt(vm.API.currentTime)/1000
        };
        vm.sessionObservation.videoAnnotations.push(annotation);
        practiceSessionService.updatePracticeSessionObservation(vm.sessionObservation);

    }

}
})()