/// <reference path="../training.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.training')
        .controller('learningWalkController', learningWalkController);

    learningWalkController.$inject = ['learningWalk', 'utils', 'activeUserContextService'];
    function learningWalkController(learningWalk, utils, activeUserContextService) {

        var vm = this;
        vm.learningWalk = learningWalk;

        var tabs = [];
        if (activeUserContextService.user.id === vm.learningWalk.createdByUserId) {
            new utils.Link('Setup', 'learning-walk-setup');
        }

        new utils.Link('Score', 'learning-walk-score');

        vm.learningWalkTabs = tabs;

        activate();

        function activate() {
        }
    }
})();