/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('practiceSessionSetupController', practiceSessionSetupController);

 practiceSessionSetupController.$inject = ['practiceSessionService', 'session', '$uibModal'];

/* @ngInject */
function practiceSessionSetupController (practiceSessionService, session, $uibModal) {
    /* jshint validthis: true */
    var vm = this;

    vm.session = session;

    vm.inviteParticipants = inviteParticipants;
    activate();

    ////////////////

    function activate() {
    }

    function inviteParticipants() {
        var modalInstance = $uibModal.open({
            animation: false,
            templateUrl: 'app/training/views/invite-participants-modal.html',
            controller: 'inviteParticipantsModalController as vm',
            size: 'lg',
            resolve: {
            }
        });

        modalInstance.result.then(function (result) {
            //todo: send invitiation notification
        });
    }
}
})();