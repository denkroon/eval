/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('practiceSessionListController', practiceSessionListController);

practiceSessionListController.$inject = ['practiceSessionService', '_', '$state', '$uibModal'];

/* @ngInject */
function practiceSessionListController(practiceSessionService, _, $state, $uibModal) {
    /* jshint validthis: true */
    var vm = this;

    vm.sessions = [];

    vm.activate = activate;
    vm.editSession = editSession;
    vm.joinSession = joinSession;
    vm.createPracticeSession = createPracticeSession;

    activate();

    ////////////////

    function activate() {

        practiceSessionService.getPracticeSessionsForUser()
        .then(function(sessions) {
            vm.sessions = sessions;
            })
    }

    function createPracticeSession() {
        practiceSessionService.createVideoPracticeSession().then(function(session) {
            $state.go('practice-session-setup', {id: session.id});
        })
    }

    function joinSession() {

        modalInstance = $uibModal.open({
            animation: false,
            templateUrl: 'app/training/views/join-session-modal.html',
            controller: 'joinPracticeSessionModalController as vm',
            size: 'lg'
        });

        modalInstance.result.then(function (result) {
            var session = result.session;
            $state.go('practice-session-notes', {id: session.id});
        });
    }

    function editSession(session) {
        // todo: go to the right place based on whether you are the creator
        // or just a participant
        $state.go('practice-session-notes', {id: session.id});
    }
}
})();