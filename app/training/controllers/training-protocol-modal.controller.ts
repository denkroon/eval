/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('trainingProtocolModalController', trainingProtocolModalController);

 trainingProtocolModalController.$inject = ['trainingService','$uibModalInstance', '$sce',  'libItem'];

/* @ngInject */
function trainingProtocolModalController (trainingService, $uibModalInstance,  $sce, libItem) {
    /* jshint validthis: true */
    var vm = this;

    vm.protocol = libItem;
	vm.config = {
                sources: [
                    {src: $sce.trustAsResourceUrl(vm.protocol.videoSrc), type: "video/mp4"}
                ],
              tracks: [
                  {
                      src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                      kind: "subtitles",
                      srclang: "en",
                      label: "English",
                      default: ""
                  }
              ],
                theme: "styles/videogular/videogular.css",
                plugins: {
                    poster: vm.protocol.videoPoster
                },
				autoPlay: true,
            };
	vm.close = function () {
        $uibModalInstance.dismiss('cancel');
    }

	vm.onPlayerReady = function() {
		console.log('played');
	}
}
})();