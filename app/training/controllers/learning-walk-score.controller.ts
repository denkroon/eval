/// <reference path="../training.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.training')
        .controller('learningWalkScoreController', learningWalkScoreController);

    learningWalkScoreController.$inject = ['evidenceCollectionService', 'learningWalkService', '$stateParams', 'enums', 'activeUserContextService'];

    /* @ngInject */
    function learningWalkScoreController(evidenceCollectionService, learningWalkService, $stateParams, enums, activeUserContextService) {

        var vm = this;

        vm.learningWalk = null;
        vm.classroom = null;

        vm.changeClassroom = changeClassroom;

        activate();

        function activate() {

            learningWalkService.getLearningWalkById(parseInt($stateParams.learningWalkId)).then(function(learningWalk) {
                vm.learningWalk = learningWalk;
            });
        }

        function changeClassroom() {
            learningWalkService.getLearningWalkObservationForCurrentUser(vm.learningWalk.id, vm.classroom.id)
                .then(function(learningWalkObservation) {
                    return evidenceCollectionService.getEvidenceCollection("LEARNING-WALK",
                            enums.EvidenceCollectionType.LEARNING_WALK_OBS,
                            learningWalkObservation.id,
                            activeUserContextService.user.id)
                        .then(function (evidenceCollection) {
                                vm.evidenceCollection = evidenceCollection;
                            }
                        );
                });
        }

    }

})();
