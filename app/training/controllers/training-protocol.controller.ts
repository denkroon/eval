/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('trainingProtocolController', trainingProtocolController);

 trainingProtocolController.$inject = ['trainingService','$stateParams', '$sce', 'protocol'];

/* @ngInject */
function trainingProtocolController (trainingService, $stateParams, $sce, protocol) {
    /* jshint validthis: true */
    var vm = this;

    vm.protocol = protocol;

    vm.activate = activate();

    activate();

    ////////////////

    function activate() {
        trainingService.getTrainingProtocolLabelGroups().then(function(labelGroups) {
            vm.labelGroups = labelGroups;
            vm.grades = trainingService.getGradeLevelsForProtocol(vm.protocol, vm.labelGroups);
            vm.subjects = trainingService.getSubjectsForProtocol(vm.protocol, vm.labelGroups);
            vm.providedBy = trainingService.getProvidedByForProtocol(vm.protocol, vm.labelGroups);
            vm.criteria = trainingService.getCriteriaAlignmentForProtocol(vm.protocol);
            vm.highLeverage = trainingService.getHighLeverageAlignmentForProtocol(vm.protocol);

            vm.config = {
                sources: [
                    {src: $sce.trustAsResourceUrl(vm.protocol.videoSrc), type: "video/mp4"}
                ],
              tracks: [
                  {
                      src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                      kind: "subtitles",
                      srclang: "en",
                      label: "English",
                      default: ""
                  }
              ],
                theme: "styles/videogular/videogular.css",
                plugins: {
                    poster: vm.protocol.videoPoster
                }
            };
        })
    }


}
})();