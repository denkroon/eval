/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('practiceSessionReportController', practiceSessionReportController);

practiceSessionReportController.$inject = ['evidenceCollection','activeUserContextService', 'enums'];

/* @ngInject */
function practiceSessionReportController (evidenceCollection, activeUserContextService, enums) {
    /* jshint validthis: true */
    var vm = this;
    vm.enums = enums;

    vm.evidenceCollection = evidenceCollection;
    vm.framework = activeUserContextService.context.framework;

    activate();

    ////////////////

    function activate() {

    }
}
})();