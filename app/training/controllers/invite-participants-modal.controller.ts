/// <reference path="../training.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.training')
        .controller('inviteParticipantsModalController', inviteParticipantsModalController);

    inviteParticipantsModalController.$inject = ['$uibModalInstance', 'enums', 'activeUserContextService'];

    /* @ngInject */
    function inviteParticipantsModalController($uibModalInstance, enums,  activeUserContextService) {

        var vm = this;

        vm.save = save;
        vm.cancel = cancel;

        /////////////////////////////////

        function save() {

            $uibModalInstance.close({
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

