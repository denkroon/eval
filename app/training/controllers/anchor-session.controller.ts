/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('anchorSessionController', anchorSessionController);

 anchorSessionController.$inject = ['session', 'evidenceCollection', 'evidenceCollectionService', 'activeUserContextService'];

/* @ngInject */
function anchorSessionController (session, evidenceCollection, evidenceCollectionService, activeUserContextService) {
    /* jshint validthis: true */
    var vm = this;

    vm.evidenceCollection = evidenceCollection;
    vm.session = session;
    vm.framework = activeUserContextService.context.framework;

    activate();

    ////////////////

    function activate() {
    }


}
})();