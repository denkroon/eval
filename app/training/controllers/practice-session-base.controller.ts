/// <reference path="../training.module.ts" />

(function(){
angular
    .module('stateeval.training')
    .controller('practiceSessionBaseController', practiceSessionBaseController);

 practiceSessionBaseController.$inject = ['$state', 'session'];

/* @ngInject */
function practiceSessionBaseController ($state, session) {
    /* jshint validthis: true */

    var vm = this;
    vm.session = session;
    vm.selectedTab = $state.current.name.split('-')[2];

    activate();

    ////////////////

    function activate() {

    }


}
})();