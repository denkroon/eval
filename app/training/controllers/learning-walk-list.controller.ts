/// <reference path="../training.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.training')
        .controller('learningWalkListController', learningWalkListController);

    learningWalkListController.$inject = ['learningWalkService', '$state', '$uibModal', 'activeUserContextService'];
    function learningWalkListController(learningWalkService, $state, $uibModal, activeUserContextService) {

        var vm = this;
        vm.learningWalks = [];

        vm.addNewLearningWalk = addNewLearningWalk;
        vm.editLearningWalk = editLearningWalk;
        vm.joinLearningWalk = joinLearningWalk;

        activate();

        function activate() {
            learningWalkService.getLearningWalksForCurrentUser().then(function(learningWalks) {
                vm.learningWalks = learningWalks;
            })
        }

        function joinLearningWalk() {

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'app/training/views/join-session-modal.html',
                controller: 'joinSessionModalController as vm',
                resolve: {
                    service: learningWalkService
                }
            });

            modalInstance.result.then(function (result) {
                var learningWalk = result.session;
                $state.go('learning-walk', {learningWalkId: learningWalk.id});
            });
        }


        function editLearningWalk(learningWalk) {
            if (learningWalk.createdByUserId === activeUserContextService.user.id) {
                $state.go('learning-walk-setup', {learningWalkId: learningWalk.id});
            }
            else {
                $state.go('learning-walk-score', {learningWalkId: learningWalk.id});
            }
        }

        function addNewLearningWalk() {
            learningWalkService.createLearningWalk()
                .then(function(learningWalk) {
                    editLearningWalk(learningWalk);
                })
        }
    }
})();