/// <reference path="../training.module.ts" />

(function() {
angular
    .module('stateeval.training')
    .controller('practiceSessionEvidenceController', practiceSessionEvidenceController);

 practiceSessionEvidenceController.$inject = ['evidenceCollection','activeUserContextService', 'enums',
    'evidenceCollectionService'];

/* @ngInject */
function practiceSessionEvidenceController (evidenceCollection, activeUserContextService, enums,
    evidenceCollectionService) {
    /* jshint validthis: true */
    var vm = this;
    vm.enums = enums;

    vm.evidenceCollection = evidenceCollection;
    vm.session = evidenceCollection.practiceSessionObservation;
    vm.framework = activeUserContextService.context.framework;

    activate();

    ////////////////

    function activate() {

    }


}
})()