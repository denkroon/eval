/// <reference path="../training.module.ts" />

namespace StateEval.Training {
    angular.module('stateeval.training')
        .filter('formatSeconds', () => {

            function pad(n) {
                return (n < 10) ? ("0" + n) : n;
            }

            return input => {
                var hours = Math.floor(input / 3600);
                var minutes = pad(Math.floor((input / 60) % 60));
                var seconds = pad(Math.floor(input % 60));

                return `${hours ? hours + ':' : ''}${minutes}:${seconds}`;
            };
        })
}