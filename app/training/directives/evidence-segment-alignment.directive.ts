/// <reference path="../training.module.ts" />

/**
 * Created by anne on 11/30/2015.
 */

(function() {
    'use strict';

    angular.module('stateeval.training')
        .directive('evidenceSegmentAlignment', evidenceSegmentAlignmentDirective)
        .controller('evidenceSegmentAlignmentController', evidenceSegmentAlignmentController);

    evidenceSegmentAlignmentController.$inject = ['trainingService', 'activeUserContextService', '$rootScope', 'rubricUtils', '_'];

    function evidenceSegmentAlignmentDirective() {
        return {
            scope: {
                alignedRubricRows: '=',
                readOnly: '=',
                done: '&'
            },
            templateUrl: 'app/training/views/evidence-segment-alignment.directive.html',
            controller: 'evidenceSegmentAlignmentController as vm',
            bindToController: true
        }
    }

    function evidenceSegmentAlignmentController(trainingService, activeUserContextService, $rootScope, rubricUtils, _) {
        var vm = this,
            flatRows,
            framework = activeUserContextService.getActiveFramework();

        vm.loaded = false;

        $rootScope.$on('change-framework', function () {
            framework = activeUserContextService.getActiveFramework();
            loadFramework();
        });

        activate();

        function activate() {
            loadFramework();
            vm.loaded = true;
        }

        function loadFramework() {
            flatRows = rubricUtils.getFlatRows(framework);

            // massaging the data to pass it to the categorized selector directive
            vm.categories = _.map(framework.frameworkNodes, function(fn) {
                return {
                    name: formatName(fn),
                    collapsed: true,
                    rows: _.map(fn.rubricRows, function(rr) {
                        return {
                            id: rr.id,
                            name: rr.shortName + ' ' + rr.title,
                            selected: _.any(vm.alignedRubricRows, { id: rr.id }),
                            inUse: false
                        };
                    })
                };
            });
        }
        // called by the categories selector when the selection has changed
        vm.changed = function changed(selectedRows) {
            var alignedRubricRows = _.map(selectedRows, function(row) { return flatRows[row.id]; });
            vm.done({$alignedRubricRows: alignedRubricRows});
        }

        function formatName(row) {
            return '<div class="pull-left">' + row.shortName + '</div>' +
                '<div class="row-title">' + row.title + '</div>';
        }
    }

})();
