/// <reference path="../training.module.ts" />

/**
 * Created by anne on 11/6/2015.
 */
(function () {
    'use strict';

    angular.module('stateeval.training')
        .directive('videoEvidenceSegmentSummary', videoEvidenceSegmentSummary)
        .controller('videoEvidenceSegmentSummaryController', videoEvidenceSegmentSummaryController);

    videoEvidenceSegmentSummaryController.$inject = ['trainingService', 'activeUserContextService', 'utils', 'enums', '$sce'];

    function videoEvidenceSegmentSummary() {
        return {
            restrict: 'E',
            scope: {
                segment: '='
            },
            templateUrl: 'app/training/views/video-evidence-segment-summary.directive.html',
            controller: 'videoEvidenceSegmentSummaryController as vm',
            bindToController: true
        }
    }

    function videoEvidenceSegmentSummaryController(trainingService, activeUserContextService, utils, enums, $sce) {
        var vm = this;
        vm.enums = enums;

        vm.expand = false;
        vm.source = '';

        vm.config = {
            sources: [
                {src: $sce.trustAsResourceUrl(vm.segment.trainingProtocol.videoSrc), type: "video/mp4"}
            ],
            tracks: [
                {
                    src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                    kind: "subtitles",
                    srclang: "en",
                    label: "English",
                    default: ""
                }
            ],
            theme: {
                url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
            },
            plugins: {
                poster: vm.segment.trainingProtocol.videoPoster
            }
        };

        activate();

        function activate() {
        }

    }

}) ();
