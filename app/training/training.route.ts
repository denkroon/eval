/// <reference path="training.module.ts" />

(function() {
 'use strict';

    angular.module('stateeval.training')
        .config(configureRoutes);

    configureRoutes.$inject = ['$stateProvider'];

    function configureRoutes($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('training-protocols', {
                url: '/training',
                parent: 'default-content',
                views: {
                    'content@base': {
                        templateUrl: 'app/training/views/training-protocol-list.html',
                        controller: 'trainingProtocolListController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Training Videos'
                }
            })
            .state('training-protocol', {
                url: '/training-protocol/:id',
                parent: 'training-protocols',
                views: {
                    'content@base': {
                        templateUrl: 'app/training/views/training-protocol.html',
                        controller: 'trainingProtocolController as vm'
                    }
                },
                resolve: {
                    protocol: ['trainingService', '$stateParams',
                        function (trainingService, $stateParams) {
                            return trainingService.getTrainingProtocolById(parseInt($stateParams.id)).then(function(protocol) {
                                return protocol;
                            })
                        }
                    ]
                },
                ncyBreadcrumb: {
                    label: 'Training Video {{vm.protocol.title}}'
                }
            })
            .state('learning-walk-list', {
                url: '/learning-walk-list',
                parent: 'default-content',
                views: {
                    'content@base': {
                        templateUrl: 'app/training/views/learning-walk-list.html',
                        controller: 'learningWalkListController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Learning Walks'
                }

            })
            .state('learning-walk', {
                url: '/learning-walk/:learningWalkId',
                parent: 'default-content',
                views: {
                    'content@base': {
                        templateUrl: 'app/training/views/learning-walk.html',
                        controller: 'learningWalkController as vm'
                    }
                },
                resolve: {
                    learningWalk: ['learningWalkService', '$stateParams', function (learningWalkService, $stateParams) {
                        return learningWalkService.getLearningWalkById($stateParams.learningWalkId)
                            .then(function (learningWalk) {
                                return learningWalk;
                            });
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Learning Walk - {{ vm.learningWalk.title}} - {{vm.learningWalk.randomDigits}} + {{vm.learningWalk.id}}'
                }

            })
            .state("learning-walk-setup", {
                url: '/learning-walk-setup',
                parent: 'learning-walk',
                views: {
                    'content@learning-walk': {
                        templateUrl: 'app/training/views/learning-walk-setup.html',
                        controller: 'learningWalkSetupController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Learning Walk - {{ vm.learningWalk.title}} - {{vm.learningWalk.joinKey}}'
                }
            })
            .state("learning-walk-score", {
                url: '/learning-walk-score',
                parent: 'learning-walk',
                views: {
                    'content@learning-walk': {
                        templateUrl: 'app/training/views/learning-walk-score.html',
                        controller: 'learningWalkScoreController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Learning Walk - {{ vm.learningWalk.title}} - {{vm.learningWalk.joinKey}}'
                }
            })
            .state('training-protocol-anchor', {
                url: '/training-protocol-anchor/:id',
                parent: 'training-protocols',
                views: {
                    'content@base': {
                        templateUrl: 'app/training/views/training-protocol-anchor.html',
                        controller: 'trainingProtocolAnchorController as vm'
                    }
                },
                resolve: {
                    anchor: ['trainingService', '$stateParams',
                        function (trainingService, $stateParams) {
                            return trainingService.getTrainingProtocolAnchorById(parseInt($stateParams.id)).then(function(anchor) {
                                return anchor;
                            })
                        }
                    ]
                },
                ncyBreadcrumb: {
                    label: 'Training Video Anchor - {{vm.anchor.trainingProtocol.title}}'
                }
            })
            .state('video-playlist', {
            url: '/video-playlist/',
            parent: 'default-content',
            views: {
                'content@base': {
                    templateUrl: 'app/training/views/video-playlist.html',
                    controller: 'videoPlaylistController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Video Playlist'
            }
        })
            .state('practice-sessions', {
                url: '/practice-sessions',
                parent: 'default-content',
                views: {
                    'content@base': {
                        templateUrl: 'app/training/views/practice-session-list.html',
                        controller: 'practiceSessionListController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Practice Sessions'
                }
            })
            .state('practice-session-base', {
                url: '/practice-session-base/:id',
                abstract: true,
                parent: 'practice-sessions',
                views: {
                    'content@base': {
                        templateUrl: 'app/training/views/practice-session-base.html',
                        controller: 'practiceSessionBaseController as vm'
                    }
                },
                resolve: {
                    session: ['practiceSessionService', '$stateParams',
                        function (practiceSessionService, $stateParams) {
                            return practiceSessionService.getPracticeSessionById(parseInt($stateParams.id)).then(function(session) {
                                return session;
                            })
                        }
                    ]
                }
            })
            .state('practice-session-setup', {
                url: '/practice-session-setup/:id',
                parent: 'practice-session-base',
                views: {
                    'content@practice-session-base': {
                        templateUrl: 'app/training/views/practice-session-setup.html',
                        controller: 'practiceSessionSetupController as vm'
                    }
                },
                data: {
                    selectedTab: 0,
                    title: 'Practice Session Setup'
                },
                ncyBreadcrumb: {
                    label: 'Practice Session - {{ vm.session.title }} (Setup)'
                }
            })
            .state('practice-session-notes', {
                url: '/practice-session-notes/:id',
                parent: 'practice-session-base',
                views: {
                    'content@practice-session-base': {
                        templateUrl: 'app/training/views/practice-session-notes.html',
                        controller: 'practiceSessionNotesController as vm'
                    }
                },
                resolve: {
                    evidenceCollection: ['evidenceCollectionService', 'practiceSessionService', 'enums', '$stateParams', 'activeUserContextService',
                        function (evidenceCollectionService, practiceSessionService, enums, $stateParams, activeUserContextService) {
                            var sessionId = parseInt($stateParams.id);
                            return practiceSessionService.getPracticeSessionObservationForUser(sessionId).then(function(observation) {
                                return evidenceCollectionService.getEvidenceCollection("PRACTICE-SESSION OBS",
                                    enums.EvidenceCollectionType.PRACTICE_SESSION_OBS,
                                    observation.id,
                                    activeUserContextService.user.id)
                                    .then(function (evidenceCollection) {
                                        return evidenceCollection;
                                    })
                            })
                        }
                    ]
                },
                data: {
                    selectedTab: 1,
                    title: 'Practice Session'
                },
                ncyBreadcrumb: {
                    label: 'Practice Session - {{ vm.session.title }} (Notes)'
                }
            })
            .state('practice-session-evidence', {
                url: '/practice-session-evidence/:id',
                parent: 'practice-session-base',
                views: {
                    'content@practice-session-base': {
                        templateUrl: 'app/training/views/practice-session-evidence.html',
                        controller: 'practiceSessionEvidenceController as vm'
                    }
                },
                resolve: {
                    evidenceCollection: ['evidenceCollectionService', 'practiceSessionService', 'enums', '$stateParams','activeUserContextService',
                        function (evidenceCollectionService, practiceSessionService, enums, $stateParams, activeUserContextService) {
                            var sessionId = parseInt($stateParams.id);
                            return practiceSessionService.getPracticeSessionObservationForUser(sessionId).then(function(observation) {
                                return evidenceCollectionService.getEvidenceCollection("PRACTICE-SESSION OBS",
                                    enums.EvidenceCollectionType.PRACTICE_SESSION_OBS,
                                    observation.id,
                                    activeUserContextService.user.id)
                                    .then(function (evidenceCollection) {
                                        return evidenceCollection;
                                    })
                            })
                        }
                    ]
                },
                data: {
                    selectedTab: 2,
                    title: 'Practice Session'
                },
                ncyBreadcrumb: {
                    label: 'Practice Session - {{ vm.session.title }} (Evidence)'
                }
            })
            .state('practice-session-report', {
                url: '/practice-session-report/:id',
                parent: 'practice-session-base',
                views: {
                    'content@practice-session-base': {
                        templateUrl: 'app/training/views/practice-session-report.html',
                        controller: 'practiceSessionReportController as vm'
                    }
                },
                resolve: {
                    evidenceCollection: ['evidenceCollectionService', 'enums', '$stateParams', 'activeUserContextService',
                        function (evidenceCollectionService, enums, $stateParams, activeUserContextService) {
                        return evidenceCollectionService.getEvidenceCollection("PRACTICE-SESSION SUMMATIVE",
                            enums.EvidenceCollectionType.PRACTICE_SESSION_SUMMATIVE, parseInt($stateParams.id),
                            activeUserContextService.user.id)
                            .then(function (evidenceCollection) {
                                    return evidenceCollection;
                                }
                            )
                    }]
                },
                data: {
                    selectedTab: 3,
                    title: 'Practice Session Report'
                },
                ncyBreadcrumb: {
                    label: 'Practice Session - {{ vm.session.title }} (Report)'
                }
            })
            .state('anchor-session', {
                url: '/anchor-session/:id',
                parent: 'practice-session-base',
                views: {
                    'content@base': {
                        templateUrl: 'app/training/views/anchor-session.html',
                        controller: 'anchorSessionController as vm'
                    }
                },
                resolve: {
                    evidenceCollection: ['evidenceCollectionService', 'enums', '$stateParams', 'activeUserContextService',
                        function (evidenceCollectionService, enums, $stateParams, activeUserContextService) {
                        return evidenceCollectionService.getEvidenceCollection("PRACTICE-SESSION",
                                    enums.EvidenceCollectionType.PRACTICE_SESSION,
                                    parseInt($stateParams.id),
                                    activeUserContextService.user.id)
                            .then(function (evidenceCollection) {
                                    return evidenceCollection;
                                }
                            )
                    }]
                },
                data: {
                    title: 'Consensus Scores',
                    displayName: 'Consensus Scores'
                }
            })
            .state('district-anchors', {
                url: '/district-anchors',
                parent: 'training-home',
                views: {
                    'content@base': {
                        templateUrl: 'app/training/views/district-anchors.html',
                        controller: 'districtAnchorsController as vm'
                    }
                },
                data: {
                    title: 'District Consensus Scores',
                    displayName: 'District Consensus Scores'
                }
            })
    }
})();
