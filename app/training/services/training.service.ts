/// <reference path="../training.module.ts" />

(function() {
 'use strict';

    angular
        .module('stateeval.training')
        .factory('trainingService', trainingService);

    trainingService.$inject = ['config', '$http', 'enums', 'activeUserContextService', 'utils'];

    /* @ngInject */
    function trainingService(config, $http, enums, activeUserContextService, utils) {
        var service = {
            getTrainingProtocols : getTrainingProtocols,
            getTrainingProtocolLabelGroups: getTrainingProtocolLabelGroups,
            getTrainingProtocolById: getTrainingProtocolById,
            getGradeLevelsForProtocol:getGradeLevelsForProtocol,
            getSubjectsForProtocol: getSubjectsForProtocol,
            getCriteriaAlignmentForProtocol: getCriteriaAlignmentForProtocol,
            getHighLeverageAlignmentForProtocol: getHighLeverageAlignmentForProtocol,
            getProvidedByForProtocol: getProvidedByForProtocol,
            addToPlaylist: addToPlaylist,
            getPlaylist: getPlaylist,
            createVideoPracticeSession: createVideoPracticeSession,
            getPracticeSessionsForUser: getPracticeSessionsForUser,
            getPracticeSessionParticipants: getPracticeSessionParticipants,
            addPracticeSessionParticipant: addPracticeSessionParticipant,
            getPracticeSessionById: getPracticeSessionById,
            getPracticeSessionForParticipant: getPracticeSessionForParticipant,
            joinPracticeSession: joinPracticeSession,
            createDistrictAnchorSession: createDistrictAnchorSession,
            getAnchorSessionsForDistrict: getAnchorSessionsForDistrict,
            getPublicAnchorSessionsForDistrict: getPublicAnchorSessionsForDistrict,
            getLearningWalkClassroomsForPracticeSession: getLearningWalkClassroomsForPracticeSession,
            newTrainingProtocolAnchor: newTrainingProtocolAnchor,
            saveTrainingProtocolAnchor: saveTrainingProtocolAnchor,
            getTrainingProtocolAnchorById: getTrainingProtocolAnchorById,
            getTrainingProtocolAnchorForDistrict: getTrainingProtocolAnchorForDistrict,
            getDistrictAnchors: getDistrictAnchors,
            newTrainingProtocolAnchorEvidenceSegment: newTrainingProtocolAnchorEvidenceSegment,
            addTrainingProtocolAnchorEvidenceSegment: addTrainingProtocolAnchorEvidenceSegment,
            deleteTrainingProtocolAnchorEvidenceSegment: deleteTrainingProtocolAnchorEvidenceSegment,
            updateTrainingProtocolAnchorEvidenceSegment: updateTrainingProtocolAnchorEvidenceSegment,
            saveEvidenceSegmentAlignment: saveEvidenceSegmentAlignment
        };

        return service;

        ////////////////

        function getEvidenceSegmentAlignmenteData(segment) {
            return {
                id: segment.id,
                alignedRubricRows: segment.alignedRubricRows || [],
            };
        }

        function saveEvidenceSegmentAlignment(segment) {
            var data = getEvidenceSegmentAlignmenteData(segment);
            var url = config.apiUrl + 'trainingprotocolanchorsegment/savealignment';
            return $http.put(url, data);
        }

        function addTrainingProtocolAnchorEvidenceSegment(segment) {
            var url = config.apiUrl + '/trainingprotocolanchorsegments';
            return $http.post(url, segment).then(function(response) {
                segment.id = response.data.id;
            });
        }

        function deleteTrainingProtocolAnchorEvidenceSegment(segment) {
            var url = config.apiUrl + '/trainingprotocolanchorsegments/' + segment.id;
            return $http.delete(url).then(function() {
            });
        }

        function updateTrainingProtocolAnchorEvidenceSegment(segment) {
            var url = config.apiUrl + '/trainingprotocolanchorsegments';
            return $http.put(url, segment).then(function(response) {
            });
        }

        function getTrainingProtocolAnchorById(id) {
            return $http.get(config.apiUrl + 'trainingprotocolanchors/' + id).then(function(response) {
                return response.data;
            })
        }

        function getTrainingProtocolAnchorForDistrict(protocolId) {
            var districtCode = activeUserContextService.context.orientation.districtCode;
            return $http.get(config.apiUrl + 'trainingprotocolanchorfordistrict/' + protocolId + '/' + districtCode).then(function(response) {
                return response.data;
            })
        }

        function getDistrictAnchors() {
            var districtCode = activeUserContextService.context.orientation.districtCode;
            return $http.get(config.apiUrl + 'trainingprotocolanchorsfordistrict/' + districtCode).then(function(response) {
                return response.data;
            })
        }

        function newTrainingProtocolAnchor(protocolId) {
            return {
                creationDateTime: new Date(),
                id: 0,
                evidenceSegments: [],
                districtCode: activeUserContextService.context.orientation.districtCode,
                trainingProtocolId: protocolId
            }
        }


        function newTrainingProtocolAnchorEvidenceSegment(protocolId, anchorId, startTime, endTime, annotation) {
            return {
                id: 0,
                startTime: startTime,
                endTime: endTime,
                annotation: annotation,
                anchorId: anchorId,
                protocolId: protocolId,
                alignedRubricRows: []
            }
        }

        function saveTrainingProtocolAnchor(anchor) {
            var url = config.apiUrl + '/trainingprotocolanchors';
            if (anchor.id != 0) {
                return $http.put(url, anchor);
            } else {
                return $http.post(url, anchor).then(function(response) {
                    anchor.id = response.data.id;
                });
            }
        }

        function getPlaylist() {
            var userId = activeUserContextService.user.id;
            var url = config.apiUrl + '/trainingprotocols/playlist/' + userId;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function addToPlaylist(protocol) {
            var playlistModel = {
                protocolId: protocol.id,
                userId: activeUserContextService.user.id
            }
            var url = config.apiUrl + '/trainingprotocols/addtplaylist';
            return $http.post(url, playlistModel)
                .then(function (response) {
                    return response.data;
                });
        }

        function getProvidedByForProtocol(protocol, labelGroups) {
            var providedBy = "";
            var groupId = _.find(labelGroups, {name: 'ProvidedBy'}).id;
            for (var i=0; i<protocol.labels.length; ++i) {

                if (protocol.labels[i].groupId == groupId) {
                    providedBy = protocol.labels[i].name;
                    break;
                }
            }

            return providedBy;
        }

        function getHighLeverageAlignmentForProtocol(protocol) {
            var alignment = "";
            for (var i=0; i<protocol.alignedHighLeveragePractices.length; ++i) {
                if (i>0) {
                    alignment+= ", ";
                }
                var practice = protocol.alignedHighLeveragePractices[i];
                alignment+= (practice.shortName);
            }
            return alignment;
        }

        function getCriteriaAlignmentForProtocol(protocol) {
            var alignment = "";
            for (var i=0; i<protocol.alignedCriteria.length; ++i) {
                if (i>0) {
                    alignment+= ", ";
                }
                var criteria = protocol.alignedCriteria[i];
                alignment+= (criteria.shortName);
            }

            return alignment;
        }

        function getSubjectsForProtocol(protocol, labelGroups) {
            var subjects = "";
            var groupId = _.find(labelGroups, {name: 'Subject Area'}).id;
            for (var i=0; i<protocol.labels.length; ++i) {

                if (protocol.labels[i].groupId == groupId) {
                    if (subjects.length > 0) {
                        subjects += ", ";
                    }

                    subjects+= protocol.labels[i].name;
                }
            }

            return subjects;
        }

        function getGradeLevelsForProtocol(protocol, labelGroups) {
            var gradeLevel = "";
            var groupId = _.find(labelGroups, {name: 'Grade Level'}).id;
            for (var i=0; i<protocol.labels.length; ++i) {

                if (protocol.labels[i].groupId == groupId) {
                    if (gradeLevel.length > 0) {
                        gradeLevel += ", ";
                    }

                    gradeLevel+= protocol.labels[i].name;
                }
            }
            return gradeLevel;
        }

        function getTrainingProtocolLabelGroups() {
            var url = config.apiUrl + '/trainingprotocollabelgroups';
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function getTrainingProtocols() {
            var url = config.apiUrl + '/trainingprotocols';
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function getTrainingProtocolById(id) {
            var url = config.apiUrl + '/trainingprotocols/' + id;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function getLearningWalkClassroomsForPracticeSession(sessionId) {
            var url = config.apiUrl + 'learningwalkclassrooms/' + sessionId;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function createLearningWalkClassroom(practiceSessionId, name) {
            var classroomModel = {
                practiceSessionId: practiceSessionId,
                name: name
            };

            var url = config.apiUrl + 'learningwalkclassrooms';
            return $http.post(url, classroomModel)
                .then(function (response) {
                    return response.data;
                });
        }

        function newSessionModel(protocol, type) {
            return {
                createdByUserId: activeUserContextService.user.id,
                districtCode: activeUserContextService.context.orientation.districtCode,
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                trainingProtocolId: protocol?protocol.id:null,
                title: protocol?"Practice Session: " + protocol.title:"Learning Walk",
                alignedRubricRows: [],
                sessionType: type,
                randomDigits: utils.getRandomSessionDigits(0, 999),
                isPrivate:true,
                isDistrictAnchor: false
            };

        }
        function createVideoPracticeSession(protocol) {
            var sessionModel = newSessionModel(protocol, enums.PracticeSessionTypeEnum.VIDEO);
            var url = config.apiUrl + 'practicesessions';
            return $http.post(url, sessionModel)
                .then(function (response) {
                    return response.data;
                });
        }

        function createDistrictAnchorSession(protocol) {
            var sessionModel = newSessionModel(protocol, enums.PracticeSessionTypeEnum.VIDEO);
            sessionModel.title = "Consensus Session " + protocol.title;
            sessionModel.isDistrictAnchor = true;
            sessionModel.districtAnchorIsPublic = false;
            var url = config.apiUrl + 'practicesessions';
            return $http.post(url, sessionModel)
                .then(function (response) {
                    return response.data;
                });
        }

        function getPracticeSessionById(id) {
            var url = config.apiUrl + 'practicesessions/' + id;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function joinPracticeSession(sessionKey) {
            var userId = activeUserContextService.user.id;
            var url = config.apiUrl + 'joinpracticesession/' + sessionKey + '/' + userId;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function getPracticeSessionForParticipant(sessionId) {
            var userId = activeUserContextService.user.id;
            var url = config.apiUrl + 'practicesessionforparticipant/' + sessionId + '/' + userId;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function getAnchorSessionsForDistrict() {
            var districtCode = activeUserContextService.context.orientation.districtCode;
            var url = config.apiUrl + 'anchorsesessionsfordistrict/' + districtCode;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function getPublicAnchorSessionsForDistrict() {
            var districtCode = activeUserContextService.context.orientation.districtCode;
            var url = config.apiUrl + 'publicanchorsesessionsfordistrict/' + districtCode;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function getPracticeSessionsForUser() {
            var userId = activeUserContextService.user.id;
            var url = config.apiUrl + 'practicesessionsforuser/' + userId;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function getPracticeSessionParticipants(sessionId) {
            var url = config.apiUrl + 'practicesessionparticipants/' + sessionId;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function addPracticeSessionParticipant(sessionId, participantId) {
            var participantModel = {
                practiceSessionId: sessionId,
                useId: participantId,
                add: true
            }
            var url = config.apiUrl + 'practicesessionparticipants';
            return $http.post(url, participantModel)
                .then(function (response) {
                    return response.data;
                });
        }


        function removePracticeSessionParticipant(sessionId, participantId) {
            var participantModel = {
                practiceSessionId: sessionId,
                useId: participantId,
                add: false
            }
            var url = config.apiUrl + '/practicesessionparticipants';
            return $http.post(url, participantModel)
                .then(function (response) {
                    return response.data;
                });
        }
    }

})();