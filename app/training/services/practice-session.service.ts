/// <reference path="../training.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.training')
    .factory('practiceSessionService', practiceSessionService);

    practiceSessionService.$inject = ['activeUserContextService', 'utils', 'config', '$http', 'enums'];
    function practiceSessionService(activeUserContextService, utils, config, $http, enums) {
        var service = {
            createVideoPracticeSession: createVideoPracticeSession,
            getPracticeSessionsForUser: getPracticeSessionsForUser,
            getPracticeSessionObservationForUser: getPracticeSessionObservationForUser,
            getPracticeSessionById: getPracticeSessionById,
            saveObserveNotes: saveObserveNotes,
            updatePracticeSessionObservation: updatePracticeSessionObservation,
            joinPracticeSession: joinPracticeSession
        };
        return service;


        function saveObserveNotes(observation, notes) {
            observation.observeNotes = notes;
            return updatePracticeSessionObservation(observation);
        }

        function joinPracticeSession(sessionId) {
            var userId = activeUserContextService.user.id;
            var url = config.apiUrl + '/joinpracticesession/' + sessionId + '/' + userId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function updatePracticeSessionObservation(observation) {
            var url = config.apiUrl + '/practicesessionobservations';
            return $http.put(url, observation).then(function (response) {
            });
        }
        function getPracticeSessionById(sessionId) {
            var url = config.apiUrl + 'practicesessions/' + sessionId;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function getPracticeSessionObservationForUser(sessionId) {
            var url = config.apiUrl + 'practicesessionobservations/' + sessionId + '/' + activeUserContextService.user.id;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function getPracticeSessionsForUser() {
            var url = config.apiUrl + 'practicesessionsforuser/' + activeUserContextService.user.id;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }

        function createVideoPracticeSession(protocol) {
            var practiceSessionModel = newPracticeSessionModel(enums.PracticeSessionTypeEnum.VIDEO);
            practiceSessionModel.trainingProtocolId = protocol.id;
            var url = config.apiUrl + 'practicesessions';
            return $http.post(url, practiceSessionModel)
                .then(function (response) {

                    return response.data;
                });
        }

        function newPracticeSessionModel(type) {
            return {
                createdByUserId: activeUserContextService.user.id,
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                title: 'New Practice Session',
                randomDigits: utils.getRandomSessionDigits(0, 999),
                sessionType: type,
                alignedRubricRows: [],
                participants: [activeUserContextService.user]
            };

        }
    }

}) ();