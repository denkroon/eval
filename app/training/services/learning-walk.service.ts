/// <reference path="../training.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.training')
    .factory('learningWalkService', learningWalkService);

    learningWalkService.$inject = ['activeUserContextService', 'utils', 'config', '$http'];
    function learningWalkService(activeUserContextService, utils, config, $http) {
        var service = {
            createLearningWalk: createLearningWalk,
            getLearningWalksForCurrentUser: getLearningWalksForCurrentUser,
            getLearningWalkObservationForCurrentUser: getLearningWalkObservationForCurrentUser,
            getLearningWalkById: getLearningWalkById,
            joinSession: joinSession,
            saveLearningWalkClassrooms:saveLearningWalkClassrooms,
            newClassroom: newClassroom,
            deleteClassroom: deleteClassroom
        };

        return service;

        function newClassroom(learningWalk, name, tags) {
            return {
                id: 0,
                name: name,
                tags: tags,
                learningWalkId: learningWalk.id,
                labels: []
            }
        }

        function deleteClassroom(classroom) {
            return $http.delete(config.apiUrl + 'learningwalkclassrooms/' + classroom.id);
        }

        function getLearningWalkClassroomsData(learningWalk) {
            return {
                id: learningWalk.id,
                classrooms: learningWalk.classrooms
            };
        }

        function saveLearningWalkClassrooms(learningWalk) {
            var url = config.apiUrl + '/savelearningwalkclassrooms';
            var data = getLearningWalkClassroomsData(learningWalk);
            return $http.put(url, data);
        }

        function joinSession(sessionKey) {
            var userId = activeUserContextService.user.id;
            var url = config.apiUrl + '/joinlearningwalk/' + sessionKey + '/' + userId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getLearningWalkById(id) {
            var url = config.apiUrl + 'learningwalks/' + id;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }


        function getLearningWalkObservationForCurrentUser(learningWalkId, classroomId) {
            var url = config.apiUrl + 'learningwalkobservation/' + learningWalkId + '/' + classroomId + '/' + activeUserContextService.user.id;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getLearningWalksForCurrentUser() {
            var url = config.apiUrl + 'learningwalksforuser/' + activeUserContextService.user.id;
            return $http.get(url).then(function (response) {
                    return response.data;
                });
        }

        function createLearningWalk() {
            var learningWalkModel = newLearningWalkModel();
            var url = config.apiUrl + 'learningwalks';
            return $http.post(url, learningWalkModel)
                .then(function (response) {
                    learningWalkModel.id = response.data.id;
                    return learningWalkModel;
                });
        }

        function newLearningWalkModel() {
            return {
                createdByUserId: activeUserContextService.user.id,
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                title: 'New Learning Walk',
                randomDigits: utils.getRandomSessionDigits(0, 999),
                participants: []
            };

        }
    }

}) ();