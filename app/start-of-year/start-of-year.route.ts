/// <reference path="start-of-year.module.ts" />

/**
 * Created by anne on 6/19/2015.
 */
(function () {
    'use strict';
    angular.module('stateeval.start-of-year')
        .config(configureRoutes);

    configureRoutes.$inject = ['$stateProvider'];

    function configureRoutes($stateProvider) {

        $stateProvider

            .state('noor-da-start-of-year-setup', {
                parent: 'noor-base',
                url: '/noor-da-start-of-year-setup/{evaluationtype}',
                views: {
                    'content@noor-base': {
                        controller: 'daStartOfYearSetupController as vm',
                        templateUrl: 'app/start-of-year/district-admin/da-start-of-year-setup.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Start of Year Setup'
                }
            })
            .state('da-start-of-year-setup', {
                parent: 'default-content',
                url: '/da-start-of-year-setup/{evaluationtype}',
                views: {
                    'content@base': {
                        controller: 'daStartOfYearSetupController as vm',
                        templateUrl: 'app/start-of-year/district-admin/da-start-of-year-setup.html'
                    }
                },
                resolve: {
                    user: ['activeUserContextService',
                        function (activeUserContextService) {
                            return activeUserContextService.user;
                        }]
                },
                ncyBreadcrumb: {
                    label: 'Start of Year Setup'
                }
            })
            .state('select-framework', {
                url: '/select-framework/{evaluationtype}/{districtCode}',
                parent: 'noor-base',
                views: {
                    'content@noor-base': {
                        controller: 'selectFrameworkController as vm',
                        templateUrl: 'app/start-of-year/district-admin/select-framework.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Select Framework for {{vm.evaluateeTerm}} Evaluations'
                }
            })
            .state('sa-start-of-year-setup', {
                url: '/sa-start-of-year-setup',
                parent: 'default-content',
                views: {
                    'content@base': {
                        controller: 'saStartOfYearSetupController as vm',
                        templateUrl: 'app/start-of-year/school-admin/sa-start-of-year-setup.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'District Annual Setup'
                }
            })
            .state('noor-pr-start-of-year-setup', {
                parent: 'noor-base',
                url: '/noor-pr-start-of-year-setup',
                views: {
                    'content@noor-base': {
                        controller: 'prStartOfYearSetupController as vm',
                        templateUrl: 'app/start-of-year/principal/pr-start-of-year-setup.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Start of Year Setup'
                }
            })
            .state('pr-start-of-year-setup', {
                parent: 'default-content',
                url: '/pr-start-of-year-setup',
                views: {
                    'content@base': {
                        controller: 'prStartOfYearSetupController as vm',
                        templateUrl: 'app/start-of-year/principal/pr-start-of-year-setup.html'
                    }
                },
                resolve: {
                    user: ['activeUserContextService',
                        function (activeUserContextService) {
                            return activeUserContextService.user;
                        }]
                },
                ncyBreadcrumb: {
                    label: 'Start of Year Setup'
                }
            })

    }
}) ();

