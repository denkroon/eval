/**
 * Created by anne on 4/25/2016.
 */
(function() {
   'use strict';

    function prStartOfYearChecklistDirective() {
        return {
            scope: {
                frameworkContext: '=',
                assignmentDelegated: '='
            },
            templateUrl: 'app/start-of-year/principal/pr-start-of-year-checklist.directive.html',
            controller: 'prStartOfYearChecklistController as vm',
            bindToController: true
        }
    }

    class prStartOfYearChecklistController {

        currentStep = 1;
        districtCode: string;
        step1Title: string;
        step2Title: string;
        step3Title:string;
        isActive: boolean;
        activationDate: any;

        constructor(
            private $state: any,
            private $scope: any,
            private enums: any,
        )
        {
            this.enums = enums;
            this.isActive = false;

            this.step1Title = 'Waiting for district to select Evaluation Framework';
            this.step2Title = 'View Evaluators Assignments for teachers';
            this.step3Title = 'Setup a default set of observation prompts (optional)';

            $scope.$watch('vm.frameworkContext', (context: any) => {
                if (context && context.isActive) {
                    this.currentStep = 2;
                    this.step1Title = context.name + ' loaded';
                    this.isActive = context.isActive;
                    this.activationDate = context.loadDateTime;
                }
            });
            $scope.$watch('vm.assignmentDelegated', (delegated: any) => {
                    if (delegated) {
                        this.step2Title = 'Assign Evaluators for teachers';
                    }
            });
        }

        stepClass(step: number) {
            if (step < this.currentStep) {
                return 'step-complete';
            }
            if (step == this.currentStep) {
                return 'step-active';
            }
            return 'step-inactive';
        }

        gotoAssignments() {
            this.$state.go('assignments-detail', {evaluationtype: this.enums.EvaluationType.TEACHER})
        }

        gotoPromptBank() {
            this.$state.go('prompt-bank', {evaluationtype: this.enums.EvaluationType.TEACHER})
        }
    }

    angular.module('stateeval.start-of-year')
    .directive('prStartOfYearChecklist', prStartOfYearChecklistDirective)
    .controller('prStartOfYearChecklistController', prStartOfYearChecklistController);

})();
