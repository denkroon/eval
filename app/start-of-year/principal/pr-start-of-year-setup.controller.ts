/**
 * Created by anne on 4/24/2016.
 */
(function() {

    'use strict';

    class prStartOfYearSetupController {

        schoolYearDisplayString: string;
        loadedContexts: [any];
        user: any;
        welcomeMessage: string;
        districtCode: string;
        districtName: string;
        schoolName: string;
        enums: any;
        schoolYear: number;
        orientation: any;
        frameworkContext: any;
        assignmentDelegated: boolean;
        frameworkIsActive: boolean;
        isAPrincipal: boolean;
        isASchoolAdmin: boolean;
        roleDisplayString: string;

        constructor(
            private frameworkService: any,
            private _: _.LoDashStatic,
            public enums: any,
            private user: any,
            private config: any,
            private assignmentsService: any,
            private activeUserContextService: any,
            private startupService: any,
            private utils: any
    )
        {
            this.enums = enums;
            this.user = user;
            this.frameworkIsActive = false;

            var location = this.getPrincipalLocation();

            this.isASchoolAdmin = this.userIsInRoleAtLocation(this.user.locationRoles, this.enums.Roles.SESchoolAdmin);
            this.isAPrincipal = this.userIsInRoleAtLocation(this.user.locationRoles, this.enums.Roles.SESchoolPrincipal);

            this.districtCode = location.districtCode;
            this.districtName = location.districtName;
            this.schoolName = location.schoolName;

            this.welcomeMessage = 'Welcome ' + user.displayName;
            this.schoolYearDisplayString = (config.schoolYear-1) + '-' + config.schoolYear;

            this.welcomeMessage = 'Welcome ' + this.user.displayName;
            frameworkService.getFrameworkContextWithoutActiveContext(config.schoolYear, this.districtCode,
                this.enums.EvaluationType.TEACHER).then((frameworkContext)=> {

                this.frameworkContext = frameworkContext;
                if (this.isLoaded()) {

                    this.frameworkIsActive = true;
                    return assignmentsService.districtDelegatedPrincipalAssignmentsForSchool(
                        activeUserContextService.context.orientation.schoolCode)
                        .then((delegate) => {
                            this.assignmentDelegated = delegate;
                        });
                }
            });
        }

        userIsInRoleAtLocation(locationRoles, role) {
            for (var i=0; i<locationRoles.length; ++i) {
                if (locationRoles[i].roleName === role) {
                    return locationRoles[i];
                }
            }
            return null;
        }

        getPrincipalLocation() {
            for (var i=0; i<this.user.locationRoles.length; ++i) {
                if (this.user.locationRoles[i].roleName === this.enums.Roles.SESchoolPrincipal) {
                    return this.user.locationRoles[i];
                }
            }
            return null;
        }

        isLoaded() {
            return this.frameworkContext != null && this.frameworkContext.isActive;
        }

        loadDate() {
            if (!this.frameworkContext || !this.frameworkContext.isActive) {
                return '';
            }
            else {
                return this.frameworkContext.loadDateTime;
            }
        }

        selectedFrameworkDisplayName() {
            if (!this.frameworkContext || !this.frameworkContext.isActive) {
                return '';
            }
            else {
                return this.frameworkContext.name;
            }
        }
    }

    angular.module('stateeval.start-of-year')
        .controller('prStartOfYearSetupController', prStartOfYearSetupController);

})();