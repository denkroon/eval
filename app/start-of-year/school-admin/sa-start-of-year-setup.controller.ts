/**
 * Created by anne on 4/21/2016.
 */
(function() {
   'use strict';

    class saStartOfYearSetupController {

        user: any;
        title: string;
        daLocations: [any];
        multipleSchoolYears: boolean = false;
        context: any;

        constructor(
            private activeUserContextService: any
        ) {

            this.user = activeUserContextService.user;
            this.title = "Welcome " + this.user.displayName + "!";
            this.daLocations = this.getDistrictAdminLocations();
            this.multipleSchoolYears = this.userHasMultipleSchoolYears();
            this.context = activeUserContextService.context;

        }

        getDistrictAdminLocations() {
            //todo;
        }

        userHasMultipleSchoolYears() {
            // false for 2016-2017 school year
            return false;
        }
    }

    angular.module('stateeval.start-of-year')
    .controller('saStartOfYearSetupController', saStartOfYearSetupController);

})();