/// <reference path="../start-of-year.module.ts" />

/**
 * selectFrameworkController - controller
 */
(function () {
    'use strict';

    class selectFrameworkController {

        title: string;
        prototypeFrameworks: any;
        evalType: any;
        frameworkTypeDisplayString: string;
        schoolYearDisplayString: string;
        selectedFrameworkContext: any;
        districtCode: string;

        constructor (
            private frameworkService: any,
            private $stateParams: any,
            private _: _.LoDashStatic,
            private $state: any,
            private config: any,
            private startupService: any,
            private user: any,
            private authenticationService: any,
            private $uibModal: any
        )
        {
            this.schoolYearDisplayString = (config.schoolYear-1) + '-' + config.schoolYear;
            this.evalType = parseInt($stateParams.evaluationtype);
            this.districtCode = $stateParams.districtCode;
            this.frameworkTypeDisplayString = frameworkService.mapEvaluationTypeToDisplayName(this.evalType);
            this.title = this.frameworkTypeDisplayString + ' Framework Setup';

            frameworkService.getPrototypeFrameworkContexts(config.schoolYear).then((prototypeFrameworks) => {
                this.prototypeFrameworks = _.filter(prototypeFrameworks, {evaluationType: this.evalType});
            });
        }

        backToSelectFrameworks() {
            return this.authenticationService.login(this.user.userName, this.user.password);
        }

        cancel() {
           this.backToSelectFrameworks();
        }

        loadFramework() {
            var modalInstance = this.$uibModal.open({
                animation: false,
                templateUrl: 'app/start-of-year/district-admin/_confirm-load-framework.html',
                controller: 'confirmLoadFrameworkModalController as vm',
                resolve: {
                    frameworkContext: this.selectedFrameworkContext
                }
            });

            modalInstance.result.then(() => {
                this.frameworkService.loadFrameworkContext(this.selectedFrameworkContext.id, this.districtCode).then(()=>{
                    this.backToSelectFrameworks();
                });
            });


        }
    }
    angular
        .module('stateeval.district-admin')
        .controller('selectFrameworkController', selectFrameworkController);

})();