/**
 * Created by anne on 4/24/2016.
 */
(function() {

    'use strict';

    class daStartOfYearSetupController {

        evalTypes: [any];
        schoolYearDisplayString: string;
        loadedContexts: [any];
        user: any;
        welcomeMessage: string;
        districtCode: string;
        districtName: string;
        enums: any;

        constructor(
            private frameworkService: any,
            private _: _.LoDashStatic,
            public enums: any,
            private $state: any,
            private config: any,
            private user: any
        )
        {
            this.enums = enums;
            this.user = user;

            this.districtCode = this.user.locationRoles[0].districtCode;
            this.districtName = this.user.locationRoles[0].districtName;

            this.welcomeMessage = 'Welcome ' + user.displayName;
            this.schoolYearDisplayString = (config.schoolYear-1) + '-' + config.schoolYear;
            frameworkService.getPrototypeFrameworkContexts(config.schoolYear).then((prototypeFrameworks) => {
                this.evalTypes = _.unique(_.pluck(prototypeFrameworks, 'evaluationType'));
                this.evalTypes = _.reject(this.evalTypes, (evalType)=> { return evalType === enums.EvaluationType.LIBRARIAN});
                frameworkService.getLoadedFrameworkContexts(this.config.schoolYear, this.districtCode).then((frameworkContexts)=> {
                    this.loadedContexts = frameworkContexts;
                });
            });
        }

        loadFramework(evalType) {
            this.$state.go('select-framework', {evaluationtype: evalType})
        }

        isLoaded(evalType) {
            return _.find(this.loadedContexts, {evaluationType: evalType});
        }

        loadDate(evalType) {
            var frameworkContext = _.find(this.loadedContexts, {evaluationType: evalType});
            if (frameworkContext) {
                return frameworkContext.loadDateTime;
            }
            else {
                return "";
            }
        }

        isActive(evalType) {
            var frameworkContext = _.find(this.loadedContexts, {evaluationType: evalType});
            if (frameworkContext) {
                return frameworkContext.isActive;
            }
            else {
                return "";
            }
        }

        selectedFrameworkDisplayName(evalType) {
            var frameworkContext = _.find(this.loadedContexts, {evaluationType: evalType});
            if (frameworkContext) {
                return frameworkContext.name;
            }
            else {
                return 'Not setup yet';
            }
        }

        mapEvaluationTypeToDisplayName(evalType) {
           return this.frameworkService.mapEvaluationTypeToDisplayName(evalType);
        }
    }

    angular.module('stateeval.start-of-year')
    .controller('daStartOfYearSetupController', daStartOfYearSetupController);

})();