/**
 * Created by anne on 4/25/2016.
 */
(function() {
   'use strict';

    function daStartOfYearChecklistDirective() {
        return {
            scope: {
                loadedContexts: '=',
                evalType: '=',
                districtCode: '='
            },
            templateUrl: 'app/start-of-year/district-admin/da-start-of-year-checklist.directive.html',
            controller: 'daStartOfYearChecklistController as vm',
            bindToController: true
        }
    }

    class daStartOfYearChecklistController {

        currentStep = 1;
        loadedContexts: any;
        evalType: number;
        districtCode: string;
        evaluateeTerm: string;
        step1Title: string;
        step2Title: string;
        step3Title:string;
        step4Title: string;
        activateBtnTitle: string;
        isActive: boolean;
        activationDate: any;

        constructor(
            private $state: any,
            private $scope: any,
            private utils: any,
            private config: any,
            private frameworkService: any
        )
        {
            this.isActive = false;
            this.evaluateeTerm = utils.getEvaluateeTermUpperCase(this.evalType);

            this.step1Title = 'Select this year\'s framework';

            this.step2Title = 'Assign Evaluators for ' + this.evaluateeTerm + 's';
            this.step3Title = 'Configure default settings for your district (optional)';
            this.step4Title = 'Setup a default set of observation prompts (optional)';

            this.activateBtnTitle = 'Activate ' + this.evaluateeTerm +
                ' Evaluations for ' + (config.schoolYear-1) + '-' + config.schoolYear;

            $scope.$watch('vm.loadedContexts', (loadedContexts: any) => {
                if (_.find(loadedContexts, {evaluationType: this.evalType})) {
                    this.currentStep = 2;
                    var context = _.find(this.loadedContexts, {evaluationType: this.evalType});
                    this.step1Title = context.name + ' loaded';
                    this.isActive = context.isActive;
                    this.activationDate = context.loadDateTime;
                }
            });
        }

        stepClass(step: number) {
            if (step < this.currentStep) {
                return 'step-complete';
            }
            if (step == this.currentStep) {
                return 'step-active';
            }
            return 'step-inactive';
        }

        activate() {
            var frameworkContext = _.find(this.loadedContexts, {evaluationType: this.evalType});

            frameworkContext.isActive = true;
            this.frameworkService.updateFrameworkContext(frameworkContext).then(() => {
                this.isActive = true;
                this.activationDate = new Date();
            });
        }

        gotoAssignments() {
            this.$state.go('assignments-summary', {evaluationtype: this.evalType})
        }

        gotoSettings() {
            this.$state.go('da-settings', {evaluationtype: this.evalType})
        }

        gotoPromptBank() {
            this.$state.go('prompt-bank', {evaluationtype: this.evalType})
        }

        selectFramework() {
            this.$state.go('select-framework', {evaluationtype: this.evalType, districtCode: this.districtCode})
        }

        isLoaded(evalType) {
            return _.find(this.loadedContexts, {evaluationType: evalType});
        }
    }

    angular.module('stateeval.start-of-year')
    .directive('daStartOfYearChecklist', daStartOfYearChecklistDirective)
    .controller('daStartOfYearChecklistController', daStartOfYearChecklistController);

})();
