/// <reference path="authorization.module.ts" />

namespace StateEval.Authorization {
    angular
        .module('stateeval.authorization')
        .config((
            $stateProvider: ng.ui.IStateProvider,
            $httpProvider: ng.IHttpProvider
        ) => {
            $httpProvider.interceptors.push('authorizationInterceptor');

            $stateProvider
                .state('authorize', {
                    url: '/authorize?userName&password&data',
                    templateUrl: 'app/authorization/views/authorization.html',
                    controller: 'authorizeController as vm'
                });
        });
}