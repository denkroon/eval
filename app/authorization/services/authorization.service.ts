namespace StateEval.Authorization {

    const DataKey = 'Authorization:AuthData';

    export interface IAuthData {
        '.expires': string,
        '.issued': string,
        access_token: string,
        expires_in: number,
        token_type: string,
        userName: string
    }

    export class AuthorizationInterceptor implements ng.IHttpInterceptor {

        private authData: IAuthData;

        constructor(private config: any) {
            var data = sessionStorage.getItem(DataKey);
            if (data && data.length) {
                this.authData = JSON.parse(data);
            }
        }

        setToken(authData: IAuthData) {
            this.authData = authData;
            sessionStorage.setItem(DataKey, JSON.stringify(authData));
        }

        request = (config: ng.IRequestConfig) => {
            if (this.authData && config && config.url && config.url.indexOf(this.config.apiUrl) == 0) {
                config.headers['Authorization'] = `Bearer ${this.authData.access_token}`;
            }
            return config;
        }
    }

    angular
        .module('stateeval.authorization')
        .service('authorizationInterceptor', AuthorizationInterceptor)
}