/// <reference path="../authorization.module.ts" />

namespace StateEval.Authorization {

    export class AuthorizeController {

        state = 'loading';

        constructor(
            private authorizationInterceptor: AuthorizationInterceptor,
            $stateParams: ng.ui.IStateParamsService,
            $http: ng.IHttpService,
            config: any
        ) {
            $http.post<IAuthData>(
                // the auth end point is actually not under /api but just under /token
                config.apiUrl.replace(/api\/$/, 'token'),
                // using jquery to form encode the payload because angular defaults to json
                $.param({
                    'grant_type': 'password',
                    userName: $stateParams['userName'],
                    password: $stateParams['password'],
                    data: $stateParams['data'],
                    'client_id': 'ngSEAuthApp'
                }
            ), {
                // this is a custom setting to avoid showing a growler when an error occurs
                skipInterceptors: true,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(
                (result) => {
                    this.authorizationInterceptor.setToken(result.data);
                },
                (error) => {
                    if (error.status == 400 && error.data.error == 'invalid_grant') {
                        this.state = 'expired'
                    }
                    else {
                        this.state = 'error'
                    }
                }
            )
        }
    }

    angular
        .module('stateeval.authorization')
        .controller('authorizeController', AuthorizeController);
}