/// <reference path="self-assessment.module.ts" />

(function() {

 'use strict';
    angular.module('stateeval.self-assessment')
        .config(configureRoutes);

    configureRoutes.$inject = ['$stateProvider'];

    function configureRoutes($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('self-assessment-home', {
                url: '/self-assessment-home',
                parent: 'default-content',
                abstract: true
            })
            .state('self-assessment-list', {
                url: '/self-assessment-list',
                parent: 'self-assessment-home',
                views: {
                    'content@base': {
                        templateUrl: 'app/self-assessment/views/self-assessment-list.html',
                        controller: 'selfAssessmentListController as vm'
                    }
                },
                data: {
                    evaluateeRequired: true,
                    title: 'Self Assessments',
                },
                ncyBreadcrumb: {
                    label: 'Self Assessments'
                }
            })
            .state('self-assessment', {
                url: '/self-assessment/:id',
                parent: 'self-assessment-list',
                views: {
                    'content@base': {
                        templateUrl: 'app/self-assessment/details/self-assessment.html',
                        controller: 'selfAssessmentController as vm'
                    }
                },
                resolve: {
                    evidenceCollection: ['evidenceCollectionService', 'enums', 'activeUserContextService', '$stateParams',
                        function (evidenceCollectionService, enums, activeUserContextService, $stateParams) {
                        return evidenceCollectionService.getEvidenceCollection("SELF-ASSESSMENT",
                                enums.EvidenceCollectionType.SELF_ASSESSMENT,
                                parseInt($stateParams.id),
                                activeUserContextService.user.id)
                            .then(function (evidenceCollection) {
                                return evidenceCollection;
                            }
                        )
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Self Assessment - {{vm.selfAssessment.title}}'
                }
            })
            .state("self-assessment-setup", {
                url: '/self-assessment-setup',
                parent: 'self-assessment',
                views: {
                    'content@self-assessment': {
                        templateUrl: 'app/self-assessment/details/setup/setup.html',
                        controller: 'selfAssessmentSetupController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Self-Assessment - {{vm.selfAssessment.title}}'
                }
            })
            .state("self-assessment-align", {
                url: '/self-assessment-align',
                parent: 'self-assessment',
                views: {
                    'content@self-assessment': {
                        templateUrl: 'app/self-assessment/details/align/align.html',
                        controller: 'selfAssessmentAlignController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Self-Assessment - {{vm.selfAssessment.title}}'
                },
                params: {
                    nodeShortName: null,
                    rowId: null
                }
            })
            .state("self-assessment-artifacts", {
                url: '/self-assessment-artifacts',
                parent: 'self-assessment',
                views: {
                    'content@self-assessment': {
                        templateUrl: 'app/self-assessment/details/artifacts/self-assessment-artifacts.html',
                        controller: 'selfAssessmentArtifactsController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Self-Assessment - {{vm.selfAssessment.title}}'
                }
            })
            .state('self-assessment-artifact-view', {
                url: '/self-assessment-artifact-view/{artifactId}',
                parent: 'self-assessment-artifacts',
                views: {
                    'content@self-assessment': {
                        templateUrl: 'app/self-assessment/details/artifacts/self-assessment-artifact-view.html',
                        controller: 'selfAssessmentArtifactViewController as vm'
                    }
                },
                resolve: {
                    artifact: ['artifactService', '$stateParams', function (artifactService, $stateParams) {
                        return artifactService.getArtifactById($stateParams.artifactId)
                            .then(function (artifact) {
                                return artifact;
                            });
                    }]
                },
                ncyBreadcrumb: {
                    label: 'View Artifact - {{vm.artifact.title}}'
                }
            })
    }

})();

