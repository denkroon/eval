/// <reference path="../self-assessment.module.ts" />

/**
 * Created by anne on 12/5/2015.
 */
angular
    .module('stateeval.self-assessment')
    .factory('selfAssessmentService', selfAssessmentService);

selfAssessmentService.$inject = ['activeUserContextService', 'config', '$http', 'enums'];

/* @ngInject */
function selfAssessmentService(activeUserContextService, config, $http, enums) {
    var service = {
        getSelfAssessmentsForEvaluation: getSelfAssessmentsForEvaluation,
        newSelfAssessmentRequest: newSelfAssessmentRequest,
        newSelfAssessment: newSelfAssessment,
        saveSelfAssessment: saveSelfAssessment,
        deleteSelfAssessment: deleteSelfAssessment,
        shareSelfAssessment: shareSelfAssessment
    };

    return service;

    ////////////////


    function newSelfAssessmentRequest() {
        var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
        var currentUserId = activeUserContextService.user.id;
        return {
            evaluationId: evaluationId,
            currentUserId: currentUserId,
            evaluatorId: 0,
            objectId: 0
        }
    }
    function shareSelfAssessment(id) {
        var request = newSelfAssessmentRequest();
        request.evaluatorId = activeUserContextService.context.evaluatee.evalData.evaluatorId;
        request.objectId = id;
        var url = config.apiUrl + 'shareselfassessment';
        return $http.put(url, request);
    }

    function deleteSelfAssessment(selfAssessment) {
        return $http.delete(config.apiUrl + 'selfassessments/' + selfAssessment.id);
    }

    function newSelfAssessment() {
        var evalData = activeUserContextService.context.evaluatee.evalData;
        var focused = evalData.planType === enums.EvaluationPlanType.FOCUSED;
        return {
            creationDateTime: new Date(),
            evaluationId: activeUserContextService.context.evaluatee.evalData.id,
            evaluationType: evalData.evaluationType,
            evaluateeId: activeUserContextService.user.id,
            schoolCode: activeUserContextService.context.orientation.schoolCode,
            id: 0,
            title: '',
            focused: focused,
            focusFrameworkNodeId: focused? evalData.focusFrameworkNodeId:null,
            focusSGFrameworkNodeId: focused? evalData.focusSGFrameworkNodeId:null,
            performanceLevel: null,
            includeInFinalReport: false,
            isShared: false
        }
    }

    function saveSelfAssessment(selfAssessment) {
         var url = config.apiUrl + '/selfassessments';
        if (selfAssessment.id != 0) {
            return $http.put(url, selfAssessment);
        } else {
            return $http.post(url, selfAssessment).then(function(response) {
                selfAssessment.id = response.data.id;
            });
        }
    }

    function getSelfAssessmentsForEvaluation() {
        var request = newSelfAssessmentRequest();
        var url = config.apiUrl + '/selfassessments';
        return $http.get(url, {params: request})
            .then(function (response) {
                return response.data;
            });
    }

}

