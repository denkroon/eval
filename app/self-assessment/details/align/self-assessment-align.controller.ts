/// <reference path="../../self-assessment.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.self-assessment')
        .controller('selfAssessmentAlignController', selfAssessmentAlignController);

    selfAssessmentAlignController.$inject = ['evidenceCollection', 'activeUserContextService', 'evidenceCollectionService'];

    /* @ngInject */
    function selfAssessmentAlignController(evidenceCollection, activeUserContextService, evidenceCollectionService) {
        var vm = this;
        vm.evidenceCollection = evidenceCollection;
    }
})();