/// <reference path="../self-assessment.module.ts" />

namespace StateEval.SelfAssessment {

    export class SelfAssessmentController {

        tabs: any[];
        isEvaluatee: boolean;
        selfAssessment: any;
        sharing: boolean;

        constructor(
            private activeUserContextService: any,
            private selfAssessmentService: any,
            private evidenceCollectionService: any,
            private $state: any,
            evidenceCollection: any,
            utils: any,
            $scope: ng.IScope
        ) {
            this.selfAssessment = evidenceCollection.selfAssessment;
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();

            this.tabs = [
                new utils.Link('Setup', 'self-assessment-setup'),
                new utils.Link('Align & Score', 'self-assessment-align'),
                new utils.Link('Artifacts', 'self-assessment-artifacts'),
                // commented out for march 31st training session:
                // new utils.Link('Report', 'self-assessment-report')
            ];

            $scope.$watch('vm.selfAssessment.isShared', (val: boolean) => {
                this.sharing = val;
            })
        }

        shareSelfAssessment() {
            this.selfAssessmentService.shareSelfAssessment(this.selfAssessment.id).then(()=> {
                this.$state.go(this.$state.current, [], {reload: true});
            })
        }
    }

    angular
        .module('stateeval.self-assessment')
        .controller('selfAssessmentController', SelfAssessmentController);
}