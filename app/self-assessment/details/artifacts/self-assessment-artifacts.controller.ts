/// <reference path="../../self-assessment.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.self-assessment')
        .controller('selfAssessmentArtifactsController', selfAssessmentArtifactsController);

    selfAssessmentArtifactsController.$inject = ['enums', 'activeUserContextService', 'evidenceCollection', '$state'];

    function selfAssessmentArtifactsController(enums, activeUserContextService, evidenceCollection, $state) {
        var vm = this;
        vm.selfAssessment = evidenceCollection.selfAssessment;
        vm.enums = enums;
        vm.context = activeUserContextService.context;

        vm.viewArtifact = viewArtifact;

        ///////////////////////////////

        activate();

        function activate() {
        }

        function viewArtifact(artifact) {
            $state.go('self-assessment-artifact-view', {artifactId: artifact.id});
        }
    }

})();