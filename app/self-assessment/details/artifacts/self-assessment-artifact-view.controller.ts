(function () {
    'use strict';

    angular.module('stateeval.self-assessment')
        .controller('selfAssessmentArtifactViewController', selfAssessmentArtifactViewController);

    selfAssessmentArtifactViewController.$inject = ['artifact'];
    function selfAssessmentArtifactViewController(artifact) {
        var vm = this;
        vm.artifact = artifact;
        vm.back = {state: 'self-assessment-artifacts'};

    }
})();