/// <reference path="../../self-assessment.module.ts" />

namespace StateEval.SelfAssessment {

    export class SelfAssessmentSetupController {

        isEvaluatee: boolean;
        isSelfAssessmentReadOnly: boolean;
        selfAssessment: any;

        title: string;

        constructor(
            private $state: ng.ui.IStateService,
            private selfAssessmentService: any,
            private _: _.LoDashStatic,
            private evidenceCollection: any,
            activeUserContextService: any,
            $scope: ng.IScope
        ) {
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();
            this.selfAssessment = evidenceCollection.selfAssessment;
            this.isSelfAssessmentReadOnly = activeUserContextService.context.selfAssessmentSetupIsReadOnly(this.selfAssessment);

            $scope.$watch('vm.selfAssessment.title', (val: string) => {
                this.title = val;
            });
        }

        titleFocused() {
            this.title = '';
        }

        titleBlurred() {
            if (!this.title.length) {
                this.title = this.selfAssessment.title;
            }
        }

        titleChanged() {
            this.selfAssessment.title = this.title;
            this.saveSelfAssessment();
        }

        saveSelfAssessment() {
            return this.selfAssessmentService.saveSelfAssessment(this.selfAssessment);
        }

        deleteSelfAssessment() {
            this.selfAssessmentService.deleteSelfAssessment(this.selfAssessment)
                .then(() => { this.$state.go("self-assessment-list"); });
        }
    }

    angular
        .module('stateeval.self-assessment')
        .controller('selfAssessmentSetupController', SelfAssessmentSetupController);

}