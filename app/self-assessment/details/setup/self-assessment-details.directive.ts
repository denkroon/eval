/// <reference path="../../self-assessment.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.self-assessment')
        .directive('selfAssessmentDetails', selfAssessmentDetailsDirective)
        .controller('selfAssessmentDetailsController', selfAssessmentDetailsController);

    selfAssessmentDetailsController.$inject = ['activeUserContextService', 'utils', 'enums', '$scope'];

    function selfAssessmentDetailsDirective() {
        return {
            scope: {
                selfAssessment: '='
            },
            templateUrl: 'app/self-assessment/details/setup/self-assessment-details.directive.html',
            controller: 'selfAssessmentDetailsController as vm',
            bindToController: true
        }
    }

    function selfAssessmentDetailsController(activeUserContextService, utils, enums, $scope) {
        var vm = this;
        vm.enums = enums;

        $scope.$watch('vm.selfAssessment.focused', function(newVal, oldVal) {
            setSelfAssessmentFocusDisplayName();
        });

        activate();

        function activate() {
            vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
            vm.evaluatee = activeUserContextService.context.evaluatee;
            vm.planType = activeUserContextService.context.evaluatee.evalData.planType;
            vm.planTypeDisplayName = utils.mapEvaluationPlanTypeForUserToString(activeUserContextService, vm.evaluatee, false);
            vm.evaluatorDisplayName = activeUserContextService.getEvaluatorDisplayName();
            vm.isEvaluatee = activeUserContextService.context.isEvaluatee();
            vm.evalFocused = vm.planType === enums.EvaluationPlanType.FOCUSED;
            setSelfAssessmentFocusDisplayName();
         }

        function setSelfAssessmentFocusDisplayName() {
            vm.selfAssessmentFocusDisplayName = vm.selfAssessment.focused? vm.planTypeDisplayName:"<span class='label label-warning'>Focus Unlocked</span>";
        }

    }
}) ();