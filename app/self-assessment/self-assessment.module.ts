/// <reference path="../core/core.module.ts" />

/**
 * Created by anne on 12/5/2015.
 */
(function() {
    'use strict';

    angular.module('stateeval.self-assessment', ['stateeval.core']);
})();

