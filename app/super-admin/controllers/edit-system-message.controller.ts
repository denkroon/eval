/// <reference path="../super-admin.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.super-admin')
        .controller('editSystemMessageController', editSystemMessageController);

    editSystemMessageController.$inject = ['systemMessageService', '$stateParams', 'logger', '$state'];

    /* @ngInject */
    function editSystemMessageController(systemMessageService, $stateParams, logger, $state) {
        var vm = this;
        vm.saveSystemMessage = saveSystemMessage;

        var messageId = parseInt($stateParams.messageId);
        activate();

        /////////////////////////

        //activate();

        function activate() {
            if (messageId) {
                systemMessageService.getSystemMessage(messageId).then(function(systemMessage) {
                    vm.systemMessage = systemMessage;
                });
            }
        }

        function saveSystemMessage() {
            systemMessageService.saveSystemMessage(vm.systemMessage).then(function(data) {
                logger.info("Successfully Saved");
                $state.go("system-messages");
            });
        }
    }

})();
