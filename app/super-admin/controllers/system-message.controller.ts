/// <reference path="../super-admin.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.super-admin')
        .controller('systemMessageController', systemMessageController);

    systemMessageController.$inject = ['systemMessageService', '$state'];

    /* @ngInject */
    function systemMessageController(systemMessageService, $state) {
        var vm = this;
        vm.addSystemMessage = addSystemMessage;
        vm.editSystemMessage = editSystemMessage;

        activate();

        /////////////////////////

        //activate();

        function activate() {
            systemMessageService.getSystemMessages().then(function (systemMessages) {
                vm.systemMessages = systemMessages;
            });
        }

        function addSystemMessage() {
            $state.go("edit-system-message", { messageId: 0 });
        }

        function editSystemMessage(messageId) {
            $state.go("edit-system-message", { messageId: messageId });
        }
    }

})();
