/// <reference path="../super-admin.module.ts" />

/**
 * Created by anne on 6/19/2015.
 */
(function() {
    'use strict';

    angular
        .module('stateeval.super-admin')
        .factory('systemMessageService', systemMessageService);

    systemMessageService.$inject = ['$http', 'config'];

    function systemMessageService($http, config) {

        var service = {
            getSystemMessages: getSystemMessages,
            getSystemMessage: getSystemMessage,
            saveSystemMessage: saveSystemMessage

        };

        return service;

        function getSystemMessages() {
            var url = config.apiUrl + 'systemMessages';
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function saveSystemMessage(systemMessage) {
            var url = config.apiUrl + 'systemMessage/save';
            return $http.post(url, systemMessage).then(function(response) {
                return response.data;
            });
        }

        function getSystemMessage(messageId) {
            var url = config.apiUrl + 'SystemMessage/' + messageId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }
    }
})();