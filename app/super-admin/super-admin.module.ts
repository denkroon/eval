/// <reference path="../core/core.module.ts" />

/**
 * Created by anne on 6/19/2015.
 */
(function() {
    'use strict';

    angular.module('stateeval.super-admin', ['stateeval.core']);
})();
