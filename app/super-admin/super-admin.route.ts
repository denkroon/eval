/// <reference path="super-admin.module.ts" />

/**
 * Created by anne on 6/19/2015.
 */
(function(){
angular.module('stateeval.super-admin')
    .config(configureRoutes);

function configureRoutes($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state('super-admin-import-errors', {
            url: '/import-errors',
            parent: 'default-content',
            views: {
                'content@base': {
                    controller: 'importErrorsController as vm',
                    templateUrl: 'app/super-admin/views/import-errors.html'
                }
            },
            data: {
                title: 'Import Errors',
                displayName: 'Import Errors'
            }
        })
    .state('system-messages', {
        url: '/system-messages',
        parent: 'default-content',
        views: {
            'content@base': {
                controller: 'systemMessageController as vm',
                templateUrl: 'app/super-admin/views/system-messages.html'
            }
        },
        data: {
            title: 'Systen Messages',
            displayName: 'Systen Messages'
        }
    }).state('edit-system-message', {
        url: '/system-messages/edit/:messageId',
        parent: 'default-content',
        views: {
            'content@base': {
                controller: 'editSystemMessageController as vm',
                templateUrl: 'app/super-admin/views/edit-system-message.html'
            }
        },
        data: {
            title: 'Systen Message',
            displayName: 'Systen Message'
        }
    });

}

})