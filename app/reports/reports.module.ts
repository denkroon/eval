/// <reference path="../core/core.module.ts" />

/**
 * Created by anne on 9/14/2015.
 */
(function() {
    'use strict';

    angular.module('stateeval.reports', ['stateeval.core']);
})();

