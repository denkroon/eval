/// <reference path="../reports.module.ts" />

(function () {

    angular
        .module('stateeval.reports')
        .factory('reportService', reportService);

    reportService.$inject = ['_', 'enums', '$http', 'utils', 'config', 'activeUserContextService'];

    /* @ngInject */
    function reportService(_, enums, $http, utils, config, activeUserContextService) {
        var service = {
            activate: activate,
            getReportOptionReportFormats: getReportOptionReportFormats,
            saveReportOptionReportFormats: saveReportOptionReportFormats,
            saveObservationReport: saveObservationReport,
            getObservationReport: getObservationReport
        };

        ////////////////

        function activate() {

        }

        activate();

        return service;

        function getObservationReport(observation) {

            var url = config.apiUrl + '/reports/observationreport/' + observation.id;
            return $http.get(url, {responseType: 'arraybuffer'}).then(function(response) {
                return response.data;
            });
        }

        function saveObservationReport(observation, html) {

            var schoolYearAsString = utils.getSchoolYear(activeUserContextService.context);
            var header = activeUserContextService.context.evaluatee.displayName + " " +
                        "Observation Report" + " " +
                        schoolYearAsString;

            var url = config.apiUrl + '/reports/saveobservationreport';
            return $http.put(url, {observationId: observation.id, html: html, header: header}).then(function(response) {
                return response.data;
            });
        }

        function saveReportOptionReportFormats(optionFormats) {
            var url = config.apiUrl + '/savereportoptionreportformats/';
            return $http.put(url, optionFormats).then(() => {
            });
        }

        function getReportOptionReportFormats(reportType, reportFormat) {
            var frameworkContextId = activeUserContextService.context.frameworkContext.id;
            var url = config.apiUrl + 'reports/reportoptions/' + frameworkContextId + '/' + reportType + '/' + reportFormat;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }
    }
})();

