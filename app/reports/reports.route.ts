/// <reference path="reports.module.ts" />

(function(){
/**
 * Created by anne on 9/14/2015.
 */
angular.module('stateeval.reports')
    .config(configureRoutes);

configureRoutes.$inject = ['$stateProvider'];

function configureRoutes($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('reports-home', {
            url: '/reports-home',
            parent: 'default-content',
            abstract: true
        })
        .state('reports-dashboard', {
            url: '/reports-dashboard',
            parent: 'reports-home',
            views: {
                'content@base': {
                    templateUrl: 'app/reports/views/reports-dashboard.html'
                }
            },
            ncyBreadcrumb: {
                label: 'Reports Dashboard'
            },
        })
        .state('evaluatee-reports', {
            url: '/evaluatee-reports',
            parent: 'reports-home',
            views: {
                'content@base': {
                    templateUrl: 'app/reports/views/evaluatee-reports.html',
                    controller: 'evaluateeReportsController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Reports'
            },
        })
        .state('evaluator-reports', {
            url: '/evaluator-reports',
            parent: 'reports-home',
            views: {
                'content@base': {
                    templateUrl: 'app/reports/views/evaluator-reports.html',
                    controller: 'evaluatorReportsController as vm'
                }
            }
        })
        .state('summative-report', {
            url: '/summative-report',
            parent: 'reports-home',
            views: {
                'content@base': {
                    templateUrl: 'app/reports/views/summative-report.html',
                    controller: 'summativeReportController as vm'
                }
            }

});
}


})();