/// <reference path="../core.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.core')
        .directive('reportScoreBlock', reportScoreBlockDirective)
        .controller('reportScoreBlockController', reportScoreBlockController)

    reportScoreBlockDirective.$inject = [];
    function reportScoreBlockDirective() {
        return {
            restrict: 'E',
            scope: {
                item: '='
            },
            bindToController: true,
            controller: 'reportScoreBlockController as vm',
            templateUrl: 'app/reports/score-block/report-score-block.directive.html'
        }
    }

    function reportScoreBlockController() {
        var vm = this;
    }

}) ();

