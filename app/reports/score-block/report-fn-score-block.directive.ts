/// <reference path="../reports.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.core')
        .directive('reportFnScoreBlock', reportFnScoreBlockDirective)
        .controller('reportFnScoreBlockController', reportFnScoreBlockController)

    reportFnScoreBlockDirective.$inject = [];
    function reportFnScoreBlockDirective() {
        return {
            restrict: 'E',
            scope: {
                evidenceCollection: '=',
                node: '='
            },
            bindToController: true,
            controller: 'reportFnScoreBlockController as vm',
            templateUrl: 'app/reports/score-block/report-fn-score-block.directive.html'
        }
    }

    reportFnScoreBlockController.$inject = ['enums'];

    function reportFnScoreBlockController(enums) {
        var vm = this;
        vm.enums = enums;
    }

}) ();

