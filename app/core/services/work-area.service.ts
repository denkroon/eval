/// <reference path="../core.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.core')
        .factory('workAreaService', WorkAreaService);

    WorkAreaService.$inject = ['enums', '$state', 'frameworkService', 'userService', 'evaluationService', 'config', '_', '$q', '$uibModal',
        'localStorageService', 'utils'];

    function WorkAreaService(enums, $state, frameworkService, userService, evaluationService, config, _, $q, $uibModal, localStorageService, utils) {

        WorkArea.prototype = {
            isEvaluatorWorkArea: function isEvaluatorWorkArea() {
                var wa = this;
                return !!_.find(evaluators, function (n) {
                    return n.tag === wa.tag;
                })
            },
            isEvaluateeWorkArea: function isEvaluateeWorkArea() {
                var wa = this;
                return !!_.find(evaluatees, function (n) {
                    return n.tag === wa.tag;
                })
            }
        };
        var nextWorkAreaId = 1;
        var evalT = enums.EvaluationType.TEACHER;
        var evalP = enums.EvaluationType.PRINCIPAL;
        var evalL = enums.EvaluationType.LIBRARIAN;
        var evalU = enums.EvaluationType.UNDEFINED;

        //LINKS
        var artifacts = new NavLink('Artifacts', 'artifacts', 'fa fa-clipboard', null);
        var ytdEvidence = new NavLink('YTD Evidence', 'ytd-evidence', 'fa fa-table', null);
        var sgGrowthGoalsTee = new NavLink('Student Growth Goals', 'sg-bundle-summary', 'fa fa-child', null);
        var sgGrowthGoalsTor = new NavLink('Student Growth Goals', 'sg-bundle-summary', 'fa fa-child', null);
        var observations = new NavLink('Observations', 'observation-list', 'fa fa-eye', null);
        var summativeEval = new NavLink('Summative Evaluation', 'summative-eval-ec', 'fa fa-table', null);
        var selfAssessments = new NavLink('Self-Assessments', 'self-assessment-list', 'fa fa-coffee', null);


        var importErrors = new NavLink('Import Errors', 'super-admin-import-errors', 'fa fa-user-secret', null);
        var setup_DE = new NavLink('Setup', 'VOID', 'fa fa-gear',
            [PromptBankNavLink(evalP), AssignmentsNavLink(evalP)]);
        var setup_PR_PR = new NavLink('Setup', 'VOID', 'fa fa-gear',
            [PromptBankNavLink(evalP), AssignmentsNavLink(evalP)]);
        var setup_PR_TR = new NavLink('Setup', 'VOID', 'fa fa-gear',
            [PromptBankNavLink(evalT), AssignmentsNavLink(evalT)]);
        var setup_DTE = new NavLink('Setup', 'VOID', 'fa fa-gear',
            [PromptBankNavLink(evalT),
                new NavLink('Assignment Requests', 'dte-request-assignment', 'fa fa-gear', null)
            ]);

        var evaluateeDashboard = new NavLink('Dashboard', 'evaluatee-summary', 'fa fa-th-large', null);
        var evaluatorDashboard = new NavLink('Dashboard', 'evaluatee-list', 'fa fa-th-large', null);
        var saDashboard = new NavLink('Dashboard', 'sa-begin-year', 'fa fa-th-large', null);
        var daDashboard = new NavLink('Dashboard', 'da-begin-year', 'fa fa-th-large', null);
        var dvDashboard = new NavLink('Dashboard', 'dv-dashboard', 'fa fa-th-large', null);

     //   var home = new NavLink('Home', 'landing', 'fa fa-th-large', null);
        var teeNav = [evaluateeDashboard, artifacts, ytdEvidence, sgGrowthGoalsTee, observations, selfAssessments,
        summativeEval];
        var pr_trNav = [ evaluatorDashboard, artifacts, ytdEvidence, sgGrowthGoalsTor, observations, selfAssessments, summativeEval, setup_PR_TR, ResourcesNavLink(evalT)];
        var pr_prNav = [evaluatorDashboard, artifacts, ytdEvidence, sgGrowthGoalsTor, selfAssessments, summativeEval,
            setup_PR_PR, ResourcesNavLink(evalP)];
        var de_prNav = [evaluatorDashboard, artifacts, ytdEvidence, sgGrowthGoalsTor, selfAssessments, summativeEval,
            setup_DE, ResourcesNavLink(evalP)];
        var dteNav = [evaluatorDashboard, artifacts, ytdEvidence, sgGrowthGoalsTor, observations, selfAssessments, summativeEval,
            setup_DTE, ResourcesNavLink(evalT)];
        var pr_libNav = [evaluatorDashboard, artifacts, ytdEvidence, sgGrowthGoalsTor, observations, selfAssessments, summativeEval,
            setup_PR_TR, ResourcesNavLink(evalL)];

        //AREAS

        // Admin Work-areas
        var SUPER = new WorkArea('SUPER', 'Super Administrator', evalU, noAdditionalInfoWorkArea,
            [importErrors]);

        var DA_TR = new WorkArea('DA_TR', 'Admin Teacher Evaluations', evalT, noAdditionalInfoWorkArea,
            [daDashboard, PromptBankNavLink(evalT), DistrictAssignmentsNavLink(evalT),
                DistrictViewerSetupNavLink(), ResourcesNavLink(evalT), SettingsNavLink(evalT)]);
        var DA_PR = new WorkArea('DA_PR', 'Admin Principal Evaluations', evalP, noAdditionalInfoWorkArea,
            [daDashboard, PromptBankNavLink(evalP),
                AssignmentsNavLink(evalP), ResourcesNavLink(evalP), SettingsNavLink(evalP)]);
        var SA_TR = new WorkArea('SA_TR', 'Admin Teacher Evaluations', evalT, noAdditionalInfoWorkArea,
            [saDashboard , PromptBankNavLink(evalT), AssignmentsNavLink(evalT), ResourcesNavLink(evalP)]);
        var SA_PR = new WorkArea('SA_PR', 'Admin Principal Evaluations', evalP, noAdditionalInfoWorkArea,
            [saDashboard, PromptBankNavLink(evalP), AssignmentsNavLink(evalP), ResourcesNavLink(evalP)]);

        var DAM_TR = new WorkArea('DAM_TR', 'Teacher Assignment', evalT, noAdditionalInfoWorkArea,
            [daDashboard, DistrictAssignmentsNavLink(evalT)]);
        var DAM_PR = new WorkArea('DAM_PR', 'Principal Assignment', evalP, noAdditionalInfoWorkArea,
            [daDashboard, AssignmentsNavLink(evalP)]);

        // Evaluation Work-areas
        var TR_ME = new WorkArea('TR_ME', 'Prepare for My Evaluation', evalT, getEvaluationForUserTeacher,
            teeNav);
        var LIB_ME = new WorkArea('LIB_ME', 'Prepare for My Evaluation', evalL, getEvaluationForUserLibrarian,
            teeNav);
        var PR_ME = new WorkArea('PR_ME', 'Prepare for My Evaluation', evalP, getEvaluationForUserPrincipal,
            teeNav);
        var PR_PR = new WorkArea('PR_PR', 'Evaluate Principals', evalP, getEvaluateesForEvaluatorPR_PR,
            pr_prNav);
        var PR_TR = new WorkArea('PR_TR', 'Evaluate Teachers', evalT, getEvaluateesForEvaluatorPR_TR,
            pr_trNav);
        var PR_LIB = new WorkArea('PR_LIB', 'Evaluate Librarians', evalL, getEvaluateesForEvaluatorPR_LIB,
            pr_libNav);
        var DE = new WorkArea('DE', 'Evaluate Principals', evalP, getEvaluateesForEvaluatorDE_PR,
            de_prNav);
        var DTE = new WorkArea('DTE', 'Evaluate Teachers', evalT, getObserveEvaluateesForDTEEvaluator,
            dteNav);

        var TRAIN = new WorkArea('TRAIN', 'Training', evalU, noAdditionalInfoWorkArea,
            [NavLink('Training Videos', 'training-protocols', 'fa fa-camera-retro', null),
                NavLink('Practice Sessions', 'practice-sessions', 'fa fa-leaf', null),
                NavLink('Consensus Scores', 'district-anchors', 'fa fa-umbrella', null), NavLink('Learning Walks', 'learning-walk-list', 'fa fa-stumbleupon', null)]);

        //todo create videos and resources
        //todo what links should an impersonate get how to switch between impersonations
        var CST = new WorkArea('CST', 'Customer Support', evalU, noAdditionalInfoWorkArea,
            []);

        //THERE SHOULD ONLY BE A SINGLE IMPERSONATION WORK AREA
        //var DA_IMP = new WorkArea('DA_IMP', 'View Evaluators as Admin', evalU, impersonateMain,
        //    []);
        //
        //var DE_IMP = new WorkArea('DE_IMP', 'View Evaluators as Viewer', evalU, impersonateMain,
        //    []);
        //
        //var DV_IMP = new WorkArea('DV_IMP', 'View Evaluator as Viewer', evalU, impersonateMain,
        //    []);
        var DV = new WorkArea('DV', 'District Viewer', evalT, noAdditionalInfoWorkArea, [dvDashboard]);

        var IMP = new WorkArea('IMP', 'View Evaluators', evalU, impersonateMain,
            []);

        var NMP = new WorkArea('NMP', 'Go Back', evalU, stopImpersonation,
            []);


        var roleAreaMapper = {
            SESuperAdmin: [SUPER],
            SEDistrictAdmin: [DA_TR, DA_PR, IMP, TRAIN],
            SEDistrictViewer: [DV, IMP],
            SESchoolAdmin: [SA_TR, SA_PR, TRAIN],
            SESchoolPrincipal: [PR_TR, PR_LIB, PR_ME, TRAIN],
            SESchoolTeacher: [TR_ME, TRAIN],
            SEDistrictEvaluator: [DE, IMP, TRAIN],
            SESchoolLibrarian: [LIB_ME, TRAIN],
            SEDistrictWideTeacherEvaluator: [DTE, TRAIN],
            SESchoolHeadPrincipal: [PR_PR, TRAIN],
            SECustomSupportL1: [CST],
            SEDistrictAssignmentManager: [DAM_TR, DAM_PR]
        };

        var viewableImpersonateRoles = {
            'PR_TR': true,
            'PR_PR': true,
            'DTE': true
        };

        var impersonators = {
            'SEDistrictAdmin': true,
            'SEDistrictViewer': true,
            'SEDistrictEvaluator': true
        };

        var evaluators = [PR_PR, PR_TR, PR_LIB, DTE, DE];
        var evaluatees = [PR_ME, TR_ME, LIB_ME];

        var service = {
            SA_TR: SA_TR,
            SA_PR: SA_PR,
            TR_ME: TR_ME,
            LIB_ME: LIB_ME,
            PR_PR: PR_PR,
            DA_TR: DA_TR,
            DA_PR: DA_PR,
            PR_ME: PR_ME,
            PR_TR: PR_TR,
            PR_LIB: PR_LIB,
            DTE: DTE,
            DE: DE,
            DV: DV,
            SUPER: SUPER,
            DAM_TR: DAM_TR,
            DAM_PR: DAM_PR,
            TRAIN: TRAIN,
            IMP: IMP,
            NMP: NMP,
            viewableImpersonateRoles: viewableImpersonateRoles,
            impersonators: impersonators,
            roleAreaMapper: roleAreaMapper,
            getAreaById: getAreaById,
            showOnImpersonate: showOnImpersonate,
            isEvaluator: isEvaluator,
            getTagsOfRole: getTagsOfRole,
            getSingleTagOfRole: getSingleTagOfRole,
            getSingleWorkAreaForRole: getSingleWorkAreaForRole,
            getWorkAreasForRole: getWorkAreasForRole,
            impersonateTitleMapper: impersonateTitleMapper,
            changeEvaluatee: changeEvaluatee,
        };


        return service;

        //SERVICE FUNCTIONS

        function changeEvaluatee(evaluatee, activeUserContextService) {
            var context = activeUserContextService.context;
            context.evaluatee = evaluatee;
            console.log('changed evaluatee:', evaluatee);
            activeUserContextService.save();
            // todo: context.evaluatee is null after switch pages
            if (context.evaluatee) {
                $state.go('evaluatee-tor', {id: context.evaluatee.id}, {reload: true});
            } else {
                $state.go('evaluatee-list', {}, {reload: true});
            }
            //Sets the currently evaluated to an evaluatee from users list of evaluatees
        }

        function impersonateTitleMapper(role) {
            var title;
            switch (role) {
                case 'SEDistrictAdmin':
                    title = 'View Evaluators';
                    break;
                case 'SEDistrictViewer':
                    title = 'View Evaluators';
                    break;
                default:
                    title = 'View Evaluators';
                    break;
            }
            service.IMP.title = title;
        }

        function showOnImpersonate(workArea) {
            return !!_.find(viewableImpersonateRoles, function (n) {
                return workArea.id === n.id;
            });
        }

        function getAreaById(id) {
            for (var i in service) {
                if (service[i].id === id) {
                    return service[i];
                }
            }
            return -1;
        }

        function getSingleWorkAreaForRole(role, evaluationType) {
            return _.findWhere(roleAreaMapper[role], {'evaluationType': evaluationType});
        }

        function getWorkAreasForRole(role, evaluationType) {
            return _.filter(roleAreaMapper[role], {'evaluationType': evaluationType});
        }

        function getSingleTagOfRole(role, evaluationType) {
            return _.findWhere(roleAreaMapper[role], {'evaluationType': evaluationType}).tag;
        }

        function isEvaluator(workAreaTag) {
            return !!_.find(evaluators, function (n) {
                return workAreaTag === n.tag;
            });
        }

        function getTagsOfRole(role, evaluationType) {
            return _.filter(roleAreaMapper[role], function (n) {
                return n.evaluationType === evaluationType ||
                    n.evaluationType === enums.EvaluationType.UNDEFINED;
            })
        }

        //GOTO WORKAREA FUNCTIONS

        function getEvaluationForEvaluatee(activeUserContextService) {
            return evaluationService.getEvaluationForUser()
                .then(function (evalData) {
                    activeUserContextService.user.evalData = evalData;
                    return evalData.evaluatorId;
                })
                .then(function (id) {
                    if (id != null) {
                        return userService.getUserById(id).then(function (evaluator) {
                            activeUserContextService.context.evaluator = evaluator;
                        })
                    }
                })
        }

        //Loads evaluator and evaluation for Principal
        function getEvaluationForUserPrincipal(activeUserContextService) {
            activeUserContextService.context.evaluatee = activeUserContextService.user;
            return getEvaluationForEvaluatee(activeUserContextService);
        }

        //Loads evaluatee and evaluation for a teacher
        function getEvaluationForUserTeacher(activeUserContextService) {
            activeUserContextService.context.evaluatee = activeUserContextService.user;
            return getEvaluationForEvaluatee(activeUserContextService);
        }

        //Loads evaluatee and evaluation for a librarian
        function getEvaluationForUserLibrarian(activeUserContextService) {
            activeUserContextService.context.evaluatee = activeUserContextService.user;
            return getEvaluationForEvaluatee(activeUserContextService);
        }

        //loads all observable evaluatees for DTE
        function getObserveEvaluateesForDTEEvaluator(activeUserContextService) {
            var context = activeUserContextService.context;
            return userService.getObserveEvaluateesForDTEEvaluator()
                .then(function (evaluatees) {
                    context.evaluatees = evaluatees;
                });
        }


        //when called generates all evaluatees for evaluator
        function getEvaluateesForEvaluatorPR_PR(activeUserContextService) {
            var context = activeUserContextService.context;
            return userService.getEvaluateesForEvaluatorPR_PR()
                .then(function (evaluatees) {
                    context.evaluatees = evaluatees;
                })
        }

        function getEvaluateesForEvaluatorDE_PR(activeUserContextService) {
            var context = activeUserContextService.context;
            return userService.getEvaluateesForEvaluatorDE_PR()
                .then(function (evaluatees) {
                    context.evaluatees = evaluatees;
                })
        }

        function getEvaluateesForEvaluatorPR_TR(activeUserContextService) {
            var context = activeUserContextService.context;
            return userService.getEvaluateesForEvaluatorPR_TR()
                .then(function (evaluatees) {
                    context.evaluatees = evaluatees;
                })
        }

        function getEvaluateesForEvaluatorPR_LIB(activeUserContextService) {
            var context = activeUserContextService.context;
            return userService.getEvaluateesForEvaluatorPR_LIB()
                .then(function (evaluatees) {
                    context.evaluatees = evaluatees;
                })
        }

        function impersonateMain(activeUserContextService, startupService) {
            var impersonateConfig = activeUserContextService.context.impersonateConfig;

            var loggedInContext = localStorageService.get('authenticatedContext');
            if (!loggedInContext) {
                loggedInContext = activeUserContextService.context;
                localStorageService.set('authenticatedContext', activeUserContextService.context);
                localStorageService.set('authenticatedUser', activeUserContextService.user);
            }

            var locationRole = {
                districtCode: impersonateConfig.districtCode,
                districtName: impersonateConfig.districtName,
                roleName: impersonateConfig.roleName,
                schoolCode: impersonateConfig.schoolCode,
                schoolName: impersonateConfig.schoolName,
                schoolYear: impersonateConfig.schoolYear,
                workAreaTag: impersonateConfig.workAreaTag
            };
            impersonateConfig.impersonatee.userOrientations = [locationRole];

            var context = startupService.contextCreation(impersonateConfig.impersonatee, [locationRole]);
            activeUserContextService.context.impersonateConfig = impersonateConfig;
            return context.changeWorkArea(context.workArea().tag)
                .then(utils.returnFunction($q.reject()));
        }

        function stopImpersonation(activeUserContextService, startupService) {
            startupService.load('authenticatedContext', 'authenticatedUser');
            localStorageService.set('authenticatedContext', null);
            localStorageService.set('authenticatedUser', null);
            activeUserContextService.context.impersonateConfig = null;
            var defaultNav = startupService.defaultNavOption(0, activeUserContextService.context.orientationOptions, {});
            var orientations = activeUserContextService.getOrientationWithNav(0, activeUserContextService.context.orientationOptions, defaultNav);
            var workAreaTag = orientations.workAreaTag;
            activeUserContextService.context.orientation = orientations;
            activeUserContextService.context.navOptions = defaultNav;
            return service[workAreaTag].initializeWorkArea(activeUserContextService, startupService)
                .then(utils.returnFunction($q.reject()));
        }

        //For workAreas that do not need any information to be loaded
        //Reminder, this service does not have direct access to the ActiveUserContextService


        function noAdditionalInfoWorkArea(activeUserContextService) {
            if (activeUserContextService.context) {
                activeUserContextService.context.evaluatees = null;
            }
            console.log('No special information is loaded for this work area');
            return $q.when();
        }

        //Oversees state change and framework setup
        //thisBinding = current workArea
        function setupFrameworks(activeUserContextService, _this) {
            return function () {
                if (_this.evaluationType) {
                    return frameworkService.getFrameworkContext(_this.evaluationType)
                        .then(function (frameworkContext) {
                            activeUserContextService.context.frameworkContext = frameworkContext;
                            var selfAseesLink = _.find(_this.navbar, function(n) {
                                return n.title === 'Self-Assessments';
                            });

                            if(selfAseesLink){
                                selfAseesLink.hide =  !frameworkContext.districtConfiguration.selfAssessmentsModuleEnabled;
                            }
                            activeUserContextService.context.framework = frameworkContext.defaultFramework;
                            activeUserContextService.context.frameworkContexts.push(frameworkContext);
                        });
                } else {
                    return $q.when();
                }
            }
        }

        function resetContextProperties(activeUserContextService, _this) {
            activeUserContextService.context.orientation = activeUserContextService.getOrientationWithNav(0, activeUserContextService.context.orientationOptions, activeUserContextService.context.navOptions);
            activeUserContextService.context.evaluatee = null;
            activeUserContextService.context.evaluator = null;
            activeUserContextService.context.evaluatees = null;
            if (_this.isEvaluatorWorkArea()) {
                activeUserContextService.context.evaluator = activeUserContextService.user;
            } else if (_this.isEvaluateeWorkArea()) {
                activeUserContextService.context.evaluatee = activeUserContextService.user;
            }
        }

        function saveAndRedirect(activeUserContextService, workArea) {
            return function () {
                var dest = '';
                if(workArea.isEvaluateeWorkArea() && (!activeUserContextService.user.evalData.planType || !activeUserContextService.user.evalData.evaluatorId)) {
                    activeUserContextService.context.incompleteEvaluatee = true;
                    dest = 'incomplete-evaluatee';
                } else {
                    activeUserContextService.context.incompleteEvaluatee = false;
                    console.log('Initializing Work Area: ',
                        activeUserContextService.context.orientation.workAreaTag,
                        activeUserContextService.context,
                        activeUserContextService.user);
                    dest = workArea.navbar[0].state;
                }
                activeUserContextService.save();
                $state.go(dest, {}, {reload: true});
            }
        }

        //CONSTRUCTOR -
        //    each work area gets an additional method called from the change-work-area controller
        //    to initialize the workArea and load appropriate information
        function WorkArea(tag, title, evaluationType, loadInformation, navbar) {
            this.id = nextWorkAreaId++;
            this.tag = tag;
            this.title = title;
            this.evaluationType = evaluationType;
            this.navbar = navbar;
            this.loadInfoMethod = loadInformation;
            this.initializeWorkArea = function (activeUserContextService, startupService) {
                var workArea = this;
                resetContextProperties(activeUserContextService, workArea);

                //impersonateTitleMapper(activeUserContextService.context.orientation.roleName);
                var loadPromise = loadInformation(activeUserContextService, startupService);
                var firstThen = loadPromise.then(setupFrameworks(activeUserContextService, workArea));
                var secondThen = firstThen.then(saveAndRedirect(activeUserContextService, workArea));
                return secondThen;


            }
        }


        //The specific object that the change-work-area directive uses to create the navbar
        function NavLink(title, state, icon, subLinks) {
            return {
                title: title,
                state: state,
                icon: icon,
                subLinks: subLinks
            }
        }

        // Helpers

        //todo should change the pages that use parameters to take information from the activeUserContextService.context.workArea().evaluationType
        function FrameworkNavLink(evalType) {
            return NavLink('Framework', 'select-framework({"evaluationtype": ' + evalType + '})', 'fa fa-list-ul', null);
        }

        function PromptBankNavLink(evalType) {
            return NavLink('Prompt Bank', 'prompt-bank({"evaluationtype": ' + evalType + '})', 'fa fa-question-circle', null);
        }

        function AssignmentsNavLink(evalType) {
            return NavLink('Assignments', 'assignments-detail({"evaluationtype": ' + evalType + '})', 'fa fa-link', null);
        }

        function DistrictAssignmentsNavLink(evalType) {
            return NavLink('Assignments', 'assignments-summary({"evaluationtype": ' + evalType + '})', 'fa fa-link', null);
        }

        function DistrictViewerSetupNavLink() {
            return NavLink('District Viewer Setup', 'district-viewer-setup', 'fa fa-link', null);
        }

        function ResourcesNavLink(evalType) {
            return NavLink('Resources', 'resource-list({"evaluationtype": ' + evalType + '})', 'fa fa-list-ol', null);
        }

        function ReportsNavLink(evalType) {
            return NavLink('Reports', 'reports-dashboard({"evaluationtype": ' + evalType + '})', 'fa fa-table', null);
        }

        function SettingsNavLink(evalType) {
            return NavLink('Settings', 'da-settings({"evaluationtype": ' + evalType + '})', 'fa fa-table', null);
        }

        function removeNonImpersonate(n) {
            return viewableImpersonateRoles[n.workAreaTag];
        }
    }
})
();
