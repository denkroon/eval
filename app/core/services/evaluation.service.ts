/// <reference path="../core.module.ts" />

/**
 * Created by anne on 6/26/2015.
 */

(function () {

    angular
        .module('stateeval.core')
        .factory('evaluationService', evaluationService);

    evaluationService.$inject = ['$http', 'config', 'activeUserContextService'];

    /* @ngInject */
    function evaluationService($http, config, activeUserContextService) {

        var service = {
            getEvaluationForUser: getEvaluationForUser,
            getEvidenceCollectionsForEvaluation: getEvidenceCollectionsForEvaluation
        };

        return service;

        ////////////////

        function getEvaluationForUser() {
            var s = '/evaluations/' + activeUserContextService.user.id + '/' + activeUserContextService.context.orientation.districtCode + '/' + activeUserContextService.context.orientation.schoolYear + '/' + activeUserContextService.context.workArea().evaluationType;
            return $http.get(config.apiUrl + s).then(function (response) {
                return response.data;
            })
        }

        function getEvidenceCollectionsForEvaluation(evaluationId) {
            var url = config.apiUrl + evaluationId + '/evidencecollections/' + activeUserContextService.user.id;
            return $http.get(url).then(function (response) {
                return response.data;
            })
        }
    }
})
();

