/// <reference path="../core.module.ts" />

(function () {

    angular
        .module('stateeval.core')
        .factory('rubricRowEvaluationService', rubricRowEvaluationService);

    rubricRowEvaluationService.$inject = ['_', 'utils', '$http', 'config', 'activeUserContextService'];

    /* @ngInject */
    function rubricRowEvaluationService(_, utils, $http, config, activeUserContextService) {
        var service = {
            getRubricRowEvaluationById: getRubricRowEvaluationById
        };

        return service;

        function getRubricRowEvaluationById(id) {
            var url = config.apiUrl + 'rubricrowevaluations/' + id;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }
    }
})();

