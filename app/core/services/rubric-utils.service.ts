/// <reference path="../core.module.ts" />

/**
 * Created by anne on 6/24/2015.
 */
/**
 * rubricUtils - service
 */

(function () {
    'use strict';

    angular
        .module('stateeval.core')
        .factory('rubricUtils', rubricUtils);

    rubricUtils.$inject = ['$sce', 'logger', '_', 'enums', '$http', '$q', 'utils'];

    function rubricUtils($sce, logger, _, enums, $http, $q, utils) {

        var colorObj = {
            merge: {
                evaluatee: 'blue-highlight',
                evaluator: 'yellow-highlight',
                combined: 'green-highlight'
            },
            default: {
                evaluatee: 'navy-highlight',
                evaluator: 'navy-highlight',
                combined: 'navy-highlight'
            }
            //type: 'default',
            //primary: '#ffea8c',
            //secondary: '#cce6ff',
            //combined: '#82ffc9',
            //evaluation: '#1ab394',
            //default: '#2f4050'
        };

        var service = {
            getRubricRowById: getRubricRowById,
            getFrameworkNodeForRubricRowId: getFrameworkNodeForRubricRowId,
            getStudentGrowthFrameworkNodes: getStudentGrowthFrameworkNodes,
            getStudentGrowthProcessRubricRow: getStudentGrowthProcessRubricRow,
            getStudentGrowthResultsRubricRow: getStudentGrowthResultsRubricRow,
            getStudentGrowthRubricRows: getStudentGrowthRubricRows,
            getStudentGrowthRubricRowsForFrameworkNode: getStudentGrowthRubricRowsForFrameworkNode,

            mapPerformanceLevelToDisplayString: mapPerformanceLevelToDisplayString,
            mapPerformanceLevelToShortDisplayString: mapPerformanceLevelToShortDisplayString,

            generateHighlightedRubricDescriptorTextFromEvaluations: generateHighlightedRubricDescriptorTextFromEvaluations,

            getRubricDescriptorText: getRubricDescriptorText,
            newRubricEvaluationDescriptorArray: newRubricEvaluationDescriptorArray,
            getFrameworkTree: getFrameworkTree,
            mergeEvidenceToHtml: mergeEvidenceToHtml,
            getFrameworkMapper: getFrameworkMapper,
            getRowMapper: getRowMapper,
            getNodeNumbers: getNodeNumbers,
            getNodeMapper: getNodeMapper,

            newSeparatedEvaluationDescriptorArray: newSeparatedEvaluationDescriptorArray,
            getFlatRows: getFlatRows,
            formatRubricRow: formatRubricRow,
            buildAlignmentDisplayString: buildAlignmentDisplayString,
            putItemLastById: putItemLastById,
            getNodeSequence: getNodeSequence,
            getRowSequence: getRowSequence,
            colorObj: colorObj,
            standardizeHtml: standardizeHtml,
            createOrderedEvaluations: createOrderedEvaluations,
            minBy: minBy,
            reverse: reverse,
            createEvalSource: createEvalSource,
            getHighlightId: getHighlightId,
            createDividedEvaluations: createDividedEvaluations,
            orderByNodesAndRows: orderByNodesAndRows
        };
        return service;

        function orderByNodesAndRows(collection, framework) {
            var nodeMapper = getFrameworkMapper(framework);
            var rowMapper = getRowMapper(framework);
            var nodeSequence = getNodeSequence(framework);
            var rowSequence = getRowSequence(framework);
            var nodes = [];
            collection = _.clone(collection);
            for (var i in collection) {
                collection[i].nodeShortName = nodeMapper[collection[i].rubricRowId];
            }
            var byNodes = _.groupBy(collection, 'nodeShortName');
            for (var j in byNodes) {
                var node = {
                    nodeShortName: j,
                    sequence: nodeSequence[j],
                    rows: []
                };
                var byRowId = _.groupBy(byNodes[j], 'rubricRowId');
                for (var k in byRowId) {
                    var row = {
                        rowId: k,
                        rowShortName: rowMapper[k],
                        sequence: rowSequence[k],
                        items: byRowId[k] || []
                    };
                    node.rows.push(row);
                }
                nodes.push(node);
            }
            return nodes;
        }

        function createDividedEvaluations(row) {
            var level;
            var list = [];
            for (var i in enums.PerformanceLevels) {
                level = row[enums.PerformanceLevels[i]];
                list[enums.PerformanceLevels[i]] = _.groupBy(level.evaluations, 'createdByUserId');
            }
            return list;
        }

        function getHighlightId(score, otherScore, $index) {
            //Returns string of either createdByUserId or the two concatenated together '5993'
            var level = $index + 1;
            var thisString = String((score.performanceLevel === level && score.createdByUserId) || '');
            var thatString = String((otherScore.performanceLevel === level && otherScore.createdByUserId) || '');
            return thisString + thatString;
        }

        function createEvalSource(type) {
            var sourceType = enums.AssociatedCollectionsAccessor[type];
            var words = sourceType.split('_');
            var value = '';
            for (var i in words) {
                var capital = words[i][0];
                var lower = words[i].substring(1, words[i].length).toLowerCase();
                value = value + capital + lower + ' ';
            }
            var trimed = value.trim();
            if(trimed === 'Self Assessments') {
                return 'Self-Assessments';
            }
            else {
                return trimed;
            }
        }

        function reverse(array) {
            var list = [];
            var length = array.length - 1;
            for (var i = length; i >= 0; i = -1) {
                list.push(array[i]);
            }
            return array
        }

        function minBy(collection, sortFn) {
            if (collection[0]) {
                var minValue = sortFn(collection[0])
            } else {
                return;
            }
            for (var i in collection) {
                minValue = minValue < sortFn(collection[i]) ? minValue : sortFn(collection[i]);
            }
            return minValue;
        }

        function createOrderedEvaluations(list, evidenceCollection, order) {
            var evidenceTypeKeys = Object.keys(enums.EvidenceCollectionType);
            var listItems = {
                collectionTypes: []
            };
            var evalsByType = _.groupBy(list, 'evidenceCollectionType');
            for (var i in evalsByType) {
                var title;
                switch (parseInt(i)) {
                    case enums.EvidenceCollectionType.OTHER_EVIDENCE:
                        title = "Other Evidence";
                        break;
                    case enums.EvidenceCollectionType.OBSERVATION:
                        title = "Observations";
                        break;
                    case enums.EvidenceCollectionType.SELF_ASSESSMENT:
                        title = "Self-assessments";
                        break;
                    case enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS:
                        title = "Student Growth Goals";
                        break;
                    default:
                        title = "Unknown";
                        break;
                }
                var collectionType = {
                    collectionInstances: [],
                    title: title
                };
                var evalsByInstance = _.groupBy(evalsByType[i], 'evidenceCollectionObjectId');
                for (var j in evalsByInstance) {
                    var id = evalsByInstance[j][0] && evalsByInstance[j][0].evidenceCollectionObjectId;
                    var collections = evidenceCollection.associatedCollections[evidenceTypeKeys[i]];
                    var collection;
                    if (parseInt(i) === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) {
                        collection = _.find(collections, {'goalBundleId': id});
                    }
                    else {
                        collection = _.find(collections, {'id': id});
                    }

                    var collectionInstance = {
                        items: [],
                        collectionType: parseInt(i),
                        title: collection.title,
                        creationDateTime: new Date(collection.creationDateTime)
                    };

                    for (var k in evalsByInstance[j]) {
                        collectionInstance.items.push(evalsByInstance[j][k]);
                    }
                    collectionType.collectionInstances.push(collectionInstance);
                }
                listItems.collectionTypes.push(collectionType);
            }
            return listItems;
        }

        function getNodeSequence(frameworks) {
            var list = [];
            if (frameworks.frameworkNodes) {
                frameworks = [frameworks];
            }
            for (var i in frameworks) {
                for (var k in frameworks[i].frameworkNodes) {
                    list[frameworks[i].frameworkNodes[k].shortName] = frameworks[i].frameworkNodes[k].sequence;
                }
            }
            return list;
        }


        function getRowSequence(frameworks) {
            var list = [];
            if (frameworks.frameworkNodes) {
                frameworks = [frameworks];
            }
            for (var i in frameworks) {
                for (var j in frameworks[i].frameworkNodes) {
                    for (var k in frameworks[i].frameworkNodes[j].rubricRows) {
                        list[frameworks[i].frameworkNodes[j].rubricRows[k].id] = frameworks[i].frameworkNodes[j].rubricRows[k].sequence;
                    }
                }
            }
            return list;
        }

        function putItemLastById(list, item) {
            var listTemp = [];
            for (var i in list) {
                if (list[i].id !== item.id) {
                    listTemp.push(list[i]);
                }
            }
            listTemp.push(item);
            return listTemp;
        }

        function buildAlignmentDisplayString(activeUserContextService, object, collection, idName) {
            //Only to be used with the alignment string directive

            var frameworks = putItemLastById(activeUserContextService.context.frameworkContext.frameworks,
                activeUserContextService.context.framework);

            var rowMapper = getRowMapper(frameworks);
            var map = getFrameworkMapper(frameworks);
            //Framework mapper overwrites other frameworks nodes with current framework leaving nodes not in current framework
            var nodeSequence = getNodeSequence(frameworks);
            var rowSequence = getRowSequence(frameworks);
            var nodes = [];

            var byRow = _.groupBy(object[collection], idName);

            for (var i in byRow) {
                var nodeShortName = map[i];
                var node = {
                    shortName: nodeShortName,
                    rows: [],
                    sequence: nodeSequence[nodeShortName]
                };
                var row = {
                    shortName: rowMapper[byRow[i][0].id],
                    sequence: rowSequence[byRow[i][0].id],
                    id: byRow[i][0].id
                };
                var nodeInArray = _.find(nodes, {shortName: node.shortName});
                if (nodeInArray) {
                    nodeInArray.rows.push(row)
                } else {
                    node.rows.push(row);
                    nodes.push(node);
                }
            }
            return nodes;
        }

        function getAlignmentString(object) {
            var nodeObj = _.groupBy(object.alignedRubricRows, 'frameworkNode')
        }

        function formatRubricRow(row, evaluations, evidenceCollection) {
            var temp = {
                data: row,
                parent: {}
            };
            var separated = newSeparatedEvaluationDescriptorArray(row, evaluations, evidenceCollection);
            for (var i in enums.PerformanceLevels) {
                temp[enums.PerformanceLevels[i]] = separated[enums.PerformanceLevels[i]];
            }
            return temp
        }

        function newSeparatedEvaluationDescriptorArray(row, evaluations, evidenceCollection) {
            var obj = {};
            evaluations = evaluations || [];
            var unseparated = newRubricEvaluationDescriptorArray(row);
            if (evaluations.length > 0) {
                evaluations = _.groupBy(evaluations, 'performanceLevel');
            }
            for (var i in enums.PerformanceLevels) {
                var descriptorHolder = unseparated[i].descriptorText;
                var attributeHolder = unseparated[i].criticalAttributeText;
                var levelEvals = evaluations[parseInt(i) + 1] || [];
                if (levelEvals.length > 0) {
                    descriptorHolder = mergeEvidenceToHtml(unseparated[i].descriptorText, levelEvals, evidenceCollection);
                    attributeHolder = mergeEvidenceToHtml(unseparated[i].criticalAttributeText, levelEvals, evidenceCollection);
                }
                obj[enums.PerformanceLevels[i]] = {
                    //paragraphs: separateParagraphs(unseparated[i].descriptorText),
                    descriptor: descriptorHolder,
                    parent: row,
                    data: unseparated[i],
                    evaluations: levelEvals,
                    checked: false,
                    criticalAttribute: attributeHolder
                }
            }
            return obj;
        }

        //returns a list of rubric rows indexed by id (uses frameworkContext rubric rows)
        function getFlatRows(framework) {
            var list = [];
            for (var i in framework.frameworkNodes) {
                for (var j in framework.frameworkNodes[i].rubricRows) {
                    list[framework.frameworkNodes[i].rubricRows[j].id] = framework.frameworkNodes[i].rubricRows[j]
                }
            }
            return list;
        }


        //returns number of framework rubric rows in each frameworkNode
        //DEPRECATED
        function getNodeNumbers(framework) {
            var list = [];
            for (var i in framework.frameworkNodes) {
                for (var j in framework.frameworkNodes[i].rubricRows) {
                    if (list[framework.frameworkNodes[i].shortName]) {
                        list[framework.frameworkNodes[i].shortName] += 1;
                    } else {
                        list[framework.frameworkNodes[i].shortName] = 1;
                    }
                }
            }
            return list;
        }

        // maps rubricRowId->frameworkNode.shortName
        //Returns a list indexed by rubricRowId accessing that rubric row's node's short name
        //rubric rows map to nodes in backwards order as entered in frameworks array [priority2, priority1]
        function getFrameworkMapper(frameworks) {
            if (!frameworks.length) {
                frameworks = [frameworks];
            }
            var list = [];
            for (var j in frameworks) {
                for (var k in frameworks[j].frameworkNodes) {
                    for (var l in frameworks[j].frameworkNodes[k].rubricRows) {
                        list[frameworks[j].frameworkNodes[k].rubricRows[l].id] = frameworks[j].frameworkNodes[k].shortName;
                    }
                }
            }
            return list;
        }

        // maps rubricRowId -> rubricRow.shortName
        //Returns a list indexed by id that accesses the rubric row's short name
        function getRowMapper(frameworks) {
            if (!frameworks.length) {
                frameworks = [frameworks];
            }
            var list = [];
            for (var i in frameworks) {
                for (var j in frameworks[i].frameworkNodes) {
                    for (var k in frameworks[i].frameworkNodes[j].rubricRows) {
                        list[frameworks[i].frameworkNodes[j].rubricRows[k].id] = frameworks[i].frameworkNodes[j].rubricRows[k].shortName;
                    }
                }
            }
            return list;
        }

        // maps frameworkNodeId -> frameworkNode.shortName
        //Returns a list indexed by id that accesses the frameworkNode's short name
        function getNodeMapper(framework) {
            var list = [];
            for (var i in framework.frameworkNodes) {
                list[framework.frameworkNodes[i].id] = framework.frameworkNodes[i].shortName;
            }
            return list;
        }

        //returns unindexed list of frameworkContext rubricRows
        function getFrameworkTree(framework) {
            var frameworkNodes = framework.frameworkNodes;
            var list = [];
            for (var i = 0; i < frameworkNodes.length; i++) {
                list.push(frameworkNodes[i]);
                for (var j = 0; j < frameworkNodes[i].rubricRows.length; j++) {
                    list.push(frameworkNodes[i].rubricRows[j])
                }
            }

            return list;
        }

        function newRubricEvaluationDescriptorArray(rubricRow) {
            var rubricDescriptors = [
                {
                    performanceLevel: enums.RubricPerformanceLevel.PL1,
                    performanceLevelDisplayName: mapPerformanceLevelToDisplayString((enums.RubricPerformanceLevel.PL1)),
                    descriptorText: '',
                    checked: false,
                    criticalAttributeText: rubricRow.lookFor1
                },
                {
                    performanceLevel: enums.RubricPerformanceLevel.PL2,
                    performanceLevelDisplayName: mapPerformanceLevelToDisplayString((enums.RubricPerformanceLevel.PL2)),
                    descriptorText: '',
                    checked: false,
                    criticalAttributeText: rubricRow.lookFor2

                },
                {
                    performanceLevel: enums.RubricPerformanceLevel.PL3,
                    performanceLevelDisplayName: mapPerformanceLevelToDisplayString((enums.RubricPerformanceLevel.PL3)),
                    descriptorText: '',
                    checked: false,
                    criticalAttributeText: rubricRow.lookFor3

                },
                {
                    performanceLevel: enums.RubricPerformanceLevel.PL4,
                    performanceLevelDisplayName: mapPerformanceLevelToDisplayString((enums.RubricPerformanceLevel.PL4)),
                    descriptorText: '',
                    checked: false,
                    criticalAttributeText: rubricRow.lookFor4

                }
            ];

            for (var i = 0; i < 4; ++i) {
                var performanceLevel = i + 1;
                rubricDescriptors[i].descriptorText = getRubricDescriptorText(rubricRow, performanceLevel);
            }

            return rubricDescriptors;
        }

        function getRubricDescriptorText(rubricRow, performanceLevel) {

            if (!rubricRow) {
                console.log('none');
            }
            switch (performanceLevel) {
                case enums.RubricPerformanceLevel.PL1:
                    return rubricRow.pL1Descriptor;
                case enums.RubricPerformanceLevel.PL2:
                    return rubricRow.pL2Descriptor;
                case enums.RubricPerformanceLevel.PL3:
                    return rubricRow.pL3Descriptor;
                case enums.RubricPerformanceLevel.PL4:
                    return rubricRow.pL4Descriptor;
            }
        }

        function mapPerformanceLevelToDisplayString(plEnum) {
            switch (plEnum) {
                case enums.RubricPerformanceLevel.PL1:
                    return "Unsatisfactory";
                case enums.RubricPerformanceLevel.PL2:
                    return "Basic";
                case enums.RubricPerformanceLevel.PL3:
                    return "Proficient";
                case enums.RubricPerformanceLevel.PL4:
                    return "Distinguished";
                default:
                    return 'N/A';
            }
        }

        function mapPerformanceLevelToShortDisplayString(plEnum) {
            switch (plEnum) {
                case enums.RubricPerformanceLevel.PL1:
                    return "UNS";
                case enums.RubricPerformanceLevel.PL2:
                    return "BAS";
                case enums.RubricPerformanceLevel.PL3:
                    return "PRO";
                case enums.RubricPerformanceLevel.PL4:
                    return "DIS";
                default:
                    return 'N/A';
            }
        }

        function separateParagraphs(text) {
            var list = [];
            var split = text.split('<\/p>');
            for (var i in split) {
                var pieces = split[i].split('<p>');
                for (var j in pieces) {
                    var removeWhiteSpace = pieces[j].trim();
                    if (!(removeWhiteSpace === '')) {
                        list.push(removeWhiteSpace);
                    }
                }
            }
            return list;
        }


        //todo this is currently a workaround to try to bring all the different formats of the html text together
        function standardizeHtml(rubricDescriptorText) {
            if (rubricDescriptorText) {
                var arr = separateParagraphs(rubricDescriptorText);
                var cleanedText = '';
                for (var i in arr) {
                    cleanedText += (arr[i] + '\u000A\u000A');
                }
                return cleanedText.trim();
            }
        }

        //not quite what you want
        //this method should rather take it on per performance level instead
        // so it doesn't end up doing the additional iteration through the evidenceArray to pull out relevant PL's

        function mergeEvidenceToHtml(rubricDescriptorText, evaluations, evidenceCollection) {
            if (rubricDescriptorText) {
                rubricDescriptorText = standardizeHtml(rubricDescriptorText);
                var combinedString = evidenceToBinaryString(rubricDescriptorText, evaluations);
                switch (evidenceCollection.data.view) {
                    case 'default':
                        return highlightFromBinaryString(rubricDescriptorText, combinedString, evidenceCollection.data.view);
                    case 'merge':
                        var grouped = _.groupBy(evaluations, 'createdByUserId');
                        var evaluateeString = evidenceToBinaryString(rubricDescriptorText, grouped[evidenceCollection.data.evaluation.evaluatee.id]);
                        var evaluatorString = evidenceToBinaryString(rubricDescriptorText, grouped[evidenceCollection.data.evaluation.evaluator.id]);
                        combinedString = mergeToTrinaryString(evaluatorString, evaluateeString);
                        return highlightFromTrinaryString(rubricDescriptorText, combinedString, evidenceCollection.data.view);
                }
            }
        }

        function evidenceToBinaryString(rubricDescriptorText, evaluations) {
            var combinedBinaryString = createBinaryString(rubricDescriptorText, null);

            for (var i in evaluations) {
                var evidenceString = createBinaryString(rubricDescriptorText, evaluations[i].rubricStatement);
                combinedBinaryString = mergeBinaryString(combinedBinaryString, evidenceString);
            }
            return combinedBinaryString;
        }

        function createBinaryString(fullText, criticalText) {
            var binaryString = '';
            if (criticalText) {
                var startIndex = fullText.indexOf(criticalText);
                var endIndex;
                if (~startIndex) {
                    endIndex = startIndex + criticalText.length - 1;
                } else {
                    startIndex = 1;
                    endIndex = 0;
                }
            }
            for (var i = 0; i < fullText.length; i++) {
                if (i < startIndex || i > endIndex || !criticalText) {
                    binaryString += '0';
                } else {
                    binaryString += '1';
                }
            }
            return binaryString;
        }

        function mergeBinaryString(string1, string2) {
            //strings must be of equal length
            var binaryString = '';
            for (var i = 0; i < string1.length; i++) {
                if (string1[i] === '1' || string2[i] === '1') {
                    binaryString += 1;
                } else {
                    binaryString += 0;
                }
            }
            return binaryString;
        }

        function highlightFromBinaryString(actualText, binaryString, view) {
            //todo is not compatible with non-span browsers
            var highlightClose = '</span>';
            var html = '';
            var isHighlighted = -1;
            for (var i = 0; i < actualText.length; i++) {
                if (binaryString[i] === '1' && isHighlighted === -1) {
                    html = addCorrectHighlightTag(html, binaryString[i], view);
                    isHighlighted = isHighlighted * -1;
                } else if (binaryString[i] === '0' && isHighlighted === 1) {
                    html += highlightClose;
                    isHighlighted = isHighlighted * -1;
                }

                if (actualText.substring(i, i + 2) === '\u000A\u000A' && isHighlighted === 1) {
                    html += highlightClose + '</p><p>';
                    html = addCorrectHighlightTag(html, binaryString[i], view);
                    i += 1;
                } else if (actualText.substring(i, i + 2) === '\u000A\u000A' && isHighlighted === -1) {
                    html += '</p><p>';
                    i += 1;
                } else {
                    html += actualText[i];
                }
            }

            html = '<p>' + html + (isHighlighted === 1 ? highlightClose : '') + '</p>';
            return html;
        }

        function addCorrectHighlightTag(html, id, view) {
            var map = {
                '1': 'evaluator',
                '2': 'evaluatee',
                '3': 'combined'
            };
            var color = colorObj[view][map[id]];
            if (view === 'default') {
                color = 'yellow-highlight';
            }
            html += '<span class=\"' + color + '\">';

            return html;
        }

        function highlightFromTrinaryString(actualText, trinaryString, view) {
            var highlightClose = '</span>';
            var highlighter = '0';
            var isHighlighted = -1;
            var html = '';
            for (var i = 0; i < actualText.length; i++) {
                if (trinaryString[i] !== '0' && isHighlighted === -1) {
                    html = addCorrectHighlightTag(html, trinaryString[i], view);
                    highlighter = trinaryString[i];
                    isHighlighted = isHighlighted * -1;
                } else if (trinaryString[i] === '0' && isHighlighted === 1) {
                    html += highlightClose;
                    isHighlighted = isHighlighted * -1;
                } else if (isHighlighted === 1 && highlighter !== trinaryString[i]) {
                    html += highlightClose;
                    html = addCorrectHighlightTag(html, trinaryString[i], view);
                    highlighter = trinaryString[i];
                }

                if (actualText.substring(i, i + 2) === '\u000A\u000A' && isHighlighted === 1) {
                    html += highlightClose + '</p><p>';
                    html = addCorrectHighlightTag(html, trinaryString[i], view);
                    i += 1;
                } else if (actualText.substring(i, i + 2) === '\u000A\u000A' && isHighlighted === -1) {
                    html += '</p><p>';
                    i += 1;
                } else {
                    html += actualText[i];
                }
            }
            html = '<p>' + html + '</p>';
            return html;

        }

        function mergeToTrinaryString(evaluateeString, evaluatorString) {
            var trinaryString = '';
            for (var i = 0; i < evaluateeString.length; i++) {
                if (evaluateeString[i] === '1' && evaluatorString[i] === '1') {
                    trinaryString += '3';
                    //combined is 3
                } else if (evaluateeString[i] === '1') {
                    trinaryString += '1';
                    //evaluatee is 1
                } else if (evaluatorString[i] === '1') {
                    trinaryString += '2';
                    //evaluator is 2
                } else {
                    trinaryString += '0';
                }
            }
            return trinaryString;
        }

        function generateHighlightedRubricDescriptorTextFromEvaluations(performanceLevel, rubricDescriptorText, evaluationsArray) {
            //changing paragraph tags into \u000A
            rubricDescriptorText = standardizeHtml(rubricDescriptorText);

            var combinedBinaryString = createBinaryString(rubricDescriptorText);
            for (var i in evaluationsArray) {

                if (evaluationsArray[i].performanceLevel === performanceLevel) {
                    var evidenceBinaryString = createBinaryString(rubricDescriptorText, evaluationsArray[i].rubricStatement);
                    combinedBinaryString = mergeBinaryString(combinedBinaryString, evidenceBinaryString);
                }
            }
            return highlightFromBinaryString(rubricDescriptorText, combinedBinaryString);
        }

        function getFrameworkNodeForRubricRowId(frameworkNodes, rubricRowId) {
            for (var i = 0; i < frameworkNodes.length; ++i) {
                var rr = _.find(frameworkNodes[i].rubricRows, {id: rubricRowId});
                if (rr != null) {
                    return frameworkNodes[i];
                }
            }
            return null;
        }

        function getRubricRowById(frameworkNodes, rubricRowId) {
            for (var i = 0; i < frameworkNodes.length; ++i) {
                var rr = _.find(frameworkNodes[i].rubricRows, {id: rubricRowId});
                if (rr != null) {
                    return rr;
                }
            }

            return null;
        }

        function getStudentGrowthProcessRubricRow(frameworkNode) {
            return getStudentGrowthRubricRow(frameworkNode, true);
        }

        function getStudentGrowthResultsRubricRow(frameworkNode) {
            return getStudentGrowthRubricRow(frameworkNode, false);
        }

        function getStudentGrowthRubricRow(frameworkNode, dot1) {
            var rr;
            for (var i = 0; i < frameworkNode.rubricRows.length; ++i) {
                rr = frameworkNode.rubricRows[i];
                if (rr.isStudentGrowthAligned && rr.shortName.match(dot1 ? /.1/ : /.2/)) {
                    return rr;
                }
            }
            return null;
        }

        function getStudentGrowthFrameworkNodes(frameworkNodes) {
            var fn, rr;
            var fnArray = [];
            for (var i = 0; i < frameworkNodes.length; ++i) {
                for (var j = 0; j < frameworkNodes[i].rubricRows.length; ++j) {
                    rr = frameworkNodes[i].rubricRows[j];
                    if (rr.isStudentGrowthAligned) {
                        fnArray.push(frameworkNodes[i]);
                        break;
                    }
                }
            }
            return fnArray;
        }

        function getStudentGrowthRubricRows(studentGrowthFrameworkNodes) {
            var rr;
            var rrArray = [];
            for (var i = 0; i < studentGrowthFrameworkNodes.length; ++i) {
                for (var j = 0; j < studentGrowthFrameworkNodes[i].rubricRows.length; ++j) {
                    rr = studentGrowthFrameworkNodes[i].rubricRows[j];
                    if (rr.isStudentGrowthAligned) {
                        rrArray.push(rr);
                    }
                }
            }
            return rrArray;
        }

        function getStudentGrowthRubricRowsForFrameworkNode(studentGrowthFrameworkNode) {
            var rr;
            var rrArray = [];
            for (var j = 0; j < studentGrowthFrameworkNode.rubricRows.length; ++j) {
                rr = studentGrowthFrameworkNode.rubricRows[j];
                if (rr.isStudentGrowthAligned) {
                    rrArray.push(rr);
                }
            }
            return rrArray;
        }
    }
})
();
