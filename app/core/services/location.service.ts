/// <reference path="../core.module.ts" />

(function() {

    angular
        .module('stateeval.core')
        .factory('locationService', locationService);

    locationService.$inject = ['_', '$http', 'config', 'activeUserContextService'];

    /* @ngInject */
    function locationService(_, $http, config, activeUserContextService) {

        var service = {
            getSchoolsInDistrict: getSchoolsInDistrict,
            getSchoolsFromDistrict: getSchoolsFromDistrict,
            getDistricts: getDistricts
        };

        return service;

        ////////////////

        function getSchoolsInDistrict() {
            var districtCode = activeUserContextService.context.orientation.districtCode;
            var url = config.apiUrl + 'schoolsInDistrict/' + districtCode;
            return $http.get(url).then(function(response) {
                return response.data;
            })
        }

        function getSchoolsFromDistrict(code) {
            var url = config.apiUrl + 'schoolsInDistrict/' + code;
            return $http.get(url).then(function(response) {
                return response.data;
            })
        }

        function getDistricts() {
            var url = config.apiUrl + 'districts';
            return $http.get(url).then(function(response) {
                return response.data;
            })
        }

    }
})();

