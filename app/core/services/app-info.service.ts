/// <reference path="../core.module.ts" />

/**
 * Created by david on 12/10/2015.
 */
(function () {
    'use strict';

    angular
        .module('stateeval.core')
        .factory('appInfoService', appInfoService);

    appInfoService.$inject = ['$http', 'config'];

    /* @ngInject */
    function appInfoService($http, config) {

        var service = {
            getVersionString: getVersionString
        };

        return service;

        ////////////////

        function getVersionString() {
            return $http.get(config.apiUrl + '/Version')
                .then(function (response) {
                    return response.data;
                })
        }

    }

})();