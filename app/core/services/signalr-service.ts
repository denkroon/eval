﻿/// <reference path="../core.module.ts" />

(function() {
    'use strict';

    angular
        .module('stateeval.core')
        .factory('signalRService', signalRService);

    signalRService.$inject = ['$http', 'config', '$rootScope', 'activeUserContextService'];

    function signalRService($http, config, $rootScope, activeUserContextService) {
        var service = {
            initialize: initialize,
            sendRequest: sendRequest
        };

        function initialize(userId) {
            //var user = activeUserContextService.getActiveUser();
            if (userId) {

                var notificationHub = $.connection.notifierHub;

                notificationHub.on('broadcastMessage', function (message) {
                    $rootScope.$broadcast("notification-received", message);
                });

                notificationHub.on('sendMessage', function (message) {
                    $rootScope.$broadcast("notification-received", message);
                });


                $.connection.hub.qs = "userid=" + userId;
                //$.connection.hub.start().then(function(data) {
                //    //alert('connection done');
                //});


                $.connection.hub.start()
                    .done(function() {
                        console.log('Now connected, connection ID=' + $.connection.hub.id);
                    })
                    .fail(function() {
                        console.log('Could not Connect!');
                    });



            }
        };

        //initialize();

        function sendRequest() {
            //Invoking greetAll method defined in hub
            this.proxy.invoke('greetAll');
        };

        return service;
    }
})();