/// <reference path="../core.module.ts" />

/**
 * Created by anne on 6/26/2015.
 */

(function () {

    angular
        .module('stateeval.core')
        .factory('userService', userService);

    userService.$inject = ['enums', 'utils', '$http', 'config', 'activeUserContextService', 'localStorageService'];

    /* @ngInject */
    function userService(enums, utils, $http, config, activeUserContextService, localStorageService) {

        var service = {
            getUserById: getUserById,
            getEvaluateesForEvaluatorPR_PR: getEvaluateesForEvaluatorPR_PR,
            getEvaluateesForEvaluatorPR_TR: getEvaluateesForEvaluatorPR_TR,
            getEvaluateesForEvaluatorPR_LIB: getEvaluateesForEvaluatorPR_LIB,
            getEvaluateesForEvaluatorDE_PR: getEvaluateesForEvaluatorDE_PR,
            getObserveEvaluateesForDTEEvaluator: getObserveEvaluateesForDTEEvaluator,
            getPrincipalsInDistrict: getPrincipalsInDistrict,
            getHeadPrincipalsInDistrict: getHeadPrincipalsInDistrict,
            getUsersInRoleAtSchool: getUsersInRoleAtSchool,
            getUsersInRoleAtDistrict: getUsersInRoleAtDistrict,
            getUsersInDistrict: getUsersInDistrict,
            getOrientationsForUser: getOrientationsForUser,
            getEvaluatorsForImpersonation: getEvaluatorsForImpersonation,
            updateProfileImageUrl: updateProfileImageUrl,
            getNewMobileAccessKey: getNewMobileAccessKey,
            updateShowConfCodeConfirm: updateShowConfCodeConfirm,
            updateShowConfShareConfirm: updateShowConfShareConfirm,
            getEvaluatorsForDTE: getEvaluatorsForDTE
        };

        return service;

        ////////////////

        function getNewMobileAccessKey() {

            return $http.get(config.apiUrl + '/users/' + activeUserContextService.user.id + '/mobileaccesskey')
                .then(function (response) {
                    return response.data;
                })
        }

        function updateProfileImageUrl(url) {
            var request = {
                userId: activeUserContextService.user.id,
                profileImageUrl: url
            };

            return $http.post(config.apiUrl + '/users/profile', request)
                .then(function (response) {
                    activeUserContextService.user.profileImageUrl = url;
                    activeUserContextService.save();
                })
        }

        function updateShowConfCodeConfirm(activeUserContextService, value) {
            var request = {
                userId: activeUserContextService.user.id,
                showConfCodeConfirm: value
            };

            return $http.post(config.apiUrl + '/users/showConfCodeConfirm', request)
                .then(function (response) {
                    activeUserContextService.user.showConfCodeConfirm = value;
                    activeUserContextService.save();
                })
        }

        function updateShowConfShareConfirm(activeUserContextService, value) {
            var request = {
                userId: activeUserContextService.user.id,
                showConfShareConfirm: value
            };

            return $http.post(config.apiUrl + '/users/showConfShareConfirm', request)
                .then(function (response) {
                    activeUserContextService.user.showConfShareConfirm = value;
                    activeUserContextService.save();
                })
        }

        function getUserById(userId) {
            return $http.get(config.apiUrl + '/users/' + userId)
                .then(function (response) {
                    return response.data;
                })
        }

        function getEvaluatorsForImpersonation(schoolCode, roleName) {
            var request = getNewCoreRequest();
            request.schoolCode = schoolCode;
            request.roleName = roleName;

            var authenticatedUser = localStorageService.get('authenticatedUser');
            request.currentUserId = authenticatedUser ? authenticatedUser.id : activeUserContextService.user.id;

            var url = config.apiUrl + 'evaluatorsforimpersonation';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function getNewCoreRequest() {
            return {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.context.orientation.districtCode,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                evaluationType: activeUserContextService.context.workArea().evaluationType,
                roleName: utils.mapEvalTypeToEvaluateeRole(activeUserContextService.context.workArea().evaluationType),
                assignedOnly: false,
                includeEvalData: true,
                currentUserId: activeUserContextService.user.id
            };
        }

        function getEvaluateesInSchool() {
            var request = getNewCoreRequest();
            var url = config.apiUrl + 'users/evaluateesinschool/';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function getObserveEvaluateesForDTEEvaluator() {
            return getEvaluateesInSchool();
        }

        function getEvaluateesForEvaluatorDE_PR() {
            var request = getNewCoreRequest();
            var url = config.apiUrl + 'users/evaluatees/depr';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function getEvaluateesForEvaluatorPR_PR() {
            return getEvaluateesInSchool();
        }

        function getEvaluateesForEvaluatorPR_TR() {
            return getEvaluateesInSchool();
        }

        function getEvaluateesForEvaluatorPR_LIB() {
            return getEvaluateesInSchool();
        }

        function getUsersInRoleAtSchool(schoolCode, roleName) {
            var request = getNewCoreRequest();
            request.schoolCode = schoolCode;
            request.roleName = roleName;
            var url = config.apiUrl + 'ssersinroleatschool';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function getUsersInRoleAtDistrict(roleName) {
            var request = getNewCoreRequest();
            request.roleName = roleName;
            var url = config.apiUrl + '/usersinroleatdistrict';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function getUsersInDistrict(districtCode) {
            var request = {
                districtCode: districtCode
            };

            var url = config.apiUrl + '/usersindistrict';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function getPrincipalsInDistrict() {
            var request = getNewCoreRequest();
            request.schoolCode = '';
            request.roleName = enums.Roles.SESchoolPrincipal;
            var url = config.apiUrl + 'usersinroleindistrictbuildings';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });

        }

        function getHeadPrincipalsInDistrict() {
            var request = getNewCoreRequest();
            request.schoolCode = '';
            request.roleName = enums.Roles.SESchoolHeadPrincipal;
            var url = config.apiUrl + 'usersinroleindistrictbuildings';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function getOrientationsForUser(id) {
            var url = config.apiUrl + 'userorientation/' + id;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getEvaluatorsForDTE(){
            return getEvaluatorsForImpersonation('', enums.Roles.SEDistrictWideTeacherEvaluator);
        }
    }

})();

