/// <reference path="../core.module.ts" />

/**
 * Created by anne on 6/27/2015.
 */

(function () {

    angular
        .module('stateeval.core')
        .factory('frameworkService', frameworkService);

    /* @ngInject */
    frameworkService.$inject = ['$http', 'config', 'enums', 'rubricUtils', 'activeUserContextService'];
    function frameworkService($http, config, enums, rubricUtils, activeUserContextService) {

        var service = {
            getFrameworkContext: getFrameworkContext,
            getFrameworkContextWithoutActiveContext: getFrameworkContextWithoutActiveContext,
            getLoadedFrameworkContexts: getLoadedFrameworkContexts,
            getPrototypeFrameworkContexts: getPrototypeFrameworkContexts,
            getPrototypeFrameworkContextById: getPrototypeFrameworkContextById,
            updateFrameworkContext: updateFrameworkContext,
            loadFrameworkContext: loadFrameworkContext,
            mapEvaluationTypeToDisplayName: mapEvaluationTypeToDisplayName,
            updateFrameworkContextOptOut: updateFrameworkContextOptOut,
            getFrameworkContextOptOut: getFrameworkContextOptOut
        };

        return service;

        function createOptOutRequest(schoolYear, districtCode, evaluationType) {
            return {
                schoolYear: schoolYear,
                districtCode: districtCode,
                evaluationType: evaluationType,
                enable: false
            };
        }

        function getFrameworkContextOptOut(schoolYear, districtCode, evaluationType) {
            var request = createOptOutRequest(schoolYear, districtCode, evaluationType);
            var url = config.apiUrl + '/frameworkcontextoptout/';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function updateFrameworkContextOptOut(schoolYear, districtCode, evaluationType, enable) {
            var request = createOptOutRequest(schoolYear, districtCode, evaluationType);
            request.enable = enable;
            var url = config.apiUrl + '/frameworkcontextoptout/';
            return $http.put(url, request);
        }

        function mapEvaluationTypeToDisplayName(evalType) {
            switch(parseInt(evalType)) {
                case enums.EvaluationType.TEACHER:
                    return "Teacher Evaluations";
                case enums.EvaluationType.PRINCIPAL:
                    return "Principal Evaluations";
                default:
                    return "Unknown: " + evalType;
            }
        }
        function loadFrameworkContext(protoContextId, districtCode) {
            var url = config.apiUrl + '/loadframeworkcontext/' + districtCode + '/' + protoContextId;
            return $http.put(url, protoContextId);
        }

        function updateFrameworkContext(frameworkContext) {

            var params = {
                frameworkContextId: frameworkContext.id,
                frameworkViewType: frameworkContext.frameworkViewType,
                isActive: frameworkContext.isActive
            };

            var url = config.apiUrl + '/frameworkcontexts';
            return $http.put(url, params);
        }

        function getLoadedFrameworkContexts(schoolYear, districtCode) {
            var url = config.apiUrl + '/loadedframeworkcontexts/' + districtCode + '/' + schoolYear;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getPrototypeFrameworkContexts(schoolYear) {
            var url = config.apiUrl + '/protoframeworkcontexts/' + schoolYear;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getPrototypeFrameworkContextById(id) {
           var url = config.apiUrl + '/protoframeworkcontextbyid/' + id;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getFrameworkContextWithoutActiveContext(schoolYear, districtCode, evaluationType) {
            var params = {
                schoolYear: schoolYear,
                districtCode: districtCode,
                evaluationType: evaluationType
            };

            var url = config.apiUrl + '/frameworkcontexts';
            return $http.get(url, { params: params}).then(function (response) {
                return response.data;
            });
        }

        function getFrameworkContextInternal(evalType) {
            var params = {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.context.orientation.districtCode,
                evaluationType: evalType
            };

            var url = config.apiUrl + '/frameworkcontexts';
            return $http.get(url, { params: params}).then(function (response) {
                return response.data;
            });
        }

        function getFrameworkContext(evalType) {
            return getFrameworkContextInternal(evalType).then(function(frameworkContext) {
                frameworkContext.frameworks = [];
                frameworkContext.showStateFramework = (
                frameworkContext.frameworkViewType === enums.FrameworkViewType.STATE_FRAMEWORK_DEFAULT ||
                frameworkContext.frameworkViewType === enums.FrameworkViewType.STATE_FRAMEWORK_ONLY ||
                frameworkContext.frameworkViewType === enums.FrameworkViewType.INSTRUCTIONAL_FRAMEWORK_DEFAULT);

                frameworkContext.showInstructionalFramework = (
                frameworkContext.frameworkViewType === enums.FrameworkViewType.STATE_FRAMEWORK_DEFAULT ||
                frameworkContext.frameworkViewType === enums.FrameworkViewType.INSTRUCTIONAL_FRAMEWORK_ONLY ||
                frameworkContext.frameworkViewType === enums.FrameworkViewType.INSTRUCTIONAL_FRAMEWORK_DEFAULT);

                if (frameworkContext.stateFramework) {
                    frameworkContext.summativeEvaluationFramework = frameworkContext.stateFramework;
                    frameworkContext.studentGrowthFrameworkNodes =
                        rubricUtils.getStudentGrowthFrameworkNodes(frameworkContext.stateFramework.frameworkNodes);
                }

                frameworkContext.defaultFramework = frameworkContext.showInstructionalFramework?
                    frameworkContext.instructionalFramework:
                    frameworkContext.stateFramework;

                if (frameworkContext.stateFramework) {
                    frameworkContext.stateFramework.state = true;
                    frameworkContext.frameworks.push(frameworkContext.stateFramework);
                }

                if (frameworkContext.instructionalFramework) {

                    if (!frameworkContext.stateFramework) {
                        frameworkContext.summativeEvaluationFramework = frameworkContext.instructionalFramework;
                    }

                    frameworkContext.frameworks.push(frameworkContext.instructionalFramework);
                }

                return frameworkContext;
            });
        }
    }

})();

