/// <reference path="../core.module.ts" />

/**
 * Created by anne on 6/26/2015.
 */

(function() {

    angular
        .module('stateeval.core')
        .factory('apiInterceptor', apiInterceptor);

    apiInterceptor.$inject = ['$q', '$rootScope', 'activeUserContextService'];

    /* @ngInject */
    function apiInterceptor($q, $rootScope, activeUserContextService) {

        return {
            responseError: function error(response) {
                if(!response.data) {
                    throw new Error('No Server Running');
                }
                if (response.config.skipInterceptors) return $q.reject(response);
                var message = response.headers('status-text') || 'An unexpected error occurred.';
                message += ' STATUS: ' + response.status;
                message += ' URL: ' + response.config.url;
                message += ' MESSAGE: ' + response.data.message;
                message += ' DETAIL: ' + (response.status === 500? response.data.exceptionMessage: response.data.messageDetail);

                $rootScope.$broadcast('server-error', { message: message, status: response.status });
                return $q.reject(response);
            },
            request: function($config) {
                if(activeUserContextService.context)
                {
                    $config.headers['orientation'] = JSON.stringify(activeUserContextService.context.orientation);
                }
                return $config;
            }
        };
    }

})();

