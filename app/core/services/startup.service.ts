/// <reference path="../core.module.ts" />

(function () {

    angular
        .module('stateeval.core')
        .factory('startupService', startupService);

    startupService.$inject = ['activeUserContextService', 'workAreaService', 'enums', 'localStorageService', '$q', 'evidenceCollectionService',
        'config', '$state', '_'];

    function startupService(activeUserContextService, workAreaService, enums, localStorageService, $q, evidenceCollectionService, config, $state, _) {

        var Context = function () {
        };
        Context.prototype = {
            changeWorkArea: function(tag) {
                var workArea = workAreaService[tag];
                return workArea.initializeWorkArea(activeUserContextService, service);
            },
            evaluationType: function() {
                return this.workArea().evaluationType;
            },
            workArea: function () {
                return workAreaService[this.orientation.workAreaTag];
            },
            isEvaluating: function () {
                return this.workArea().isEvaluatorWorkArea() && (this.evaluatee);
            },
            isEvaluatee: function () {
                return this.evaluatee && (this.evaluatee.id === activeUserContextService.user.id);
            },
            isAssignedEvaluator: function () {
                return this.evaluatee.evalData.evaluatorId === activeUserContextService.user.id;
            },
            isImpersonating: function () {
                return !!localStorageService.get('authenticatedContext');
            },
            // assumes active work-area has an active evaluatee
            evaluationIsReadOnly: function () {
                return this.isImpersonating() || (this.evaluatee && this.evaluatee.evalData.wfState > enums.WfState.DRAFT);
            },
            observationIsReadOnly: function (observation) {
                return  this.evaluationIsReadOnly() ||
                    observation.wfState >= enums.WfState.OBS_LOCKED_TEE_REVIEW ||
                    (observation.evaluateeId ===activeUserContextService.user.id && observation.selfEvalComplete) ||
                        // not the evaluator && !(the evaluatee and it's shared)
                    ((observation.evaluatorId != activeUserContextService.user.id) &&
                    !(observation.evaluateeId ===activeUserContextService.user.id && observation.selfEvalShared));
            },
            observationConferenceIsReadOnly: function (observation) {
                return  this.evaluationIsReadOnly() ||
                    observation.wfState >= enums.WfState.OBS_LOCKED_TEE_REVIEW ||
                    (observation.evaluatorId != activeUserContextService.user.id &&
                        observation.evaluateeId != activeUserContextService.user.id);
            },
            observationSetupIsReadOnly: function (observation) {
                return  this.evaluationIsReadOnly() ||
                    observation.wfState >= enums.WfState.OBS_LOCKED_TEE_REVIEW ||
                    observation.evaluatorId != activeUserContextService.user.id;
            },
            selfAssessmentSetupIsReadOnly: function (selfAssessment) {
                return  this.evaluationIsReadOnly() ||
                        selfAssessment.evaluateeId != activeUserContextService.user.id;
            },
            goalBundleIsReadOnly: function (bundle) {
                return  this.evaluationIsReadOnly();
            },
            artifactIsReadOnly: function (artifact) {
                return this.evaluationIsReadOnly() || artifact.createdByUserId != activeUserContextService.user.id;
            },
            selfAssessmentIsReadOnly: function (selfAssessment) {
                return this.evaluationIsReadOnly() || selfAssessment.evaluateeId != activeUserContextService.user.id;
            },
            getRolesForCurrentLocation: function () {
                var list = [];
                var currentLocation = this.orientationOptions[this.orientation.schoolYear][this.orientation.districtName][this.orientation.schoolName];
                for(var i in currentLocation) {
                    list.push(currentLocation[i].roleName);
                }
                list = _.uniq(list);

                var sp = list.indexOf('SESchoolPrincipal');
                var hp = list.indexOf('SESchoolHeadPrincipal');
                if(~sp && ~hp) {
                    list.splice(sp, 1);
                }
                return list;
            }
        };

        var User = function () {
        };

        var userDefaults = {
            schoolYear: config.schoolYear,
            districtName: undefined,
            schoolName: undefined,
            workAreaTag: undefined
        };
        var workAreaPriority = {
            'DA_TR': 0,
            'DA_PR': 1,
            'DTE': 2,
            'DV': 3,
            'DAM_TR': 4,
            'DAM_PR': 5,
            'PR_PR': 6,
            'PR_TR': 7,
            'PR_ME': 8,
            'TR_ME': 9,
            'SA_TR': 10,
            'SA_PR': 11,
            'IMP': 12
        };

        var loginUserData = {};
        var order = enums.UserOrientationPropOrder;

        var service = {
        Context: Context,
            setupContext: setupContext,
            setUser: setUser,
            load: load,
            contextCreation: contextCreation,
            changeEvaluationTypesToWorkAreaTags: changeEvaluationTypesToWorkAreaTags,
            defaultNavOption: defaultNavOption,
            getRolesForCurrentLocation: getRolesForCurrentLocation

        };
        return service;

        function getRolesForCurrentLocation(context) {
            var list = [];
            var currentLocation = context.orientationOptions[context.orientation.schoolYear][context.orientation.districtName][context.orientation.schoolName];
            for(var i in currentLocation) {
                list.push(currentLocation[i].roleName);
            }
            list = _.uniq(list);

            var sp = list.indexOf('SESchoolPrincipal');
            var hp = list.indexOf('SESchoolHeadPrincipal');
            if(~sp && ~hp) {
                list.splice(sp, 1);
            }
            return list;
        }

        //loads user into application from login screen
        function setUser(userTemp) {
            if (userTemp) {
                loginUserData = userTemp;
                var user = new User();
                for(var i in loginUserData) {
                    user[i] = loginUserData[i];
                }
                activeUserContextService.user = user;
                localStorageService.set('entryUser', user);
            } else {
                console.log('Received No User');
            }
        }

        //Builds ORIENTATION_OPTIONS object and sets default workArea on the context and user objects and puts them on AUCS
        function changeEvaluationTypesToWorkAreaTags(locationRoles) {
            var list = [];
            for (var i in locationRoles) {
                var tags = workAreaService.getTagsOfRole(locationRoles[i].roleName, locationRoles[i].evaluationType);
                for (var j in tags) {
                    var lr = _.clone(locationRoles[i]);
                    lr.workAreaTag = tags[j].tag;
                    delete lr.evaluationType;
                    list.push(lr);
                }
            }
            return list;
        }

        function createOrientation(index, ungrouped) {
            if (!order[index]) {
                return ungrouped[0];
            }
            var grouped = _.groupBy(ungrouped, order[index]);
            for (var i in grouped) {
                grouped[i] = createOrientation(index + 1, grouped[i]);
            }
            return grouped;
        }

        function defaultNavOption(index, current, obj) {

            var level = userDefaults[order[index]];
            if (level && current[level]) {
                //if there is a user default in this category use it
                obj[order[index]] = level;
            } else {
                //first attribute of current order
                obj[order[index]] = Object.keys(current)[0];
            }
            if (order[index + 1]) {
                defaultNavOption(index + 1, current[obj[order[index]]], obj);
            }
            return obj
        }

        function setupContext(tag) {
            if (!activeUserContextService.user) {
                console.log('No login or cached information.');
                $state.go('login');
                return $q.when();
            }
            var context = contextCreation(activeUserContextService.user, undefined);

                context.changeWorkArea(tag || context.workArea().tag)
                .then(function () {
                    activeUserContextService.save();
                });
        }

        function contextCreation(userData, premadeOrientations) {
            var context = new Context();
            var user = new User();
            var userOrientations = premadeOrientations || changeEvaluationTypesToWorkAreaTags(userData.userOrientations);

            userDefaults.workAreaTag = userOrientations[0].workAreaTag;

            //where the tag with the most priority is chosen

            for (var i in userOrientations) {
                if (workAreaPriority[userOrientations[i].workAreaTag] < workAreaPriority[userDefaults.workAreaTag]) {
                    userDefaults.workAreaTag = userOrientations[i].workAreaTag;
                }
            }
            context.orientationOptions = createOrientation(0, userOrientations);
            var dNO = defaultNavOption(0, context.orientationOptions, {});
            var y = _.uniq(_.pluck(userData.userOrientations, 'schoolYear')).length;
            var d = _.uniq(_.pluck(userData.userOrientations, 'districtName')).length;
            var s = _.uniq(_.pluck(userData.userOrientations, 'schoolName')).length;
            context.showOptions = !(y === 1 && d === 1 && s === 1);

            //NavOption is the only thing that remembers the users state, using navOption to traverse through
            // orientation will find you how the user is oriented
            //to find current execute context.navOption = opt;
            context.additionalWorkAreas = ['TRAIN'];
            context.orientation = activeUserContextService.getOrientationWithNav(0, context.orientationOptions, dNO);
            context.navOptions = dNO;
            context.frameworkContexts = [];
            // todo: change this to true as we approach release, for now it's causing a problem since
            // PR1 test user is the most common PR to use, but he doesn't have any assigned teachers
            context.assignedEvaluatees = true;
            for(var i in userData) {
                user[i] = userData[i];
            }
            activeUserContextService.user = user;
            activeUserContextService.context = context;
            return context;
        }

        function load(con, use) {
            var contextTemp = localStorageService.get(con);
            var context = new Context();
            var user = new User();
            if (!contextTemp) {
                console.log('No cached data');
                $state.go('login');
            } else {
                var userTemp = localStorageService.get(use);
                for (var i in contextTemp) {
                    context[i] = contextTemp[i];
                }
                for (var i in userTemp) {
                    user[i] = userTemp[i];
                }
                activeUserContextService.user = user;
                activeUserContextService.context = context;
                //workAreaService.impersonateTitleMapper(context.orientation.roleName);

            }
            return $q.when();

        }


    }
})();