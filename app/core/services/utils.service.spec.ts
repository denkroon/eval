/// <reference path="../core.module.ts" />


describe('getResponseWithoutRemoveCodeSelection', function() {

    var utils;

    var before = '<p><span clientid="8575f456-4273-43e4-940b-e7bc5944a9bb" class="editor-selected"><span class="node-desc">1a</span>Line1<span class="node-desc-end">1a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'8575f456-4273-43e4-940b-e7bc5944a9bb\')" title="Remove"></span></span></p><p><span clientid="76b41a0f-a9fc-4ccc-ba07-c7b5b373870f" class="editor-selected"><span class="node-desc">1b</span>Line2<span class="node-desc-end">1b</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'76b41a0f-a9fc-4ccc-ba07-c7b5b373870f\')" title="Remove"></span></span></p><p><span clientid="558558af-05e6-4160-aaa5-892ae0df440c" class="editor-selected"><span class="node-desc">1c</span>Line3<span class="node-desc-end">1c</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'558558af-05e6-4160-aaa5-892ae0df440c\')" title="Remove"></span></span></p>';

    beforeEach(() => {
        angular.mock.module('stateeval.core');
    });

    beforeEach(inject((
        _utils_
    ) => {
      utils = _utils_;
    }));

    it('make sure the remove selection class changed', function() {
        var classToRemove = 'remove-code-selection';
        expect(before.indexOf(classToRemove)).toBeGreaterThan(0);
     });

    it('getResponseWithoutRemoveCodeSelection: make sure only element with remove class is removed', function() {
        var result = utils.getResponseWithoutRemoveCodeSelection(before);
        expect(result.indexOf('remove-code-selection')).toEqual(-1);
        expect(result.indexOf('1a</span>Line1')).toBeGreaterThan(0);
        expect(result.indexOf('1b</span>Line2')).toBeGreaterThan(0);
        expect(result.indexOf('1c</span>Line3')).toBeGreaterThan(0);
    });
});

describe('getTextWithoutCoding', function() {

    var utils;

    var before = '<p><span clientid="8575f456-4273-43e4-940b-e7bc5944a9bb" class="editor-selected"><span class="node-desc">1a</span>Line1<span class="node-desc-end">1a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'8575f456-4273-43e4-940b-e7bc5944a9bb\')" title="Remove"></span></span></p><p><span clientid="76b41a0f-a9fc-4ccc-ba07-c7b5b373870f" class="editor-selected"><span class="node-desc">1b</span>Line2<span class="node-desc-end">1b</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'76b41a0f-a9fc-4ccc-ba07-c7b5b373870f\')" title="Remove"></span></span></p><p><span clientid="558558af-05e6-4160-aaa5-892ae0df440c" class="editor-selected"><span class="node-desc">1c</span>Line3<span class="node-desc-end">1c</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'558558af-05e6-4160-aaa5-892ae0df440c\')" title="Remove"></span></span></p>';

    beforeEach(() => {
        angular.mock.module('stateeval.core');
    });

    beforeEach(inject((
        _utils_
    ) => {
        utils = _utils_;
    }));

    it('make sure only coding elements are removed', function() {

        expect(before.indexOf('node-desc')).toBeGreaterThan(0);
        expect(before.indexOf('remove-code-selection')).toBeGreaterThan(0);
        expect(before.indexOf('removeRubricCode')).toBeGreaterThan(0);

        var result = utils.getTextWithoutCoding(before);
        expect(result.indexOf('node-desc')).toEqual(-1);
        expect(result.indexOf('remove-code-selection')).toEqual(-1);
        expect(result.indexOf('removeRubricCode')).toEqual(-1);
    });
});

describe('getTextWithShortName', function() {

    var utils;

    var test1 = '<p><span clientid="3bd57217-7080-43b6-a764-6d98cad1f15c" class="editor-selected"><span class="node-desc">1a</span>Line1<span class="node-desc-end">1a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'3bd57217-7080-43b6-a764-6d98cad1f15c\')" title="Remove"></span></span></p><p><span clientid="d6167612-bd84-47d5-819b-b4c9c02b6cf8" class="editor-selected"><span class="node-desc">1b</span>Line2<span class="node-desc-end">1b</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'d6167612-bd84-47d5-819b-b4c9c02b6cf8\')" title="Remove"></span></span></p><p><span clientid="1a076129-7609-4937-befe-bd04d1ca4280" class="editor-selected"><span class="node-desc">1a</span>Line3<span class="node-desc-end">1a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'1a076129-7609-4937-befe-bd04d1ca4280\')" title="Remove"></span></span></p>';

    // test2 has nested 1a1b1c
    var test2 = '<p><span style="font-family: arial, helvetica, sans-serif; font-size: 12px; line-height: normal; text-align: justify;"><span clientid="41a5531a-245a-4e74-9481-08539bf13bd4" class="editor-selected"><span class="node-desc">1a</span><span clientid="06e554a8-f6af-47d3-a62b-806d07397432" class="editor-selected"><span class="node-desc">1b</span><span clientid="809b503b-79bc-4555-b33e-4386b5f97523" class="editor-selected"><span class="node-desc">1c</span>Lorem ipsum dolor sit amet<span class="node-desc-end">1c</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'809b503b-79bc-4555-b33e-4386b5f97523\')" title="Remove"></span></span><span class="node-desc-end">1b</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'06e554a8-f6af-47d3-a62b-806d07397432\')" title="Remove"></span></span><span class="node-desc-end">1a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'41a5531a-245a-4e74-9481-08539bf13bd4\')" title="Remove"></span></span>, sodales ae.</span><br></p>';

    // contains 4a3a2a1a <some text> 1a2a3a4a
    var test3 = '<p>Reply from <span class="editor-selected" clientid="551e817c-9204-4fae-93e9-2f8caeef7b04"><span class="node-desc">4a</span><span class="editor-selected" clientid="b51e8859-6c74-4e0a-87b2-a585a5cd344e"><span class="node-desc">3a</span><span class="editor-selected" clientid="220e4e28-5717-4257-91bb-18eaabb9f990"><span class="node-desc">2a</span><span class="editor-selected" clientid="e76ea7d8-6d66-4a6a-8b9a-55efc5273908"><span class="node-desc">1a</span>teacher for "Teacher Pre-Conference Summary<span class="node-desc-end">1a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'e76ea7d8-6d66-4a6a-8b9a-55efc5273908\')" title="Remove"></span></span><span class="node-desc-end">2a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'220e4e28-5717-4257-91bb-18eaabb9f990\')" title="Remove"></span></span><span class="node-desc-end">3a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'b51e8859-6c74-4e0a-87b2-a585a5cd344e\')" title="Remove"></span></span><span class="node-desc-end">4a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'551e817c-9204-4fae-93e9-2f8caeef7b04\')" title="Remove"></span></span>"</p><p><br></p>';

    beforeEach(() => {
        angular.mock.module('stateeval.core');
    });

    beforeEach(inject((
        _utils_
    ) => {
        utils = _utils_;
    }));

    it('should get both 1a', function() {
        var result = utils.getTextWithShortName(test1, '1a');
        expect(result.indexOf('1a</span>Line1')).toBeGreaterThan(0);
        expect(result.indexOf('1a</span>Line3')).toBeGreaterThan(0);
    });

    it('should not get 1b', function() {
        var result = utils.getTextWithShortName(test1, '1a');
        expect(result.indexOf('1b</span>Line2')).toEqual(-1);
    });

    it('nested should only get 1a', function() {
        var result = utils.getTextWithShortName(test2, '1a');
        expect(result.indexOf('1b</span>')).toEqual(-1);
        expect(result.indexOf('1c</span>')).toEqual(-1);
    });

    it('nested should only get 1b', function() {
        var result = utils.getTextWithShortName(test2, '1b');
        expect(result.indexOf('1a</span>')).toEqual(-1);
        expect(result.indexOf('1c</span>')).toEqual(-1);
    });

    it('nested should only get 1c', function() {
        var result = utils.getTextWithShortName(test2, '1c');
        expect(result.indexOf('1a</span>')).toEqual(-1);
        expect(result.indexOf('1b</span>')).toEqual(-1);
    });

    it('nested should only get 4a', function() {
        var result = utils.getTextWithShortName(test3, '4a');
        console.log(angular.mock.dump(result));
        expect(result.indexOf('3a</span>')).toEqual(-1);
        expect(result.indexOf('2a</span>')).toEqual(-1);
        expect(result.indexOf('1a</span>')).toEqual(-1);
    });
});

describe('removeAnnotation', function() {

    var utils;
    var uuid = '3bd57217-7080-43b6-a764-6d98cad1f15c';
    var before = '<p><span clientid="3bd57217-7080-43b6-a764-6d98cad1f15c" class="editor-selected"><span class="node-desc">1a</span>Line1<span class="node-desc-end">1a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'3bd57217-7080-43b6-a764-6d98cad1f15c\')" title="Remove"></span></span></p><p><span clientid="d6167612-bd84-47d5-819b-b4c9c02b6cf8" class="editor-selected"><span class="node-desc">1b</span>Line2<span class="node-desc-end">1b</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'d6167612-bd84-47d5-819b-b4c9c02b6cf8\')" title="Remove"></span></span></p><p><span clientid="1a076129-7609-4937-befe-bd04d1ca4280" class="editor-selected"><span class="node-desc">1a</span>Line3<span class="node-desc-end">1a</span><span class="glyphicon glyphicon-remove remove-code-selection" onclick="removeRubricCode(\'1a076129-7609-4937-befe-bd04d1ca4280\')" title="Remove"></span></span></p>';


    beforeEach(() => {
        angular.mock.module('stateeval.core');
    });

    beforeEach(inject((
        _utils_
    ) => {
        utils = _utils_;
    }));

    it('should only remove the first 1a only', function() {
        var result = utils.removeAnnotation(before, uuid);
        expect(result.indexOf('1a</span>Line1')).toEqual(-1);
        expect(result.indexOf('1b</span>Line2')).toBeGreaterThan(0);
        expect(result.indexOf('1a</span>Line3')).toBeGreaterThan(0);
    });

    it('should remove the clientId from the html', function() {
        var result = utils.removeAnnotation(before, uuid);
        expect(result.indexOf(uuid)).toEqual(-1);
    });
});


