/// <reference path="../core.module.ts" />

/**
 * Created by anne on 6/26/2015.
 */

(function () {

    angular
        .module('stateeval.core')
        .factory('activeUserContextService', activeUserContextService);

    activeUserContextService.$inject = ['_', 'enums', 'localStorageService',
        'utils', '$rootScope', '$sce'];

    /* @ngInject */
    function activeUserContextService(_, enums, localStorageService,
                  utils, $rootScope, $sce) {


        var service = {
            //All fields on the user and context objects can be directly accessed and modified in other services
            //or controllers, setting anything directly with user= or context= will create a new object unbinding
            //the other services and controllers from the activeUserContextService
            save: save,

            getActiveUser: getActiveUser,
            getActiveEvaluatee: getActiveEvaluatee,
            getEvaluateesForActiveUser: getEvaluateesForActiveUser,

            getEvaluatorDisplayName: getEvaluatorDisplayName,

            getActiveDistrictCode: getActiveDistrictCode,
            getActiveSchoolCode: getActiveSchoolCode,
            getActiveDistrictName: getActiveDistrictName,
            getActiveSchoolName: getActiveSchoolName,

            getFrameworkContext: getFrameworkContext,

            getActiveFramework: getActiveFramework,
            getStateFramework: getStateFramework,

            setActiveFramework: setActiveFramework,

            // utilities
            getRolesDisplayStringForActiveUser: getRolesDisplayStringForActiveUser,
            getRolesDisplayStringForLoggedInUser: getRolesDisplayStringForLoggedInUser,
            getEvaluateeTermUpperCase: getEvaluateeTermUpperCase,
            getEvaluateeTermLowerCase: getEvaluateeTermLowerCase,

    /*        getImpersonatedUser: getImpersonatedUser,
            getLoggedInUser: getLoggedInUser,
*/
            clear: clear,
            selfPrint: selfPrint,
            getOrientationWithNav: getOrientationWithNav
        };

        service.user = {};
        service.context = {};

        return service;

        function selfPrint() {
            console.log(service.context);
        }


        //user: {
        //        NOT KEEPING LOCATION ROLES OR HAS MULTIPLE BUILDINGS
        //        id,
        //        firstName,
        //        lastName,
        //        displayName,
        //        evaluationId,
        //        schoolCode,
        //        schoolName,
        //        districtCode,
        //        districtName,
        //        role,
        //        evalData
        //},
        //context = {
        //    orientationOptions: { -
        //        years: {,
        //          districtCode: {
        //              schools: {
        //                  roles: {
        //                      workAreaTitle: {
        //                          workAreaTag,
        //                          schoolName,
        //                          schoolCode,
        //                          districtName,
        //                          districtCode,
        //                          roleName,
        //                          schoolYear
        //    },
        //    orientation = current low branch of orientation options, updated once per change work area
        //    frameworkContext: {} - resultingObject from frameworkService.getFrameworkContext()
        //    framework - set to frameworkContext.defaultFramework
        //    frameworkContexts: [] - holds on to a list of used frameworks
        //    evaluator
        //    evaluatee
        //    evaluatees,
        //}
        //Context.prototype {
        //    workArea(),
        //    isEvaluating,
        //    isEvaluator,
        //}

        function getOrientationWithNav(index, current, navObj) {
            var nextDown = current[navObj[enums.UserOrientationPropOrder[index]]];
            if(enums.UserOrientationPropOrder[index + 1]) {
                return getOrientationWithNav(index + 1, nextDown, navObj)
            } else {
                if (current[navObj[enums.UserOrientationPropOrder[index]]]) {
                    return current[navObj[enums.UserOrientationPropOrder[index]]];
                } else {
                    current = current[Object.keys(current)[0]];
                    current.workAreaTag = navObj.workAreaTag;
                    return current;
                }
            }
        }

        function getActiveEvaluatee() {
            return service.context.evaluatee;
        }

        function getActiveUser() {
            return service.user
        }

       /* function getLoggedInUser() {
            return service.user;
        }*/

        function getActiveDistrictName() {
            return service.context.orientation.districtName;
        }

        function getActiveSchoolName() {
            return service.context.orientation.schoolName;
        }

        function getStateFramework() {
            return service.context.frameworkContext.stateFramework;
        }

        function getActiveFramework() {
            return service.context.framework;
        }

        function setActiveFramework(framework) {
            service.context.framework = framework;
            console.log('Changing framework', framework);
            $rootScope.$broadcast('change-framework');
            save();
        }

        //clears all context and user info by property iteration
        function clear() {
            localStorageService.set('context', null);
            localStorageService.set('user', null);
            localStorageService.set('authenticatedUser', null);
            localStorageService.set('authenticatedContext', null);
            for (var i in service.context) {
                delete service.context[i];
            }
            for (var i in service.user) {
                delete service.user[i];
            }
        }

        //Saves to local storage (user, context)
        function save() {
            localStorageService.set('context', service.context);
            localStorageService.set('user', service.user);

        }

        function getActiveDistrictCode() {
            return service.context.orientation.districtCode;
        }

        function getActiveSchoolCode() {
            return service.context.orientation.schoolCode;
        }

        function getEvaluateesForActiveUser() {
            return service.context.evaluatees;
        }

        function getEvaluatorDisplayName() {
            if (service.context.evaluator === null) {
                return $sce.trustAsHtml('<span class="label label-warning">NOT SET</span>');
            }
            else {
                return $sce.trustAsHtml(service.context.evaluator.displayName);
            }
        }

        function getFrameworkContext() {
            return service.context.frameworkContext;
        }

        function getRolesDisplayStringForActiveUser() {
            return utils.mapRoleNameToFriendlyName(service.context.orientation.role);
        }

        function getRolesDisplayStringForLoggedInUser() {
            //todo: should be logged in user
            return utils.mapRoleNameToFriendlyName(service.context.orientation.role);
        }

        function getEvaluateeTermLowerCase() {
            return utils.getEvaluateeTermLowerCase(service.context.workArea().evaluationType);
        }

        function getEvaluateeTermUpperCase() {
            return utils.getEvaluateeTermUpperCase(service.context.workArea().evaluationType);
        }
    }
})
();

