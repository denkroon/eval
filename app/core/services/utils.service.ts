/// <reference path="../core.module.ts" />

/**
 * Created by anne on 6/26/2015.
 */

(function() {
    'use strict';

    angular
        .module('stateeval.core')
        .factory('utils', utils);

    utils.$inject = ['enums', '$sce', '_'];

    function utils(enums, $sce, _) {

        var service = {
           getStudentGrowthFocusFrameworkNode: getStudentGrowthFocusFrameworkNode,
            mapEvalTypeToFriendlyRoleName:mapEvalTypeToFriendlyRoleName,
            mapWfStateToString: mapWfStateToString,

            mapLibItemTypeToString: mapLibItemTypeToString,
            mapEvalRequestTypeToString: mapEvalRequestTypeToString,
            mapEvalRequestStatusToString: mapEvalRequestStatusToString,
            mapEvaluationPlanTypeToString: mapEvaluationPlanTypeToString,
            mapEvaluationPlanTypeForUserToString: mapEvaluationPlanTypeForUserToString,
            mapEvaluationPlanTypeToStringCondensed: mapEvaluationPlanTypeToStringCondensed,
            mapEvaluationFocusToString: mapEvaluationFocusToString,
            mapEvaluationFocusForUserToString: mapEvaluationFocusForUserToString,
            mapLastYearEvaluationFocusForUserToString: mapLastYearEvaluationFocusForUserToString,
            mapRoleNameToFriendlyName: mapRoleNameToFriendlyName,
            mapFrameworkViewTypeToString: mapFrameworkViewTypeToString,
            getSafeHtml: getSafeHtml,
            mapEventTypeToItemType: mapEventTypeToItemType,
            mapEvidenceCollectionTypeToFullString: mapEvidenceCollectionTypeToFullString,
            getEvaluateeTermLowerCase: getEvaluateeTermLowerCase,
            getEvaluateeTermUpperCase: getEvaluateeTermUpperCase,
            stripParameters: stripParameters,
            getSelectedText: getSelectedText,
            getTextWithoutCoding: getTextWithoutCoding,
            getTextWithShortName:getTextWithShortName,
            getRole: getRole,
            getImpersonationSchool: getImpersonationSchool,
            getSchoolYear: getSchoolYear,
            getNewGuid:getNewGuid,
            mapPromptTypeToString: mapPromptTypeToString,
            removeAnnotation:removeAnnotation,
            stripGrowthGoalsRow: stripGrowthGoalsRow,
            mapEvalTypeToEvaluateeRole: mapEvalTypeToEvaluateeRole,
            appendScript: appendScript,
            getRandomSessionDigits: getRandomSessionDigits,
            Link: Link,
            containsCoding: containsCoding,
            getResponseWithoutRemoveCodeSelection: getResponseWithoutRemoveCodeSelection,
            uiRouterString: uiRouterString,
            wiggleEvaluatingDropdown: wiggleEvaluatingDropdown,
            identity: identity,
            returnFunction: returnFunction,
            getEncodedText: getEncodedText,
            getEvalLogo: getEvalLogo,
            gotoEvidenceCollection: gotoEvidenceCollection

        };

        return service;

        function mapSenderWorkAreaToRecipientWorkArea(event) {
            switch (event.workAreaTag) {
                case 'PR_PR':
                    return 'PR_ME';
                case 'PR_TR':
                case 'DTE_TR':
                    return 'TR_ME';
                case 'TR_ME':
                    return event.schoolCode?'PR_TR':'DTE_TR';
                case 'PR_ME':
                    return 'DE_PR';
            }
        }

        function gotoEvidenceCollection(event, activeUserContextService, startupService, $state) {

            if (event.schoolCode !== activeUserContextService.context.orientation.schoolCode) {
                var tag = mapSenderWorkAreaToRecipientWorkArea(event);
                startupService.setupContext(tag);
                // todo: can we go to a specific page?
            }
            else {
                if (event.evidenceCollectionType === enums.EvidenceCollectionType.OBSERVATION) {
                    $state.go('observation-setup', {observationId: event.evidenceCollectionObjectId});
                }
                else if (event.evidenceCollectionType === enums.EvidenceCollectionType.SELF_ASSESSMENT) {
                    $state.go('self-assessment-setup', {id: event.evidenceCollectionObjectId});
                }
                else if (event.evidenceCollectionType === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) {
                    $state.go('sg-bundle-summary', {id: event.evidenceCollectionObjectId});
                }
                else if (event.evidenceCollectionType === enums.EvidenceCollectionType.OTHER_EVIDENCE) {
                    $state.go('ytd-evidence');
                }
            }
        }
        function getEvalLogo() {
            return 'images/eval_logo.jpg';
        }
        function getEncodedText(clientId: string, text: string) {
            var div = $(document.createElement('DIV'));
            div.html(text);
            return div.find(`[clientid=${clientId}]`).text();
        }

        function mapWfStateToString(wfState) {

        }

        function returnFunction(variable) {
            return function() {
                return variable
            }
        }

        function identity(variable) {
            return variable;
        }
        function wiggleEvaluatingDropdown() {
            $(document.body).addClass('no-evaluatee');
            setTimeout(function() {$(document.body).removeClass('no-evaluatee');}, 600);
        }

        function uiRouterString(state) {
            var regex = /\(.*\)/;
            var parens = regex.exec(state) && regex.exec(state)[0] || '';
            var paramsString = parens.substring(1, parens.length - 1);
            var params = JSON.parse(paramsString || '{}');
            var destination = state.replace(regex, '');
            return {
                state: destination,
                params: params
            }
        }

        function containsCoding(text) {
            return !text?false:text.toUpperCase().indexOf('CLIENTID')!=-1;
        }

        function Link(title, state, params, data, disabled) {
            this.title = title;
            this.state = state;
            this.params = params;
            this.data = data;
            this.disabled = disabled;
        }

        function getRandomSessionDigits(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }

        function appendScript(pathToScript) {
            var head = document.getElementsByTagName("head")[0];
            var js = document.createElement("script");
            js.type = "text/javascript";
            js.src = pathToScript;
            head.appendChild(js);
        }

        function mapEvalTypeToFriendlyRoleName(evalType) {
            return mapRoleNameToFriendlyName(mapEvalTypeToEvaluateeRole((evalType)));
        }

        function mapEvalTypeToEvaluateeRole(evalType) {
            switch (evalType) {
                case enums.EvaluationType.LIBRARIAN:
                    return enums.Roles.SESchoolLibrarian;
                    break;
                case enums.EvaluationType.PRINCIPAL:
                    return enums.Roles.SESchoolPrincipal;
                    break;
                case enums.EvaluationType.TEACHER:
                    return enums.Roles.SESchoolTeacher;
                    break;
            }
        }

        function stripGrowthGoalsRow(current, evidenceCollection) {
            return (!(current.isStudentGrowthAligned &&
            (evidenceCollection.request.collectionType === enums.EvidenceCollectionType.OBSERVATION ||
            evidenceCollection.request.collectionType === enums.EvidenceCollectionType.SELF_ASSESSMENT ||
            evidenceCollection.request.collectionType === enums.EvidenceCollectionType.PRACTICE_SESSION)))
        }

        function mapPromptTypeToString(promptType) {
            switch (promptType) {
                case enums.PromptType.PreConf:
                    return "Pre-Conference";
                case enums.PromptType.PostConf:
                    return "Post-Conference";
                case enums.PromptType.StudentGrowthGoal:
                    return "Student Growth Goal";
            }
        }
        function getSchoolYear(context) {
            return (context.orientation.schoolYear-1) + '-' + context.orientation.schoolYear;
        }
        function getRole(context) {
            return mapRoleNameToFriendlyName(context.orientation.roleName);
        }

        function getImpersonationSchool(context) {
            return context.orientation.schoolName;
        }

        function getSelectedText() {
            var txt = '';
            if (window.getSelection) {
                txt = window.getSelection().toString();
            } else if (document.getSelection) {
                txt = document.getSelection().toString();
            }
            else return;

            return txt;
        }

        function getSafeHtml(html) {
            return $sce.trustAsHtml(html);
        }

        function stripParameters(sref) {
            var regex = /\(.*\)/;
            return sref.replace(regex, '');
        }

        function mapLibItemTypeToString(itemType) {
            var string = '';
            switch (itemType) {
                case(enums.ArtifactLibItemType.FILE):
                    string = 'File';
                    break;
                case(enums.ArtifactLibItemType.WEB):
                    string = 'Website URL';
                    break;
                case (enums.ArtifactLibItemType.PROFPRACTICE):
                    string = 'Professional Practice';
                    break;
            }
            return string;
        }

        function mapEvalRequestTypeToString(type) {
            switch (type) {
                case enums.EvalAssignmentRequestType.ASSIGNED_EVALUATOR:
                    return "ASSIGNED EVALUATOR";
                case enums.EvalAssignmentRequestType.OBSERVATION_ONLY:
                    return "OBSERVATIONS ONLY";
                default:
                    return "UNKNOWN";
            }
        }

        function mapEvalRequestStatusToString(status) {
            switch(status) {
                case enums.EvalAssignmentRequestStatus.ACCEPTED:
                    return "ACCEPTED";
                case enums.EvalAssignmentRequestStatus.REJECTED:
                    return "REJECTED";
                case enums.EvalAssignmentRequestStatus.PENDING:
                    return "PENDING";
                default:
                    return "UNKNOWN";
            }
        }

        function mapEvidenceCollectionTypeToFullString(type) {
            switch(parseInt(type)){
                case enums.EvidenceCollectionType.OTHER_EVIDENCE:
                    return "Other Evidence";
                case enums.EvidenceCollectionType.OBSERVATION:
                    return "Observation";
                case enums.EvidenceCollectionType.SELF_ASSESSMENT:
                    return "Self-assessment";
                case enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS:
                    return "Student Growth Goal";
                case enums.EvidenceCollectionType.SUMMATIVE:
                    return "Summative";
                case enums.EvidenceCollectionType.YEAR_TO_DATE:
                    return "Year to Date";
                default:
                    return "Unknown";
            }
        }

        function mapEvaluationPlanTypeToStringCondensed(activeUserContextService, evaluatee) {
            var planType = mapEvaluationPlanTypeToString(evaluatee.evalData.planType)[0];
            if (planType === 'F') {
                return planType + '&nbsp;(' + mapEvaluationFocusForUserToString(activeUserContextService, evaluatee) + ')';
            }
            else {
                return planType;
            }
        }

        function mapEvaluationPlanTypeForUserToString(activeUserContextService, evaluatee, br) {
            var retval = mapEvaluationPlanTypeToString(evaluatee.evalData.planType);
            if (br) {
                retval += "<br/>";
            }
            else {
                retval += ", ";
            }

            if (evaluatee.evalData.planType === enums.EvaluationPlanType.FOCUSED) {
                retval+= "<strong>" + mapEvaluationFocusForUserToString(activeUserContextService, evaluatee) + "</strong>";
            }
            else {
                retval+= "<strong>C1-C8</strong>";
            }

            return retval;
        }

        function mapEvaluationPlanTypeToString (planType) {
            switch (planType) {
                case enums.EvaluationPlanType.COMPREHENSIVE: return 'Comprehensive';
                case enums.EvaluationPlanType.FOCUSED: return 'Focused';
                case enums.EvaluationPlanType.UNDEFINED: return 'N/A';
            }
        }

        function mapEvaluationFocusForUserToString (activeUserContextService, user) {

            var frameworkNodes = activeUserContextService.getFrameworkContext().stateFramework.frameworkNodes;
            var focusFrameworkNode = _.find(frameworkNodes, {id: user.evalData.focusFrameworkNodeId});
            var sgFocusFrameworkNode = _.find(frameworkNodes, {id: user.evalData.focusSGFrameworkNodeId});
            return mapEvaluationFocusToString(focusFrameworkNode, sgFocusFrameworkNode);
        }


        function mapLastYearEvaluationFocusForUserToString (activeUserContextService, user) {

            var frameworkNodes = activeUserContextService.getFrameworkContext().stateFramework.frameworkNodes;
            var focusFrameworkNode = _.find(frameworkNodes, {id: user.evalData.lastYearFocusFrameworkNodeId});
            var sgFocusFrameworkNode = _.find(frameworkNodes, {id: user.evalData.lastYearFocusSGFrameworkNodeId});
            return mapEvaluationFocusToString(focusFrameworkNode, sgFocusFrameworkNode);
        }

        function getStudentGrowthFocusFrameworkNode(activeUserContextService, user) {
            var frameworkNodes = activeUserContextService.getFrameworkContext().stateFramework.frameworkNodes;
            var sgNode = _.find(frameworkNodes, {id: user.evalData.focusSGFrameworkNodeId});
            if (sgNode) {
                return sgNode;
            }
            return _.find(frameworkNodes, {id: user.evalData.focusFrameworkNodeId});
        }

        function mapEventTypeToItemType(eventType) {
            switch(eventType) {
                case enums.EventType.OBSERVATION_CREATED:
                    return "Obs";
                case enums.EventType.ARTIFACT_REJECTED:
                    return "OE";
                case enums.EventType.ARTIFACT_SUBMITTED:
                    return "OE";
                default:
                    return "UNK";
            }
        }
        function mapEvaluationFocusToString (focusFrameworkNode, focusSGFrameworkNode)
        {
            var str = focusFrameworkNode.shortName;
            if (focusSGFrameworkNode && focusFrameworkNode.id !== focusSGFrameworkNode.id) {
                str += (', ' + focusSGFrameworkNode.shortName + ' (SG)');
            }

            return str;
        }

        function mapRoleNameToFriendlyName (roleName)
        {
            if (roleName === enums.Roles.SESchoolTeacher) {
                return 'Teacher';
            }
            else if (roleName === enums.Roles.SESchoolPrincipal) {
                return 'Principal';
            }
            else if (roleName === enums.Roles.SESchoolHeadPrincipal) {
                return "Head Principal";
            }
            else if (roleName === enums.Roles.SEDistrictAdmin) {
                return "District Admin";
            }
            else if (roleName === enums.Roles.SESchoolAdmin) {
                return "School Admin";
            }
            else if (roleName === enums.Roles.SEDistrictAssignmentManager) {
                return "District Assignment Manager";
            }
            else if (roleName === enums.Roles.SEDistrictViewer) {
                return "District Viewer";
            }
            else if (roleName === enums.Roles.SEDistrictEvaluator) {
                return "District Evaluator";
            }
            else if (roleName === enums.Roles.SEDistrictWideTeacherEvaluator) {
                return "District-wide Teacher Evaluator";
            }
            else if (roleName === enums.Roles.SESuperAdmin) {
                return "Super Admin";
            }
            else if (roleName === enums.Roles.SECustomSupportL1) {
                return "Customer Support";
            }
            else if (roleName === enums.Roles.SESchoolLibrarian) {
                return 'Librarian';
            }
        }

        function mapFrameworkViewTypeToString(viewType) {
            switch (viewType) {
                case enums.FrameworkViewType.INSTRUCTION_FRAMEWORK_ONLY:
                    return "Instructional Framework Only";
                case enums.FrameworkViewType.INSTRUCTIONAL_FRAMEWORK_DEFAULT:
                    return "Instructional Framework Default";
                case enums.FrameworkViewType.STATE_FRAMEWORK_DEFAULT:
                    return "State Framework Default";
                case enums.FrameworkViewType.STATE_FRAMEWORK_ONLY:
                    return "State Framework Only";
            }
        }

        function getEvaluateeTermLowerCase(evalType)
        {
            var term = mapRoleNameToFriendlyName(mapEvalTypeToEvaluateeRole(evalType));
            return term[0].toLowerCase() + term.slice(1);
        }

        function getEvaluateeTermUpperCase(evalType)
        {
            var term = mapRoleNameToFriendlyName(mapEvalTypeToEvaluateeRole(evalType));
            return term[0].toUpperCase() + term.slice(1);
        }

        function getTextWithoutCoding(codedString) {
            var elm = $("<div>" + codedString + "</div>");
            return elm.text();
        }

        function getTextWithShortName(codedString, shortName) {
            var elm = $('<div>' + codedString + '</div>');
            var coded = elm.find('[rubricrow]');
            coded.each((index, el) => {
                if (el.getAttribute('rubricrow') != shortName) {
                    var parent = $(el).parent().get(0);
                    // $(el).attr({"contenteditable":"false"});
                    $(el).replaceWith($(el.childNodes));
                    parent.normalize();
                }
            })
            return elm.html();
        }

        function getResponseWithoutRemoveCodeSelection(response) {
            var elm = $("<div>" + response + "</div>");
            elm.find(".remove-code-selection").remove();
            elm.removeClass();
            return elm.html();
        }

        function removeAnnotation(notes, clientid) {
            var elm = $('<div>' + notes + '</div>');
            var selectedElm = elm.find('[clientid="' + clientid + '"]');
            var parent = selectedElm.parent().get(0);
            selectedElm.replaceWith($(selectedElm.get(0).childNodes));
            parent.normalize();
            return elm.html();
        }

        function getNewGuid() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

    }
})();
