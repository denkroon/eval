/// <reference path="../core.module.ts" />

(function() {

    angular
        .module('stateeval.core')
        .factory('notificationService', notificationService);

    /* @ngInject */
    notificationService.$inject = ['$http', 'config', 'activeUserContextService'];

    function notificationService($http, config, activeUserContextService) {

        var service = {
            getNotificationsForEvaluation: getNotificationsForEvaluation,
            getNotificationsForUser: getNotificationsForUser
        };

        return service;

        function getNotificationsForUser() {
            var schoolYear = activeUserContextService.context.orientation.schoolYear;
            var userId = activeUserContextService.user.id;
            var url = config.apiUrl + 'notificationsforuser/' + schoolYear + '/' + userId;

            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function getNotificationsForEvaluation() {
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var userId = activeUserContextService.user.id;
            var url = config.apiUrl + 'notificationsforevaluation/' + evaluationId + '/' + userId;

            return $http.get(url).then(function(response) {
                return response.data;
            });
        }
    }

})();

