/// <reference path="../core.module.ts" />

/**
 * Created by anne on 8/23/2015.
 */

(function () {
    'use strict';

    angular
        .module('stateeval.core')
        .factory('authenticationService', authenticationService);

    authenticationService.$inject = ['$http', 'config'];
    function authenticationService($http, config) {

        return {

            authenticateUser: authenticateUser,
            setCredentials: setCredentials,
            clearCredentials: clearCredentials
        };

        function authenticateUser(username, password) {
            return $http.post(config.apiUrl + 'authenticate', {'username': username, 'password': password})
                .then(function (response) {
                    return response.data;
                })
        }

        function setCredentials(username, password) {
            //TODO: Request header field Authorization is not allowed by Access-Control-Allow-Headers.
            // $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
        }

        function clearCredentials() {
            //TODO: Request header field Authorization is not allowed by Access-Control-Allow-Headers.
            // $http.defaults.headers.common.Authorization = 'Basic ';
        }
    }
})();
