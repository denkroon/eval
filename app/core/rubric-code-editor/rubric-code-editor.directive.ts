/// <reference path="../core.module.ts" />

namespace StateEval.Core {
    var core = angular.module('stateeval.core');

    interface CodeElements {
        [rowName: string]: Element[]
    }

    export class RubricCodeEditorController {
        private previousText: string;
        text: string;
        saveMessage: string;

        toolbar = [       
        [], [], [], []
        ];

        toolsAdded: string[] = [];

        evidenceCollection: any;
        evidenceType: any;
        evalSession: any;
        userResponse: any;

        showLegend = false;
        editorName: string;

        private lastCheckedRange: Range;
        private codeElements: CodeElements = {};
        private timer: ng.IPromise<void>;

        private clientIdRegex = /clientid="(.*?)"/g

        constructor(
            private _: _.LoDashStatic,
            private activeUserContextService: any,
            private textAngularManager: any,
            private utils: any,
            private $timeout: ng.ITimeoutService,
            private observationService: any,
            private practiceSessionService: any,
            private enums: any,
            private $element: ng.IAugmentedJQuery,
            private $confirm: any,
            $rootScope: ng.IRootScopeService,
            $scope: ng.IScope
        ) {
            this.editorName = this.utils.getNewGuid();

            $timeout(this.resetToolbar);
            // $rootScope.$on('change-framework', this.resetToolbar);
            $scope.$watch(() => this.text, () => { this.textChanged() });

            if (this.userResponse) {
                this.text = this.userResponse.responseWithOutRemoveTag;
            }

            $(document).on('keydown', this.keyDown);

            $scope.$on('$destroy', () => {
                this.removeTools();
                $(document).off('keydown', this.keyDown);
            });

            this.lockUsedSelections();

            this.showLegend = this.evidenceCollection.observation.inUseClientIds.length > 0;
        }

        // Handles when the user presses the backspace key while having a element in use selected
        private keyDown = (e: JQueryEventObject) => {
            var allowedKeys= [16, 17, 17, 37, 38, 39, 40];
            if($.inArray(e.which, allowedKeys) == -1) 
                e.preventDefault();
        }

        // called by Angular when all child items are ready
        $postLink() {
            var editor = this.textAngularManager.retrieveEditor(this.editorName);
            // monkey patch the unfocus function so the toolbars no longer get disabled on blur
            editor.editorFunctions.unfocus = function () { };
            // force call focus to enable the toolbars from the start
            editor.editorFunctions.focus();
        }

        private allClientIds(text: string) {
            var match: RegExpExecArray;
            var ids: string[] = [];
            while (match = this.clientIdRegex.exec(text)) {
                ids.push(match[1]);
            }
            return ids;
        }

        private textChanged() {
            this.lockUsedSelections();

            var previous = this.allClientIds(this.previousText);
            var current = this.allClientIds(this.text);

            var diff = _.difference(previous, current);

            if (diff.length > 0) {
                this.$confirm(
                    {
                        text: 'This will remove text that has been coded, do you wish to continue?',
                        ok: "Yes",
                        cancel: "Cancel"
                    })
                    .then(() => {
                        this.observationService.deleteCodedRubricRowAnnotations(this.evidenceCollection, diff);
                        this.save();
                    }, () => {
                        this.text = this.previousText;
                    });
            }
            else {
                this.save();
            }
        }

        private save() {
            this.previousText = this.text;
            this.saveMessage = "Saving...";

            var responseId = parseInt(this.userResponse.id);
            var codedResponse = this.text;
            var me = this;

            if (this.timer) this.$timeout.cancel(this.timer);
            
            this.timer = this.$timeout(() => {
                if (me.evidenceType === me.enums.EvidenceType.RR_ANNOTATION_PRECONF_PROMPT ||
                me.evidenceType === me.enums.EvidenceType.RR_ANNOTATION_POSTCONF_PROMPT) {
                    me.observationService.updateUserPromptResponse(me.evalSession.id, responseId, codedResponse);
                } else if (me.evidenceType === me.enums.EvidenceType.RR_ANNOTATION_TEE_PRECONF_SUMMARY) {
                    me.evalSession.evaluateePreConNotes = codedResponse;
                    me.observationService.saveObserveNotes(me.evalSession.id, codedResponse, 'evaluateePreConNotes');
                } else if (me.evidenceType === me.enums.EvidenceType.RR_ANNOTATION_TEE_POSTCONF_SUMMARY) {
                    me.evalSession.evaluateePostConNotes = codedResponse;
                    me.observationService.saveObserveNotes(me.evalSession.id, codedResponse, 'evaluateePostConNotes');
                }
            }, 1000);
        }

        /**
         * Removes the custom tools registered with textAngular by this instance
         */
        private removeTools() {
            this._.forEach(this.toolsAdded, t => { this.textAngularManager.removeTool(t); });
            this.toolsAdded = [];
        }

        // Customizes the toolbar buttons according to the selected framework
        private resetToolbar = () => {
            var framework = this.activeUserContextService.getActiveFramework();
            var editor = this.textAngularManager.retrieveEditor(this.editorName);
            if (!editor) {
                return;
            }
            var tb = editor.toolbars[0];
            this.removeTools();

            this._.forEach(framework.frameworkNodes, (n: any, groupIndex: number) => {
                if (this.evidenceCollection.getNodeById(n.id).disabled) return;

                var toolId = this.utils.getNewGuid();
                this.textAngularManager.addToolToToolbar(
                    toolId,
                    {
                        class: 'btn btn-default framework_node',
                        buttontext: n.shortName,
                        tooltiptext: `${n.shortName} - ${n.title}`
                    },
                    tb, groupIndex);
                this.toolsAdded.push(toolId);

                this._.forEach(n.rubricRows, (r, btnIndex: number) => {
                    if (this.evidenceCollection.getRowById(r.id).disabled) return;
                    if (this.utils.stripGrowthGoalsRow(r, this.evidenceCollection)) {
                        var toolId = this.utils.getNewGuid();
                        var regex = new RegExp(`rubricrow=${r.shortName}`);
                        this.textAngularManager.addToolToToolbar(
                            toolId,
                            {
                                buttontext: r.shortName,
                                tooltiptext: `${r.shortName} - ${r.title}`,
                                // called when the button is clicked
                                action: () => {
                                    // Using the already computed code elements because
                                    // somehow when clicking we can't properly compute them
                                    if (this.codeElements[r.shortName]) {
                                        this.removeEncoding(r.shortName);
                                    }
                                    else {
                                        this.encodeSelection(r);
                                    }
                                },
                                // called by textangular to know if the button should be active (pressed)
                                activeState: () => {
                                    var matches = this.getCodeElements();
                                    return matches[r.shortName] && !this.selectionSplitsCode(r.shortName);
                                },
                                // called by textangular to know if the button should be disabled
                                disabled: () => {
                                    var matches = this.getCodeElements();
                                    var selection = window.getSelection();
                                    //console.log(selection);

                                    return (!matches[r.shortName] && (this.selectionSplitsCodes() || selection.rangeCount == 0 || selection.getRangeAt(0).toString().length == 0)) ||
                                        (matches[r.shortName] && this.selectionSplitsCode(r.shortName)) ||
                                        this._.any(matches[r.shortName], e => e.getAttribute('contenteditable') == 'false');
                                }
                            },
                            tb,
                            groupIndex, // HACK: we have 5 static button groups first
                            btnIndex);
                        this.toolsAdded.push(toolId);
                    }
                });
            });
        }

        /**
         * Removes all the coding elements for the given row name from the selection
         */
        private removeEncoding(rowName: string) {
            var elements = this.codeElements[rowName];
            this._.forEach(elements, el => {
                var clientId = el.getAttribute('clientid');
                var realElement = this.$element.find(`[clientid=${clientId}]`);
                var parent = realElement.parent().get(0);
                realElement.replaceWith($(realElement.get(0).childNodes));
                this.observationService.deleteCodedRubricRowAnnotation(this.evidenceCollection, clientId);
                // merge all the text nodes, needs to be in a timeout to work somehow
                this.$timeout(() => { parent.normalize() });
            });
            this.lastCheckedRange = null;
            this.codeElements = {};
        }

        /**
         * Determines if the selection splits any coding element
         */
        private selectionSplitsCodes() {
            return this._.any(this.codeElements, elements => this._.any(elements, (element) => {
                var e = this.$element;
                if(!e)
                    return false;
                var realElement = e.find(`[clientid=${element.getAttribute('clientid')}]`).get(0);
                if(!realElement)
                    return false;
                return !realElement.isEqualNode(element);    
            }));
        }
        /**
         * Determines if the selection splits a specific encoding
         */
        private selectionSplitsCode(rowName: string) {
            return this._.any(this.codeElements, elements => this._.any(elements, (element) => {
                if (element.getAttribute('rubricrow') != rowName) return false
                var e = this.$element;
                if(!e)
                    return false;
                var realElement = e.find(`[clientid=${element.getAttribute('clientid')}]`).get(0);
                if(!realElement)
                    return false;
                return !realElement.isEqualNode(element);    
            }));
        }


        /**
         * Wraps the text selection with an encoding element for the given rubric row
         */
        private encodeSelection(rubric: any) {
            var selection = window.getSelection();
            if (selection.rangeCount == 0) return;

            var range = selection.getRangeAt(0);
            if (!range.toString().length) return;

            var content = range.cloneContents();
            var el = document.createElement('SPAN');
            var promptResponseId = parseInt(this.userResponse.id);
            var clientId = this.utils.getNewGuid();
            el.setAttribute('rubricrow', rubric.shortName);
            el.setAttribute('clientid', clientId);
            el.setAttribute('class', 'coded-section');
            el.appendChild(content);
            range.deleteContents();
            range.insertNode(el);
            this.lastCheckedRange = null;
            this.codeElements = {};

            // reselects the text that was just encoded
            selection.removeAllRanges()
            range = document.createRange();
            range.selectNodeContents(el);
            selection.removeAllRanges();
            selection.addRange(range);

            // This code looks for the node right after the span that was just inserted
            // and see if it's a text node. If it's not a text node, it adds one with
            // a space in it, allowing the user to place the cursor and type something
            // after the new element.
            this.$timeout(() => {
                var nextSibling = this.$element.find(`span[clientid=${clientId}]`).get(0).nextSibling;
                if (!nextSibling || nextSibling.nodeType != Node.TEXT_NODE) {

                    //console.log('this.element=');
                    //console.log(this.$element);
                    this.$element.find(`span[clientid=${clientId}]`).after(document.createTextNode(' '))
                }
            })
            
//function addNewUserPromptResponseRubricRowAnnotation(rubricRow, annotation, evidenceType, userPromptResponseId, clientId)

            this.evidenceCollection.addNewUserPromptResponseRubricRowAnnotation(rubric,range.toString(), this.evidenceType, promptResponseId , clientId);

            //function addNewObservationNotesRubricRowAnnotation(rubricRow, annotation, evidenceType, clientId)

            // this.evidenceCollection.addNewObservationNotesRubricRowAnnotation(rubric, range.toString(), this.evidenceType, clientId);
        }

        /**
         * Returns all elements matching a given rubric row which either wrap, intersect with or are contained
         * by the current selection.
         */
        private getCodeElements() {
            /**
             * Adds an element to the code elements hash if it has a rubricrow attribute
             */
            var add = (element: Element) => {
                var rowName = element.getAttribute('rubricrow');
                if (rowName) {
                    if (!elements[rowName]) elements[rowName] = [];
                    elements[rowName].push(element);
                    unsorted.push(element);

                    // if one element is in use, disable editing on the whole text
                    if (_.includes(this.evidenceCollection.observation.inUseClientIds, element.getAttribute('clientid'))) {
                        this.$element.find('.ta-bind').attr('contenteditable', 'false');
                    }
                }
            }

            var elements: CodeElements = {};
            var unsorted: Element[] = [];
            var selection = window.getSelection();
            if (selection.rangeCount > 0) {
                var range = selection.getRangeAt(0);

                // Optimization so we don't compute that stuff for every toolbar button
                if (this.compareRanges(this.lastCheckedRange, range)) {
                    return this.codeElements;
                }

                this.lastCheckedRange = range;

                // Default to making the text editable
                this.$element.find('.ta-bind').attr('contenteditable', 'true');

                // Find any parent to the selection that has a rubricrow attribute
                $(range.commonAncestorContainer).parents('[rubricrow]').each((index, element) => { add(element) });

                // Find any direct children. Note that using cloneContents will create a new DOM fragment. For any
                // element that's only partially in the selection, a new element will be automatically created. The
                // new element will have the same properties as the old one but it will contain only the part of the
                // old element's content that's included in the selection.
                var children = $(range.cloneContents().childNodes);
                for (var i = 0; i < children.length; i++) {
                    var child = children.get(i);
                    if (child.getAttribute) add(child);
                }

                // Find all of the children of the direct children that have a rubricrow attribute
                children.find(`[rubricrow]`).each((index, element) => { add(element) });
                this.codeElements = elements;
            }
            else {
                this.codeElements = {};
                this.lastCheckedRange = null;
            }

            return elements;
        }

        /**
         * Compares two ranges to see if they are the same
         */
        private compareRanges(a: Range, b: Range) {
            if (!a || !b) return false;
            return a.compareBoundaryPoints(Range.START_TO_START, b) == 0 && a.compareBoundaryPoints(Range.END_TO_END, b) == 0;
        }

        /**
         * Find selection elements that are in use and prevents the user from editing them
         */
        private lockUsedSelections() {
            this.$timeout(() => {
                this.$element.find('.ta-bind [rubricrow]').removeAttr('contenteditable');
                _.forEach(this.evidenceCollection.observation.inUseClientIds, (id: string) => {
                    this.$element.find(`.ta-bind [clientid=${id}]`).attr('contenteditable', 'false');
                })
            });
        }
    }

    core.component('rubricCodeEditor', {
        bindings: {
            evidenceCollection: '=',
            evidenceType: '=',
            evalSession: '=',
            userResponse: '='
        },
        controller: RubricCodeEditorController,
        templateUrl: 'app/core/rubric-code-editor/rubric-code-editor.html'
    });
}