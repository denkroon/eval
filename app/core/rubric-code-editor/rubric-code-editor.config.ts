/// <reference path="../core.module.ts" />

namespace StateEval.Core {
    var core = angular.module('stateeval.core');
    var registeredIds : number[] = [];

    core.config((
        $provide: ng.auto.IProvideService
    ) => {
        $provide.decorator('taOptions', (
            // Fullscreen: any,
            // taRegisterTool: any,
            $delegate: any
        ) => {
            $delegate.forceTextAngularSanitize = false;
            return $delegate;
        });
    })
}