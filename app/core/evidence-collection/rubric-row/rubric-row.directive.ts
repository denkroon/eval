/// <reference path="../../core.module.ts" />

(function () {
    'use strict';

    function rubricRowDirective($parse, rubricUtils) {
        return {
            restrict: 'E',
            scope: {
                row: '='
            },
            templateUrl: 'app/core/evidence-collection/rubric-row/rubric-row.directive.html',
            controller: 'rubricRowController as vm',
            bindToController: true
        }
    }

    interface CustomScope extends ng.IScope {
        highlight?: ()=>void
    }

    class RubricRowController {

        criticalOn = false;
        toggle = true;


        context:any;
        isEvaluatee:any;
        state:any;
        row:any;

        score:any;
        otherScore:any;
        root:any;
        dividedEvaluations:any;
        orderedEvaluations:any;
        colorObj;
        evaluations;
        criticalAttributesEnabled: boolean;

        constructor(private utils:any,
                    private $q:ng.IQService,
                    public enums:any,
                    public evidenceCollectionService:any,
                    public rubricUtils:any,
                    $scope:CustomScope,
                    activeUserContextService:any) {
            this.context = activeUserContextService.context;
            this.isEvaluatee = this.context.isEvaluatee();
            this.state = evidenceCollectionService.state;
            this.colorObj = rubricUtils.colorObj;
            this.score = this.row.score;
            this.otherScore = this.row.otherScore;
            this.evaluations = this.row.evaluations;
            this.criticalAttributesEnabled = this.context.frameworkContext.districtConfiguration.criticalAttributesEnabled;

            $scope.$watch(() => {
                    return this.row.evaluations.length;
                },
                () => {
                    if (this.row) {
                        this.dividedEvaluations = rubricUtils.createDividedEvaluations(this.row);
                        var sorted = _.sortBy(this.row.evaluations, function (n) {
                            return rubricUtils.minBy(n.alignedEvidences, function (evidence) {
                                return evidence.creationDateTime;
                            })
                        });
                        this.orderedEvaluations = rubricUtils.reverse(sorted);


                    }
                });
        }


        mouseUpCriticalAttribute = (row, level) => {
            if (!this.context.frameworkContext.districtConfiguration.criticalAttributesReferenceOnly) {
                this.evidenceCollectionService.state.evaluation.source = 'criticalAttribute';
                this.mouseUp(row, level);
            }
        };

        mouseUpRubricText = (row, level) => {
            this.evidenceCollectionService.state.evaluation.source = 'rubricText';
            this.mouseUp(row, level);

        };


        mouseUp(row, level) {
            var levelName = this.enums.PerformanceLevels[level - 1];
            if (this.row.root.readOnly || this.evidenceCollectionService.state.view != 'row') {
                return;
            }
            if (this.evidenceCollectionService.state.evaluation.row) {
                if (this.evidenceCollectionService.state.evaluation.row.data.id !== row.data.id) {
                    return;
                }
            }

            //create new
            var selectedText = this.utils.getSelectedText();
            var descriptorText = this.rubricUtils.standardizeHtml(row[levelName].data.descriptorText);
            var attributeText = this.rubricUtils.standardizeHtml(row[levelName].data.criticalAttributeText);
            if (selectedText.length < 50 || (!~descriptorText.indexOf(selectedText) && !~attributeText.indexOf(selectedText))) {
                return this.$q.when(0);
            }
            var tempEvals = row[levelName].evaluations.concat([{rubricStatement: selectedText}]);
            var evaluation = this.evidenceCollectionService.state.evaluation;


            row[levelName].descriptor = this.rubricUtils.mergeEvidenceToHtml(descriptorText, tempEvals, row.root);
            row[levelName].criticalAttribute = this.rubricUtils.mergeEvidenceToHtml(attributeText, tempEvals, row.root);


            this.evidenceCollectionService.state.evaluation.selectedText = selectedText;
            this.evidenceCollectionService.state.evaluation.level = level;
            this.evidenceCollectionService.state.evaluation.row = row;

            this.evidenceCollectionService.calcPosition('rubric-row');

        }

        showCritical(toggle) {
            if(this.criticalAttributesEnabled && this.row.data.lookFor1) {
                if(this.state.view === 'row') {
                    return toggle;
                } else {
                    if(this.state.viewOptions.filter === 'custom') {
                        return this.state.viewOptions.criticalAttributes;
                    } else {
                        return toggle;
                    }
                }
            } else {
                return false;
            }
        }

        showEvaluations(toggle) {
            if(this.state.view === 'row') {
                return toggle;
            } else {
                if(this.state.viewOptions.filter === 'custom') {
                    return this.state.viewOptions.evaluations;
                } else {
                    return toggle;
                }
            }
        }


    }
    angular.module('stateeval.core')
        .directive('rubricRow', rubricRowDirective)
        .controller('rubricRowController', RubricRowController);

})();
