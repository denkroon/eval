/// <reference path="../core.module.ts" />

(function () {
    'use strict';

    function evidenceCollectionDirective() {
        return {
            restrict: 'E',
            scope: {
                evidenceCollection: '=',
                redirect: '=?'
            },
            templateUrl: 'app/core/evidence-collection/evidence-collection.directive.html',
            controller: 'evidenceCollectionController as vm',
            bindToController: true
        }
    }

    class EvidenceCollectionController {
        //TODO: use proper types
        evidenceCollection:any;
        redirect:any;
        otherEvidence:boolean;
        summative:boolean;
        user:any;
        state:any;
        isEvaluator:boolean;
        isEvaluatee: boolean;
        observation:any;
        name:any;
        evaluateeTerm:any;
        showOtherTerm:any;
        row:any;
        node:any;
        video_config:any;
        showBothCB: boolean;
        viewOptions: any;
        filterAll: any;
        context: any;
        timer: ng.IPromise<void>;

        // vertical position of the container element, used to computer the
        // relative position of the evidence builder and success message.
        basePosition:number;

        // This is used to set the vertical position of the evidence builder
        builderPosition = 150;

        // When true, show the "success" message
        evidenceCreated = false;

        fadeout = false;
        // Vertical position of the success message so it's aligned with the
        // newly created evidence
        successPosition:number;

        // injected by rubric-helper
        startClick:(a:any, b:any)=>void;

        constructor(public enums:any,
                    public evidenceCollectionService:any,
                    public activeUserContextService:any,
                    private smoothScroll:any,
                    private $timeout:ng.ITimeoutService,
                    private _:_.LoDashStatic,
                    private $sce:ng.ISCEService,
                    $scope:ng.IScope) {
            this.context = activeUserContextService.context;
            var collectionType = this.evidenceCollection.request.collectionType;
            this.summative = collectionType === enums.EvidenceCollectionType.SUMMATIVE;
            this.user = activeUserContextService.user;
            this.state = evidenceCollectionService.state;
            this.viewOptions = this.state.viewOptions;

            this.filterAll = 0;
            this.filterAll = Boolean(this.filterAll);

            this.isEvaluator = !activeUserContextService.context.isEvaluatee();
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();
            this.observation = this.evidenceCollection.observation;
            this.name = activeUserContextService.context.framework.name;
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
            this.showBothCB = false;

            // evaluatee should see merged scores by default (if they exist) on Align&Score screen after the observation
            // has started final submission
            if (this.evidenceCollection.request.collectionType === this.enums.EvidenceCollectionType.OBSERVATION) {
                if (activeUserContextService.context.observationIsReadOnly(this.observation) &&
                    this.isEvaluatee &&
                    this.observation.selfEvalShared) {
                    this.showBothCB = true;
                    this.toggleIncludeEvaluateeScores();
                }
            }


            this.video_config = {
                sources: [
                    {
                        src: $sce.trustAsResourceUrl('https://ucarecdn.com/c0f3695c-7fb7-48f9-a624-cd13c8318e0b/livechnnmichael_20160325_202112.mp4'),
                        type: "video/mp4"
                    }
                ],
                tracks: [
                    {
                        src: "https://ucarecdn.com/c0f3695c-7fb7-48f9-a624-cd13c8318e0b/livechnnmichael_20160325_202112.mp4",
                        kind: "subtitles",
                        srclang: "en",
                        label: "English",
                        default: ""
                    }
                ],
                theme: "styles/videogular/videogular.css",
                plugins: {
                    poster: 'https://ucarecdn.com/1255bec9-e031-4781-9686-e8dceea9db5e/creatingalignedevidenceposter.PNG'
                }
            };

            if (this.isEvaluator) {
                this.showOtherTerm = this.evaluateeTerm;
            }
            else {
                this.showOtherTerm = "Evaluator";
            }


            $scope.$watch("vm.viewOptions.filter === 'custom'", (val: boolean) => {
                if (val) {
                    this.initfilters();
                }
            });

            $timeout(() => {
                // getting hold of the directive's base position for later calculations
                // using a timeout is needed to give the browser time to place the
                // element first.
                var el = angular.element('evidence-collection');
                this.basePosition = el.offset().top;
            });

            this.cancel();
        }

        /**
         * Cancels the evidence builder
         */
        cancel() {
            this.evidenceCollectionService.cancelEvaluationBuilder(this.evidenceCollection);
            this.builderPosition = 150;
        }

        /**
         * Deselects an available evidence from the evidence builder
         */
        deselect(evidence:any) {
            this._.remove(this.state.selectedEvidence, evidence);
        }

        /**
         * Called by the "Go to Rubric" button
         */
        goToRubric() {
            // get hold of the rubric row element
            var target = angular.element('#rubric-row');

            // auto scroll to the element, taking 1s
            this.smoothScroll(target.get(0), {duration: 1000});

            // update the evidence builder position to align it to the rubric row
            this.state.basicPosition = target.offset().top - this.basePosition;
        }

        /**
         * Called when the user decides to clear the selected text
         */
        clearRubric() {
            this.evidenceCollectionService.clearSelectedText(this.evidenceCollection);
        }

        createEvaluation() {
            var list = _.map(this.evidenceCollectionService.state.selectedEvidence, (e) => this.evidenceCollectionService.newAlignedEvidence(e));
            var row = this.evidenceCollectionService.state.evaluation.row;
            return row.root.addNewEvaluation(
                this.evidenceCollectionService.state.evaluation.level,
                this.evidenceCollectionService.state.evaluation.selectedText,
                list,
                this.evidenceCollectionService.state.evaluation.row,
                this.evidenceCollectionService.state.evaluation.source).then((rrEval) => {
                this.cancel();
                this.evidenceCreated = true;

                this.$timeout(() => {
                    // bit of a hack because we have to use the element id to scroll to it
                    // the timeout ensures the element has been added to the page before
                    // we try to reference it.
                    var el = angular.element(`#alignedrreval-${rrEval.id}`);

                    this.smoothScroll(el.get(0), {duration: 300});
                    this.successPosition = el.offset().top - this.basePosition;
                    this.fadeout = true;
                });

                this.$timeout(() => {
                    this.fadeout = false;
                    this.evidenceCreated = false;
                }, 2000);
            });
        }

        statementOfPerformanceChanged() {
            this.evidenceCollection.saveNotice = 'Saving...';

            if (this.timer) {
                this.$timeout.cancel(this.timer);
            }

            this.timer = this.$timeout(() => {
                return this.evidenceCollection.scoreItem(this.node.score).then(() => {
                    this.evidenceCollection.saveNotice = 'All changes saved';
                });
            }, 1000);
        }

        toggleIncludeEvaluateeScores() {
            var view = this.evidenceCollection.data.view === 'merge' ? 'default' : 'merge';
            var userId = this.evidenceCollection.data.view === 'merge' ? this.activeUserContextService.user.id : 0;


            this.evidenceCollectionService.getEvidenceCollection('OBSERVATION', this.enums.EvidenceCollectionType.OBSERVATION, this.evidenceCollection.id, userId, view)
                .then((data) => {
                    this.evidenceCollection = data;
                    this.startClick(this.node.data.shortName, this.row && this.row.data.id);
                });
        }

        togglefilter() {

            this.filterAll = this.viewOptions.rubric &&
                             this.viewOptions.criticalAttributes &&
                             this.viewOptions.evidence &&
                             this.viewOptions.evaluations;

            this.filterAll = Boolean(this.filterAll);
        }

        initfilters() {
            this.viewOptions.rubric = true;
            this.viewOptions.criticalAttributes = false;
            this.viewOptions.evidence = true;
            this.viewOptions.evaluations = true;
            this.togglefilter();
        }

        allfilters() {
            this.viewOptions.rubric = this.filterAll;
            this.viewOptions.criticalAttributes = this.filterAll;
            this.viewOptions.evidence = this.filterAll;
            this.viewOptions.evaluations = this.filterAll;
        }
    }

    angular.module('stateeval.core')
        .directive('evidenceCollection', evidenceCollectionDirective)
        .controller('evidenceCollectionController', EvidenceCollectionController);
})();
