/// <reference path="../core.module.ts" />

(function () {

    angular
        .module('stateeval.core')
        .factory('evidenceCollectionService', evidenceCollectionService);
    evidenceCollectionService.$inject = ['activeUserContextService', 'rubricUtils', 'enums', 'utils',
        'config', '$http', '$q', '_'];

    function evidenceCollectionService(activeUserContextService, rubricUtils, enums, utils,
                                       config, $http, $q, _) {

        var frameworkList = {};
        // todo: handle switching frameworks
        function EvidenceCollection(name, request, data, viewType) {
            this.name = name;
            this.id = request.collectionObjectId;
            this.request = request;
            this.observation = data.observation;
            this.learningWalkObservation = data.learningWalkObservation;
            this.selfAssessment = data.selfAssessment;
            this.studentGrowthGoalBundle = data.studentGrowthGoalBundle;
            this.otherEvidenceCollection = data.otherEvidenceCollection;
            this.practiceSessionObservation = data.practiceSessionObservation;
            this.observations = data.observations;
            this.associatedCollections = {};
            this.availableEvidence = data.availableEvidence;
            this.evaluations = data.rubricRowEvaluations;
            this.score = (data.evidenceCollectionScores && data.evidenceCollectionScores[0]) || this.newEvidenceCollectionScore(this.id, null);
            for (var i in enums.EvColTypeAccessor) {
                var collectionList = data[enums.EvColTypeAccessor[i]];
                this.associatedCollections[i] = _.groupBy(collectionList, 'id');
                for (var j in this.associatedCollections[i]) {
                    this.associatedCollections[i][j] = this.associatedCollections[i][j][0];
                }
            }
            var stateV = activeUserContextService.context.frameworkContext.stateFramework;
            var instrV = activeUserContextService.context.frameworkContext.instructionalFramework;
            var arr = [];
            if (stateV) {
                arr.push(stateV);
            }
            if (instrV) {
                arr.push(instrV);
            }

            this.data = {
                view: viewType,
                evaluation: {
                    evaluatee: activeUserContextService.context.evaluatee,
                    evaluator: activeUserContextService.context.evaluator,
                }
            };
            if (activeUserContextService.context.evaluatee && activeUserContextService.context.evaluator) {
                this.data.evaluation[activeUserContextService.context.evaluatee.id] = 'evaluatee';
                this.data.evaluation[activeUserContextService.context.evaluator.id] = 'evaluator';
                activeUserContextService.context.isEvaluatee() ?
                    this.data.evaluation[String(activeUserContextService.context.evaluatee.id) + String(activeUserContextService.context.evaluator.id)] = 'combined' :
                    this.data.evaluation[String(activeUserContextService.context.evaluator.id) + String(activeUserContextService.context.evaluatee.id)] = 'combined';
            }
            var build = buildFramework(arr, data, this);

            this.rubric = build.rubric;
            this.tree = build.tree;


            //LIST OF CONDITIONALS
            this.readOnly = false;
            this.hideNodeScores = false;

            console.log(name, ': ', this);


            //if the ECOL is a summative obj new score objs will come in through the data property
            //data.summativeRubricRowScore, data.summativeFrameworkNode scores

            //other evidence and goals do not retrieve any scores

            //summative ECOL does not recieve available evidence
            this.updateInUse();
            this.updateDone();
            this.updateEvidenceDone();
            //console.log('Starting test');
            //var test = rubricUtils.testMergeHighlighter(this);
            //console.log('test finished', test);
        }

        EvidenceCollection.prototype = {
            // this can be used for any item that has evidenceCollectionObjectId property
            itemCreatedInCurrentCollection: function (item) {
                if (this.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE &&
                    item.evidenceCollectionType === enums.EvidenceCollectionType.OTHER_EVIDENCE) {
                    return true;
                }
                else {
                    return (this.request.collectionObjectId === item.evidenceCollectionObjectId &&
                            this.request.collectionType === item.evidenceCollectionType);
                }
            },
            isEvaluator: function () {
                switch (this.request.collectionType) {
                    case enums.EvidenceCollectionType.OBSERVATION:
                        return activeUserContextService.user.id === this.observation.evaluatorId;
                        break;
                    case enums.EvidenceCollectionType.SUMMATIVE:
                    case enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS:
                    case enums.EvidenceCollectionType.YEAR_TO_DATE:
                        return activeUserContextService.user.id === activeUserContextService.context.evaluatee.evalData.evaluatorId;
                        break;
                    case enums.EvidenceCollectionType.SELF_ASSESSMENT:
                    case enums.EvidenceCollectionType.LEARNING_WALK_OBS:
                    case enums.EvidenceCollectionType.PRACTICE_SESSION_OBS:
                        return true;
                        break;
                }
            },
            getActiveFrameworkName: function getActiveFrameworkName() {
                if (this.request.collectionType === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) {
                    return activeUserContextService.context.frameworkContext.stateFramework.name;
                }
                else {
                    return activeUserContextService.context.framework.name;
                }
            },
            getRows: function (nodeShortName) {
                var that = this;
                return _.filter(that.rubric, function (n) {
                    var mapper = that.tree[that.getActiveFrameworkName()].frameworkMapper;
                    return mapper[n.data.id] === nodeShortName;
                });
            },
            getNode: function (nodeShortName) {
                for (var i in this.tree) {
                    var node = _.find(this.tree[i].nodes, function (n) {
                        return n.data.shortName === nodeShortName;
                    });
                    if (node) {
                        return node;
                    }
                }
                throw new Error('could not find node');
            },
            getNodeById: function (id) {
                return _.find(this.tree[this.getActiveFrameworkName()].nodes, function (n) {
                    return id === n.data.id;
                })
            },
            getRow: function (rowShortName) {
                return _.find(this.rubric, function (n) {
                    return rowShortName === n.data.shortName;
                });
            },
            getRowById: function (rowId) {
                return this.rubric[rowId];
            },
            newRubricRowScore: function newRubricRowScore(rowId, performanceLevel, userId) {
                var evaluatee = activeUserContextService.context.evaluatee;
                return {
                    id: 0,
                    isActive: true,
                    evaluationId: evaluatee ? evaluatee.evalData.id : null,
                    createdByUserId: userId || activeUserContextService.user.id,
                    performanceLevel: performanceLevel,
                    rubricRowId: rowId,
                    evidenceCollectionType: this.request.collectionType,
                    evidenceCollectionObjectId: this.request.collectionObjectId
                };
            },
            newFrameworkNodeScore: function newFrameworkNodeScore(nodeId, performanceLevel, userId) {
                var evaluatee = activeUserContextService.context.evaluatee;
                return {
                    id: 0,
                    evaluationId: evaluatee ? evaluatee.evalData.id : null,
                    createdByUserId: userId || activeUserContextService.user.id,
                    performanceLevel: performanceLevel,
                    frameworkNodeId: nodeId,
                    evidenceCollectionType: this.request.collectionType,
                    evidenceCollectionObjectId: this.request.collectionObjectId,
                    statementOfPerformance: ''
                };
            },
            newEvidenceCollectionScore: function newEvidenceCollectionScore(evidenceCollectionId, performanceLevel) {
                var evaluatee = activeUserContextService.context.evaluatee;
                return {
                    id: 0,
                    evaluationId: evaluatee ? evaluatee.evalData.id : null,
                    createdByUserId: activeUserContextService.user.id,
                    performanceLevel: performanceLevel,
                    evidenceCollectionType: this.request.collectionType,
                    evidenceCollectionObjectId: this.request.collectionObjectId,
                }
            },
            scoreItem: function scoreItem(score) {
                var url = config.apiUrl;
                if (this.request.collectionType === enums.EvidenceCollectionType.SUMMATIVE ||
                    this.request.collectionType === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS ||
                    this.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE) {
                    url += (score.frameworkNodeId ?
                        'evidencecollections/scoresummativeframeworknode' : 'evidencecollections/scoresummativerubricrow');
                } else {
                    if (score.frameworkNodeId || score.rubricRowId) {
                        url += (score.frameworkNodeId ?
                            'evidencecollections/scoreframeworknode' : 'evidencecollections/scorerubricrow');
                    } else {
                        url += 'evidencecollections/scorecollection';
                    }
                }

                return $http.put(url, score)
                    .then(function (response) {
                        score.id = response.data.id;
                    })
            },
            newRubricRowAnnotationParams: function newRubricRowAnnotationParams(evidenceType, rubricRowId, annotation, clientId) {
                return {
                    isActive: true,
                    evidenceCollectionType: this.request.collectionType,
                    evidenceCollectionObjectId: this.request.collectionObjectId,
                    evaluationId: this.request.evaluationId,
                    rubricRowId: rubricRowId,
                    annotation: annotation,
                    userId: activeUserContextService.user.id,
                    evidenceType: evidenceType,
                    clientId: clientId
                }
            },
            newRubricNoteRubricRowAnnotationParams: function newRubricRowAnnotationParams(rubricRowId, annotation) {
                return {
                    isActive: true,
                    evidenceCollectionType: (this.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE) ?
                        enums.EvidenceCollectionType.OTHER_EVIDENCE : this.request.collectionType,
                    evidenceCollectionObjectId: (this.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE) ?
                        this.otherEvidenceCollection.id : this.request.collectionObjectId,
                    evaluationId: this.request.evaluationId,
                    rubricRowId: rubricRowId,
                    annotation: annotation,
                    userId: activeUserContextService.user.id,
                    evidenceType: enums.EvidenceType.RR_ANNOTATION_RUBRIC_NOTE
                }
            },
            addNewObservationNotesRubricRowAnnotation: function addNewObservationNotesRubricRowAnnotation(rubricRow, annotation, evidenceType, clientId) {
                var url = config.apiUrl + 'rubricrowannotation/observationnotes';
                var params = this.newRubricRowAnnotationParams(evidenceType, rubricRow.id, annotation, clientId);
                var that = this;
                return $http.post(url, params).then(function (response) {
                    that.addAvailableEvidence(response.data, that.getRow(rubricRow.shortName));
                });
            },
            addNewRubricNoteRubricRowAnnotation: function addNewRubricNoteRubricRowAnnotation(row, annotation) {
                var url = config.apiUrl + 'rubricrowannotation/rubricnote';
                var params = this.newRubricNoteRubricRowAnnotationParams(row.data.id, annotation);
                var that = this;
                return $http.post(url, params).then(function (response) {
                    that.addAvailableEvidence(response.data, row);
                });
            },
            addNewUserPromptResponseRubricRowAnnotation: function addNewUserPromptResponseRubricRowAnnotation(rubricRow, annotation, evidenceType, userPromptResponseId, clientId) {
                var url = config.apiUrl + 'rubricrowannotation/userpromptresponse';
                var params = this.newRubricRowAnnotationParams(evidenceType, rubricRow.id, annotation, clientId);
                params.userPromptResponseId = userPromptResponseId;
                var that = this;
                return $http.post(url, params).then(function (response) {
                    that.addAvailableEvidence(response.data, that.getRow(rubricRow.shortName));
                });
            },
            addNewConferenceNotesRubricRowAnnotation: function addNewConferenceNotesRubricRowAnnotation(rubricRow, annotation, evidenceType, clientId) {
                var url = config.apiUrl + 'rubricrowannotation/conferencenotes';
                var params = this.newRubricRowAnnotationParams(evidenceType, rubricRow.id, annotation, clientId);
                var that = this;
                return $http.post(url, params).then(function (response) {
                    that.addAvailableEvidence(response.data, that.getRow(rubricRow.shortName));
                });
            },
            addNewEvaluation: function addNewEvaluation(performanceLevel, rubricStatement, alignedEvidence, row, source) {
                var that = this;
                if (!performanceLevel) {
                    console.log('saved without level');
                    return $q.when();
                }
                var newEval = newRubricRowEvaluation(performanceLevel, rubricStatement, alignedEvidence, row, this, source);
                return createEvaluation(newEval, row, this)
                    .then(function () {
                        that.updateMergeText(row, enums.PerformanceLevels[performanceLevel - 1]);
                        that.updateInUse();
                        that.updateDone();
                        return newEval;
                    })
            },
            deleteEvaluation: function deleteEvaluation(evaluation) {
                var row = this.rubric[evaluation.rubricRowId];
                var level = enums.PerformanceLevels[evaluation.performanceLevel - 1];

                var that = this;
                return deleteRubricRowEvaluation(evaluation)
                    .then(function () {
                        that.evaluations.splice(that.evaluations.indexOf(evaluation), 1);
                        for (var i in row.parent) {
                            var node = row.parent[i];
                            node.evaluations.splice(node.evaluations.indexOf(evaluation), 1);
                            for (var j in enums.PerformanceLevels) {
                                var nodeLevelEvals = node[enums.PerformanceLevels[j]].evaluations;
                                nodeLevelEvals.splice(nodeLevelEvals.indexOf(evaluation, 1))
                            }
                        }
                        row.evaluations.splice(row.evaluations.indexOf(evaluation), 1);
                        row[level].evaluations.splice(row[level].evaluations.indexOf(evaluation), 1);
                        that.updateMergeText(that.rubric[evaluation.rubricRowId], level);
                        that.updateInUse();
                        that.updateDone();
                    });
            },
            updateEvaluation: function updateEvaluation(evaluation) {
                var that = this;
                return updateRubricRowEvaluation(evaluation).then(function () {
                    that.updateInUse();
                });
            },
            getCollectionObject: function getCollectionObject() {
                var collectionType = this.request.collectionType;
                if (collectionType === enums.EvidenceCollectionType.OBSERVATION) {
                    return this.observation;
                }
                else if (collectionType === enums.EvidenceCollectionType.SELF_ASSESSMENT) {
                    return this.selfAssessment;
                }
                else if (collectionType === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) {
                    return this.studentGrowthGoalBundle;
                }
                else if (collectionType === enums.EvidenceCollectionType.OTHER_EVIDENCE) {
                    return this.otherEvidenceCollection;
                }
                else {
                    return null;
                }
            },
            updateInUse: function updateInUse() {
                if (this.request.collectionType === enums.EvidenceCollectionType.OBSERVATION) {
                    this.observation.inUseClientIds = [];
                }
                for (var i in this.availableEvidence) {
                    this.availableEvidence[i].inUse = false;
                    for (var j in this.evaluations) {
                        for (var k in this.evaluations[j].alignedEvidences) {
                            if (this.availableEvidence[i].id === this.evaluations[j].alignedEvidences[k].availableEvidenceId) {
                                this.availableEvidence[i].inUse = true;
                            }
                        }
                    }
                }

                if (this.request.collectionType === enums.EvidenceCollectionType.OBSERVATION) {
                    for (var i in this.availableEvidence) {
                        if (this.availableEvidence[i].inUse && this.isRubricRowAnnotationEvidenceType(this.availableEvidence[i])) {
                            this.observation.inUseClientIds.push(this.availableEvidence[i].rubricRowAnnotation.clientId);
                        }
                    }
                }
            },
            isRubricRowAnnotationEvidenceType: function (availableEvidence) {
                return availableEvidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_OBSERVATION_NOTES ||
                    availableEvidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_TOR_PRECONF_SUMMARY ||
                    availableEvidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_TOR_POSTCONF_SUMMARY ||
                    availableEvidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_TEE_PRECONF_SUMMARY ||
                    availableEvidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_TEE_POSTCONF_SUMMARY ||
                    availableEvidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_PRECONF_PROMPT ||
                    availableEvidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_POSTCONF_PROMPT;
            },
            deleteRubricNoteRubricRowAnnotation: function deleteRubricNoteRubricRowAnnotation(availableEvidence) {
                var url = config.apiUrl + 'rubricnoteannotation/' + availableEvidence.rubricRowAnnotationId + '/delete';
                var that = this;
                return $http.post(url).then(function (data) {
                    that.deleteAvailableEvidence(availableEvidence);
                })
            },
            addAvailableEvidence: function addAvailableEvidence(availableEvidence, row) {
                availableEvidence.inUse = false;
                row.evidences.push(availableEvidence);
                this.availableEvidence.push(availableEvidence);
                for (var i in row.parent) {
                    row.parent[i].evidences.push(availableEvidence);
                }
                this.updateEvidenceDone();
            },
            deleteAvailableEvidence: function deleteAvailableEvidence(availableEvidence) {
                var row = this.getRowById(availableEvidence.rubricRowId);
                row.evidences.splice(row.evidences.indexOf(availableEvidence), 1);
                this.availableEvidence.splice(this.availableEvidence.indexOf(availableEvidence), 1);
                for (var i in row.parent) {
                    row.parent[i].evidences.splice(row.parent[i].evidences.indexOf(availableEvidence), 1);
                }
                this.updateEvidenceDone();
            },
            deleteUnusedAvailableEvidenceById: function deleteUnusedAvailableEvidenceById(availableEvidenceId) {
                var availableEvidence = _.find(this.availableEvidence, {id: availableEvidenceId});
                if (availableEvidence && !availableEvidence.inUse) {
                    this.deleteAvailableEvidence(availableEvidence);
                }
            },
            unlockObservationFocus: function unlockObservationFocus(observation) {
                this.observation.focused = false;
                this.observation.focusFrameworkNodeId = null;
                this.observation.focusSGFrameworkNodeId = null;
                this.observationService.saveObservation(observation);
            },
            unlockSelfAssessmentFocus: function unlockSelfAssessmentFocus(seflAssessment) {
                this.selfAssessment.focused = false;
                this.selfAssessment.focusFrameworkNodeId = null;
                this.selfAssessment.focusSGFrameworkNodeId = null;
                this.selfAssessmentService.saveSelfAssessment(this.selfAssessment);
            },
            updateMergeText: function updateMergeText(row, level) {
                if (row) {
                    row[level].descriptor = rubricUtils.mergeEvidenceToHtml(row.data[enums.PLAccessor[level]], row[level].evaluations, this);
                    row[level].criticalAttribute = rubricUtils.mergeEvidenceToHtml(row[level].data.criticalAttributeText, row[level].evaluations, this);
                }
            },
            updateDone: function updateDone() {
                for (var i in this.tree) {
                    this.tree[i].evaluationDone = true;
                    for (var j in this.tree[i].nodes) {
                        this.tree[i].nodes[j].evaluationDone = true;
                        for (var k in this.tree[i].nodes[j].rows) {
                            if (!this.tree[i].nodes[j].rows[k].evaluations.length) {
                                this.tree[i].nodes[j].evaluationDone = false;
                                this.tree[i].evaluationDone = false;
                            }
                        }

                    }
                }
            },
            updateEvidenceDone: function updateEvidenceDone() {
                for (var i in this.tree) {
                    this.tree[i].evidenceDone = true;
                    for (var j in this.tree[i].nodes) {
                        this.tree[i].nodes[j].evidenceDone = true;
                        for (var k in this.tree[i].nodes[j].rows) {
                            if (!this.tree[i].nodes[j].rows[k].evidences.length) {
                                this.tree[i].nodes[j].evidenceDone = false;
                                this.tree[i].evidenceDone = false;
                            }
                        }
                    }
                }
            }
            //getCollectionFramework() {
            //    if (this.request.collectionType === ){
            //
            //    }
            //}
        };

        function createEvaluation(evaluation, row, collection) {
            return createRubricRowEvaluation(evaluation)
                .then(function () {
                    collection.evaluations.push(evaluation);
                    for (var i in row.parent) {
                        row.parent[i].evaluations.push(evaluation);
                        row.parent[i][enums.PerformanceLevels[evaluation.performanceLevel - 1]].evaluations.push(evaluation);
                    }
                    collection.rubric[row.data.id].evaluations.push(evaluation);
                    collection.rubric[row.data.id][enums.PerformanceLevels[evaluation.performanceLevel - 1]].evaluations.push(evaluation);
                });
        }

        function newRubricRowEvaluation(performanceLevel, rubricStatement, alignedEvidences, row, collection, source) {
            var evaluatee = activeUserContextService.context.evaluatee;
            var eval = {
                isActive: true,
                rubricRowId: row.data.id,
                evaluationId: evaluatee ? evaluatee.evalData.id : null,
                evidenceCollectionType: (collection.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE) ?
                    enums.EvidenceCollectionType.OTHER_EVIDENCE : collection.request.collectionType,
                evidenceCollectionObjectId: (collection.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE) ?
                    collection.otherEvidenceCollection.id : collection.request.collectionObjectId,
                createdByUserId: activeUserContextService.user.id,
                createdByEvaluatee: activeUserContextService.user.id === evaluatee.id,
                rubricStatement: rubricStatement,
                performanceLevel: performanceLevel,
                alignedEvidences: alignedEvidences,
                additionalInput: '',
                fromCriticalAttr: (source === 'criticalAttribute' ? true : false)
            };

            return eval;
        }

        var state = {
            selectedEvidence: [],
            evaluation: {},
            view: '',
            ignoreFrameworkChange: false,
            frameworkName: '',
            observationEvidenceChanged: false,
            selfAssessmentEvidenceChanged: false,
            otherEvidenceChanged: false,
            studentGrowthEvidenceChanged: false,
            viewOptions: {
                filter: 'normal',
                evidence: true,
                rubric: true,
                criticalAttributes: true,
                evaluations: true
            }
        };

        var service = {
            calcPosition : calcPosition,
            getFramework: getFramework,
            getCurrentFramework: getCurrentFramework,
            getEvidenceCollection: getEvidenceCollection,
            newEvidenceCollection: newEvidenceCollection,
            newAlignedEvidence: newAlignedEvidence,
            getAvailableEvidenceForEvaluation: getAvailableEvidenceForEvaluation,
            getAvailableEvidencesForTorTee: getAvailableEvidencesForTorTee,
            getOtherEvidenceCollectionsForTorTee: getOtherEvidenceCollectionsForTorTee,
            getRubricRowEvaluationsForTorTee: getRubricRowEvaluationsForTorTee,
            getRubricRowLastVisits: getRubricRowLastVisits,
            getRubricRowLastModifiedDates: getRubricRowLastModifiedDates,
            getRubricRowLastVisitsForEC: getRubricRowLastVisitsForEC,
            getRubricRowLastModifiedDatesForEC: getRubricRowLastModifiedDatesForEC,
            cancelEvaluationBuilder: cancelEvaluationBuilder,
            clearSelectedText: clearSelectedText,
            recordLastVisit: recordLastVisit,
            getRubricRowLastVisitsForSGBundle: getRubricRowLastVisitsForSGBundle,
            getRubricRowLastModifiedDatesForSGBundle: getRubricRowLastModifiedDatesForSGBundle,
            checkForEvidenceChange: checkForEvidenceChange,
            getObservationRawScore: getObservationRawScore,
            changeToStateFramework: changeToStateFramework,
            redirectWithHelper: null,
            state: state
        };

        return service;

        function createTemporaryEvaluation() {

        }

        function changeToStateFramework() {
            service.state.ignoreFrameworkChange = true;
            service.state.frameworkName = activeUserContextService.context.frameworkContext.stateFramework.name;
        }

        function getObservationRawScore() {
            // todo: #116718535
            return '4/16';
        }

        function checkForEvidenceChange(rubricRowModifiedByRowId, rubricRowVisitedByRowId,
                                        object, evidenceCollectionType, rubricRowId) {

            var evidenceChanged = false;
            var rubricRowModified = [];
            var rubricRowVisited = [];

            if (rubricRowModifiedByRowId[rubricRowId]) {

                rubricRowModifiedByRowId[rubricRowId].forEach((rowModified) => {
                    if (rowModified.evidenceCollectionType === evidenceCollectionType &&
                        rowModified.evidenceCollectionObjectId === object.id) {
                        rubricRowModified.push(rowModified);
                    }
                });

                if (rubricRowModified.length > 0) {
                    if (rubricRowVisitedByRowId[rubricRowId]) {
                        rubricRowVisitedByRowId[rubricRowId].forEach((rowVisited) => {
                            if (rowVisited.evidenceCollectionType === evidenceCollectionType &&
                                rowVisited.evidenceCollectionObjectId === object.id) {
                                rubricRowVisited.push(rowVisited);
                            }
                        });

                        if (rubricRowVisited.length > 0) {
                            // go through each rubric row modification and see if the rubric row has been
                            // visited since
                            rubricRowModified.forEach((nextLastModified) => {

                                var rrLastVisit = _.find(rubricRowVisited, {rubricRowId: nextLastModified.rubricRowId});

                                if (!rrLastVisit) {
                                    // something new, not visited yet
                                    evidenceChanged = true;
                                }
                                else {
                                    if (rrLastVisit.lastVisitDateTime < nextLastModified.lastModifiedDateTime) {
                                        // something new, not visited yet
                                        evidenceChanged = true;
                                    }
                                }
                            });
                        } else {
                            // something new, not visited yet
                            evidenceChanged = true;
                        }
                    }
                    else {
                        // something new, not visited yet
                        evidenceChanged = true;
                    }
                }
            }
            return evidenceChanged;
        }

        function recordLastVisit(evidenceCollection, rubricRowId) {

            var lastVisitModel = {
                evidenceCollectionType: evidenceCollection.request.collectionType,
                evidenceCollectionObjectId: evidenceCollection.request.collectionObjectId,
                evaluationId: activeUserContextService.context.evaluatee.evalData.id,
                lastVisitDateTime: new Date(),
                rubricRowId: rubricRowId,
                userId: activeUserContextService.user.id
            };

            return $http.post(config.apiUrl + 'evidencecollections/recordvisit', lastVisitModel)
                .then(function (response) {
                });
        }

        function clearSelectedText(evidenceCollection) {
            for (var i in enums.PerformanceLevels) {
                evidenceCollection.updateMergeText(service.state.evaluation.row, enums.PerformanceLevels[i]);
            }
            service.state.evaluation = {};
        }

        function cancelEvaluationBuilder(evidenceCollection) {
            for (var i in enums.PerformanceLevels) {
                evidenceCollection.updateMergeText(service.state.evaluation.row, enums.PerformanceLevels[i]);
            }
            service.state.evaluation = {};
            service.state.selectedEvidence = [];
        }

        function getRubricRowLastVisits() {
            var url = config.apiUrl + 'rubricrowslastvisitdates/' + activeUserContextService.user.id;

            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getRubricRowLastModifiedDates() {
            var url = config.apiUrl + 'rubricrowslastmodifieddates/' + activeUserContextService.user.id;

            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getRubricRowLastVisitsForEC(evidenceCollection) {
            var url = config.apiUrl + 'rubricrowslastvisitdatesforec/' +
                activeUserContextService.user.id + '/' +
                evidenceCollection.request.collectionType + '/' +
                evidenceCollection.request.collectionObjectId;

            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getRubricRowLastModifiedDatesForEC(evidenceCollection) {
            var url = config.apiUrl + 'rubricrowslastmodifieddatesforec/' +
                activeUserContextService.user.id + '/' +
                evidenceCollection.request.collectionType + '/' +
                evidenceCollection.request.collectionObjectId;

            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getRubricRowLastVisitsForSGBundle(bundleId) {
            var url = config.apiUrl + 'rubricrowslastvisitdatesforec/' +
                activeUserContextService.user.id + '/' +
                enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS + '/' +
                bundleId;

            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getRubricRowLastModifiedDatesForSGBundle(bundleId) {
            var url = config.apiUrl + 'rubricrowslastmodifieddatesforec/' +
                activeUserContextService.user.id + '/' +
                enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS + '/' +
                bundleId;

            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getAvailableEvidencesForTorTee() {

            var request = {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.context.orientation.districtCode,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                evaluationType: activeUserContextService.context.workArea().evaluationType,
                evaluatorId: activeUserContextService.user.id,
                roleName: utils.mapEvalTypeToEvaluateeRole(activeUserContextService.context.workArea().evaluationType),
                assignedOnly: activeUserContextService.context.assignedEvaluatees
            };

            var url = config.apiUrl + 'availableevidencetortee';

            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function getRubricRowEvaluationsForTorTee() {

            var request = {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.context.orientation.districtCode,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                evaluationType: activeUserContextService.context.workArea().evaluationType,
                evaluatorId: activeUserContextService.user.id,
                roleName: utils.mapEvalTypeToEvaluateeRole(activeUserContextService.context.workArea().evaluationType),
                assignedOnly: activeUserContextService.context.assignedEvaluatees
            };

            var url = config.apiUrl + 'rubricrowevaluationstortee';

            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }


        function getOtherEvidenceCollectionsForTorTee(activeUserContextService) {

            var request = {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.context.orientation.districtCode,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                evaluationType: activeUserContextService.context.workArea().evaluationType,
                evaluatorId: activeUserContextService.user.id,
                roleName: utils.mapEvalTypeToEvaluateeRole(activeUserContextService.context.workArea().evaluationType),
                assignedOnly: activeUserContextService.context.assignedEvaluatees
            };

            var url = config.apiUrl + 'otherevidencecollectionstortee';

            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function newAlignedEvidence(availableEvidence) {
            var alignedEvidence = {
                rubricRowEvaluationId: 0,
                alignedEvidenceId: 0,
                data: availableEvidence,
                availableEvidenceId: availableEvidence.id,
                additionalInput: '',
                evidenceType: availableEvidence.evidenceType,
                availableEvidenceObjectId: 0
            };

            switch (availableEvidence.evidenceType) {
                case enums.EvidenceType.ARTIFACT:
                    alignedEvidence.availableEvidenceObjectId = availableEvidence.artifactBundleId;
                    break;
                case enums.EvidenceType.STUDENT_GROWTH_GOAL:
                    alignedEvidence.availableEvidenceObjectId = availableEvidence.studentGrowthGoalId;
                    break;
                default:
                    alignedEvidence.availableEvidenceObjectId = availableEvidence.rubricRowAnnotationId;
                    break;
            }

            return alignedEvidence;
        }

        function newEvidenceCollection(name, request, data, view) {
            return new EvidenceCollection(name, request, data, view);
        }

        function getAvailableEvidenceForEvaluation(request) {
            var evaluatee = activeUserContextService.context.evaluatee;
            var request = {
                evaluationId: evaluatee ? evaluatee.evalData.id : null,
                collectionObjectId: request.collectionObjectId,
                collectionType: request.collectionType
            };

            var url = config.apiUrl + 'evidencecollection/availableevidence/';
            return $http.get(url, {params: request}).then(function (response) {
                return response.data;
            });
        }

        function getEvidenceCollection(name, collectionType, collectionObjectId, currentUserId, viewType?:string) {
            viewType = viewType || 'default';
            var evaluatee = activeUserContextService.context.evaluatee;
            var request = {
                evaluationId: evaluatee ? evaluatee.evalData.id : null,
                currentUserId: 0,
                collectionType: collectionType,
                collectionObjectId: collectionObjectId
            };

            request.currentUserId = currentUserId;

            var url = config.apiUrl + 'evidencecollections/';
            return $http.get(url, {params: request}).then(function (response) {
                return new EvidenceCollection(name, request, response.data, viewType);
            });
        }


        function getFramework(name) {
            return frameworkList[name]
        }

        function getCurrentFramework() {
            return frameworkList[this.getActiveFrameworkName()];
        }

        function calcPosition(id) {

            if (typeof id === 'undefined') {
                service.state.evaluation.basicPosition = 150;
                return;
            }

            var base = angular.element('evidence-collection').length > 0 ? angular.element('evidence-collection').offset().top : 0;

            var target = angular.element(id);
            var targetTop = target.length > 0 ? target.offset().top : 0;

            service.state.evaluation.basicPosition = targetTop - base < 150 ? 150  : targetTop - base;
        }

        function buildFramework(frameworkViews, data, evidenceCollection) {
            var userId = activeUserContextService.user.id;
            var useAllRows = evidenceCollection.request.collectionType === enums.EvidenceCollectionType.SUMMATIVE ||
                evidenceCollection.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE;
            var useOnlySG = evidenceCollection.request.collectionType === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS;
            var evaluations = _.groupBy(data.rubricRowEvaluations, 'rubricRowId');
            var frameworkNodeScores = _.groupBy(data.frameworkNodeScores, 'frameworkNodeId');
            var evidences = _.groupBy(data.availableEvidence, 'rubricRowId');
            var rubricRowScores = _.groupBy(data.rubricRowScores, 'rubricRowId');
            var nodeSumScores = _.groupBy(data.summativeFrameworkNodeScores, 'frameworkNodeId');
            var rowSumScores = _.groupBy(data.summativeRubricRowScores, 'rubricRowId');
            var alignment = [];
            switch (evidenceCollection.request.collectionType) {
                case enums.EvidenceCollectionType.OBSERVATION:
                    alignment = _.groupBy(data.observation.alignedRubricRows, 'id');
                    break;
                case enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS:
                    alignment = _.groupBy(data.studentGrowthGoalBundle.alignedRubricRows, 'id');
                    var processRows = [];
                    var resultsRows = [];
                    for (var i in evidenceCollection.studentGrowthGoalBundle.goals) {
                        processRows[evidenceCollection.studentGrowthGoalBundle.goals[i].processRubricRowId] = true;
                        resultsRows[evidenceCollection.studentGrowthGoalBundle.goals[i].resultsRubricRowId] = true;
                    }
                    break;
            }


            var views = [];
            for (var i in frameworkViews) {
                views.push(_.cloneDeep(frameworkViews[i]))
            }
            //builds .rubric
            var fullRows = _(_.reduce(views,
                function (total, next) {
                    return total.concat(rubricUtils.getFlatRows(next));
                }, []))
                .uniq('id')
                .filter(function (n) {
                    return n
                })
                .groupBy('id')
                .mapValues(function (value) {
                    var rowObj = value[0];
                    var r = rubricUtils.formatRubricRow(value[0], evaluations[rowObj.id], evidenceCollection);
                    r.score = {};
                    if (r.data.shortName === '1a') {
                        var x = 1;
                    }
                    r.otherScore = {};
                    if (evidenceCollection.request.collectionType === enums.EvidenceCollectionType.SUMMATIVE ||
                        evidenceCollection.request.collectionType === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS ||
                        evidenceCollection.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE ||
                        (evidenceCollection.request.collectionType === enums.EvidenceCollectionType.PRACTICE_SESSION && evidenceCollection.request.currentUserId === 0)) {
                        r.scores = rubricRowScores[rowObj.id] || [];
                        r.score = rowSumScores[rowObj.id] && rowSumScores[rowObj.id][0] || evidenceCollection.newRubricRowScore(rowObj.id, null, null);
                    } else {
                        var groupedRowScores = _.groupBy(rubricRowScores[rowObj.id], 'createdByUserId');
                        r.score = groupedRowScores[userId] && groupedRowScores[userId][0] || evidenceCollection.newRubricRowScore(rowObj.id, null, null);
                        if (evidenceCollection.data.view === 'merge') {
                            var otherId = activeUserContextService.context.isEvaluatee() ? activeUserContextService.context.evaluator.id : activeUserContextService.context.evaluatee.id;
                            r.otherScore = groupedRowScores[otherId] && groupedRowScores[otherId][0] || evidenceCollection.newRubricRowScore(rowObj.id, null, otherId);
                        }

                    }
                    r.evaluations = evaluations[r.data.id] || [];
                    r.root = evidenceCollection;
                    r.evidences = evidences[r.data.id] || [];
                    r.aligned = alignment[r.data.id] && !!alignment[r.data.id].length;
                    r.disabled = false;
                    r.hidden = !(useAllRows || useOnlySG && r.data.isStudentGrowthAligned || !useOnlySG && !r.data.isStudentGrowthAligned);
                    if (processRows) {
                        r.hidden = r.hidden || (evidenceCollection.studentGrowthGoalBundle.wfState === enums.WfState.SGBUNDLE_PROCESS_SHARED && resultsRows[r.data.id]);
                    }
                    return r;
                }).value();
            //builds .tree
            var tree = {};
            for (var i in views) {
                var name = views[i].name;
                tree[name] = {};
                tree[name].nodes = [];
                tree[name].indexedNodes = [];
                for (var j in views[i].frameworkNodes) {
                    // todo: setting to true to work-around bug see 112124123
                    var nodeId = views[i].frameworkNodes[j].id;
                    var node = {
                        rows: [],
                        root: evidenceCollection,
                        parent: tree[name],
                        data: views[i].frameworkNodes[j],
                        evidences: [],
                        evaluations: [],
                        aligned: false,
                        disabled: false,
                        score: {},
                        scores: [],
                        otherScore: {},
                        evaluationDone: false,
                        evidenceDone: false,
                        hidden: true
                    };
                    for (var q in enums.PerformanceLevels) {
                        node[enums.PerformanceLevels[q]] = {
                            evaluations: []
                        }
                    }
                    for (var k in views[i].frameworkNodes[j].rubricRows) {
                        var rowId = views[i].frameworkNodes[j].rubricRows[k].id;
                        fullRows[rowId].parent[name] = node;
                        node.rows.push(fullRows[rowId]);
                        node.evaluations = node.evaluations.concat(fullRows[rowId].evaluations);
                        for (var l in enums.PerformanceLevels) {
                            if (node.data.shortName === 'D1') {
                                var x = 1;
                            }
                            node[enums.PerformanceLevels[l]].evaluations = node[enums.PerformanceLevels[l]].evaluations.concat(fullRows[rowId][enums.PerformanceLevels[l]].evaluations || []);
                        }
                        node.evidences = node.evidences.concat(fullRows[rowId].evidences);
                        node.aligned = node.aligned || fullRows[rowId].aligned;
                        node.hidden = node.hidden && fullRows[rowId].hidden;
                        delete views[i].frameworkNodes[j].rubricRows[k].frameworkNodeShortName;
                    }
                    //fills scores with non summative scores during summative scoring
                    if (evidenceCollection.request.collectionType === enums.EvidenceCollectionType.SUMMATIVE ||
                        evidenceCollection.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE ||
                        (evidenceCollection.request.collectionType === enums.EvidenceCollectionType.PRACTICE_SESSION && evidenceCollection.request.currentUserId === 0)) {
                        node.scores = frameworkNodeScores[nodeId] || [];
                        //node.score = nodeSumScores[nodeId] && nodeSumScores[nodeId][0]
                        //    || evidenceCollection.newFrameworkNodeScore(nodeId, null);
                        node.score = nodeSumScores[nodeId] && nodeSumScores[nodeId][0] || evidenceCollection.newFrameworkNodeScore(nodeId, null, null);
                    } else {
                        var groupedNodeScores = _.groupBy(frameworkNodeScores[nodeId], 'createdByUserId');
                        node.score = groupedNodeScores[userId] && groupedNodeScores[userId][0] || evidenceCollection.newFrameworkNodeScore(nodeId, null, null);
                        if (evidenceCollection.data.view === 'merge') {
                            var otherNodeId = activeUserContextService.context.isEvaluatee() ? activeUserContextService.context.evaluator.id : activeUserContextService.context.evaluatee.id;
                            node.otherScore = groupedNodeScores[otherNodeId] && groupedNodeScores[otherNodeId][0] || evidenceCollection.newFrameworkNodeScore(nodeId, null, otherNodeId);

                        }
                    }
                    tree[name].nodes.push(node);
                }

                tree[name].data = views[i];
                tree[name].parent = tree;
                tree[name].frameworkMapper = rubricUtils.getFrameworkMapper(views[i]);
                tree[name].evaluationDone = false;
                tree[name].evidenceDone = false;

                // observation focus needs to disable all nodes that don't have a focus rubric row child
                // and disable all rows that aren't under state focus node.
                if ((evidenceCollection.request.collectionType === enums.EvidenceCollectionType.OBSERVATION && evidenceCollection.observation.focused) ||
                    (evidenceCollection.request.collectionType === enums.EvidenceCollectionType.SELF_ASSESSMENT && evidenceCollection.selfAssessment.focused)) {

                    // todo: this can be moved out since it's constant across trees
                    var stateName = activeUserContextService.context.frameworkContext.stateFramework.name;
                    var focusNode = _.find(tree[stateName].nodes, function (node) {
                        var focusId = evidenceCollection.request.collectionType === enums.EvidenceCollectionType.OBSERVATION ? evidenceCollection.observation.focusFrameworkNodeId : evidenceCollection.selfAssessment.focusFrameworkNodeId;
                        return node.data.id === focusId;
                    });

                    tree[name].nodes.forEach(function (node) {
                        node.rows.forEach(function (row) {
                            row.disabled = !_.any(focusNode.rows, function (focusRow) {
                                return row.data.id === focusRow.data.id;
                            });
                        });
                        node.disabled = (_.filter(node.rows, {disabled: false}).length === 0);
                    })
                }


            }
            return {
                rubric: fullRows,
                tree: tree
            };
        }


        function deleteRubricRowEvaluation(rrEvaluation) {
            var url = config.apiUrl + 'rubricrowevaluations/' + rrEvaluation.id;
            return $http.delete(url);
        }

        function createRubricRowEvaluation(rrEvaluation) {
            var url = config.apiUrl + 'rubricrowevaluations/';
            return $http.post(url, rrEvaluation).then(function (response) {
                rrEvaluation.id = response.data.id;
                return response.data;
            });
        }

        function updateRubricRowEvaluation(rrEvaluation) {
            var url = config.apiUrl + 'rubricrowevaluations/';
            return $http.put(url, rrEvaluation).then(function (response) {
                //rrEvaluation.id = response.date.id;
                return response.data;
            });
        }

    }
})();