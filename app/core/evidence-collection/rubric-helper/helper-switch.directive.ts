/// <reference path="../core.module.ts" />

(function() {
    'use strict';

    angular.module('stateeval.core')
    .directive('helperSwitch', helperSwitchDirective)
        .controller('helperSwitchController', helperSwitchController);

    helperSwitchDirective.$inject = [];
    function helperSwitchDirective() {
        return {
            restrict: 'E',
            scope:{
                item: '=',
                type: '='
            },
            templateUrl: 'app/core/evidence-collection/rubric-helper/helper-switch.directive.html',
            controller: 'helperSwitchController'
        }
    }
    helperSwitchController.$inject = ['enums', '$scope', 'activeUserContextService', 'rubricUtils'];
    function helperSwitchController(enums, $scope, activeUserContextService, rubricUtils) {
        $scope.enums = enums;
        $scope.view = $scope.item.root.data.view;
        $scope.user = activeUserContextService.user;
            $scope.$watch(() => {return $scope.item.evaluations.length}, function(newVal, oldVal) {
                $scope.dividedEvaluations = rubricUtils.createDividedEvaluations($scope.item);
            })
       }
}) ();