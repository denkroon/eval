﻿/// <reference path="../core.module.ts" />

(function () {
    'use strict';
    angular.module('stateeval.core')
        .directive('rubricHelper', rubricHelperDirective)
        .controller('rubricHelperController', rubricHelperController);

    function rubricHelperDirective() {
        return {
            restrict: 'E',
            templateUrl: 'app/core/evidence-collection/rubric-helper/rubric-helper.html',
            controller: 'rubricHelperController'
        }
    }

    function rubricHelperController($scope, activeUserContextService, _,
                                    $rootScope, $stateParams, evidenceCollectionService, enums, $state) {

        //If the type is a OBS, PRACTICE_SESSION, or SELF_ASSESSMENT then none of the SG rows should appear
        var context = activeUserContextService.context;
        $scope.enums = enums;
        $scope.vm.context = context;
        evidenceCollectionService.redirectWithHelper = click;
        $scope.vm.evidenceCollectionService = evidenceCollectionService;
        $scope.state = $scope.vm.evidenceCollectionService.state;
        $scope.vm.showEvidenceOnly = false;
        $scope.vm.scoringNow = false;
        $scope.vm.notScoring = true;
        var evidenceCollection = $scope.vm.evidenceCollection;
        switch (evidenceCollection && evidenceCollection.request.collectionType) {
            case enums.EvidenceCollectionType.OTHER_EVIDENCE:
                break;
            case enums.EvidenceCollectionType.OBSERVATION:
                evidenceCollection.readOnly = activeUserContextService.context.observationIsReadOnly(evidenceCollection.observation);
                // todo: #116718535, see evienceCollection.getObservationRawScore
                $scope.vm.rawScore = $scope.vm.evidenceCollectionService.getObservationRawScore();
                if ($scope.vm.evidenceCollection.observation.focused) {
                    evidenceCollectionService.changeToStateFramework();
                }
                break;
            case enums.EvidenceCollectionType.SELF_ASSESSMENT:
                evidenceCollection.readOnly = context.selfAssessmentIsReadOnly(evidenceCollection.selfAssessment);
                if ($scope.vm.evidenceCollection.selfAssessment.focused) {
                    evidenceCollectionService.changeToStateFramework();
                }
                break;
            case enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS:
                evidenceCollection.readOnly =  context.evaluationIsReadOnly() || !context.isAssignedEvaluator();
                evidenceCollection.hideNodeScores = true;

                evidenceCollectionService.changeToStateFramework();
                break;
            case enums.EvidenceCollectionType.SUMMATIVE:
                evidenceCollection.readOnly = context.evaluationIsReadOnly() ||
                    !context.isAssignedEvaluator();
                evidenceCollectionService.changeToStateFramework();
                break;
            case enums.EvidenceCollectionType.LEARNING_WALK_OBS:
                break;
            case enums.EvidenceCollectionType.LEARNING_WALK_SUMMATIVE:
                break;
            case enums.EvidenceCollectionType.PRACTICE_SESSION_OBS:
                break;
            case enums.EvidenceCollectionType.PRACTICE_SESSION_SUMMATIVE:
                break;
            case enums.EvidenceCollectionType.YEAR_TO_DATE:
                evidenceCollection.readOnly = context.evaluationIsReadOnly() || !context.isAssignedEvaluator();
                evidenceCollection.hideNodeScores = true;
                break;
        }

        $scope.vm.name = evidenceCollectionService.state.ignoreFrameworkChange ? evidenceCollectionService.state.frameworkName : activeUserContextService.context.framework.name;
        $scope.vm.type = $scope.vm.type || 'item';
        $scope.vm.selected = [];
        $scope.click = click;
        $scope.vm.startClick = startClick;
        $scope.toggle = toggle;
        $scope.scoreCollection = scoreCollection;
        $scope.evidenceCollectionFilter = noEvidences;
        $scope.checkEvidenceOnly = checkEvidenceOnly;

        if (!$scope.vm.node) {
            startClick($stateParams.nodeShortName, $stateParams.rowId);
        }

        function checkEvidenceOnly(value) {
            $scope.vm.showEvidenceOnly = !value;
            if (!noEvidences($scope.vm[$scope.vm.state.view], null, null)) {
                startClick(null, null);
            }
        }

        function scoreCollection(level) {
            if (evidenceCollection.readOnly) {
                return;
            } else if (level == $scope.vm.evidenceCollection.score.performanceLevel) {
                $scope.vm.evidenceCollection.score.performanceLevel = null;
                $scope.vm.notScoring = true;
            } else {
                $scope.vm.evidenceCollection.score.performanceLevel = level;
            }
            $scope.vm.evidenceCollection.scoreItem($scope.vm.evidenceCollection.score);
        }

        function startClick(nodeShortName, rowId) {
            var row, node;
            if ($scope.noHelperDefault) {
                return;
            } else if (!nodeShortName && rowId) {
                //Only gets row
                row = $scope.vm.evidenceCollection.getRowById(rowId);
                click(row.parent[$scope.vm.name], row);
            } else if (nodeShortName && !rowId) {
                //Only gets node
                click($scope.vm.evidenceCollection.getNode(nodeShortName), null);
            } else if (!nodeShortName && !rowId) {
                //Defaults without node or row
                //clickFirstActiveNodeRow();
                node = _.find($scope.vm.evidenceCollection.tree[$scope.vm.name].nodes, {disabled: false});
                click(node, null);
            } else {
                //Uses both node and row but checks that they are consistent
                row = $scope.vm.evidenceCollection.getRowById(rowId);
                node = $scope.vm.evidenceCollection.getNode(nodeShortName);
                if (row.parent[$scope.vm.name].data.id === node.data.id) {
                    click(node, row);
                } else {
                    console.log('Row not in node');
                }
            }
        }

        function click(node, row) {
            if(node.disabled) {
                return false;
            } else if(row && row.disabled) {
                return false;
            } else if (node && !row) {
                toggle(node);
                //todo always open
                evidenceCollectionService.state.view = 'node';
                $scope.vm.node = node;
                $scope.vm.row = null;
            } else if (node && row) {
                $scope.vm.collapseId = node.data.id;
                $scope.vm.node = node;
                $scope.vm.row = row;
                $scope.vm.selected = [];
                evidenceCollectionService.state.view = 'row';
                evidenceCollectionService.recordLastVisit($scope.vm.evidenceCollection, row.data.id);
            }

            if ($scope.vm.redirect) {
                $scope.vm.redirect(node, row);
            }
            return true;
        }

        function toggle(node) {
            $scope.vm.collapseId = ($scope.vm.collapseId === node.data.id ? undefined : node.data.id);
        }

        $rootScope.$on('change-framework', function () {
            if(!evidenceCollectionService.state.ignoreFrameworkChange) {
                $scope.vm.name = activeUserContextService.context.framework.name;
                if($scope.vm.row && $scope.vm.row.parent[$scope.vm.node]) {
                    $scope.vm.node = $scope.vm.row.parent[$scope.vm.name];
                    evidenceCollectionService.state.view = 'row';
                } else {
                    $scope.vm.row = null;
                    $scope.vm.node = evidenceCollection.tree[$scope.vm.name].nodes[0];
                    evidenceCollectionService.state.view = 'node';
                }
                toggle($scope.vm.node);
            }
        });

        function noEvidences(value, index, array) {
            return !$scope.vm.showEvidenceOnly || value.evidences.length;
        }


    }
})();


