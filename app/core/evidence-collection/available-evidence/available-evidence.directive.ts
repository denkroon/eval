/// <reference path="../../core.module.ts" />

(function () {
    'use strict';

    function availableEvidenceDirective() {
        return {
            restrict: 'E',
            scope: {
                evidenceCollection: '=',
                row: '='
            },
            templateUrl: 'app/core/evidence-collection/available-evidence/available-evidence.directive.html',
            controller: 'availableEvidenceController as vm',
            bindToController: true
        }
    }

    class availableEvidenceController {
        row:any;
        evidenceCollection:any;
        multipleCollections:any;
        isEvaluator:boolean;
        rubricNote:string = '';
        hideUsed:boolean;
        showEvidence:boolean = true;
        showRubricNoteArea:boolean = false;
        filters:any[];
        state;
        context;
        viewOptions: any;

        constructor(private $scope:ng.IScope,
                    public enums:any,
                    public evidenceCollectionService:any,
                    public activeUserContextService:any,
                    public rubricUtils) {
            this.context = activeUserContextService.context;
            this.viewOptions = evidenceCollectionService.state.viewOptions;
            this.state = evidenceCollectionService.state;
            this.multipleCollections = this.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.SUMMATIVE || this.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE;
            this.isEvaluator = this.evidenceCollection.isEvaluator();
            this.hideUsed =
                this.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE ||
                this.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.SUMMATIVE;
                            ;
         //   this.showEvidence = this.evidenceCollection.request.collectionType !== enums.EvidenceCollectionType.YEAR_TO_DATE;

            this.filters = [];
            this.filters.push({name: 'All', selected: true});
            this.filters.push(
                {
                    name: 'Artifacts',
                    fn: function (value, index, array) {
                        return value.evidenceType === enums.EvidenceType.ARTIFACT;
                    }
                }
            );
            if (this.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.OBSERVATION ||
                this.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE ||
                this.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.SUMMATIVE) {
                this.filters.push(
                    {
                        name: 'Obs Notes',
                        fn: function (value, index, array) {
                            return value.evidenceType === enums.EvidenceType.RR_ANNOTATION_OBSERVATION_NOTES;
                        }
                    }
                );
                this.filters.push(
                    {
                        name: 'Pre-Conf',
                        fn: function (value, index, array) {
                            return value.evidenceType === enums.EvidenceType.RR_ANNOTATION_PRECONF_PROMPT ||
                                value.evidenceType === enums.EvidenceType.RR_ANNOTATION_TEE_PRECONF_SUMMARY ||
                                value.evidenceType === enums.EvidenceType.RR_ANNOTATION_TOR_PRECONF_SUMMARY;
                        }
                    }
                );
                this.filters.push(
                    {
                        name: 'Post-Conf',
                        fn: function (value, index, array) {
                            return value.evidenceType === enums.EvidenceType.RR_ANNOTATION_POSTCONF_PROMPT ||
                                value.evidenceType === enums.EvidenceType.RR_ANNOTATION_TEE_POSTCONF_SUMMARY ||
                                value.evidenceType === enums.EvidenceType.RR_ANNOTATION_TOR_POSTCONF_SUMMARY;
                        }
                    }
                );
            }
            this.filters.push(
                {
                    name: 'Other Evidence',
                    fn: function (value, index, array) {
                        return value.evidenceType === enums.EvidenceType.RR_ANNOTATION_RUBRIC_NOTE;
                    }
                }
            );

        }

        selectedFilter() {
            var _this = this;
            var filter = _.find(this.filters, {selected: true}).fn || function () {
                    return true;
                };
            return function (value, index, array) {
                return filter(value, index, array) && !(_this.hideUsed && value.inUse);
            }
        }

        addRubricNoteAnnotation() {
            this.showRubricNoteArea = true;
            this.rubricNote = '';
        }

        saveRubricNoteAnnotation() {
            var that = this;
            this.evidenceCollection.addNewRubricNoteRubricRowAnnotation(this.row, this.rubricNote).then(function () {
                that.rubricNote = '';
                that.showRubricNoteArea = false;
            })
        }

        cancelRubricNoteAnnotation() {
            this.rubricNote = '';
            this.showRubricNoteArea = false;
        }

        toggleHideUsed() {
            this.hideUsed = !this.hideUsed;
        }

        check(evidence) {


            this.evidenceCollectionService.calcPosition();

            if (this.evidenceCollection.readOnly ||
                this.evidenceCollectionService.state.view === 'node' ||
                (this.evidenceCollectionService.state.evaluation.row && evidence.rubricRowId !== this.evidenceCollectionService.state.evaluation.row.data.id)) {
                return;
            }
            if (_.includes(this.evidenceCollectionService.state.selectedEvidence, evidence)) {
                _.remove(this.evidenceCollectionService.state.selectedEvidence, evidence);
            }
            else {
                this.evidenceCollectionService.state.selectedEvidence.push(evidence);
            }

            if(this.evidenceCollectionService.state.selectedEvidence.length) {
                if(!this.evidenceCollectionService.state.evaluation.row) {
                    this.evidenceCollectionService.state.evaluation.row = this.evidenceCollection.getRowById(evidence.rubricRowId);
                }
            }


        }

        mapEvaluateeTerm(type) {
            return type.replace('Evaluatee', this.activeUserContextService.getEvaluateeTermUpperCase());
        }

    }

    angular.module('stateeval.core')
        .directive('availableEvidence', availableEvidenceDirective)
        .controller('availableEvidenceController', availableEvidenceController);
})();
