/// <reference path="../core.module.ts" />

(function () {
'use strict';

    angular.module('stateeval.core')
    .directive('reportAvailableEvidence', reportAvailableEvidenceDirective)
    .controller('reportAvailableEvidenceController', reportAvailableEvidenceController);

    reportAvailableEvidenceDirective.$inject = [];
    function reportAvailableEvidenceDirective() {
        return {
            restrict: 'E',
            scope: {
                evidenceCollection: '=',
                row: '='
            },
            templateUrl: 'app/core/evidence-collection/available-evidence/report-available-evidence.directive.html',
            controller: 'reportAvailableEvidenceController as vm',
            bindToController: true
        }
    }

    reportAvailableEvidenceController.$inject = ['enums', 'evidenceCollectionService', 'activeUserContextService', 'config', '$scope', 'rubricUtils'];
    function reportAvailableEvidenceController(enums, evidenceCollectionService, activeUserContextService, config, $scope, rubricUtils) {
        var vm = this;
        vm.evidenceCollectionService = evidenceCollectionService;
        vm.enums = enums;
        vm.showEvidence = true;

        vm.mapEvaluateeTerm = mapEvaluateeTerm;

        function mapEvaluateeTerm(type) {
            return type.replace('Evaluatee', activeUserContextService.getEvaluateeTermUpperCase());
        }
    }
}) ();
