/// <reference path="../core.module.ts" />

/**
 * Created by anne on 9/13/2015.
 */
(function () {
    'use strict';

    angular.module('stateeval.core')
        .directive('rubricRowEvaluation', rubricRowEvaluationDirective)
        .controller('rubricRowEvaluationController', rubricRowEvaluationController);


    function rubricRowEvaluationDirective() {
        return {
            restrict: 'E',
            scope: {
                rrEval: '=',
                report: '=',
                evidenceCollection: '=',
                deleteFcn: '&',
                updateFcn: '&'
            },
            templateUrl: 'app/core/evidence-collection/rubric-row-evaluation/rubric-row-evaluation.directive.html',
            controller: 'rubricRowEvaluationController as vm',
            bindToController: true
        }
    }

    rubricRowEvaluationController.$inject = ['utils', 'enums', 'config', 'evidenceCollectionService', '$scope', 'activeUserContextService', 'rubricUtils'];

    function rubricRowEvaluationController(utils, enums, config, evidenceCollectionService, $scope, activeUserContextService, rubricUtils) {
        var vm = this;
        vm.rubricUtils = rubricUtils;
        vm.enums = enums;
        vm.user = activeUserContextService.user;
        vm.evidenceCollectionService = evidenceCollectionService;
        vm.additionalInput = '';
        vm.itemCreatedHere = vm.evidenceCollection.itemCreatedInCurrentCollection(vm.rrEval);
        vm.ratingInitailCssClass = 'initial ';
        vm.evalSourceType = rubricUtils.createEvalSource(vm.rrEval.evidenceCollectionType);
        if (vm.evidenceCollection.data.view === 'merge') {
            vm.ratingInitailCssClass+= ((activeUserContextService.context.evaluatee.id === vm.rrEval.createdByUserId) ? "initial-secondary":"initial-primary");
        }
        else {
            // default style is already assigned in css
            vm.ratingInitailCssClass+= 'initial-default';
        }

        // there's only one student growth goal bundle so no instance title, just the source type
        if (vm.rrEval.evidenceCollectionType === vm.enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) {
            vm.sourceTitle = "<label>Source (" + vm.evalSourceType + ")</label>";
        }
        else {
            vm.sourceTitle = "<label>Source (" + vm.evalSourceType + "):&nbsp;</label>" +
                             "<span>" +
                                vm.evidenceCollection.associatedCollections
                                [vm.enums.AssociatedCollectionsAccessor[vm.rrEval.evidenceCollectionType]]
                                [vm.rrEval.evidenceCollectionObjectId]
                                .title  +
                            "</span>"
        }

        vm.editModeRREval = false;
        vm.startEditRREval = startEditRREval;
        vm.saveRREval = saveRREval;
        vm.cancelEditRREval = cancelEditRREval;
        vm.deleteRREval = deleteRREval;
        vm.clearRREvalComment = clearRREvalComment;

        vm.evidenceEditModes = [];
        vm.startEditEvidence = startEditEvidence;
        vm.saveEvidence = saveEvidence;
        vm.cancelEditEvidence = cancelEditEvidence;
        vm.deleteEvidence = deleteEvidence;
        vm.clearEvidenceComment = clearEvidenceComment;

        vm.editing = false;

        function startEditEvidence(evidence) {
            if (vm.editing) return;
            vm.editing = true;
            vm.evidenceEditModes[evidence.id] = true;
            vm.additionalInput = evidence.additionalInput;
        }

        function cancelEditEvidence(evidence) {
            vm.editing = false;
            vm.additionalInput = '';
            vm.evidenceEditModes[evidence.id] = false;
        }

        function deleteEvidence(evidence) {
            if (vm.editing) return;
            vm.rrEval.alignedEvidences = _.reject(vm.rrEval.alignedEvidences, {id: evidence.id});
            saveEvaluation();
        }

        function saveEvidence(evidence) {
            evidence.additionalInput = vm.additionalInput;
            saveEvaluation().then(function () {
                vm.editing = false;
                vm.evidenceEditModes[evidence.id] = false;
                vm.additionalInput = '';
            })
        }

        function clearEvidenceComment(evidence) {
            if (vm.editing) return;
            evidence.additionalInput = '';
            saveEvaluation();
        }

        function startEditRREval() {
            if (vm.editing) return;
            vm.editing = true;
            vm.editModeRREval = true;
            vm.additionalInput = vm.rrEval.additionalInput;
        }

        function cancelEditRREval() {
            vm.editing = false;
            vm.additionalInput = '';
            vm.editModeRREval = false;
        }

        function deleteRREval() {
            if (vm.editing) return;
            vm.deleteFcn()
                .then(function () {
                    vm.editing = false;
                    vm.editModeRREval = false;
                })
        }

        function saveRREval() {
            vm.rrEval.additionalInput = vm.additionalInput;
            saveEvaluation().then(function () {
                vm.editing = false;
                vm.additionalInput = '';
                vm.editModeRREval = false;
            })
        }

        function clearRREvalComment() {
            if (vm.editing) return;
            vm.rrEval.additionalInput = '';
            saveEvaluation();
        }

        function saveEvaluation() {
            return vm.updateFcn();
        }
    }
})();
