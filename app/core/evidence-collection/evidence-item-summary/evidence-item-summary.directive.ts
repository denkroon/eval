/// <reference path="../../core.module.ts" />

(function() {
    angular.module('stateeval.core')
        .directive('evidenceItemSummary', evidenceItemSummaryDirective)
        .controller('evidenceItemSummaryController', evidenceItemSummaryController);

    evidenceItemSummaryDirective.$inject = [];

    function evidenceItemSummaryDirective() {

        return {
            bindToController: true,
            restrict: 'E',
            scope: {
                evidenceCollection: '=',
                evidence: '=',
                readOnly: '=',
                showSource: '=',
                report: '='
            },
            templateUrl: 'app/core/evidence-collection/evidence-item-summary/evidence-item-summary.directive.html',
            controller: 'evidenceItemSummaryController as vm'
        }
    }

    evidenceItemSummaryController.$inject = ['utils', 'enums', 'rubricUtils', 'activeUserContextService',
        'observationService', 'userPromptService', '$confirm', 'evidenceCollectionService'];

    function evidenceItemSummaryController(utils, enums, rubricUtils, activeUserContextService,
       observationService, userPromptService, $confirm, evidenceCollectionService) {
        var vm = this;
        vm.enums = enums;
        vm.state = evidenceCollectionService.state;
        vm.itemCreatedHere = vm.evidenceCollection.itemCreatedInCurrentCollection(vm.evidence);
        vm.readOnlyStudentGrowthEvidence =
            ((vm.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) &&
             (vm.evidence.evidenceType === enums.EvidenceType.STUDENT_GROWTH_GOAL_PROMPT ||
                                           vm.evidence.evidenceType === enums.EvidenceType.STUDENT_GROWTH_GOAL));

        vm.evalSourceType = rubricUtils.createEvalSource(vm.evidence.evidenceCollectionType);
        vm.removeEvidence = removeEvidence;

        // we display the  entire conference prompt/response, but we strip out all coding
        // except any for the current rubric row
        if (vm.evidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_PRECONF_PROMPT ||
            vm.evidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_POSTCONF_PROMPT) {
            var rrAnnotation = vm.evidence.rubricRowAnnotation;
            var rowMapper = rubricUtils.getRowMapper(activeUserContextService.getActiveFramework());
            rrAnnotation.annotation = utils.getTextWithShortName(rrAnnotation.annotation, rowMapper[rrAnnotation.rubricRowId]);
        }

        // for observation notes, we retrieve the notes here from the clientId so that the user can continue
        // to edit coded areas that are not in-use.
        // NOTE: we have to be careful to get the correct observation from the evidence collection. We want the one that
        // is being used by the notes editor, so that we are retrieving the observation notes for this annotation
        // from the most up-to-date version. That observation is directly off the evidenceCollection. But if we're in the YTD
        // or SUMMATIVE view, then we need to get it off the observations collection.
        if (vm.evidence.evidenceType === enums.EvidenceType.RR_ANNOTATION_OBSERVATION_NOTES) {
            var rrAnnotation = vm.evidence.rubricRowAnnotation;
            var observation = null;
            if (vm.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.OBSERVATION) {
                observation = vm.evidenceCollection.observation;
            }
            else {
                observation = _.find(vm.evidenceCollection.observations, {id: rrAnnotation.evidenceCollectionObjectId});
            }
            rrAnnotation.annotation = utils.getEncodedText(rrAnnotation.clientId, observation.observeNotes);
        }

        // there's only one student growth goal bundle so no instance title, just the source type
        if (vm.evidence.evidenceCollectionType === vm.enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) {
            vm.sourceTitle = "<label>Source (" + vm.evalSourceType + ")</label>";
        }
        else if (vm.evidence.evidenceCollectionType === vm.enums.EvidenceCollectionType.OTHER_EVIDENCE &&
                 vm.evidence.evidenceType === vm.enums.EvidenceType.RR_ANNOTATION_RUBRIC_NOTE) {
            vm.sourceTitle = "<label>Source (YTD Other Evidence)</label>";
        }
        else if (vm.evidence.evidenceCollectionType === vm.enums.EvidenceCollectionType.OTHER_EVIDENCE &&
            vm.evidence.evidenceType === vm.enums.EvidenceType.ARTIFACT) {
            vm.sourceTitle = "<label>Source (" + vm.evalSourceType + "):&nbsp;</label>" +
                "<span>" + vm.evidence.artifactBundle.shortName + "</span>";
        }
        else {
            vm.sourceTitle = "<label>Source (" + vm.evalSourceType + "):&nbsp;</label>" +
                "<span>" +
                vm.evidenceCollection.associatedCollections
                    [vm.enums.AssociatedCollectionsAccessor[vm.evidence.evidenceCollectionType]]
                    [vm.evidence.evidenceCollectionObjectId]
                    .title +
                "</span>";
        }


        function deleteAnnotationFromDbAndUpdateEvidenceCollection() {
            observationService.deleteCodedRubricRowAnnotation(vm.evidenceCollection, vm.evidence.rubricRowAnnotation.clientId);
        }

        function removeAnnotationFromCodedNotes(notes) {
            return utils.removeAnnotation(notes, vm.evidence.rubricRowAnnotation.clientId);
        }

        function removeObservationNotePropertyAnnotationEvidence(propName) {
            var observation = vm.evidenceCollection.observation;
           var newVal = removeAnnotationFromCodedNotes(observation[propName]);
            observation[propName] = newVal;
            observationService.saveObserveNotes(observation.id, newVal, propName).then(function() {
                deleteAnnotationFromDbAndUpdateEvidenceCollection();
            })
        }

        function removeObservationConferencePromptAnnotationEvidence() {
            userPromptService.getUserPromptResponseById(vm.evidence.rubricRowAnnotation.userPromptResponseId).then(function(userPromptResponse) {
                userPromptResponse.response =  removeAnnotationFromCodedNotes(userPromptResponse.response);
                userPromptService.saveObservationUserPromptResponse(userPromptResponse).then(function() {
                    deleteAnnotationFromDbAndUpdateEvidenceCollection();
                })
            })
        }

        function removeEvidence($event: ng.IAngularEvent) {
            $event.stopPropagation();
            $event.preventDefault();
            $confirm({ text: 'Are you sure you want to remove this evidence?' }).then(() => {

                if (vm.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.OBSERVATION) {
                    switch (vm.evidence.evidenceType) {
                        case enums.EvidenceType.RR_ANNOTATION_OBSERVATION_NOTES:
                            removeObservationNotePropertyAnnotationEvidence('observeNotes');
                            break;
                        case enums.EvidenceType.RR_ANNOTATION_TOR_PRECONF_SUMMARY:
                            removeObservationNotePropertyAnnotationEvidence('evaluatorPreConNotes');
                            break;
                        case enums.EvidenceType.RR_ANNOTATION_TOR_POSTCONF_SUMMARY:
                            removeObservationNotePropertyAnnotationEvidence('evaluatorPostConNotes');
                            break;
                        case enums.EvidenceType.RR_ANNOTATION_TEE_PRECONF_SUMMARY:
                            removeObservationNotePropertyAnnotationEvidence('evaluateePreConNotes');
                            break;
                        case enums.EvidenceType.RR_ANNOTATION_TEE_POSTCONF_SUMMARY:
                            removeObservationNotePropertyAnnotationEvidence('evaluateePostConNotes');
                            break;
                        case enums.EvidenceType.RR_ANNOTATION_PRECONF_PROMPT:
                        case enums.EvidenceType.RR_ANNOTATION_POSTCONF_PROMPT:
                            removeObservationConferencePromptAnnotationEvidence();
                            break;
                        case enums.EvidenceType.RR_ANNOTATION_RUBRIC_NOTE:
                            vm.evidenceCollection.deleteRubricNoteRubricRowAnnotation(vm.evidence);
                            break;
                        default:
                            throw Error("evidence-item-summary: removeEvidence: unrecognized evidence type: " + vm.evidence.evidenceType);
                    }
                }
                else if (vm.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.YEAR_TO_DATE) {
                    switch (vm.evidence.evidenceType) {
                        case enums.EvidenceType.RR_ANNOTATION_RUBRIC_NOTE:
                            vm.evidenceCollection.deleteRubricNoteRubricRowAnnotation(vm.evidence);
                            break;
                        default:
                            throw Error("evidence-item-summary: removeEvidence: unrecognized evidence type: " + vm.evidence.evidenceType);
                    }
                }
                else if (vm.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.SELF_ASSESSMENT) {
                    switch (vm.evidence.evidenceType) {
                        case enums.EvidenceType.RR_ANNOTATION_RUBRIC_NOTE:
                            vm.evidenceCollection.deleteRubricNoteRubricRowAnnotation(vm.evidence);
                            break;
                        default:
                            throw Error("evidence-item-summary: removeEvidence: unrecognized evidence type: " + vm.evidence.evidenceType);
                    }
                }
            });
        }
    }
})();


