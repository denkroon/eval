/// <reference path="../../core.module.ts" />

(function () {
    'use strict';
    function scoreButtonGroupDirective() {
        return {
            restrict: 'E',
            scope: {
                item: '='
            },
            templateUrl: 'app/core/evidence-collection/score-button-group/score-button-group.html',
            controller: 'scoreButtonGroupController as vm',
            bindToController: true
        }
    }

    class ScoreButtonGroupController {
        item;
        score;
        otherScore;
        state;
        colorObj;
        dividedEvaluations;
        user;
        districtConfig;

        constructor(public enums,
                    public activeUserContextService,
                    public rubricUtils,
                    private evidenceCollectionService,
                    private $q,
                    $scope: ng.IScope)
        {
            this.state = evidenceCollectionService.state;
            this.score = this.item.score;
            this.districtConfig = activeUserContextService.context.frameworkContext.districtConfiguration;
            this.otherScore = this.item.otherScore;
            this.colorObj = rubricUtils.colorObj;
            this.user = activeUserContextService.user;
            $scope.$watch(() => {return this.item.evaluations.length},
                () => {this.dividedEvaluations = rubricUtils.createDividedEvaluations(this.item)});
        }

        redirectToRow() {
            if(this.item.rows) {
                throw 'sent a node';
            }
            if (typeof this.evidenceCollectionService.redirectWithHelper === 'function') {
                this.evidenceCollectionService.redirectWithHelper(this.item.parent[this.activeUserContextService.context.framework.name], this.item);
            } else {
                console.log('helper not defined');
            }
        }

        getHighlightId (score, otherScore, $index): string {
            return this.rubricUtils.getHighlightId(score, otherScore, $index);
        }

        scoreItem(level) {
            if(!this.item.root.readOnly) {
                this.score.performanceLevel = (this.score.performanceLevel === level ? null : level);
                return this.item.root.scoreItem(this.score);
            } else {
                return this.$q.when();
            }
        }

        //count(level) {
        //    return this.item.root.data.view === 'default' ? this.item[level].evaluations.length : this.dividedEvaluations[level][this.user.id].length;
        //}
    }

    angular.module('stateeval.core')
        .directive('scoreButtonGroup', scoreButtonGroupDirective)
        .controller('scoreButtonGroupController', ScoreButtonGroupController);

})();

