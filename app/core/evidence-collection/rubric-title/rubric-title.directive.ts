(function () {
    'use strict';

    function rubricTitleDirective() {
        return {
            restrict: 'E',
            scope: {
                item: '=',
                evalStyle: '@',
                hideScores: "="
            },
            templateUrl: 'app/core/evidence-collection/rubric-title/rubric-title.directive.html',
            controller: 'rubricTitleController as vm',
            bindToController: true
        }
    }

    class RubricTitleController {
        item:any;
        score:any;

        constructor(public enums:any) {
            this.score = this.item.score;
        }
    }

    angular.module('stateeval.core')
    .directive('rubricTitle', rubricTitleDirective)
    .controller('rubricTitleController', RubricTitleController);
}) ();
