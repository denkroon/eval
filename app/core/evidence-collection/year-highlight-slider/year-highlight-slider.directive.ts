/// <reference path="../../core.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.core')
        .directive('yearHighlightSlider', yearHighlightSliderDirective)
        .controller('yearHighlightSliderController', yearHighlightSliderController);

    yearHighlightSliderDirective.$inject = [];
    function yearHighlightSliderDirective() {
        return {
            restrict: 'E',
            templateUrl: 'app/core/evidence-collection/year-highlight-slider/year-highlight-slider.html',
            controller: 'yearHighlightSliderController'


        }
    }

    yearHighlightSliderController.$inject = ['$scope', 'enums', 'rubricUtils'];
    function yearHighlightSliderController($scope, enums, rubricUtils) {

        var evaluations = _.sortBy($scope.vm.row.evaluations, creationDateTime);
        if(evaluations.length > 0) {
            $scope.vm.beginning = (new Date(_.first(evaluations).creationDateTime));
            $scope.vm.end = (new Date(_.last(evaluations).creationDateTime));
            $scope.vm.row.modifiedEvaluations = [];
            enums.var = 1;

            $scope.vm.options = {
                start: [$scope.vm.beginning.valueOf(), $scope.vm.end.valueOf()],
                range: {min: $scope.vm.beginning.valueOf(), max: $scope.vm.end.valueOf()}
            };

            //todo figure out how to debounce the slider or go back to change
            $scope.vm.events = {
                update: updateHighlighting
            };
            updateHighlighting([[$scope.vm.beginning.valueOf(), $scope.vm.end.valueOf()]]);
        }

        function updateHighlighting(values) {
            $scope.vm.beginning = new Date(Number(values[0][0]));
            $scope.vm.end = new Date(Number(values[0][1]));
            $scope.displayBeginning = enums.Months[$scope.vm.beginning.getMonth() - 1] + ' ' + $scope.vm.beginning.getDate();
            $scope.displayEnd = enums.Months[$scope.vm.end.getMonth() - 1] + ' ' + $scope.vm.end.getDate();

            for (var i in enums.PerformanceLevels) {
                var evaluations = _.filter($scope.vm.row[enums.PerformanceLevels[i]].evaluations, function (n) {
                    if ((new Date(n.creationDateTime)).valueOf()
                        >= $scope.vm.beginning.valueOf()
                        && (new Date(n.creationDateTime)).valueOf()
                        <= $scope.vm.end.valueOf()) {
                        $scope.vm.row.modifiedEvaluations.push($scope.vm.row[enums.PerformanceLevels[i]].evaluations);
                        return true;
                    }
                    return false;
                });
                $scope.vm.row[enums.PerformanceLevels[i]].modifiedDescriptor =
                    rubricUtils.mergeEvidenceToHtml($scope.vm.row[enums.PerformanceLevels[i]].data.descriptorText, evaluations, $scope.vm.row.root);
            }
            console.log('CHANGE', enums.var++);
        }



        function creationDateTime(n) {
            return n.creationDateTime;
        }
    }
})();
