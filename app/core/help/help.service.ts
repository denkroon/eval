namespace StateEval.Core {

    const settingsKey = 'Help:Hide'

    export class HelpService {

        static get HelpHidden() { return 'HelpService:Hidden'; }

        constructor(
            private $rootScope: ng.IRootScopeService
        ) {}

        hideHelp() {
            localStorage.setItem(settingsKey, 'true');
            this.$rootScope.$broadcast(HelpService.HelpHidden, true);
        }

        showHelp() {
            localStorage.setItem(settingsKey, 'false');
            this.$rootScope.$broadcast(HelpService.HelpHidden, false);
        }

        helpVisible() {
            return localStorage.getItem(settingsKey) != 'true';
        }
    }

    angular.module('stateeval.core')
        .service('help', HelpService);
}