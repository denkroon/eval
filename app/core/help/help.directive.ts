namespace StateEval.Core {

    export class HelpController {
        showPopup = false;
        hideButton: boolean;

        constructor(
            private help: StateEval.Core.HelpService,
            $scope: ng.IScope
        ) {
            this.hideButton = !help.helpVisible();
            $scope.$on(HelpService.HelpHidden, (event, hidden: boolean) => {
                this.hideButton = hidden;
            })
        }

        toggleButton() {
            if (this.hideButton)
                this.help.hideHelp();
            else
                this.help.showHelp();
        }

        togglePopup() {
            console.log('toggle')
            this.showPopup = !this.showPopup;
        }
    }

    angular.module('stateeval.core')
        .directive('help', () => {
            return {
                restrict: 'E',
                transclude: true,
                scope: true,
                templateUrl: 'app/core/help/help.html',
                controller: HelpController,
                controllerAs: 'vm'
            }
        })
}