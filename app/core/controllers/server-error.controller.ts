/// <reference path="../core.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.core')
        .controller('serverErrorController', serverErrorController);

    serverErrorController.$inject = ['$stateParams', '$state', 'activeUserContextService'];
    function serverErrorController($stateParams, $state, activeUserContextService) {
        var vm = this;

        vm.goHome = goHome;

        vm.errorInfo = $stateParams.errorInfo;

        function goHome() {
            $state.go(activeUserContextService.context.workArea().navbar[0].state)
        }
    }
})();