/// <reference path="../core.module.ts" />

/**
 * globalController - controller
 */
(function () {
    angular
        .module('stateeval.core')
        .controller('globalController', globalController);

    globalController.$inject = ['$rootScope', '$state', 'signalRService', 'config', 'utils'];

    /* @ngInject */

    function globalController($rootScope, $state, signalRService, config, utils) {
        var vm = this;

        vm.errorMessage = '';

        vm.signalRUrl = config.apiUrl.replace('/api/', '/signalR/hubs');

        utils.appendScript(vm.signalRUrl);

        activate();

        function activate() {

            $rootScope.$on('server-error', function (response, errorInfo) {
                $state.go('server-error', {errorInfo: errorInfo});
            });
        }
    }
})();

