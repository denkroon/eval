/// <reference path="../core.module.ts" />
(function() {
 'use strict';

    class noOrientationBaseController {

        user: any;
        evalLogo: string;
        showOrientation: boolean;

        constructor(
            private utils: any,
            private user: any

        ) {
            this.showOrientation = false;
            this.user = user;
            this.evalLogo = utils.getEvalLogo();
        }
    }

    angular
        .module('stateeval.core')
        .controller('noOrientationBaseController', noOrientationBaseController);
})();


