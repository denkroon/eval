/// <reference path="../core.module.ts" />
namespace StateEval.Core {

    export class BaseController {

        versionString = '';
        evaluating: boolean;
        impersonating: boolean;

        constructor(
            appInfoService: any,
            activeUserContextService: any
        ) {
            this.evaluating = activeUserContextService.context.isEvaluating();
            this.impersonating = activeUserContextService.context.isImpersonating();
            appInfoService.getVersionString().then((versionString: string) => {
                this.versionString = versionString;
            });
        }
    }

    angular
        .module('stateeval.core')
        .controller('baseController', BaseController);
}
