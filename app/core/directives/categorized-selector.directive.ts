/// <reference path="../core.module.ts" />

(function() {
    'use strict';

    angular.module('stateeval.core')
        .directive('categorizedSelector', categorizedSelectorDirective)
        .controller('categorizedSelectorController', categorizedSelectorController);

    categorizedSelectorDirective.$inject = [];
    function categorizedSelectorDirective() {
        return {
            restrict: 'E',
            templateUrl: 'app/core/views/categorized-selector.directive.html',
            controller: 'categorizedSelectorController as vm',
            scope: {},
            bindToController: {
                editHeader: '@',
                viewHeader: '@',
                categories: '=',
                readOnly: '=',
                changed: '&',
                locked: '=',
                unlocked: '='
            }
        }
    }

    categorizedSelectorController.$inject = ['_'];
    function categorizedSelectorController(_) {
        var vm = this;

        vm.unlock = unlock;

        function unlock() {
            vm.unlocked();
        }
        // default to edit mode only when there is no selection
        vm.editMode = selectedRows().length == 0 && !vm.readOnly;

        vm.selectedRowCount = selectedRows().length;
        vm.showDone = false;
        updateShowDone();

        var initialSelection;

        function updateShowDone() {
            vm.showDone = selectedRows().length>0;
        }

        vm.toggleEditMode = function toggleEditMode() {
            vm.editMode = !vm.editMode;
            if (vm.editMode) {
                // saving list of selected rows when entering edit mode.
                initialSelection = selectedRows();
             }
            else {
                // getting list of selected rows when done with edit mode.
                var newSelection = selectedRows();
                if (_.xor(newSelection, initialSelection).length != 0) {
                    var e = new ChangeEvent();
                    // if there are differences, call the parent
                    vm.changed({ $selectedRows: newSelection, $event: e });

                    // if the changes are cancelled, we re-select the initial items
                    if (e.cancelled()) {
                        vm.clear();
                        _.forEach(initialSelection, function(s) { s.selected = true });
                        vm.editMode = true;
                    }
                }
                vm.selectedRowCount = selectedRows().length;
            }
        }

        vm.clear = function clear() {
            _.forEach(vm.categories, function(c) {
                _.forEach(c.rows, function(r) { if (!r.inUse) r.selected = false; });
            });
            updateShowDone();
        }

        // used by the view to do some custom handling for categories that have selected rows
        vm.selectionCount = function selectionCount(category) {
            return _.select(category.rows, { selected: true }).length;
        }

        vm.canClear = function canClear() {
            return _.any(vm.categories, function(c) {
                return _.any(c.rows, { selected: true });
            });
        }

        vm.toggleRow = function toggleRow(row, $event) {
            if ($event.target.tagName == 'INPUT') {
                updateShowDone();
                return;
            }

            if (!row.inUse && !row.readOnly) {
                row.selected = !row.selected;
                updateShowDone();
            }
        }

        // returns an array with the ids of all the rows selected at the time of calling
        function selectedRows() {
            return _(vm.categories)
                .pluck('rows')
                .flatten()
                .filter({ selected: true })
                .value();
        }
    }

    // simple event object used to let a parent controller cancel the changes made
    // in a directive.
    function ChangeEvent() {
        this._cancel = false;
    }

    ChangeEvent.prototype = {
        cancel: function() {
            this._cancel = true;
        },
        cancelled: function() {
            return this._cancel;
        }
    }

}) ();
