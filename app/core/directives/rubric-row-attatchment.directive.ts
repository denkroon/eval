﻿/// <reference path="../core.module.ts" />

(function () {
    'use strict';
    angular.module('stateeval.core')
        .directive('rubricRowAttatchment', rubricRowAttatchmentDirective)
        .controller('rubricRowAttatchmentController', rubricRowAttatchmentController);

    rubricRowAttatchmentDirective.$inject = ['$rootScope', '$q', '$http', '$timeout', 'activeUserContextService',
        'observationService', '$stateParams', 'artifactService'];
    rubricRowAttatchmentController.$inject = ['$scope', 'activeUserContextService', 'userPromptService',
        'config', '_', '$rootScope', '$stateParams'];

    function rubricRowAttatchmentDirective($rootScope, $q, $http, $timeout, activeUserContextService,
       observationService, $stateParams, artifactService) {
        return {
            restrict: 'A',
            scope: {
                rubricRow: '=',
                evidenceCollection: '='
            },
            //templateUrl: 'app/core/views/rubric-helper.html',
            link: function (scope, elm, attrs) {

                var row = scope.evidenceCollection.rubric[scope.rubricRow.id];;

                for (var i in row.evidences) {
                    if (evidenceIsUsed(row, row.evidences[i])) {
                        $(elm).append("<span class='fa fa-file'></span>");
                    }
                    else {
                        $(elm).append("<span class='fa fa-file-o'></span>");
                    }
                }
            },
            controller: 'rubricHelperController as vm'
        }
    }

    function evidenceIsUsed(row, evidence) {
        var inUse = false;
        for (var i in row.evaluations) {
            for (var j in row.evaluations[i].alignedEvidences) {
                if (row.evaluations[i].alignedEvidences[j].availableEvidenceId === evidence.id) {
                    inUse = true;
                    break;
                }
            }
            if (inUse) {
                break;
            }
        }
        return inUse;
    }

    function rubricRowAttatchmentController($scope, activeUserContextService, userPromptService, config, _, $rootScope, $stateParams) {
        var vm = this;
        activate();

        function activate() {
        }

    }
})();


