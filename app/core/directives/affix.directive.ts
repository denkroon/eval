/// <reference path="../core.module.ts" />

(function () {
'use strict';
    // taken and adapted from http://jsfiddle.net/MatthewLothian/kBpxb/7/
    angular.module('stateeval.core')
        .directive('affix', ['$window', '$timeout', function ($window, $timeout) {
            return {
                $scope: true,
                link: function ($scope, $element, $attrs) {
                    var relativeTo = // overide or use default: body (html for IE) element
                        !!$attrs.relativeTo ? angular.element(window.document.getElementById($attrs.relativeTo)) :
                            !$scope.$root.isIE ? // specify your own isIE check will default to body element
                                angular.element(window.document.getElementsByTagName('body')) :
                                angular.element(window.document.getElementsByTagName('html')), // required by IE
                        win = !!$attrs.relativeTo ? relativeTo : angular.element($window),
                        // if a relativeTo other than the default is set we will
                        // offset the affixed position by it's offset
                        relativeToOffset = null,
                        fixedAt = null,
                        left = null,
                        width = null,
                        el = $($element[0]);

                    // boolean
                    $scope.affixed = false;
                    $scope.offset = 0;

                    // Obviously, whenever a scroll occurs, we need to check and possibly
                    // adjust the position of the affixed $element.
                    win.on('scroll', checkPosition);

                    // on resize recalculate fixedAt position
                    win.on('resize', resize);

                    function resize() {
                        $scope.$apply(function () {
                            $scope.affixed = false;
                            $scope.offset = {};
                        });
                        relativeToOffset = null;
                        fixedAt = null;
                        left = null;
                        width = null;

                        $timeout(function() {
                            relativeToOffset = relativeToOffset || win[0].offsetTop || 0;
                            fixedAt = fixedAt || el.offset().top - relativeToOffset;
                            left = left || el.offset().left;
                            width = el.outerWidth() || el.parent().outerWidth();
                            $scope.offset = { left: left + 'px', width: width + 'px' };
                            $scope.affixed = fixedAt <= relativeTo[0].scrollTop;
                        }, 1000);                
                    };

                    // calculate if we need to affix element
                    function checkPosition() {
                        relativeToOffset = relativeToOffset || win[0].offsetTop || 0;
                        fixedAt = fixedAt || el.offset().top - relativeToOffset;
                        left = left || el.offset().left;
                        width = el.outerWidth() || el.parent().outerWidth();
                        $scope.$apply(function () {
                            $scope.offset = { left: left + 'px', width: width + 'px' };
                            $scope.affixed = fixedAt <= relativeTo[0].scrollTop;
                        });
                    }

                    $scope.$on('$destroy', () => {
                        win.off('scroll', checkPosition);
                        win.off('resize', resize);
                    });

                    $scope.$on('$viewContentLoaded',
                         function(event){
                            relativeToOffset = relativeToOffset || win[0].offsetTop || 0;
                            fixedAt = fixedAt || el.offset().top - relativeToOffset;
                            left = left || el.offset().left;
                            width = el.outerWidth() || el.parent().outerWidth();
                            $scope.offset = { left: left + 'px', width: width + 'px' };
                            $scope.affixed = fixedAt <= relativeTo[0].scrollTop;
                        }
                    );                    
                }
            };
        }]);
}) ();
