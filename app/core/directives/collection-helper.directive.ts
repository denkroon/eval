/// <reference path="../core.module.ts" />

(function() {
    'use strict';

    angular.module('stateeval.core')
    .directive('collectionHelper', collectionHelperDirective);

    collectionHelperDirective.$inject = [];
    function collectionHelperDirective() {
        return {
            restrict: 'E',
            templateUrl: 'app/core/views/collection-helper.directive.html'
        }
    }
}) ();