/// <reference path="../core.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.core')
        .directive('datetimepicker', datetimepickerDirective);

    datetimepickerDirective.$inject = [];
    function datetimepickerDirective() {
		return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attributes, ctrl) {

				var format = typeof attributes.format === 'undefined' ? 'YYYY-MM-DD' : attributes.format;

                element.datetimepicker({
                    format: format
                });
                var picker = element.data("DateTimePicker");

                ctrl.$formatters.push(function (value) {
                    var date = moment(value);
                    if (date.isValid()) {
                        return date.format(format);
                    }
                    return '';
                });

                element.on('change', function (event) {
                    console.log('date changed!',  picker.getDate())
                    scope.$apply(function() {
                        var date = picker.getDate();
                        ctrl.$setViewValue(date.valueOf());
                    });
                });
            }
        };

    }

})();
