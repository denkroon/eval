﻿/// <reference path="../core.module.ts" />

(function () {
    'use strict';
    angular.module('stateeval.core')
        .directive('frameworkNodeStyle', frameworkNodeStyleDirective)
        .controller('frameworkNodeController', frameworkNodeStyleController);

    frameworkNodeStyleDirective.$inject = ['$rootScope', '$q', '$http', '$timeout', 'observationService', '$stateParams', 'rubricRowEvaluationService', 'enums'];
    frameworkNodeStyleController.$inject = ['$scope', 'activeUserContextService', 'userPromptService',
        'config', '_', '$rootScope', '$stateParams'];

    function frameworkNodeStyleDirective($rootScope, $q, $http, $timeout, observationService, $stateParams, rubricRowEvaluationService, enums) {
        return {
            restrict: 'A',
            scope: {
                frameworkNode: '=',
                evidenceCollection:  '='
            },
            //templateUrl: 'app/core/views/rubric-helper.html',
            link: function (scope, elm, attrs) {

                var collectionObject = scope.evidenceCollection.getCollectionObject();
                var alignedRubricRows = collectionObject && collectionObject.alignedRubricRows;
                var node = scope.evidenceCollection.getNode(scope.frameworkNode.shortName);

                // done is if there is at least one rubricRowEvaluation for each aligned rubric row within the criteria
                // there has to be at least one rubric row that has evidence available
                var nodeDone = true;
                var evidenceCount = 0;
                for (var r in node.rows) {
                    var rowShortName = node.rows[r];
                    if (alignedRubricRows && _.find(alignedRubricRows, {id: node[rowShortName].data.id})) {
                        $(elm).find(".node-name").css('background-color', '  #2f4050', '!important');
                    }
                    if (node[rowShortName].evidences.length > 0) {
                        evidenceCount++;
                        if (node[rowShortName].evaluations.length === 0) {
                            nodeDone = false;
                        }
                    }
                }

                if (evidenceCount>0 && nodeDone) {
                    $(elm).find(".node-name").css('background-color', '#1ab394', '!important');
                }
            },
            controller: 'rubricHelperController as vm'
        }
    }

    function frameworkNodeStyleController($scope, activeUserContextService, userPromptService, config, _, $rootScope, $stateParams) {
        var vm = this;
        activate();

        function activate() {
        }



    }
})();


