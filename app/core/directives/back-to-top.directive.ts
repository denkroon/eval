(function() {
    'use strict';

    class BackToTopController {
        visible = false;

        constructor($scope: ng.IScope, private smoothScroll: any) {
            angular.element(document).on('scroll', () => {
                $scope.$apply('vm.visible = ' + (document.body.scrollTop > 20));
            });
        }

        clicked() {
            this.smoothScroll(document.body, {
                offset: 0,
                duration: 500
            });
        }
    }

    angular
        .module('stateeval.core')
        .controller('backToTopController', BackToTopController)
        .directive('backToTop', () => {
            return {
                restrict: 'E',
                scope: true,
                replace: true,
                templateUrl: 'app/core/views/back-to-top.directive.html',
                controller: 'backToTopController as vm'
            }
        });
})();