﻿/// <reference path="../core.module.ts" />

(function () {
    'use strict';
    angular.module('stateeval.core')
        .directive('messageNotifier', messageNotifierDirective)
        .controller("messageNotifierController", messageNotifierController);

    messageNotifierDirective.$inject = ['$rootScope', '$q', '$http', '$timeout', 'observationService', 'activeUserContextService', 'enums', '$uibModal', 'utils', '$filter'];

    function messageNotifierDirective($rootScope, $q, $http, $timeout, observationService, activeUserContextService, enums, $uibModal, utils, $filter) {
        return {
            restrict: 'E',
            scope: {
            },
            template: "<div ng-show='vm.showNotificaiton' class='message_notifier'><div class=\"pull-right\" ng-click=\"vm.closeMessage();\" style=\"margin-right: 10px; cursor: pointer;\">X</div><div class=\"clearfix\"></div>" +
                "<div><div class='notification-message-title'>{{vm.message.Title}}</div><div class='notification-message'>{{vm.message.Description}}</div></div></div>",
            link: function (scope, elm, attrs) {
            },
            controller: 'messageNotifierController as vm'
        }
    }

    messageNotifierController.$inject = ['observationService', 'config', 'enums', '$scope', 'activeUserContextService'];
    function messageNotifierController(observationService, config, enums, $scope, activeUserContextService) {
        var vm = this;
        vm.enums = enums;
        vm.messages = [];
        vm.message = {
        };
        vm.showNotificaiton = false;

        vm.closeMessage = function () {
            if (vm.messages.length > 0) {
                var index = vm.messages.indexOf(vm.message);
                vm.messages.splice(index, 1);
                vm.message = vm.messages[vm.messages.length - 1];
                vm.showNotificaiton = vm.messages.length > 0;
            }
        }

        $scope.$on('notification-received', function (e, message) {
            var user = activeUserContextService.getActiveUser();
            if (message.ReceiverUserId == user.id) {
                vm.messages.push(message);
                vm.message = message;
                vm.showNotificaiton = true;
                $scope.$apply();
            }
        });
    }
})();


