/// <reference path="../core.module.ts" />

(function () {
'use strict';

    angular.module('stateeval.core')
    .directive('listFilter', listFilterDirective)
    .controller('listFilterController', listFilterController);

    listFilterDirective.$inject = [];
    function listFilterDirective() {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'app/core/views/list-filter.directive.html',
            controller: 'listFilterController as vm',
            bindToController: {
                filters: '=',
                list: '='
            }
        }
    }

    listFilterController.$inject = [];
    function listFilterController() {
        var vm = this;

        vm.opened = false;

        vm.toggle = function toggle() {
            vm.opened = !vm.opened;
            if (!vm.opened) {
                _.forEach(vm.filters, function(f) { f.selected = false; });
                vm.filters[0].selected = true; // set back to All on close
            }
        }

        vm.select = function select(filter) {
            _.forEach(vm.filters, function(f) { f.selected = false; });
            filter.selected = true;
        }

        vm.filterCount = function filterCount(filter) {
            return _.filter(vm.list, filter.fn).length;
        }
    }
}) ();
