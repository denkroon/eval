(function() {
    'use strict';

    angular
        .module('stateeval.core')
        .directive('pageHeader', () => {
            return {
                restrict: 'E',
                scope: {
                    title: '='
                },
                transclude: true,
                templateUrl: 'app/core/views/page-header.directive.html',
            }
        });
})();
