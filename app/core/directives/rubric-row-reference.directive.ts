(function () {
    'use strict';

    angular.module('stateeval.core')
        .directive('rubricRowReference', rubricRowReferenceDirective)
        .controller('rubricRowReferenceController', rubricRowReferenceController);

    rubricRowReferenceDirective.$inject = [];
    function rubricRowReferenceDirective() {
        return {
            restrict: 'E',
            scope: {
                evidenceCollection: '=',
                row: '='
            },
            templateUrl: 'app/core/views/rubric-row-reference.directive.html',
            controller: 'rubricRowReferenceController as vm',
            bindToController: true
        }
    }

    rubricRowReferenceController.$inject = ['utils', 'enums'];

    function rubricRowReferenceController(utils, enums) {
        var vm = this;
        vm.enums = enums;
        vm.getSafeHtml = utils.getSafeHtml;
    }
})();
