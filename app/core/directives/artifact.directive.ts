(function() {
    'use strict';

    angular.module('stateeval.core')
    .directive('artifact', artifactDirective)
    .controller('artifactController', artifactController);

    artifactDirective.$inject = [];
    function artifactDirective() {
        return {
            restrict: 'E',
            scope: {
                artifact: '=',
                bundle: '=',
                back: '='
            },
            controller: 'artifactController as vm',
            bindToController: true,
            templateUrl: 'app/core/views/artifact.directive.html'
        }
    }
    artifactController.$inject = ['enums', 'activeUserContextService', 'utils', 'artifactService', '$stateParams', '$state'];
    function artifactController(enums, activeUserContextService, utils, artifactService, $stateParams, $state) {
        var vm = this;
        vm.enums = enums;

        vm.evaluateeTerm = utils.getEvaluateeTermUpperCase(activeUserContextService.context.evaluationType());
        vm.source = '';
        vm.origin = $stateParams.artifactOrigin || 'artifacts';


        vm.itemTypeToString = utils.mapLibItemTypeToString;
        vm.itemDisplayName = artifactService.libItemDisplayName;
        vm.viewItem = artifactService.viewItem;

        vm.goBack = goBack;

        activate();

        function activate() {

            if (vm.artifact.createdByUserId === activeUserContextService.context.evaluatee.id) {
                vm.source = vm.evaluateeTerm;
            }
            else {
                vm.source = 'Evaluator';
            }
        }

        function goBack() {
            $state.go(vm.back.state);
        }

    }
}) ();