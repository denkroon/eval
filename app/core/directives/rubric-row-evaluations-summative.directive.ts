/**
 * Created by anne on 9/13/2015.
 */
(function () {
    'use strict';

    angular.module('stateeval.core')
        .directive('rubricRowEvaluationsSummative', rubricRowEvaluationsSummativeDirective)
        .controller('rubricRowEvaluationsSummativeController', rubricRowEvaluationsSummativeController);


    function rubricRowEvaluationsSummativeDirective() {
        return {
            restrict: 'E',
            scope: {
                rrEvals: '=',
                evidenceCollection: '='
            },
            templateUrl: 'app/core/views/rubric-row-evaluations-summative.directive.html',
            controller: 'rubricRowEvaluationsSummativeController as vm',
            bindToController: true
        }
    }

    rubricRowEvaluationsSummativeController.$inject = ['enums'];

    function rubricRowEvaluationsSummativeController(enums) {
        var vm = this;
        vm.enums = enums;

        activate();

        function activate() {

        }


    }
})();
