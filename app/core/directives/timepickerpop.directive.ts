/// <reference path="../core.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.core')
        .directive('timepickerPop', timepickerPopDirective);

    timepickerPopDirective.$inject = ['$document'];
    function timepickerPopDirective($document) {
        return {
            restrict: 'E',
            templateUrl: 'app/core/views/timepickerpop.directive.html',
            scope: {
                inputTime: "=",
                showMeridian: "=",
                readonly: "=",
                changed: '&'
            },
            controller: function ($scope, $element) {
                $scope.isOpen = false;
                $scope.timeRegex = $scope.showMeridian ? '(0?[1-9]|1[012])(:[0-5]\\d) [APap][mM]' : '([01]\\d|2[0-3]):([0-5]\\d)';

                $scope.toggle = function () {
                    $scope.isOpen = !$scope.isOpen;
                };

                $scope.open = function () {
                    $scope.isOpen = true;
                    if (!$scope.readonly) {

                        var min = Math.floor(moment().minute() / 15) * 15;
                        var curdate = moment().minute(min).toDate();
                        $scope.inputTime = $scope.inputTime ? $scope.inputTime : curdate;
                    }
                };

                // $scope.valueChanged = function() {
                //     console.log('changed!')
                //     $scope.changed();
                // }
            },
            link: function (scope, element, attrs) {

                element.bind('click', function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                });

                $document.bind('click', function (event) {
                    scope.$apply(function () {
                        scope.isOpen = false;
                    });
                });
            }
        }
    }
})();
