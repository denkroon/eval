﻿var rubricCodeScope = null;
(function () {
    'use strict';
    angular.module('stateeval.core')
        .directive('rubricCoder', rubricCoderDirective);

    rubricCoderDirective.$inject = ['$rootScope', '$q', '$http', '$timeout',
        'observationService', 'activeUserContextService', 'enums', 'utils'];

    function rubricCoderDirective($rootScope, $q, $http, $timeout,
                                  observationService, activeUserContextService, enums, utils) {
        return {
            restrict: 'A',
            scope: {
                evidenceCollection: '=',
                evalSession: '=',
                evidenceType: '='
            },
            link: function (scope, elm, attrs) {
                rubricCodeScope = scope;
                var evidenceType = attrs.evidenceType;
                var timer;
                var framework;

                framework = activeUserContextService.getActiveFramework();
                reRender();

                $rootScope.$on('change-framework', function () {
                    framework = activeUserContextService.getActiveFramework();
                    if (framework != null) {
                        reRender();
                    }
                });

                scope.$on("code-removed", function (event, args) {
                    var clientId = args.clientId;
                    saveResponse().then(function() {
                        observationService.deleteCodedRubricRowAnnotation(scope.evidenceCollection, clientId).then(function (data) {
                        });
                    });
                 });

                function reRender() {
                    if (!framework || !framework.name) {
                        return false;
                    }

                    elm.find(".custom-group").remove();

                    var observation = scope.evidenceCollection.observation;

                    for (var i in framework.frameworkNodes) {
                        var frameworkNode = framework.frameworkNodes[i];
                        // evidenceCollection tracks whether disabled due to focus
                        var node = scope.evidenceCollection.getNodeById(framework.frameworkNodes[i].id);
                        if (node.disabled) {
                            continue;
                        }
                        if (frameworkNode.shortName) {
                            var nodeDiv = $("<div title='" +  frameworkNode.title + "' class='custom-group btn-group'><div class='btn btn-default btn-sm btn-small framework_node_toolbar framework_node'>" + frameworkNode.shortName + "</div>");
                            for (var j in frameworkNode.rubricRows) {
                                var rubricRow = frameworkNode.rubricRows[j];

                                var row = scope.evidenceCollection.getRowById(rubricRow.id);
                                if (row.disabled) {
                                    continue;
                                }
                                if (rubricRow.shortName) {
                                    var btn = $("<div title='" + rubricRow.title + "' class='btn btn-default btn-sm btn-small rubric_toolbar rubric_" + rubricRow.shortName + "'>" + rubricRow.shortName + "</div>");
                                    btn[0].rubric = rubricRow;
                                    btn[0].rubric.frameworkNodeShortName = frameworkNode.shortName;
                                    btn.bind("click", buttonClicked);
                                    nodeDiv.append(btn);
                                }
                            }

                            $(elm).append(nodeDiv);
                        }
                    }

                    function buttonClicked() {
                        var clientId = utils.getNewGuid();
                        var startNode = $("<span class='node-desc'>" + this.rubric.shortName + "</span>");
                        var endNode = $("<span class='node-desc-end'>" + this.rubric.shortName + "</span><span class='glyphicon glyphicon-remove remove-code-selection' onClick=\"removeRubricCode('" + clientId + "')\" title='Remove'></span>");
                        var className = "selected_area" + this.rubric.id;
                        var wrapperSpan = document.createElement("span");

                        wrapperSpan.setAttribute("clientId", clientId.toString());

                        wrapperSpan.className = className + " editor-selected";

                        var msg = "";
                        if (window.getSelection) {
                            var sel = window.getSelection();
                            var area = $(".code-area-" + attrs.codeId);
                            if (area.find($(sel.focusNode)).length > 0) {
                                if (sel.rangeCount) {
                                    var range = sel.getRangeAt(0).cloneRange();
                                    range.surroundContents(wrapperSpan);
                                    sel.removeAllRanges();
                                    sel.addRange(range);
                                    //msg = wrapperSpan.textContent;
                                    var elm = $(wrapperSpan).clone();
                                    
                                    elm.find(".node-desc").remove();
                                    elm.find('.node-desc-end').remove();                                    
                                    msg = elm.text();
                                                     
                                    if (msg) {
                                        if (scope.evalSession) {

                                            if (scope.evidenceType === enums.EvidenceType.RR_ANNOTATION_PRECONF_PROMPT ||
                                                scope.evidenceType === enums.EvidenceType.RR_ANNOTATION_POSTCONF_PROMPT) {

                                                var responseId = parseInt(attrs.codeId);

                                                scope.evidenceCollection.addNewUserPromptResponseRubricRowAnnotation(this.rubric, msg,
                                                    scope.evidenceType, responseId, clientId);
                                            }
                                            else if (scope.evidenceType ===  enums.EvidenceType.RR_ANNOTATION_TEE_PRECONF_SUMMARY ||
                                                scope.evidenceType ===  enums.EvidenceType.RR_ANNOTATION_TEE_POSTCONF_SUMMARY) {
                                                scope.evidenceCollection.addNewConferenceNotesRubricRowAnnotation(this.rubric, msg,
                                                    scope.evidenceType, clientId);
                                            }

                                            $("." + className).prepend(startNode);
                                            $("." + className).append(endNode);
                                            //startNode.insertBefore($("." + className));
                                            //endNode.insertAfter($("." + className));
                                            $("." + className).removeClass(className);
                                            saveResponse();


                                        }
                                    } else {
                                        wrapperSpan.remove();
                                    }
                                }
                            }

                            /*   if (msg) {

                             startNode.insertBefore($("." + className));
                             endNode.insertAfter($("." + className));
                             $("." + className).removeClass(className);
                             }*/
                        }

                        //codeArea = elm.next(".code-area-" + attrs.codeId);
                        //codedResponse = $(codeArea).html();
                        //var text = utils.getTextWithShortName(codedResponse, "1a");
                        //$(codeArea).html(text);
                    }
                }

                function saveResponse() {
                    var responseId = parseInt(attrs.codeId);
                    var codeArea = elm.next(".code-area-" + attrs.codeId);
                    var codedResponse = $(codeArea).html();

                    if (scope.evidenceType === enums.EvidenceType.RR_ANNOTATION_PRECONF_PROMPT ||
                        scope.evidenceType === enums.EvidenceType.RR_ANNOTATION_POSTCONF_PROMPT) {
                        return observationService.updateUserPromptResponse(scope.evalSession.id, responseId, codedResponse);
                    } else if (scope.evidenceType === enums.EvidenceType.RR_ANNOTATION_TEE_PRECONF_SUMMARY) {
                        scope.evalSession.evaluateePreConNotes = codedResponse;
                        return observationService.saveObserveNotes(scope.evalSession.id, codedResponse, 'evaluateePreConNotes');
                    } else if (scope.evidenceType === enums.EvidenceType.RR_ANNOTATION_TEE_POSTCONF_SUMMARY) {
                        scope.evalSession.evaluateePostConNotes = codedResponse;
                        return observationService.saveObserveNotes(scope.evalSession.id, codedResponse, 'evaluateePostConNotes');
                    }
                }
            }
        }
    }
})();




function removeRubricCode(clientId) {
    var injector = angular.element('*[ng-app]').injector();
    var $confirm = injector.get('$confirm');    
    
    $confirm({text:'Are you sure to delete?', title:''}).then(function()
    {
        var codeArea = $("[clientid=\'" + clientId + "\']");
        codeArea.find(".node-desc, .node-desc-end, .remove-code-selection").remove();
        codeArea.contents().unwrap();
        rubricCodeScope.$broadcast("code-removed", { clientId: clientId });
    });
    
  
  /*  if (confirm('Are you sure to delete?')) {
        var codeArea = $("[clientid=\'" + clientId + "\']");
        codeArea.find(".node-desc, .node-desc-end, .remove-code-selection").remove();
        codeArea.contents().unwrap();
        rubricCodeScope.$broadcast("code-removed", { clientId: clientId });
    }
    */
}