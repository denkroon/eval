/// <reference path="../core.module.ts" />

(function() {
    'use strict';

    angular.module('stateeval.core')
    .directive('alignmentString', alignmentStringDirective)
    .controller('alignmentStringController', alignmentStringController)

    alignmentStringDirective.$inject = [];
    function alignmentStringDirective() {
        return {
            restrict: 'E',
            scope: {
                object: '=',
                redirect: '=',
                inLine: '=',
                collection: '@',
                idName: '@'
            },
            controller: 'alignmentStringController as vm',
            bindToController: true,
            templateUrl: 'app/core/views/alignment-string.directive.html'
        }
    }
    alignmentStringController.$inject = ['activeUserContextService', 'rubricUtils', '$rootScope', '$scope'];
    function alignmentStringController(activeUserContextService, rubricUtils, $rootScope, $scope) {
        var vm = this;

        vm.collection = vm.collection || 'alignedRubricRows';
        vm.idName = vm.idName || 'id';

        $rootScope.$on('change-framework', function() {
            if (vm.object) {
                activate();
            }
        });

        $scope.$watch('vm.object', function (newValue, oldValue) {
            vm.items = newValue;
            if (vm.object) {
                activate();
            }
        });

        function activate() {
            vm.alignment = rubricUtils.buildAlignmentDisplayString(activeUserContextService, vm.object, vm.collection, vm.idName);
            var x = 1;
        }

        //the redirect function gets nullified if the vm.startClosed = true;

    }
}) ();