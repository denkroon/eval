(function() {
    'use strict';

    angular
        .module('stateeval.core')
        .directive('evalAlert', () => {
            return {
                restrict: 'E',
                scope: {
                },
                transclude: true,
                templateUrl: 'app/core/views/eval-alert.directive.html',
            }
        });
})();
