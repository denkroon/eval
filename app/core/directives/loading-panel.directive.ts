(function() {

    angular
        .module('stateeval.core')
        .directive('loadingPanel', () => {
            return {
                restrict: 'E',
                transclude: true,
                templateUrl: 'app/core/views/loading-panel.html',
                scope: {
                    // loaded should evaluate to true when the content is loaded and should be shown
                    loaded: '='
                }
            }
        })

})();