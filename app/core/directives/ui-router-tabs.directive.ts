/// <reference path="../core.module.ts" />

(function() {
    'use strict';

    angular.module('stateeval.core')
    .directive('uiRouterTabs', uiRouterTabsDirective)
        .controller('uiRouterTabsController', uiRouterTabsController);

    uiRouterTabsDirective.$inject = [];
    function uiRouterTabsDirective() {
        return {
            restrict: 'E',
            templateUrl: 'app/core/views/ui-router-tabs.directive.html',
            controller: 'uiRouterTabsController',
            scope: {
                links: '=',
                navigate: '=?',
            }
        }
    }


    uiRouterTabsController.$inject = ['$scope', '$state'];
    function uiRouterTabsController($scope, $state) {
        $scope.state = $state;
        $scope.navigate = $scope.navigate || function (link) {
            $state.go(link.state, link.params);
        }
    }
}) ();