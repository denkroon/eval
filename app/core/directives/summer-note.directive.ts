﻿/*

    Issues:

    - uses globals: summerNoteScope and removeCode
    - does not really nest codes: adding a nested code will split the parent's span in 2!
    - wide open for script injection: API does not sanitize!
    - does not handle pasting with the mouse (can paste over and remove some coding tags)

*/

var summerNoteScope = null;

(function () {
    'use strict';
    angular.module('stateeval.core')
        .directive('summerNoteNew', SummerNoteDirective)
        .controller("summerNoteModalController", summerNoteModalController)
        .controller("summerNoteController", summerNoteController);

    function SummerNoteDirective($rootScope, $timeout, observationService,
                                 practiceSessionService, activeUserContextService, enums, $uibModal, utils, $filter) {
        return {
            restrict: 'A',
            scope: {
                noteOwner: '=',
                evidenceCollection: '=',
                noteProperty: '=',
                saveMessage: '='
            },
            controller: 'summerNoteController as vm',
            link: function (scope, elm, attrs) {
                summerNoteScope = scope;
                var timer;
                var framework;
                var codeBeforeChange = "";

                elm.summernote({
                    minHeight: 200,
                    toolbar: [
                        ['edit', ['undo', 'redo']],
                        ['style', ['bold', 'italic', 'underline']],
                        ['alignment', ['ul', 'ol']],
                        ['misc', ['fullscreen']]
                    ],
                    callbacks: {
                        onPaste: function (e) {
                            var bufferText = ((e.originalEvent || e).clipboardData).getData('Text');
                            e.preventDefault();
                            setTimeout(function () {
                                document.execCommand('insertText', false, bufferText);
                            }, 10);
                        },
                        onKeyup: function (e) {
                            saveNote();
                        },
                        onEnter: function (e) {
                            //validateChange();
                        },
                        onKeypress: function (e) {
                            var k = 1;
                        },
                        onKeydown: function (e) {
                            if (!(e.keyCode >= 37 && e.keyCode <= 40) && e.ctrlKey == false) {
                                var event = e || window.event;
                                validateChange(e, e.keyCode);
                            }

                            if(e.ctrlKey && (e.keyCode==86 || e.keyCode==88))
                            {
                                var event = e || window.event;
                                validateChange(e, e.keyCode);
                            }

                            codeBeforeChange = elm.summernote('code');
                        },
                        onChange: function (contents, $editable) {

                        }
                    }
                });

                elm.on('summernote.change', function (we, contents, $editable) {
                });

                function validateChange(event, keyCode) {
                    var selection = getSelection();

                    if ($(selection.container).hasClass("node-desc") || $(selection.container).hasClass("node-desc-end") || $(selection.container).hasClass("remove-code-selection")) {
                        if (keyCode == 13 && codeBeforeChange) {
                            elm.summernote('code', codeBeforeChange);
                        }

                        event.preventDefault();
                    } else {
                        if (selection.selectedArea.toString()) {
                            if ($(selection.selectedArea).find(".node-desc").length > 0 || $(selection.selectArea).find(".node-desc-end").length > 0) {
                                validateMessageDeleteWhichContainCode(event, selection, keyCode);
                            } else {
                                validateChangeWithinCode(event, selection, keyCode);
                            }
                        }
                    }


                    console.log('summernote\'s content is changed.');
                }

                function validateMessageDeleteWhichContainCode(event, selection, keyCode) {
                    if ($(selection.selectedArea).find(".node-desc").length > 0 || $(selection.selectArea).find(".node-desc-end").length > 0) {

                        event.preventDefault();
                        if (keyCode == 13 && codeBeforeChange) {
                            elm.summernote('code', codeBeforeChange);
                        }
                        var modalInstance = $uibModal.open({
                            templateUrl: 'app/core/views/observation-note-modal-deletion.html',
                            controller: 'summerNoteModalController as vm',
                            resolve: {
                                data: function () {
                                    return {
                                        //message: "Before you can edit this text, you would have to remove the existing selection. once you finish editing, re-select the desired section again and apply coding.?"
                                    };
                                }
                            }
                        });

                        modalInstance.result.then(function (result) {
                            var clientIdList = [];
                            $(selection.selectedContent).find(".editor-selected").each(function (index, selectedElm) {
                                var clientId = $(selectedElm).attr("clientid");
                                clientIdList.push(clientId);
                            });

                            if (clientIdList.length > 0) {
                                observationService.deleteCodedRubricRowAnnotations(scope.evidenceCollection, clientIdList).then(function (data) {
                                    selection.selectionElm.deleteFromDocument();
                                    saveNote();
                                });
                            }
                        });
                    }
                }

                function validateChangeWithinCode(event, selection, keyCode) {
                    var container = selection.container;

                    if ($(container).hasClass("editor-selected") || $(container).parents(".editor-selected").length>0) {
                        if (keyCode == 13 && codeBeforeChange) {
                            elm.summernote('code', codeBeforeChange);
                        }
                        var clientId = $(container).attr('clientid');
                        event.preventDefault();
                        var modalInstance = $uibModal.open({
                            templateUrl: 'app/core/views/observation-note-modal.html',
                            controller: 'summerNoteModalController as vm',
                            resolve: {
                                data: function () {
                                    return {
                                        message: "Before you can edit this text, you would have to remove the existing selection. once you finish editing, re-select the desired section again and apply coding.?"
                                    };
                                }
                            }
                        });

                        modalInstance.result.then(function (result) {
                            var container1 = $("[clientid=\'" + clientId + "\']");
                            container1.find(".node-desc, .node-desc-end, .remove-code-selection").remove();
                            container1.contents().unwrap();

                            $(container).find(".node-desc, .node-desc-end, .remove-code-selection").remove();
                            $(container).contents().unwrap();
                            codeBeforeChange = elm.summernote('code');
                            observationService.deleteCodedRubricRowAnnotation(scope.evidenceCollection, clientId).then(function (data) {
                                saveNote();
                            });
                        });
                    }
                }

                function getSelection() {
                    var range, sel, container;
                    if (window.getSelection) {
                        sel = window.getSelection();
                        if (sel.getRangeAt) {
                            if (sel.rangeCount > 0) {
                                range = sel.getRangeAt(0);
                            }
                        } else {
                            // Old WebKit selection object has no getRangeAt, so
                            // create a range from other selection properties
                            range = document.createRange();
                            range.setStart(sel.anchorNode, sel.anchorOffset);
                            range.setEnd(sel.focusNode, sel.focusOffset);

                            // Handle the case when the selection was selected backwards (from the end to the start in the document)
                            if (range.collapsed !== sel.isCollapsed) {
                                range.setStart(sel.focusNode, sel.focusOffset);
                                range.setEnd(sel.anchorNode, sel.anchorOffset);
                            }
                        }

                        if (range) {
                            container = range.commonAncestorContainer;
                            // Check if the container is a text node and return its parent if so
                            return {
                                container: container.nodeType === 3 ? container.parentNode : container,
                                selectedArea: container,
                                selectionElm: sel,
                                selectedContent: range.cloneContents()
                            }
                        }
                    }
                }

                if (scope.noteOwner) {
                    elm.summernote('code', scope.noteOwner[scope.noteProperty]);
                }

                scope.$on("code-deleted", function (event, args) {
                    var clientId = args.clientId;
                    observationService.deleteCodedRubricRowAnnotation(scope.evidenceCollection, clientId).then(function (data) {
                        saveNote();
                    });
                });

                function saveNote() {
                    $timeout(() => {
                        scope.saveMessage = "Saving...";
                    })
                    $timeout.cancel(timer);
                    timer = $timeout(() => {
                        ensureEndSpace();
                        scope.noteOwner[scope.noteProperty] = elm.summernote('code');
                        if (scope.noteOwner.id) {
                            if (scope.evidenceCollection.observation) {
                                scope.evidenceCollection.observation[scope.noteProperty] = elm.summernote('code');
                                observationService.saveObserveNotes(scope.noteOwner.id, elm.summernote('code'), scope.noteProperty).then(function () {
                                    scope.saveMessage = "All changes saved";
                                });
                            }
                            else if (scope.evidenceCollection.practiceSessionObservation) {
                                practiceSessionService.saveObserveNotes(
                                    scope.evidenceCollection.practiceSessionObservation, elm.summernote('code')).then(function () {
                                    scope.saveMessage = "All changes saved";
                                });
                            }
                        }
                    }, 1000);
                }

                framework = activeUserContextService.getActiveFramework();
                reRender();

                $rootScope.$on('change-framework', function () {
                    framework = activeUserContextService.getActiveFramework();
                    if (framework != null) {
                        reRender();
                    }
                });

                scope.$watch('noteOwner', function (newValue, oldValue) {
                    if (scope.noteOwner) {
                        elm.summernote('code', scope.noteOwner[scope.noteProperty]);
                        // HACK: scroll back to window top because summernote grabs focus
                        // when code is set...
                        scrollTo(0, 0);
                    }
                });

                function reRender() {
                    var finder = $(elm).next(".note-editor").find(".note-toolbar");
                    finder.find(".custom-group").remove();

                    var btnGroup = $("<div class='custom-group btn-group'></div>");
                    var dateButton = $("<div class='btn btn-default btn-sm btn-small'><span title='insert date' class='fa fa-calendar'></span></div>");
                    var timeButton = $("<div class='btn btn-default btn-sm btn-small'><span title ='insert time' class='fa fa-clock-o'></span></div>");
                    btnGroup.append(dateButton);
                    btnGroup.append(timeButton);
                    dateButton.bind("click", function () {
                        elm.summernote('pasteHTML', $filter('date')(new Date(), 'MM/dd/yyyy'));
                        saveNote();
                    });

                    timeButton.bind("click", function () {
                        elm.summernote('pasteHTML', $filter('date')(new Date(), 'HH:mm:ss a'));
                        saveNote();
                    });

                    $(finder).append(btnGroup);

                    if (!framework || !framework.name) {
                        return false;
                    }

                    if (finder.length > 0) {
                        var observation = scope.evidenceCollection.observation;
                        for (var i in framework.frameworkNodes) {

                            var frameworkNode = framework.frameworkNodes[i];

                            // evidenceCollection tracks whether disabled due to focus
                            var node = scope.evidenceCollection.getNodeById(framework.frameworkNodes[i].id);
                            if (node.disabled) {
                                continue;
                            }
                            if (frameworkNode.shortName) {
                                var nodeDiv = $("<div title='" + frameworkNode.title + "' class='custom-group btn-group'><div class='btn btn-default btn-sm btn-small framework_node_toolbar framework_node'>" + frameworkNode.shortName + "</div>");
                                for (var j in frameworkNode.rubricRows) {


                                    if (utils.stripGrowthGoalsRow(frameworkNode.rubricRows[j], scope.evidenceCollection)) {

                                        var rubricRow = frameworkNode.rubricRows[j];

                                        var row = scope.evidenceCollection.getRowById(rubricRow.id);
                                        if (row.disabled) {
                                            continue;
                                        }
                                        if (rubricRow.shortName) {
                                            var btn = $("<div title='" + rubricRow.title + "' class='btn btn-default btn-sm btn-small rubric_toolbar rubric_" + rubricRow.shortName + "'>" + rubricRow.shortName + "</div>");
                                            btn[0].rubric = rubricRow;
                                            btn[0].rubric.frameworkNodeShortName = frameworkNode.shortName;
                                            btn.unbind("mousedown");
                                            // mousedown happens before the selection is cleared
                                            btn.bind("mousedown", buttonClicked);
                                            nodeDiv.append(btn);
                                        }


                                    }
                                }

                                $(finder).append(nodeDiv);
                            }
                        }
                    }

                    function buttonClicked() {
                        var highlight = window.getSelection(),
                            spn: HTMLSpanElement = document.createElement('span'),
                            range = highlight.getRangeAt(0);
                        var content = range.cloneContents();
                        if (highlight) {
                            var msg = highlight.toString();
                            if (msg) {
                                var clientId = utils.getNewGuid();
                                spn.setAttribute('clientid', clientId);
                                spn.setAttribute('contentEditable', false);
                                spn.className = `editor-selected selected_area${this.rubric.shortName}`;
                                spn.innerHTML = `<span contentEditable="false" class="node-desc">${this.rubric.shortName}</span><span contentEditable="false" class="node-desc-end">${this.rubric.shortName}</span><span contentEditable="false" class="glyphicon glyphicon-remove remove-code-selection" onClick=\"removeCode('${clientId}')\" title="Remove"></span>`;
                                $(spn).find(".node-desc").after(content);

                                elm.summernote('insertNode', spn);

                                if (scope.noteProperty === 'observeNotes') {
                                    scope.evidenceCollection.addNewObservationNotesRubricRowAnnotation(this.rubric, msg,
                                        enums.EvidenceType.RR_ANNOTATION_OBSERVATION_NOTES, clientId);
                                }
                                else if (scope.noteProperty === 'evaluatorPreConNotes') {
                                    scope.evidenceCollection.addNewConferenceNotesRubricRowAnnotation(this.rubric, msg,
                                        enums.EvidenceType.RR_ANNOTATION_TOR_PRECONF_SUMMARY, clientId);
                                }
                                else if (scope.noteProperty === 'evaluatorPostConNotes') {
                                    scope.evidenceCollection.addNewConferenceNotesRubricRowAnnotation(this.rubric, msg,
                                        enums.EvidenceType.RR_ANNOTATION_TOR_POSTCONF_SUMMARY, clientId);
                                }
                            }

                            ensureEndSpace();
                            codeBeforeChange = elm.summernote('code');
                            saveNote();
                        }
                    }

                }
                function ensureEndSpace() {
                    // Ensuring we add a space after the last span if there is none, so the user can add more text
                    var code:string = elm.summernote('code');
                    if (/<\/span><\/p>$/.test(code)) {
                        elm.summernote('code', code.replace(/<\/p>$/, '&nbsp;</p>'));
                    }
                }
            }
        }
    }

    summerNoteModalController.$inject = ['$uibModalInstance', 'enums', 'data'];
    function summerNoteModalController($uibModalInstance, enums, data) {
        var vm = this;
        vm.enums = enums;
        vm.message = data.message;
        vm.remove = remove;
        vm.cancel = cancel;
        vm.continueChange = continueChange;

        function remove() {
            $uibModalInstance.close({
                action: 1
            });
        }

        function continueChange() {
            $uibModalInstance.close({
                action: 2
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

    summerNoteController.$inject = ['$scope'];
    function summerNoteController($scope) {
        var vm = this;
    }

})();



function removeCode(clientId) {
    var injector = angular.element('*[ng-app]').injector();
    var $confirm = injector.get('$confirm');

    $confirm({text:'Are you sure to delete?', title:'Remove coding'}).then(function() {
        var codeArea = $("[clientid=\'" + clientId + "\']");
        codeArea.find(".node-desc, .node-desc-end, .remove-code-selection").remove();
        codeArea.contents().unwrap();
        summerNoteScope.$broadcast("code-deleted", { clientId: clientId });
    });
}