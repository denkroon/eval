/// <reference path="core.module.ts" />

(function () {
    "use strict";
    var core = angular.module('stateeval.core');

    core.config(configureRoutes);

    configureRoutes.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function configureRoutes($stateProvider, $urlRouterProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/login');

        $httpProvider.interceptors.push('apiInterceptor');

        $stateProvider
            .state('base', {
                abstract: true,
                templateUrl: 'app/core/views/base.html',
                controller: 'baseController as vm',
                resolve: {
                    activeUserContext: ['startupService', function (startupService) {
                        return startupService.load('context', 'user');
                    }]
                }
            })
            .state('login', {
                url: '/login',
                controller: 'loginController as vm',
                templateUrl: 'app/login/login.html'
            })
            .state('default-content', {
                parent: 'base',
                abstract: true,
                views: {
                    'left-navbar@base': {
                        templateUrl: 'app/layout/left-nav/left-navbar.html',
                        controller: 'leftNavbarController as vm'
                    },
                    'eval-profile@base': {
                        templateUrl: 'app/layout/top-nav/evaluating-user-profile.html',
                        controller: 'evaluatingUserProfileController as vm'
                    }
                }
            })
            .state('server-error', {
                url: '/server-error',
                parent: 'default-content',
                views: {
                    'content@base': {
                        controller: 'serverErrorController as vm',
                        templateUrl: 'app/core/error-pages/server-error.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Server Error'
                },
                params: {
                    errorInfo: null
                }
            })
            .state('private-mode-error', {
                url: '/private-mode-error',
                templateUrl: 'app/core/error-pages/private-mode-error.html'
            })
            .state('incomplete-evaluatee', {
                url: '/incomplete-evaluatee',
                parent: 'default-content',
                views: {
                    'content@base': {
                        controller: 'incompleteEvaluateeController as vm',
                        templateUrl: 'app/core/error-pages/incomplete-evaluatee.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Incomplete Setup Notification'
                },
            })
            .state('no-frameworks', {
                parent: 'no-orientation-base',
                url: '/no-frameworks',
                views: {
                    'content@no-orientation-base': {
                        controller: 'noFrameworksController as vm',
                        templateUrl: 'app/core/error-pages/no-frameworks.html',
                    }
                },
            })
            .state('VOID', {});
    }

})();