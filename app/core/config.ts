/// <reference path="core.module.ts" />

(function () {
    'use strict';

    var core = angular.module('stateeval.core');

    /*
     // configure toaster
     core.config(toastrConfig);
     toastrConfig.$inject = ['toastr'];

     function toastrConfig(toastr) {
     // need to do this here manually to prevent a circular reference
     toastr.options.timeOut = 4000;
     toastr.options.positionClass = 'toast-bottom-right';
     }
     */

    function getAPIUrl() {
       return 'http://test.eval-wa.org/StateEval_WebAPI_Test/api/';

        // comment in this if you need to go to support server (updated nightly)
        // return 'http://69.56.85.28/StateEval_WebApi_Support/api/';
        var host = window.location.host;

        if (host.indexOf("localhost:20000")!=-1) {
            // in dev, the API is proxied from a relative path
            return 'http://test.eval-wa.org/StateEval_WebAPI_Test/api/';
        }

        if (host.indexOf("localhost")!=-1) {
            // in dev, the API is proxied from a relative path
            return '/StateEvalWeb/api/';
        }
		else if (host.indexOf('detroit.hocprofessional')!=-1) {
            // support server
            return 'http://detroit.hocprofessional.com/StateEval_WebApi_trunk/api/';
        }
        else if (host.indexOf('test')!=-1) {
            return 'http://test.eval-wa.org/StateEval_WebAPI_Test/api/';
        }
        else if (host.indexOf('sandbox')!=-1) {
            return 'http://sandbox.eval-wa.org/StateEval_WebAPI_Sandbox/api/';
        }
        else if (host.indexOf('demo')!=-1) {
            return 'http://test.eval-wa.org/StateEval_WebAPI_Demo/api/';
        }
		else if (host.indexOf('labs')!=-1) {
            return 'http://labs.eval-wa.org/StateEval_WebAPI/api/';
        }
 		else if (host.indexOf('sb2')!=-1) {
            return 'http://sb2.eval-wa.org/StateEval_WebAPI/api/';
        }
		else if (host.indexOf('eval-wa.org')===0) {
            return 'https://eval-wa.org/StateEval_WebAPI_WA_Prod/api/';
        }
        else if (host.indexOf('69.56.85.26')!=-1) {
            // staging server
            return 'https://69.56.85.26/StateEval_WebApi/api/';
        }
        else if (host.indexOf('69.56.85.28')!=-1) {
            // support server
            return 'http://69.56.85.28/StateEval_WebApi_Support/api/';
        }
    }

    // global constants
    var config = {
        appErrorPrefix: '[eval Error] ',
        appTitle: 'eval',
        // apiUrl: 'http://test.eval-wa.org/StateEval_WebAPI_Test/api/',
        // apiUrl:  'http://localhost/StateEvalWebAPI/api/',
        apiUrl: getAPIUrl(),
        schoolYear: 2016
    };

    var enums = {
        Months: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ],
        ObservationType: {
            'UNDEFINED': 0,
            'FORMAL': 1,
            'INFORMAL': 2
        },
        StudentGrowthFormPromptType: {
            'GOAL_SETTING': 1,
            'GOAL_MONITORING': 2,
            'GOAL_REVIEW': 3
        },
        EvColTypeAccessor: {
            'OBSERVATION' : 'observations',
            'STUDENT_GROWTH_GOALS': 'studentGrowthGoals',
            'SELF_ASSESSMENT': 'selfAssessments',
            'OTHER_EVIDENCE': 'otherEvidenceCollections'
        },
        AssociatedCollectionsAccessor: {
            '1': 'OTHER_EVIDENCE',
            '2': 'OBSERVATION',
            '3': 'SELF_ASSESSMENT',
            '4': 'STUDENT_GROWTH_GOALS',
            '5': 'SUMMATIVE',
            '6': 'LEARNING_WALK_OBS',
            '7': 'LEARNING_WALK_SUMMATIVE',
            '8': 'PRACTICE_SESSION_OBS',
            '9': 'PRACTICE_SESSION_SUMMATIVE',
            '10': 'YEAR_TO_DATE'
        },
        EvidenceCollectionType: {
            'UNDEFINED': 0,
            'OTHER_EVIDENCE': 1,
            'OBSERVATION': 2,
            'SELF_ASSESSMENT': 3,
            'STUDENT_GROWTH_GOALS': 4,
            'SUMMATIVE': 5,
            'LEARNING_WALK_OBS': 6,
            'LEARNING_WALK_SUMMATIVE': 7,
            'PRACTICE_SESSION_OBS': 8,
            'PRACTICE_SESSION_SUMMATIVE': 9,
            'YEAR_TO_DATE': 10
        },
        EvidenceType: {
            'ARTIFACT': 1,
            'RR_ANNOTATION_OBSERVATION_NOTES': 2,
            'RR_ANNOTATION_PRECONF_PROMPT': 3,
            'RR_ANNOTATION_TOR_PRECONF_SUMMARY': 4,
            'RR_ANNOTATION_POSTCONF_PROMPT': 5,
            'RR_ANNOTATION_TOR_POSTCONF_SUMMARY': 6,
            'STUDENT_GROWTH_GOAL': 7,
            'RR_ANNOTATION_TEE_PRECONF_SUMMARY': 8,
            'RR_ANNOTATION_TEE_POSTCONF_SUMMARY': 9,
            'RR_ANNOTATION_RUBRIC_NOTE': 10,
            'STUDENT_GROWTH_GOAL_PROMPT': 11,
            'TRAINING_EVIDENCE_SEGMENT': 12
        },
        EvidenceTypeMapper: {
            '1': 'Artifact',
            '2': 'Observation Notes',
            '3': 'Pre-Conference Prompt',
            '4': 'Pre-Conference Summary (Evaluator)',
            '5': 'Post-Conference Prompt',
            '6': 'Post-Conference Summary (Evaluator)',
            '7': 'Student Growth Goal',
            '8': 'Pre-Conference Summary (Evaluatee)',
            '9': 'Post-Conference Summary (Evaluatee)',
            '10':'Other Evidence',
            '11': 'Student Growth Goal Prompt',
            '12': 'Video Evidence Segment'
        },
        PerformanceLevelShortNameMapper: {
            '1': 'UNS',
            '2': 'BAS',
            '3': 'PRO',
            '4': 'DIS'
        },
        itemTypeMapper: {
            '1': 'Artifact',
            '2': 'Artifact Linked to Student Growth Goal',
            '3': 'Artifact Linked to Observation',
            '4': 'Artifact Linked to Self Assessment',
            '5': 'Observation',
            '6': 'Self-Assessment',
            '7': 'Student Growth Goal'
        },
        ItemType: {
            'ARTIFACT_OTHER_EVIDENCE': 1,
            'ARTIFACT_LINKED_TO_SG_GOAL': 2,
            'ARTIFACT_LINKED_TO_OBSERVATION': 3,
            'ARTIFACT_LINKED_TO_SELF_ASSESSMENT': 4,
            'OBSERVATION': 5,
            'SELF_ASSESSMENT': 6,
            'STUDENT_GROWTH_GOAL': 7
        },
        EvaluationTypes: [
            'Undefined',
            'Principal Evaluation',
            'Teacher Evaluation',
            'Librarian Evaluation'
        ],
        UserOrientationPropOrder: [
            'schoolYear',
            'districtName',
            'schoolName',
            'workAreaTag'
        ],
        PLAccessor: {
            UNSATISFACTORY: 'pL1Descriptor',
            BASIC: 'pL2Descriptor',
            PROFICIENT: 'pL3Descriptor',
            DISTINGUISHED: 'pL4Descriptor'
        },
        PerformanceLevels: [
            'UNSATISFACTORY',
            'BASIC',
            'PROFICIENT',
            'DISTINGUISHED'
        ],
        EvalAssignmentRequestStatus: {
            'PENDING': 1,
            'ACCEPTED': 2,
            'REJECTED': 3
        },
        EvalAssignmentRequestType: {
            'OBSERVATION_ONLY': 1,
            'ASSIGNED_EVALUATOR': 2
        },
        EventType: {
            'OBSERVATION_CREATED': 1,
            'ARTIFACT_SUBMITTED': 2,
            'ARTIFACT_REJECTED': 3
        },
        RubricPerformanceLevel: {
            'UNDEFINED': 0,
            'PL1': 1,
            'PL2': 2,
            'PL3': 3,
            'PL4': 4
        },
        StudentGrowthGoalProcessType: {
            'UNDEFINED': 0,
            'DEFAULT_FORM': 1,
            'DISTRICT_FORM': 2,
            'OFFLINE_FORM': 3
        },
        WfState: {
            'UNDEFINED': 0,
            'EVAL_DRAFT': 1,
            'EVAL_READY_FOR_CONFERENCE': 2,
            'EVAL_READY_FOR_FORMAL_RECEIPT': 3,
            'EVAL_RECEIVED': 4,
            'EVAL_SUBMITTED': 5,
            'OBS_IN_PROGRESS_TOR': 6,
            'OBS_LOCKED_TEE_REVIEW': 7,
            'OBS_LOCKED_SEALED': 8,
            'OBS_UNLOCK_REQUEST_TOR': 9,
            'OBS_UNLOCK_REQUEST_TEE': 10,
            'SGBUNDLE_STARTED': 11,
            'SGBUNDLE_PROCESS_SHARED': 12,
            'SGBUNDLE_PROCESS_COMPLETE': 13
        },
        ArtifactLibItemType: {
            'UNDEFINED': 0,
            'FILE': 1,
            'WEB': 2,
            'PROFPRACTICE': 3
        },
        SchoolYear: {
            'UNDEFINED': 0,
            'SY_2012_2013': 2013,
            'SY_2013_2014': 2014,
            'SY_2014_2015': 2015,
            'SY_2015_2016': 2016
        },
        EvaluationType: {
            'UNDEFINED': 0,
            'PRINCIPAL': 1,
            'TEACHER': 2,
            'LIBRARIAN': 3
        },
        PromptType: {
            'UNDEFINED': 0,
            'PreConf': 1,
            'PostConf': 2,
            'StudentGrowthGoal': 3
        },
        EvaluationPlanType: {
            'UNDEFINED': 0,
            'COMPREHENSIVE': 1,
            'FOCUSED': 2
        },
        FrameworkViewType: {
            'UNDEFINED': 0,
            'STATE_FRAMEWORK_ONLY': 1,
            'STATE_FRAMEWORK_DEFAULT': 2,
            'INSTRUCTIONAL_FRAMEWORK_DEFAULT': 3,
            'INSTRUCTIONAL_FRAMEWORK_ONLY': 4
        },
        Roles: {
            'SESuperAdmin': 'SESuperAdmin',
            'SEDistrictAdmin': 'SEDistrictAdmin',
            'SEDistrictViewer': 'SEDistrictViewer',
            'SESchoolAdmin': 'SESchoolAdmin',
            'SESchoolPrincipal': 'SESchoolPrincipal',
            'SESchoolTeacher': 'SESchoolTeacher',
            'SEDistrictEvaluator': 'SEDistrictEvaluator',
            'SEDistrictWideTeacherEvaluator': 'SEDistrictWideTeacherEvaluator',
            'SESchoolHeadPrincipal': 'SESchoolHeadPrincipal',
            'SECustomSupportL1': 'SECustomSupportL1',
            'SEDistrictAssignmentManager': 'SEDistrictAssignmentManager',
            'SESchoolLibrarian': 'SESchoolLibrarian'
        },
        PracticeSessionTypeEnum: {
            'UNDEFINED': 0,
            'LIVE': 1,
            'VIDEO': 2
        },
        ReportFormatTypeEnum: {
            'UNDEFINED': 0,
            'SUMMARY': 1,
            'DETAILED': 2,
            'CUSTOM': 3,
            'FINAL': 4
        },
        ReportTypeEnum: {
            'UNDEFINED': 0,
            'SUMMATIVE': 1,
            'OBSERVATION': 2
        },
        ObservationReportOptions: {
            'INSTRUCTIONAL_VIEW': 'Instructional View',
            'STATE_VIEW': 'State View',
            'AVAILABLE_EVIDENCE': 'Available Evidence',
            'ALIGNED_EVIDENCE': 'Aligned Evidence',
            'OBSERVATION_NOTES': 'Observation Notes',
            'PRE_CONFERENCE': 'Pre-Conference',
            'POST_CONFERENCE': 'Post-Conference'
        },
        SummativeReportOptions: {
            'INSTRUCTIONAL_VIEW': 'Instructional View',
            'STATE_VIEW': 'State View',
            'AVAILABLE_EVIDENCE': 'Available Evidence',
            'ALIGNED_EVIDENCE': 'Aligned Evidence',
            'OBSERVATION_NOTES': 'Observation Notes',
            'PRE_CONFERENCE': 'Pre-Conference',
            'POST_CONFERENCE': 'Post-Conference'
        }
};

    // make the config settings available to config fucntions
    // old way only makes it visible to current module?
    // core.value('config', config);

    core.config(configure);

    configure.$inject = ['$logProvider', 'exceptionHandlerProvider', '$provide', '$breadcrumbProvider'];
    function configure($logProvider, exceptionHandlerProvider, $provide, $breadcrumbProvider) {

        // $provider makes it injectible to other modules
        $provide.constant('config', config);
        $provide.constant('enums', enums);

        // todo-anne get default schoolyear from db
        $provide.value('schoolYear', enums.SchoolYear.SY_2014_2015);

        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        exceptionHandlerProvider.configure(config.appErrorPrefix);

        $breadcrumbProvider.setOptions(
            {
            templateUrl: 'app/layout/top-nav/breadcrumbs.html'
            }
        );
    }

    core.run(run);

    run.$inject = ['$rootScope', '$state', '$cookieStore', 'enums', 'activeUserContextService', 'logger', 'utils', 'rubricUtils', 'evidenceCollectionService'];

    function run($rootScope, $state, $cookieStore, enums, activeUserContextService, logger, utils, rubricUtils, evidenceCollectionService) {
        $rootScope.$state = $state;
        $rootScope.enums = enums;

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            console.log(error);
            throw 'STATE CHANGE ERROR';
        });

        $rootScope.$on('$stateChangeStart', function (event, toParams, fromParams) {
        });

        $rootScope.$on('$stateChangeSuccess', function (event, toParams, fromParams) {
            console.log(event);
            console.log(toParams);
            if(evidenceCollectionService.state.ignoreFrameworkChange) {
                evidenceCollectionService.state.ignoreFrameworkChange = false;
                evidenceCollectionService.state.frameworkName = '';
            }
        });

    }

})();
