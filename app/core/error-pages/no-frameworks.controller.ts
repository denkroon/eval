/// <reference path="../core.module.ts" />

(function() {
    'use strict';

    class noFrameworksController {

        user: any;
        title: string;

        constructor(
            private userService: any,
            private $stateParams: any
        ) {
            userService.getUserById(parseInt(this.$stateParams.userId)).then((user) => {
                this.user = user;
                this.title = "Welcome " + this.user.displayName + "!";
            });
        }
    }

    angular.module('stateeval.core')
        .controller('noFrameworksController', noFrameworksController);
})();