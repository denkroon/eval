/// <reference path="../core.module.ts" />

(function() {
    'use strict';

    class incompleteEvaluateeController {

        evaluatee: any;
        title: string;

        constructor(
            private activeUserContextService: any
        ) {

            this.evaluatee = activeUserContextService.context.evaluatee;
            this.title = "Welcome " + this.evaluatee.displayName + "!";
        }
    }

    angular.module('stateeval.core')
        .controller('incompleteEvaluateeController', incompleteEvaluateeController);
})();