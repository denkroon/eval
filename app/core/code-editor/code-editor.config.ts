/// <reference path="../core.module.ts" />

namespace StateEval.Core {
    var core = angular.module('stateeval.core');
    var registeredIds : number[] = [];

    core.config((
        $provide: ng.auto.IProvideService
    ) => {
        $provide.decorator('taOptions', (
            Fullscreen: any,
            taRegisterTool: any,
            $delegate: any
        ) => {

            taRegisterTool('date', {
                iconclass: 'fa fa-calendar',
                action: function() {
                    this.$editor().wrapSelection('insertHTML', moment().format('MM/DD/YYYY'))
                }
            });

            taRegisterTool('time', {
                iconclass: 'fa fa-clock-o',
                action: function() {
                    this.$editor().wrapSelection('insertHTML', moment().format('hh:mm:ss a'))
                }
            });

            taRegisterTool('fullScreen', {
                iconclass: 'fa fa-arrows-alt',
                action: function() {
                    if (Fullscreen.isEnabled()) {
                        Fullscreen.cancel();
                    }
                    else {
                        Fullscreen.enable(document.getElementById(this.$editor()._name));
                    }
                }
            })

            $delegate.forceTextAngularSanitize = false;
            return $delegate;
        });
    })
}