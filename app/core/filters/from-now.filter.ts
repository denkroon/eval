/// <reference path="../core.module.ts" />

(function () {
'use strict';
    // taken and adapted from http://jsfiddle.net/MatthewLothian/kBpxb/7/
    angular.module('stateeval.core')
        .filter('fromNow', function() {
            return function(input) {
                return moment(input).fromNow();
            };
        })
}) ();
