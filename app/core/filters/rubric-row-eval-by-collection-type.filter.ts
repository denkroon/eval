/// <reference path="../core.module.ts" />

(function () {
    'use strict';
     angular.module('stateeval.core')
     .filter('rubricRowEvalByCollectionType', ['enums', '_', function(enums, _) {
         return function (data, collectionType) {
             var results = _.filter(data, {evidenceCollectionType: collectionType});
             return results;
         }
     }]);
})();
