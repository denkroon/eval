/// <reference path="../core.module.ts" />

(function () {
'use strict';
    // taken and adapted from http://jsfiddle.net/MatthewLothian/kBpxb/7/
    angular.module('stateeval.core')
         .filter('toTrusted', ['$sce', function ($sce) {
             return function (text) {
                 return $sce.trustAsHtml(text);
             };
         }]);
}) ();
