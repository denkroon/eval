/// <reference path="../blocks/exception/exception.module.ts" />
/// <reference path="../blocks/logger/logger.module.ts" />

/**
 * Created by anne on 6/19/2015.
 */
(function () {
    'use strict';

    angular
        .module('stateeval.core', [
            'blocks.exception',
            'blocks.logger',
            'ui.router',                    // Routing
            'ui.bootstrap',                  // Angular Bootstrap
            'lodash',
            'ngAnimate',
            'angular-confirm',
            'inspinia',
            'LocalStorageModule',
            // 'kendo.directives',
            'checklist-model',
            'ngCookies',
            'ngMessages',
            'ya.nouislider',
            'ngSanitize',
            'com.2fdevs.videogular',
            'com.2fdevs.videogular.plugins.controls',
            'com.2fdevs.videogular.plugins.poster',
            'com.2fdevs.videogular.plugins.overlayplay',
            'cgPrompt',
            'angular-intro',
            'ncy-angular-breadcrumb',
            'templates',
            'angular-loading-bar',
            'smoothScroll',
            'stateeval.authorization',
            'textAngular',
            'FBAngular'
        ])
        .config((
            cfpLoadingBarProvider: any,
            $httpProvider: ng.IHttpProvider
        ) => {
            $httpProvider.defaults.ignoreLoadingBar = true;
            cfpLoadingBarProvider.includeSpinner = false;
            cfpLoadingBarProvider.latencyThreshold = 0;
        })
        .run((
            $confirmModalDefaults: any,
            cfpLoadingBar: any,
            $rootScope: ng.IRootScopeService
        ) => {
            $rootScope.$on('$stateChangeStart', () => { cfpLoadingBar.start(); });
            $rootScope.$on('$stateChangeSuccess', () => { cfpLoadingBar.complete(); });
            $rootScope.$on('$stateChangeError ', () => { cfpLoadingBar.complete(); });
            $confirmModalDefaults.templateUrl = 'app/core/views/default-confirm-modal.html';
        });
})();

