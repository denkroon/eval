//import describe = testing.describe;
describe('availableEvidence and evaluations', function () {
    var evidenceCollectionService,
        enums,
        $httpBackend,
        $http,
        config,
        enums,
        rubricUtils,
        utils,
        evidenceCollection,
        evidenceCollectionData,
        observationService,
        activeUserContextService,
        $q;

        evidenceCollectionData =  {
            availableEvidence: [{
                "id": 1,
                "evidenceType": 1,
                "evaluationId": 51,
                "rubricRowId": 109, // just make sure it matches the mock-data for 1a
                "artifactBundleId": 1,
                "evidenceCollectionType": 2,
                "evidenceCollectionObjectId": 1,
                "inUseByTee": false,
                "userId": null
            },
                {
                    "id": 2,
                    "evidenceType": 1,
                    "evaluationId": 51,
                    "rubricRowId": 109, // just make sure it matches the mock-data for 1a
                    "artifactBundleId": 2,
                    "evidenceCollectionType": 2,
                    "evidenceCollectionObjectId": 1,
                    "inUseByTee": false,
                    "userId": null
                }],
            request: {
                "evaluationId": 51,
                "currentUserId": 59,
                "collectionType": 2,
                "collectionObjectId": 1
            },
            rubricRowEvaluations: [],
            observation: {
                id:1,
                focused:false
            }
        };

    var rubricRowEvaluationData = {
            id:1,
            rubricRowId: 109,
            evaluationId: 51,
            evidenceCollectionType: 2,
            evidenceCollectionObjectId: 1,
            createdByUserId: 93,
            rubricStatement: 'test',
            performanceLevel: 1,
            alignedEvidences: [
                {
                    id: 1,
                    availableEvidenceId: evidenceCollectionData.availableEvidence[0].id
                }],
            additionalInput: ''
        };

    beforeEach(function () {
        angular.mock.module('stateeval.observation');
        angular.mock.module('stateeval.core', function ($provide) {
            $provide.factory('activeUserContextService', function () {
                var service = {
                    user: { id: 93},
                    context: {
                        frameworkContext: {
                            stateFramework: TestHelper.TR_FrameworkContext.stateFramework,
                            instructionalFramework: TestHelper.TR_FrameworkContext.instructionalFramework
                        },
                        framework: TestHelper.TR_FrameworkContext.instructionalFramework,
                        evaluatee: {
                            id: 93,
                            evalData: {
                                id: 1,
                                evaluationType: 2
                            }
                        }
                    }
                };
                return service;
            });
        });

        inject(function (_$http_, _$httpBackend_, _config_, _evidenceCollectionService_, _enums_, _rubricUtils_,
            _observationService_, _activeUserContextService_, _utils_, _$q_) {
            $http = _$http_;
            $httpBackend = _$httpBackend_;
            config = _config_;
            evidenceCollectionService = _evidenceCollectionService_;
            enums = _enums_;
            rubricUtils = _rubricUtils_;
            observationService = _observationService_;
            activeUserContextService = _activeUserContextService_;
            utils = _utils_;
            $q = _$q_;
        });

        $httpBackend.when('GET', new RegExp('\\' + "/StateEvalWeb/api/evidencecollections/")).respond(function () {
            return [200, evidenceCollectionData, {}]
        });

        $httpBackend.when('POST', new RegExp('\\' + "/StateEvalWeb/api/rubricrowevaluations/")).respond(function () {
            return [200, rubricRowEvaluationData, {}]
        });

        $httpBackend.when('DELETE', new RegExp('\\' + "/StateEvalWeb/api/rubricrowevaluations/")).respond(function () {
            return [200, {}, {}]
        });

        evidenceCollectionService.getEvidenceCollection('OBSERVATION',
            enums.EvidenceCollectionType.OBSERVATION, 1, 1).then(function(collection) {
            evidenceCollection = collection;
        });

        $httpBackend.flush();
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('misc evidenceCollectionService node/row functions', function() {

        it ('should return the correct rows from call to getRows', function() {
            var rows = evidenceCollection.getRows("D1");
            expect(rows.length).toEqual(6);
        });

        it('should return the correct node from call to getNode', function() {
            var node = evidenceCollection.getNode('D1');
            expect(node).not.toBeNull();
            expect(node.data.shortName).toEqual('D1');
        });

        it('should return the correct node from call to getNodeById', function() {
            var node = evidenceCollection.getNode('D1');
            node = evidenceCollection.getNodeById(node.data.id);
            expect(node).not.toBeNull();
            expect(node.data.shortName).toEqual('D1');
        });

        it ('should return the correct row from call to getRow', function() {
            var row = evidenceCollection.getRow('1a');
            expect(row).not.toBeNull();
            expect(row.data.shortName).toEqual('1a');
        });

        it ('should return the correct row from call to getRowById', function() {
            var row = evidenceCollection.getRow('1a');
            row = evidenceCollection.getRowById(row.data.id);
            expect(row).not.toBeNull();
            expect(row.data.shortName).toEqual('1a');
        })
    });

    describe('initializes availableEvidence', function () {
        it('should set evidenceCollection.availableEvidence', function() {
            expect(evidenceCollection.availableEvidence.length).toEqual(2);
        });

        it('should set the evidence on the correct row', function()
        {
            var row = evidenceCollection.getRow('1a');
            expect(row.evidences.length).toEqual(2);
        });

        it('should set the evidence on the correct node', function() {
            var node = evidenceCollection.getNode('D1');
            expect(node.evidences.length).toEqual(2);
        });
    });

    describe('initializes evaluations', function () {
        it('should set the evidecnceCollection.evaluations', function() {
            expect(evidenceCollection.evaluations.length).toEqual(0);
        });
    });

    describe('when a new evaluation is added', function () {

        afterEach(function () {
            $httpBackend.flush();
        });

        it('should add new evaluation to expected locations', function() {
            var node = evidenceCollection.getNode('D1');
            var row = evidenceCollection.getRow('1a');
            var alignedEvidence = [evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[0])];
            evidenceCollection.addNewEvaluation(1, 'test', alignedEvidence, row).then(function() {
                expect(evidenceCollection.evaluations.length).toEqual(1);
                expect(node.evaluations.length).toEqual(1);
                expect(row.evaluations.length).toEqual(1);
                expect(evidenceCollection.rubric[row.data.id].evaluations.length).toEqual(1);
                expect(evidenceCollection.rubric[row.data.id]['UNSATISFACTORY'].evaluations.length).toEqual(1);
            });
        });

        it('should set availableEvidence.inUse ', function() {
            var row = evidenceCollection.getRow('1a');
            var alignedEvidence = [evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[0])];
            evidenceCollection.addNewEvaluation(1, 'test', alignedEvidence, row).then(function() {
                expect(row.evidences[0].inUse).toEqual(true);
            });

        });
    });

    describe('when an evaluation is deleted', function () {

        afterEach(function{
            $httpBackend.flush();
        });

        it('should remove the evaluation from the expected locations', function() {
            var node = evidenceCollection.getNode('D1');
            var row = evidenceCollection.getRow('1a');
            var alignedEvidence = [evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[0])];
            evidenceCollection.addNewEvaluation(1, 'test', alignedEvidence, row).then(function(evaluation) {
                evidenceCollection.deleteEvaluation(evaluation).then(function() {
                    expect(evidenceCollection.evaluations.length).toEqual(0);
                    expect(node.evaluations.length).toEqual(0);
                    expect(row.evaluations.length).toEqual(0);
                    expect(evidenceCollection.rubric[row.data.id].evaluations.length).toEqual(0);
                    expect(evidenceCollection.rubric[row.data.id]['UNSATISFACTORY'].evaluations.length).toEqual(0);
                });
            });

        });

        it('should set availableEvidence.inUse to false', function() {
            var row = evidenceCollection.getRow('1a');
            var alignedEvidence = [evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[0])];
            evidenceCollection.addNewEvaluation(1, 'test', alignedEvidence, row).then(function(evaluation) {
                evidenceCollection.deleteEvaluation(evaluation).then(function() {
                    expect(row.evidences[0].inUse).toEqual(false);
                });
            });

        });
    });

    describe('when a new evaluation is added with two available evidence', function () {

        afterEach(function{
            $httpBackend.flush();
        });

        it('should set both availableEvidence.inUse to true', function() {
            var node = evidenceCollection.getNode('D1');
            var row = evidenceCollection.getRow('1a');
            var alignedEvidence = [evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[0]),
                                   evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[1])];
            evidenceCollection.addNewEvaluation(1, 'test', alignedEvidence, row).then(function() {
                expect(row.evidences[0].inUse).toEqual(true);
                expect(row.evidences[1].inUse).toEqual(true);
             });
        });
    });

    describe('when an evaluation is deleted with two available evidence', function () {

        afterEach(function{
            $httpBackend.flush();
        });

        it('should set both availableEvidence.inUse to false', function() {
            var node = evidenceCollection.getNode('D1');
            var row = evidenceCollection.getRow('1a');
            var alignedEvidence = [evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[0]),
                evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[1])];
            evidenceCollection.addNewEvaluation(1, 'test', alignedEvidence, row).then(function(evaluation) {
                evidenceCollection.deleteEvaluation(evaluation).then(function() {
                    expect(row.evidences[0].inUse).toEqual(false);
                    expect(row.evidences[1].inUse).toEqual(false);
                });
            });
        });
    });

    describe('when two evaluations are added with shared available evidence and one is deleted', function () {

        afterEach(function{
            $httpBackend.flush();
        });

        it('should set the availableEvidence.inUse from the deleted evaluation to false', function() {
            var row = evidenceCollection.getRow('1a');
            var alignedEvidence0 = [evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[0])];
            var alignedEvidence1 = [evidenceCollectionService.newAlignedEvidence(evidenceCollectionData.availableEvidence[1])];
            evidenceCollection.addNewEvaluation(1, 'test1', alignedEvidence0, row).then(function(evaluation0) {
                evidenceCollection.addNewEvaluation(1, 'test2', alignedEvidence1, row).then(function(evaluation1) {
                    evidenceCollection.deleteEvaluation(evaluation0).then(function() {
                        expect(row.evidences[0].inUse).toEqual(false);
                        expect(row.evidences[1].inUse).toEqual(true);
                    });
                });
            });
        });
    });

    describe('when a observation note is added', function () {

        var observationNoteAvailableEvidence;
        beforeEach(function() {
            observationNoteAvailableEvidence = {
                "id": 4,
                "evidenceType": 2,
                "evaluationId": 51,
                "rubricRowId": 109, // just make sure it matches the mock-data for 1a
                "rubricRowAnnotationId": 1,
                "evidenceCollectionType": 2,
                "evidenceCollectionObjectId": 1,
                "inUseByTee": false,
                "userId": null
            };

            // add a new observation note annotation
            $httpBackend.when('POST', new RegExp('\\' + "/StateEvalWeb/api/rubricrowannotation/create")).respond(function () {
                return [200, observationNoteAvailableEvidence, {}]
            });
        })
        afterEach(function{
            $httpBackend.flush();
        });

        it('should add new availableEvidence to expected locations', function() {
            var row = rubricUtils.getRubricRowById(activeUserContextService.context.frameworkContext.stateFramework.frameworkNodes, 109);
            evidenceCollection.addNewObservationNotesRubricRowAnnotation(row, 'test',
                enums.EvidenceType.RR_ANNOTATION_OBSERVATION_NOTES, utils.getNewGuid()).then(function() {
                var  node = evidenceCollection.getNode('D1');
                row = evidenceCollection.getRow('1a');

                expect(evidenceCollection.availableEvidence.length).toEqual(3);
                expect(node.evidences.length).toEqual(3);
                expect(row.evidences.length).toEqual(3);
                expect(evidenceCollection.rubric[row.data.id].evidences.length).toEqual(3);
            });
        });

        it('should set availableEvidence.inUse false', function() {
            var row = rubricUtils.getRubricRowById(activeUserContextService.context.frameworkContext.stateFramework.frameworkNodes, 109);
            evidenceCollection.addNewObservationNotesRubricRowAnnotation(row, 'test',
                enums.EvidenceType.RR_ANNOTATION_OBSERVATION_NOTES, utils.getNewGuid()).then(function() {
                var row = evidenceCollection.getRow('1a');
                expect(row.evidences[2].inUse).toEqual(false);
            });
        });
    });

    describe('when a observation note is deleted', function () {

        var observationNoteAvailableEvidence;
        beforeEach(function() {
            observationNoteAvailableEvidence = {
                "id": 4,
                "evidenceType": 2,
                "evaluationId": 51,
                "rubricRowId": 109, // just make sure it matches the mock-data for 1a
                "rubricRowAnnotationId": 1,
                "evidenceCollectionType": 2,
                "evidenceCollectionObjectId": 1,
                "inUseByTee": false,
                "userId": null
            };

            // add a new observation note annotation
            $httpBackend.when('POST', new RegExp('\\' + "/StateEvalWeb/api/rubricrowannotation/create")).respond(function () {
                return [200, observationNoteAvailableEvidence, {}]
            });
        })
        afterEach(function{
            $httpBackend.flush();
        });

        it('should only remove the deleted available evidence and leave the others intact', function() {
            var row = rubricUtils.getRubricRowById(activeUserContextService.context.frameworkContext.stateFramework.frameworkNodes, 109);
            evidenceCollection.addNewObservationNotesRubricRowAnnotation(row, 'test',
                enums.EvidenceType.RR_ANNOTATION_OBSERVATION_NOTES, utils.getNewGuid()).then(function() {
                    evidenceCollection.deleteUnusedAvailableEvidenceById(evidenceCollectionData.availableEvidence[1].id);
                    expect(evidenceCollection.availableEvidence.length).toEqual(2);
                    expect(evidenceCollection.availableEvidence[0].id).toEqual(1);
                    expect(evidenceCollection.availableEvidence[1].id).toEqual(4);

                    var  node = evidenceCollection.getNode('D1');
                    row = evidenceCollection.getRow('1a');
                    expect(row.evidences[0].id).toEqual(1);
                    expect(row.evidences[1].id).toEqual(4);
                    expect(node.evidences[0].id).toEqual(1);
                    expect(node.evidences[1].id).toEqual(4);

            });
        });
    });

    describe('Observation Evidence Collection Scoring', function () {
        it('should correctly score frameworkNodes', function () {
            var url = config.apiUrl;
            var spy = spyOn($http, 'put').and.returnValue($q.when({data: {id: 0}}));
            var rubricRowScore = evidenceCollection.newRubricRowScore(1, 1);
            evidenceCollection.scoreItem(rubricRowScore);
            expect(spy).toHaveBeenCalledWith(url + 'evidencecollections/scorerubricrow', rubricRowScore)
        });

        it('should correctly score rubricRows', function () {
            var url = config.apiUrl;
            var spy = spyOn($http, 'put').and.returnValue($q.when({data: {id: 0}}));
            var frameworkNodeScore = evidenceCollection.newFrameworkNodeScore(1, 1);
            evidenceCollection.scoreItem(frameworkNodeScore);
            expect(spy).toHaveBeenCalledWith(url + 'evidencecollections/scoreframeworknode', frameworkNodeScore)

        });
    });
        //describe('Summative Evidence Collection Scores', function () {
        //
        //    it('should correctly score summative rubricRows', function () {
        //        var summative;
        //        var url = config.apiUrl;
        //        var spy = spyOn($http, 'put').and.returnValue($q.when({data: {id: 0}}));
        //        evidenceCollectionService.getEvidenceCollection('SUMMATIVE',
        //            enums.EvidenceCollectionType.SUMMATIVE, 1, 1).then(function(collection) {
        //            summative = collection;
        //        });
        //        waitsFor(function() {
        //            return !!summative;
        //        });
        //
        //        var rubricRowScore = summative.newRubricRowScore(1, 1);
        //        evidenceCollection.scoreItem(rubricRowScore);
        //        expect(spy).toHaveBeenCalledWith(url + 'evidencecollections/scoresummativerubricrow', rubricRowScore);
        //
        //    });
        //    it('should correctly score summative frameworkNodes', function () {
        //        var summative;
        //        var url = config.apiUrl;
        //        var spy = spyOn($http, 'put').and.returnValue($q.when({data: {id: 0}}));
        //        evidenceCollectionService.getEvidenceCollection('SUMMATIVE',
        //            enums.EvidenceCollectionType.SUMMATIVE, 1, 1).then(function(collection) {
        //            summative = collection;
        //        });
        //
        //        waitsFor((function() {
        //            return !!summative;
        //        }));
        //
        //        var frameworkNodeScore = summative.newFrameworkNodeScore(1, 1);
        //        this.summative.scoreItem(frameworkNodeScore);
        //        expect(spy).toHaveBeenCalledWith(url + 'evidencecollections/scoresummativeframeworknode', frameworkNodeScore)
        //
        //    });
        //})



});