/// <reference path="../resource.module.ts" />

/**
 * Created by anne on 12/11/2015.
 */

(function() {
    'use strict';

    angular.module('stateeval.resources')
        .controller('resourceViewController', resourceViewController);

    resourceViewController.$inject = ['enums', 'utils', 'resource', 'artifactService', '$state'];

    function resourceViewController(enums, utils, resource, artifactService, $state) {
        var vm = this;
        vm.resource = resource;
        vm.enums = enums;

        vm.itemTypeToString = utils.mapLibItemTypeToString;
        vm.itemDisplayName = artifactService.libItemDisplayName;
        vm.viewItem = artifactService.viewItem;

        vm.goBack = goBack;

        activate();

        function activate() {

        }

        function goBack() {
            $state.go('resource-list');
        }

    }

})();