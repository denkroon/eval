/// <reference path="../resource.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.resources')
        .controller('resourceListController', resourceListController);

    resourceListController.$inject = ['enums', 'activeUserContextService', 'resourceService', '$state'];

    function resourceListController(enums, activeUserContextService, resourceService, $state) {
        var vm = this;

        var workAreaTag = activeUserContextService.context.orientation.workAreaTag;
        var userIsASchoolAdmin = (workAreaTag.indexOf('SA') === 0);
        var userIsADistrictAdmin = (workAreaTag.indexOf('DA') === 0);
        vm.isAdmin = userIsADistrictAdmin || userIsASchoolAdmin;

        vm.showSchoolResources = !userIsADistrictAdmin;
        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();

        vm.editResource = editResource;
        vm.deleteResource = deleteResource;
        vm.addNewResource = addNewResource;
        vm.viewResource = viewResource;

        ////////////////////////////////////////////

        activate();

        function activate() {

            var evalType = activeUserContextService.context.evaluationType();
            vm.displayName = evalType===enums.EvaluationType.PRINCIPAL?"Principal Evaluations Resource Bank":"Teacher Evaluations Resource Bank";

            loadData().then(function() {
            })
        }

        function loadData() {
            return getResources().then(function (resources) {
                vm.resources = resources;
            });
        }

        function deleteResource(resourceId) {
            resourceService.deleteResource(resourceId).then(function() {
                loadData();
            });
        }

        function editResource(resourceId) {
            $state.go("resource-builder", { resourceId: resourceId });
        }

        function viewResource(resourceId) {
            $state.go('resource-view', {resourceId: resourceId});
        }
        function addNewResource() {
            var resource = resourceService.getNewResource();
            resourceService.saveResource(resource).then(function() {
                $state.go('resource-builder', {resourceId: resource.id});
            })
        }

        function getResources() {
            var resources = [];

            return resourceService.getDistrictAdminDefinedResources()
                .then(function(districtResources) {
                    addResources(resources, districtResources, userIsADistrictAdmin);
                    if (vm.showSchoolResources) {
                        return resourceService.getSchoolAdminDefinedResources();
                    }
                    else {
                        return [];
                    }
                }).then(function (schoolResources) {
                    addResources(resources, schoolResources, userIsASchoolAdmin);
                    return resources;
                });
        }

        function addResources(allResources, newResources, editable) {
            newResources.forEach(function(resource) {
                resource.editable = editable;
                allResources.push(resource);
            });
        }
    }

})();