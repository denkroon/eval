/// <reference path="../resource.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.resources')
        .controller('resourceBuilderController', resourceBuilderController);

    resourceBuilderController.$inject = ['resourceService', '$state', '_', 'enums',
        'artifactService', 'resource', '$timeout', 'utils'];

    function resourceBuilderController(resourceService, $state, _, enums,
           artifactService, resource, $timeout, utils) {

        var vm = this;
        vm.utils = utils;
        vm.resource = resource;
        vm.title = vm.resource.title;

        vm.enums = enums;
        vm.resourceService = resourceService;
        vm.itemDisplayName = artifactService.libItemDisplayName;
        vm.artifactService = artifactService;
        vm.viewItem = artifactService.viewItem;

        vm.itemToEdit = null;

        vm.builderForm = {};

        vm.goBack = goBack;

        // functions for editing lib item
        // called from the directive
        vm.doneItemEdit = doneItemEdit;
        vm.cancelItemEdit = cancelItemEdit;
        vm.editItem = editItem;
        vm.createItem = createItem;
        vm.save = save;
        vm.removeItem = removeItem;
        vm.autoSave = autoSave;

        vm.newItem = false;

        vm.deleteResource = deleteResource;

        ///////////////////////////////////////////

        activate();

        function activate() {
        }

        function deleteResource() {
            resourceService.deleteResource(vm.resource.id).then(function() {
                goBack();
            })
        }

        function doneItemEdit(item) {
            if (vm.newItem) {
                vm.resource.libItems.push(item);
            }
            vm.itemToEdit = null;
            save().then(function() {
                resourceService.getResourceById(vm.resource.id).then(function(resource) {
                    vm.resource = resource;
                })
            })
        }

        function cancelItemEdit() {
            vm.itemToEdit = null;
        }

        function editItem(item) {
            vm.newItem = false;
            vm.itemToEdit = item;
        }

        function createItem(itemType) {
            vm.newItem = true;
            vm.itemToEdit = artifactService.newLibItem(itemType);
        }

        function removeItem(item) {
            vm.resource.libItems = _.reject(vm.resource.libItems, {id: item.id});
            save();
        }

        function viewItem(item) {
            artifactService.viewItem(item);
        }

        var timer = null;

        function autoSave() {

            vm.saveNotice = 'Saving...';

            if (timer) {
                $timeout.cancel(timer);
            }

            timer = $timeout(function () {
                return resourceService.saveResource(vm.resource).then(function() {
                    vm.saveNotice = 'All changes saved';
                });
            }, 1000);
        }

        function save() {
            if (vm.resource.title === '' || !vm.resource.title)
            {
                vm.resource.title = vm.title;
            }
            else {
                vm.title = vm.resource.title;
            }

            vm.saveNotice = 'Saving...';
            return resourceService.saveResource(vm.resource).then(function() {
                $timeout(function() {
                    vm.saveNotice = 'All changes saved';
                }, 1000);
            });
        }

        function goBack() {
            $state.go('resource-list');
        }
    }
})
();