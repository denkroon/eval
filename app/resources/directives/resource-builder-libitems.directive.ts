/// <reference path="../resource.module.ts" />

(function () {
'use strict';

    angular.module('stateeval.resources')
    .directive('resourceBuilderLibItems', resourceBuilderLibItems)
        .controller('resourceBuilderLibItemsController', resourceBuilderLibItemsController);

    resourceBuilderLibItems.$inject = [];
    resourceBuilderLibItemsController.$inject = ['resourceService', 'artifactService', '$state', 'enums', 'utils'];

    function resourceBuilderLibItems() {
        return {
            restrict: 'E',
            scope: {
                resource: '=',
                editItemFcn: '=',
                createItemFcn: '='
            },
            templateUrl: 'app/resources/views/resource-builder-libitems.directive.html',
            controller: 'resourceBuilderLibItemsController as vm',
            bindToController: true
        }
    }

    function resourceBuilderLibItemsController(resourceService, artifactService, $state, enums, utils) {
        var vm = this;

        vm.enums = enums;
        vm.utils = utils;

        vm.createItem = createItem;
        vm.removeItem = removeItem;
        vm.editItem = editItem;
        vm.viewItem = viewItem;

        vm.itemDisplayName = artifactService.libItemDisplayName;

        function editItem(item) {
            vm.editItemFcn(item);
        }

        function createItem(itemType) {
            vm.createItemFcn(artifactService.newLibItem(itemType));
        }
        function removeItem(item) {
            vm.resource.libItems = _.reject(vm.resource.libItems, {id: item.id});
            resourceService.saveResource(vm.resource);
        }

        function viewItem(item) {
            artifactService.viewItem(item);
        }
    }

}) ();
