/// <reference path="../resource.module.ts" />

/**
 * Created by anne on 11/30/2015.
 */

(function() {
    'use strict';

    angular.module('stateeval.resources')
        .directive('resourceBuilderAlignment', resourceBuilderAlignmentDirective)
        .controller('resourceBuilderAlignmentController', resourceBuilderAlignmentController);

    resourceBuilderAlignmentController.$inject = ['resourceService', 'activeUserContextService', '$rootScope', 'rubricUtils'];

    function resourceBuilderAlignmentDirective() {
        return {
            scope: {
                resource: '=',
                readOnly: '='
            },
            templateUrl: 'app/resources/views/resource-builder-alignment.directive.html',
            controller: 'resourceBuilderAlignmentController as vm',
            bindToController: true
        }
    }

    function resourceBuilderAlignmentController(resourceService, activeUserContextService, $rootScope, rubricUtils) {
        var vm = this,
            flatRows,
            framework = activeUserContextService.getActiveFramework();

        vm.loaded = false;

        $rootScope.$on('change-framework', function () {
            framework = activeUserContextService.getActiveFramework();
            loadFramework();
        });

        loadFramework();
        vm.loaded = true;

        function loadFramework() {
            flatRows = rubricUtils.getFlatRows(framework);

            // massaging the data to pass it to the categorized selector directive
            vm.categories = _.map(framework.frameworkNodes, function(fn) {
                return {
                    name: formatName(fn),
                    collapsed: true,
                    rows: _.map(fn.rubricRows, function(rr) {
                        return {
                            id: rr.id,
                            name: rr.shortName + ' ' + rr.title,
                            selected: _.any(vm.resource.alignedRubricRows, { id: rr.id }),
                            inUse: false
                        };
                    })
                };
            });
        }

        // called by the categories selector when the selection has changed
        vm.changed = function changed(selectedRows) {
            vm.resource.alignedRubricRows = _.map(selectedRows, function(row) { return flatRows[row.id]; });
            resourceService.saveResource(vm.resource);
        }
        function formatName(row) {
            return '<div class="pull-left">' + row.shortName + '</div>' +
                '<div class="row-title">' + row.title + '</div>';
        }
    }

})();
