/// <reference path="resource.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.resources')
        .config(configureRoutes);

    configureRoutes.$inject = ['$stateProvider'];

    function configureRoutes($stateProvider) {

        $stateProvider
            .state('resource-home', {
                url: '/resource-home',
                abstract: true,
                parent: 'default-content'
            })
            .state('resource-list', {
                url: '/resource-list/:evaluationtype',
                parent: 'resource-home',
                views: {
                    'content@base': {
                        templateUrl: 'app/resources/views/resource-list.html',
                        controller: 'resourceListController as vm'
                    }
                },
                data: {
                    title: 'Resource Bank',
                },
                ncyBreadcrumb: {
                    label: 'Resources for {{vm.evaluateeTerm}} Evaluations'
                },
            })
            .state('resource-builder', {
                url: '/resource-builder/{resourceId}',
                parent: 'resource-list',
                views: {
                    'content@base': {
                        templateUrl: 'app/resources/views/resource-builder.html',
                        controller: 'resourceBuilderController as vm'
                    }
                },
                resolve: {
                    resource: ['resourceService', '$stateParams', function(resourceService, $stateParams) {
                        return resourceService.getResourceById($stateParams.resourceId)
                            .then(function(resource) {
                                return resource;
                            });
                    }]
                },
                data: {
                    title: 'Edit Resource',
                },
                ncyBreadcrumb: {
                    label: 'Edit Resource'
                }
            })
            .state('resource-view', {
                url: '/resource-view/{resourceId}',
                parent: 'resource-list',
                views: {
                    'content@base': {
                        templateUrl: 'app/resources/views/resource-view.html',
                        controller: 'resourceViewController as vm'
                    }
                },
                resolve: {
                    resource: ['resourceService', '$stateParams', function(resourceService, $stateParams) {
                        return resourceService.getResourceById($stateParams.resourceId)
                            .then(function(resource) {
                                return resource;
                            });
                    }]
                },
                data: {
                    title: 'View Resource'
                },
                ncyBreadcrumb: {
                    label: 'View Resource'
                }
            })
    }
})();