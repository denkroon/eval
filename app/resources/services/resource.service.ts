/// <reference path="../resource.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.resources')
        .factory('resourceService', resourceService);

    resourceService.$inject = ['$http', 'config', 'enums', 'activeUserContextService', '_'];

    function resourceService($http, config, enums, activeUserContextService, _) {

        return {
            getSchoolAdminDefinedResources: getSchoolAdminDefinedResources,
            getDistrictAdminDefinedResources: getDistrictAdminDefinedResources,
            saveResource: saveResource,
            getNewResource: getNewResource,
            getResourceById: getResourceById,
            deleteResource: deleteResource
        };

        function deleteResource(resourceId) {
            return $http.delete(config.apiUrl + 'resources/delete/' + resourceId).then(function () {
            });
        }

        function getResourceById(resourceId) {
            var url = config.apiUrl + 'resources/' + resourceId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getNewResource() {
            return {
                id: 0,
                title: 'New Resource',
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                libItems: [],
                alignedRubricRows: []
            }
        }

        function saveResource(resource) {
            var url = config.apiUrl + 'resources';
            if (resource.id != 0) {
                return $http.put(url, resource);
            } else {
                return $http.post(url, resource).then(function(response) {
                    resource.id = response.data.id;
                });
            }
        }

        function getResourceRequestParam() {
            return {
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                schoolCode: activeUserContextService.context.orientation.schoolCode
            }
        }

        function getSchoolAdminDefinedResources() {
            var requestParam = getResourceRequestParam();
            var url = config.apiUrl + 'resources/school/';
            return $http.get(url, {params: requestParam}).then(function (response) {
                return response.data;
            });
        }

        function getDistrictAdminDefinedResources() {
            var requestParam = getResourceRequestParam();
            var url = config.apiUrl + 'resources/district/';
           return $http.get(url, {params: requestParam}).then(function (response) {
                return response.data;
            });
        }
    }
})();