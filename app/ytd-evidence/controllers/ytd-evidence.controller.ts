/// <reference path="../ytd-evidence.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.ytd-evidence')
        .controller('ytdEvidenceController', ytdEvidenceController);

    ytdEvidenceController.$inject = ['evidenceCollection', 'activeUserContextService', 'evidenceCollectionService', 'config',
    'enums', '$scope'];

    function ytdEvidenceController(evidenceCollection, activeUserContextService, evidenceCollectionService, config, enums,
    $scope) {
        var vm = this;
        vm.enums = enums;

        vm.isEvaluatee = activeUserContextService.context.isEvaluatee();
        vm.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();
        vm.evidenceCollection = evidenceCollection;
        vm.evidenceCollectionService = evidenceCollectionService;

    }
})();


