/// <reference path="./ytd-evidence.module.ts" />

/**
 * Created by anne on 11/3/2015.
 */

(function() {
angular.module('stateeval.ytd-evidence')
    .config(configureRoutes);

function configureRoutes($stateProvider, $urlRouterProvider) {

    $stateProvider

        .state('ytd-evidence', {
            url: '/ytd-evidence',
            parent: 'default-content',
            views: {
                'content@base': {
                    controller: 'ytdEvidenceController as vm',
                    templateUrl: 'app/ytd-evidence/views/ytd-evidence.html'
                }
            },
            resolve: {
                evidenceCollection: ['evidenceCollectionService', 'enums', 'activeUserContextService',
                    function (evidenceCollectionService, enums, activeUserContextService) {
                    return evidenceCollectionService.getEvidenceCollection("YTD",
                                enums.EvidenceCollectionType.YEAR_TO_DATE,
                                null,
                                activeUserContextService.user.id)
                        .then(function (evidenceCollection) {
                                return evidenceCollection;
                            }
                        )
                }]
            },
            data: {
                evaluateeRequired: true,
                title: 'Year to Date Evidence View'
            },
            ncyBreadcrumb: {
                label: 'Year to Date Evidence View'
            },
            params: {
                nodeShortName: null,
                rowId: null
            }
        });

}
})()