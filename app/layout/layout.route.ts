/// <reference path="layout.module.ts" />

(function () {
    "use strict";
    var core = angular.module('stateeval.layout');

    core.config(configureRoutes);

    configureRoutes.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
    function configureRoutes($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('landing', {
                url: '/landing',
                controller: 'landingController as vm',
                templateUrl: 'app/layout/landing/landing.html',
                resolve: {
                    load: ['localStorageService', 'activeUserContextService', '$state', function(localStorageService, activeUserContextService, $state) {
                        var user = localStorageService.get('entryUser');
                        if(user) {
                            activeUserContextService.user = user;
                        } else {
                            $state.go('login');
                        }
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Welcome to eVAL!'
                },
            })
            .state('home', {
                url: '/home',
                controller: 'landingController as vm',
                templateUrl: 'app/layout/landing/landing.html',
                /*resolve: {
                    load: ['localStorageService', 'activeUserContextService', '$state', function(localStorageService, activeUserContextService, $state) {
                        var user = localStorageService.get('entryUser');
                        if(user) {
                            activeUserContextService.user = user;
                        } else {
                            $state.go('login');
                        }
                    }]
                },*/
                ncyBreadcrumb: {
                    label: 'Welcome to eVAL!'
                },
            })
            .state('dashboard-home', {
                abstract: true,
                parent: 'default-content'
            })
            .state('evaluator-dashboard', {
                url: '/evaluator-dashboard',
                parent: 'dashboard-home',
                views: {
                    'content@base': {
                        templateUrl: 'app/layout/dashboards/evaluator-dashboard.html',
                        controller: 'evaluatorDashboardController as vm'
                    }
                },
                abstract: true
            })
            .state("evaluatee-list", {
                url: '/evaluatee-list',
                parent: 'evaluator-dashboard',
                views: {
                    'content@evaluator-dashboard': {
                        templateUrl: 'app/layout/evaluatee-summary/evaluatee-list.html',
                        controller: 'evaluateeListController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'All {{vm.evaluateeTerm}}s'
                },
                data: {
                    title: 'Evaluator Dashboard'
                }
            })
            .state("evaluatee-list-coverage", {
                url: '/evaluatee-list-coverage',
                parent: 'evaluator-dashboard',
                views: {
                    'content@evaluator-dashboard': {
                        templateUrl: 'app/layout/coverage/evaluatee-list-coverage.html',
                        controller: 'evaluateeListCoverageController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Converage for all {{vm.evaluateeTerm}}s'
                }
            })
            .state("evaluatee-tor", {
                url: '/evaluatee-tor/:id',
                parent: 'evaluatee-list',
                views: {
                    'content@evaluator-dashboard': {
                        templateUrl: 'app/layout/evaluatee-summary/evaluatee.html',
                        controller: 'evaluateeController as vm'
                    }
                },
                resolve: {
                    evaluatee: ['activeUserContextService', '$stateParams', function (activeUserContextService, $stateParams) {
                        activeUserContextService.context.evaluatee = _.find(activeUserContextService.context.evaluatees, {id: parseInt($stateParams.id)});
                        return activeUserContextService.context.evaluatee;
                    }],
                    evidenceCollection: ['evidenceCollectionService', 'activeUserContextService', 'enums',
                        function (evidenceCollectionService, activeUserContextService, enums) {
                        return evidenceCollectionService.getEvidenceCollection("YTD",
                                            enums.EvidenceCollectionType.YEAR_TO_DATE,
                                            null,
                                            activeUserContextService.user.id)
                            .then(function (evidenceCollection) {
                                    return evidenceCollection;
                                }
                            )
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Active {{vm.evaluateeTerm}} - {{vm.evaluatee.displayName}}'
                },
                params: {
                    id: null
                }
            })
            .state("evaluatee-dashboard", {
                url: '/evaluatee-dashboard',
                parent: 'dashboard-home',
                abstract:true,
                views: {
                    'content@base': {
                        templateUrl: 'app/layout/dashboards/evaluatee-dashboard.html',
                        controller: 'evaluateeDashboardController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Dashboard'
                }
            })
            .state("evaluatee-summary", {
                url: '/evaluatee-summary',
                parent: 'evaluatee-dashboard',
                views: {
                    'content@evaluatee-dashboard': {
                        templateUrl: 'app/layout/evaluatee-summary/evaluatee.html',
                        controller: 'evaluateeController as vm'
                    }
                },
                resolve: {
                    evaluatee: ['activeUserContextService', function (activeUserContextService) {
                        return activeUserContextService.context.evaluatee;
                    }],
                    evidenceCollection: ['evidenceCollectionService', 'activeUserContextService', 'enums',
                        function (evidenceCollectionService, activeUserContextService, enums) {
                        return evidenceCollectionService.getEvidenceCollection("YTD",
                            enums.EvidenceCollectionType.YEAR_TO_DATE,
                            null,
                            activeUserContextService.user.id)
                            .then(function (evidenceCollection) {
                                    return evidenceCollection;
                                }
                            )
                    }]
                },
                ncyBreadcrumb: {
                    label: 'Dashboard'
                }
            })
            .state("evaluatee-coverage", {
                url: '/evaluatee-coverage',
                parent: 'evaluatee-dashboard',
                views: {
                    'content@evaluatee-dashboard': {
                        templateUrl: 'app/layout/coverage/evaluatee-coverage.html',
                        controller: 'evaluateeCoverageController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Coverage'
                }
            })
            .state("evaluatee-coverage-tor", {
                url: '/evaluatee-coverage-tor/:id',
                parent: 'evaluatee-list',
                views: {
                    'content@evaluator-dashboard': {
                        templateUrl: 'app/layout/coverage/evaluatee-coverage.html',
                        controller: 'evaluateeCoverageController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Coverage for {{vm.evaluateeTerm}} - {{vm.evaluatee.displayName}}'
                }
            })
            .state('dv-dashboard', {
                url: '/dv-dashboard',
                parent: 'dashboard-home',
                views: {
                    'content@base': {
                        templateUrl: 'app/layout/dashboards/dv-dashboard.html'
                    }
                }
            })
            .state('evaluator-begin-year', {
                url: '/tor-begin-year',
                parent: 'evaluator-dashboard',
                views: {
                    'content@evaluator-dashboard' : {
                        templateUrl: 'app/layout/dashboards/begin-year.html',
                        controller: 'beginYearController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Start of Year Setup for {{vm.evaluateeTerm}} Evaluations'
                }
            })
            .state('sa-dashboard', {
                url: '/sa-dashboard',
                parent: 'dashboard-home',
                views: {
                    'content@dashboard-home': {
                        templateUrl: 'app/layout/dashboards/sa-dashboard.html',
                        controller: 'saDashboardController as vm'
                    }
                }
            })
            .state('sa-begin-year', {
                url: '/sa-begin-year',
                parent: 'sa-dashboard',
                views: {
                    'content@sa-dashboard' : {
                        templateUrl: 'app/layout/dashboards/begin-year.html',
                        controller: 'beginYearController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Start of Year Setup for {{vm.evaluateeTermUC}} Evaluations'
                }
            })
            .state('da-dashboard', {
                url: '/da-dashboard',
                parent: 'dashboard-home',
                views: {
                    'content@base': {
                        templateUrl: 'app/layout/dashboards/da-dashboard.html',
                        controller: 'daDashboardController as vm'
                    }
                }
            })
            .state('da-begin-year', {
                url: '/da-begin-year',
                parent: 'da-dashboard',
                views: {
                    'content@da-dashboard' : {
                        templateUrl: 'app/layout/dashboards/begin-year.html',
                        controller: 'beginYearController as vm'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Start of Year Setup for {{vm.evaluateeTermUC}} Evaluations'
                }
            })

    }

})();