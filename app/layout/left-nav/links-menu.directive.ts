(function () {
    'use strict';

    angular.module('stateeval.layout')
        .directive('linksMenu', linksMenuDirective)
        .controller('linksMenuController', linksMenuController);

    linksMenuDirective.$inject = [];
    function linksMenuDirective() {
        return {
            restrict: 'E',
            scope: {},
            replace: true,
            templateUrl: 'app/layout/left-nav/links-menu.directive.html',
            controller: 'linksMenuController as vm',
            bindToController: true
        }
    }

    linksMenuController.$inject = ['$state', 'activeUserContextService', 'workAreaService', '$scope'];
    function linksMenuController($state, activeUserContextService, workAreaService, $scope) {
        var vm = this;
        vm.wa = activeUserContextService.context.workArea();
        vm.go = go;

        function go (state) {
            $state.go('')
        }

    }
})();
