(function () {
    'use strict';

    angular.module('stateeval.layout')
        .directive('userOrientation', userOrientationDirective)
        .controller('userOrientationController', userOrientationController)
        .filter('number', toNumberFilter);

    function userOrientationDirective() {
        return {
            restrict: 'E',
            scope: {
                config: '='
            },
            templateUrl: 'app/layout/left-nav/user-orientation.directive.html',
            controller: 'userOrientationController as vm',
            bindToController: true
        }
    }

    function userOrientationController(enums, workAreaService, activeUserContextService, startupService,
                                       utils, localStorageService, locationService, userService, $scope, $q, frameworkService) {
        var vm = this;
        vm.enums = enums;
        vm.utils = utils;

        vm.context = activeUserContextService.context;
        vm.order = enums.UserOrientationPropOrder;
        vm.displayNames = ['Year', 'District', 'School', 'Work Area'];
        vm.workAreaService = workAreaService;
        vm.lengths = {};
        vm.assignedOnly = vm.context.assignedEvaluatees;
        vm.displayContext = vm.context.impersonateConfig ? localStorageService.get('authenticatedContext') : vm.context;

        vm.impersonateeList = [];
        vm.imp = {};
        vm.impersonatee = null;
        vm.tag = '';
        vm.impersonateConfig = null;

        vm.showImpersonationPopover = false;
        vm.showWorkAreaPopover = false;

        var roleMap = {
            'PR_PR': vm.enums.Roles.SESchoolHeadPrincipal,
            'PR_TR': vm.enums.Roles.SESchoolPrincipal,
            'DTE': vm.enums.Roles.SEDistrictWideTeacherEvaluator
        };

        var workAreaMap = {};
        workAreaMap[enums.Roles.SESchoolHeadPrincipal] = 'PR_PR';
        workAreaMap[enums.Roles.SESchoolPrincipal] = 'PR_TR';
        workAreaMap[enums.Roles.SEDistrictWideTeacherEvaluator] = 'DTE';

        vm.impTitleMap = {
            'PR_PR': 'Principal Evaluating Teachers',
            'PR_PR': 'Head Principal Evaluating Principals',
            'DTE': 'District-wide Teacher Evaluator'
        };

        vm.roleMap = roleMap;

        vm.config.openImpersonationPopover = openImpersonationPopover;



        setAssignedOnlyLabel();

        vm.change = change;,

        vm.openImpersonationPopover = openImpersonationPopover;
        vm.closeImpersonationPopover = closeImpersonationPopover;
        vm.openWorkAreaPopover = openWorkAreaPopover;
        vm.closeWorkAreaPopover = closeWorkAreaPopover;
        vm.closeAll = closeAll;

        vm.loadImpersonatees = loadImpersonatees;
        vm.impersonate = impersonate;
        vm.changeWorkArea = changeWorkArea;
        vm.toggle = toggle;
        vm.defaultSchool = defaultSchool;

        function defaultSchool() {
            vm.imp.school = vm.imp[vm.imp.workAreaTag].schools[0];
        }

        function setAssignedOnlyLabel() {
            if (vm.workAreaService[vm.context.navOptions.workAreaTag].isEvaluatorWorkArea()) {
                var evalType = vm.workAreaService[vm.context.navOptions.workAreaTag].evaluationType;
                var evaluateeRole = utils.mapRoleNameToFriendlyName(utils.mapEvalTypeToEvaluateeRole(evalType));
                vm.assignedOnlyLabel = "Assigned " + evaluateeRole + "s only";
            }
        }

        function toggle(whereFrom) {
            console.log(whereFrom);
            vm.showWorkAreaPopover || vm.showImpersonationPopover ? vm.closeAll() : vm.openWorkAreaPopover();
        }

        function change(arr, spot, index) {
            var currentProperty = enums.UserOrientationPropOrder[index];
            var nextProperty = enums.UserOrientationPropOrder[index + 1];
            vm[currentProperty] = arr;
            vm.lengths[currentProperty] = Object.keys(arr).length;
            if (!vm[currentProperty][spot]) {
                console.log('previous field not found', spot, 'changing to', Object.keys(vm[currentProperty])[0]);
                spot = Object.keys(vm[currentProperty])[0];
            }
            vm.context.navOptions[currentProperty] = spot.toString();
            if (nextProperty) {
                var nextSpot = vm.context.navOptions[nextProperty];
                change(vm[currentProperty][spot], nextSpot, index + 1);
            }
            setAssignedOnlyLabel();
        }

        function changeWorkArea(tag) {
            if (tag == 'IMP') {
                if (!vm.context.impersonateConfig) {
                    vm.context.impersonateConfig = {
                        districtCode: vm.context.orientation.districtCode,
                        districtName: vm.context.orientation.districtName,
                        schoolYear: vm.context.orientation.schoolYear,
                    };
                }
                openImpersonationPopover()
            } else {
                vm.context.changeWorkArea(tag);
            }
        }

        function openImpersonationPopover() {
            vm.showImpersonationPopover = true;
            vm.showWorkAreaPopover = false;
            loadImpersonatees();
        }

        function openWorkAreaPopover() {
            if (!vm.showImpersonationPopover) {
                vm.showWorkAreaPopover = true;
                change(vm.context.orientationOptions, vm.context.orientation.schoolYear, 0);
            }
        }

        function closeImpersonationPopover() {
            vm.showImpersonationPopover = false;
            if(!vm.config.withoutWorkAreaPopover){
                vm.showWorkAreaPopover = true;
                vm.context.impersonateConfig = null;
                vm.config.withoutWorkAreaPopover = false;
            }
        }

        function closeWorkAreaPopover() {
            vm.showWorkAreaPopover = false;
        }

        function closeAll() {
            closeImpersonationPopover();
            closeWorkAreaPopover();
        }

        function loadImpersonatees() {

            vm.imp = {
                workAreaTag: '',
                school: '',
                impersonatee: '',
            };
            var callbacks = {
                'PR_PR': userService.getHeadPrincipalsInDistrict,
                'PR_TR': userService.getHeadPrincipalsInDistrict,
                'DTE': userService.getEvaluatorsForDTE
            };
            var promiseList = [];

            return frameworkService.getLoadedFrameworkContexts(vm.context.impersonateConfig.schoolYear, vm.context.impersonateConfig.districtCode)
            .then(function(data) {
                var grouped = _.groupBy(data, 'evaluationType');
                for (var i in workAreaService.viewableImpersonateRoles) {
                    vm.imp[i] = {
                        evaluationType: workAreaService[i].evaluationType,
                        configured: !!grouped[workAreaService[i].evaluationType]
                    };
                    var schoolsPromise = associateBySchool(callbacks[i], vm.imp[i]);
                    promiseList.push(schoolsPromise);
                }


                $q.all(promiseList).then(function () {
                    for (var i in vm.imp) {
                        if (vm.imp[i].configured) {
                            vm.imp.workAreaTag = i;
                            break;
                        }
                    }
                })
            });

        }

        function impersonate(impersonatee, impersonateeList, school, workAreaTag) {
            vm.context.impersonateConfig.schoolName = school ? school.schoolName : '';
            vm.context.impersonateConfig.schoolCode = school ? school.schoolCode : '';
            vm.context.impersonateConfig.impersonateeList = impersonateeList;
            vm.context.impersonateConfig.impersonatee = impersonatee;
            vm.context.impersonateConfig.workAreaTag = workAreaTag;
            vm.context.impersonateConfig.roleName = roleMap[workAreaTag];
            vm.showImpersonationPopover = false;
            vm.context.changeWorkArea('IMP');
        }

        function associateBySchool(backend, container) {
             return backend()
            .then(function(impersonatees) {
                var list = [];
                for (var i in impersonatees) {
                    for (var j in impersonatees[i].locationRoles) {
                        //ignore non impersonate roles
                        var locationRole = impersonatees[i].locationRoles[j];
                        if (!workAreaService.viewableImpersonateRoles[workAreaMap[locationRole.roleName]]) {
                            continue;
                        }

                        var schoolInList = _.find(list, function (n) {
                            return n.schoolCode === impersonatees[i].locationRoles[j].schoolCode;
                        });
                        if (!schoolInList) {
                            schoolInList = {
                                schoolCode: impersonatees[i].locationRoles[j].schoolCode,
                                schoolName: impersonatees[i].locationRoles[j].schoolName,
                                impersonateeList: []
                            };
                            list.push(schoolInList)
                        }
                        schoolInList.impersonateeList.push(impersonatees[i]);
                    }
                }
                container.schools = list;
            });
        }
    }

    function toNumberFilter() {
        return function (input) {
            return parseInt(input, 10);
        };
    }


})();
