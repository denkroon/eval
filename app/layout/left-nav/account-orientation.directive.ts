(function() {
    'use strict';

    angular.module('stateeval.layout')
    .directive('accountOrientation', accountOrientationDirective)
    .controller('accountOrientationController', accountOrientationController);

    accountOrientationDirective.$inject = [];
    function accountOrientationDirective() {
        return {
            restrict: 'E',
            scope: {},
            controller: 'accountOrientationController as vm',
            bindToController: true,
            templateUrl: 'app/layout/left-nav/account-orientation.directive.html'
        }
    }
    accountOrientationController.$inject = ['activeUserContextService', 'startupService', 'workAreaService'];
    function accountOrientationController(activeUserContextService, startupService, workAreaService) {
        var vm = this;
        vm.workAreaService = workAreaService;
        vm.userOrientations = startupService.changeEvaluationTypesToWorkAreaTags(activeUserContextService.user.userOrientations)
    }
}) ();