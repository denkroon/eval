(function () {
    'use strict';

    angular.module('stateeval.core')
        .controller('leftNavbarController', leftNavbarController);

    function leftNavbarController(activeUserContextService, workAreaService, $state, enums,
                                  utils, startupService, localStorageService, $scope, _) {
        var vm = this;
        vm.evalLogo = utils.getEvalLogo();
        vm.context = activeUserContextService.context;
        vm.user = activeUserContextService.user;
        vm.workAreaService = workAreaService;
        vm.enums = enums;
        vm.utils = utils;
        var displayContext = vm.impersonator ? vm.impersonator : vm.context;
        vm.impersonationSchool = utils.getImpersonationSchool(displayContext);
        vm.role = utils.getRole(displayContext);
        vm.schoolYear = utils.getSchoolYear(displayContext);
        vm.state = $state;
        vm.assignedOnly = false;
        vm.showOrientation = false;
        vm.impersonateConfig = vm.context.impersonating;
        vm.impersonator = localStorageService.get('authenticatedContext');
        vm.impersonatorUser = localStorageService.get('authenticatedUser');
        vm.userInfo = vm.impersonator ? vm.impersonatorUser : vm.user;
        vm.displayContext = vm.impersonator ? vm.impersonator : vm.context;
        vm.evaluateeTerm = vm.context.evaluatees ? activeUserContextService.getEvaluateeTermUpperCase() : null;

        vm.showImpersonationOnly = false;

        vm.clickLink = clickLink;
        vm.clickSubLink = clickSubLink;
        vm.changeEvaluatee = changeEvaluatee;
        vm.initWorkArea = initWorkArea;
        vm.changeImpersonatee = changeImpersonatee;
        vm.editOptions = editOptions;
        vm.config = {};

        ////////////////////////

        activate();

        function activate() {
            activateHighlighting();

            vm.rolesDisplayString = _.map(startupService.getRolesForCurrentLocation(vm.displayContext), function (role) {
                return utils.mapRoleNameToFriendlyName(role);
            }).join(', ');
        }


        $scope.$watch('vm.state.current.name', function (newValue, oldValue) {
            if (newValue !== 'login') {
                activateHighlighting();
            }
        });
        $scope.$watch('vm.context.navOptions.workAreaTag', function (newVal, oldVal) {
            vm.oldVal = oldVal;
        });


        function editOptions() {
            vm.showOrientation = true;
            vm.config.openImpersonationPopover();
            vm.config.withoutWorkAreaPopover = true;
        }


        function activateHighlighting() {
            var wa = vm.context.workArea();
            vm.parent = -1;
            vm.child = -1;
            for (var i in wa.navbar) {
                var state = utils.stripParameters(wa.navbar[i].state);
                if ($state.includes(state)) {
                    vm.parent = i;
                }
                for (var j in wa.navbar[i].subLinks) {
                    state = utils.stripParameters(wa.navbar[i].subLinks[j].state);
                    if ($state.includes(state)) {
                        vm.parent = i;
                        vm.child = j;
                    }
                }
            }
            vm.parent = Number(vm.parent);
            vm.child = Number(vm.child);
        }

        function clickLink(state, index, link) {
            if (activeUserContextService.context.incompleteEvaluatee) {
                $state.go('incomplete-evaluatee');
            } else {
                var destination = utils.uiRouterString(state);
                var routerState = $state.get(destination.state);
                if ((routerState.data && routerState.data.evaluateeRequired) &&
                    (vm.context.evaluatees && (!vm.context.evaluatees.length || !vm.context.evaluatee))) {
                    utils.wiggleEvaluatingDropdown();
                } else {
                    if (!link.subLinks) {
                        vm.parent = index;
                        $state.go(destination.state, destination.params);
                    }
                }
            }
        }

        function clickSubLink(state, index, parentIndex) {
            var destination = utils.uiRouterString(state);
            var routerState = $state.get(destination.state);
            if ((routerState.data && routerState.data.evaluateeRequired) &&
                (vm.context.evaluatees && (!vm.context.evaluatees.length || !vm.context.evaluatee))) {
                utils.wiggleEvaluatingDropdown();
            } else {
                vm.parent = parentIndex;
                vm.child = index;
                $state.go(destination.state, destination.params);
            }
        }

        function changeEvaluatee() {
            workAreaService.changeEvaluatee(vm.context.evaluatee, activeUserContextService);
        }

        function initWorkArea(tag) {
            vm.context.changeWorkArea(tag)
                .then(null, failInitWorkArea);
        }

        function failInitWorkArea() {
            vm.context.navOptions.workAreaTag = vm.oldVal;
        }

        function changeImpersonatee() {
            workAreaService['IMP'].initializeWorkArea(activeUserContextService, startupService);
        }
    }
})();