/// <reference path="../../core/core.module.ts" />

(function () {
    'use strict';

    function coverageDirective() {
        return {
            restrict: 'E',
            scope: {
                singleView: '=',
                evaluatee: '='
            },
            templateUrl: 'app/layout/coverage/coverage.directive.html',
            controller: 'coverageController as vm',
            bindToController: true
        }
    }

    class CoverageController {

        // having trouble with the select initializing the default value
        // from the model when it isn't a string
        seeWhatsLeft: string;
        viewEvaluations: string;

        evaluations;
        context;
        done;
        rubricUtils;
        enums;
        doneTerm;
        arrayTerm;
        name;
        singleView: boolean;
        evaluatee;
        evidenceCollections;
        fullCollection;

        constructor(private activeUserContextService: any,
                    private observationService: any,
                    private evidenceCollectionService: any,
                    private evaluationService: any,
                    private enums: any,
                    private utils: any,
                    private $state: any,
                    $scope,
                    $rootScope) {

            this.context = activeUserContextService.context;
            this.seeWhatsLeft = "0";
            this.viewEvaluations = "0";

            this.flipEvidenceEvaluationView();

            this.name = this.context.framework.name;

            $rootScope.$on('change-framework', () => {
                this.name = this.context.framework.name;
            });

            $scope.$watch(() => {
                    return this.singleView
                },
                (singleView) => {
                    if (singleView) {
                        this.setCoverageEvaluatee(this.evaluatee);
                    }
                    else {
                        this.setCoverageEvaluatees();
                    }
                });
        }

        flipEvidenceEvaluationView() {
            if (this.viewEvaluations === "1") {
                this.doneTerm = 'evaluationDone';
                this.arrayTerm = 'evaluations';
            } else {
                this.doneTerm = 'evidenceDone';
                this.arrayTerm = 'evidences';
            }
        }

        chooseSingle(index, evidenceCollection) {
            if (this.singleView) {
                if (evidenceCollection.request.collectionType === this.enums.EvidenceCollectionType.OBSERVATION) {
                    this.observationService.gotoObservation(this.activeUserContextService, evidenceCollection.observation);
                }
                else if (evidenceCollection.request.collectionType === this.enums.EvidenceCollectionType.OTHER_EVIDENCE) {
                    this.$state.go('ytd-evidence');
                }
                else if (evidenceCollection.request.collectionType === this.enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) {
                    this.$state.go('sg-bundle-summary');
                }
            }
            else {
                var context = this.activeUserContextService.context;
                context.evaluatee = context.evaluatees[index];
                this.evaluatee = context.evaluatees[index];
                this.singleView = true;
            }
        };

        setCoverageEvaluatee = (evaluatee) => {
            return this.evaluationService.getEvidenceCollectionsForEvaluation(evaluatee.evalData.id)
                .then((eCols) => {
                    this.context.evaluatee = evaluatee;
                    var evidenceCollections = [];
                    var conglomerateEvaluations = [];
                    var conglomerateEvidence = [];
                    var collectionTypes = _.groupBy(eCols, 'collectionType');
                    for (var i in collectionTypes) {
                        for (var j in collectionTypes[i]) {
                            var type = parseInt(i);
                            if (type !== this.enums.EvidenceCollectionType.SELF_ASSESSMENT) {
                                var name, collectionObjectId, observation, otherEvidenceCollection, studentGrowthGoalBundle;
                                switch (type) {
                                    case this.enums.EvidenceCollectionType.OTHER_EVIDENCE:
                                        name = 'Other Evidence';
                                        otherEvidenceCollection = collectionTypes[i][j].otherEvidenceCollection;
                                        break;
                                    case this.enums.EvidenceCollectionType.OBSERVATION:
                                        name = collectionTypes[i][j].observation.title;
                                        observation = collectionTypes[i][j].observation;
                                        collectionObjectId = collectionTypes[i][j].observation.id;
                                        break;
                                    case this.enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS:
                                        name = 'Student Growth Goals';
                                        studentGrowthGoalBundle = collectionTypes[i][j].studentGrowthGoalBundle;
                                        break;
                                }
                                var evidenceCollection = this.evidenceCollectionService.newEvidenceCollection(
                                    name,
                                    {collectionType: parseInt(i), collectionObjectId: collectionObjectId},
                                    {
                                        observation: observation,
                                        otherEvidenceCollection: otherEvidenceCollection,
                                        studentGrowthGoalBundle: studentGrowthGoalBundle,
                                        rubricRowEvaluations: collectionTypes[i][j].rubricRowEvaluations,
                                        availableEvidence: collectionTypes[i][j].availableEvidence
                                    }
                                );
                                evidenceCollections.push(evidenceCollection);
                                conglomerateEvaluations = conglomerateEvaluations.concat(collectionTypes[i][j].rubricRowEvaluations);
                                conglomerateEvidence = conglomerateEvidence.concat(collectionTypes[i][j].availableEvidence);
                            }
                        }
                    }

                    this.evidenceCollections =  evidenceCollections;
                    this.fullCollection = this.evidenceCollectionService.newEvidenceCollection('Conglomerate Evaluations',
                        {collectionType: this.enums.EvidenceCollectionType.SUMMATIVE},
                        {rubricRowEvaluations: conglomerateEvaluations,
                            availableEvidence: conglomerateEvidence})
                    this.seeWhatsLeft = '0';

                })
        };

        setCoverageEvaluatees() {
            return this.evidenceCollectionService.getRubricRowEvaluationsForTorTee()
                .then((evaluations) => {
                    this.evidenceCollectionService.getAvailableEvidencesForTorTee()
                        .then((evidences) => {
                            var evaluatee = this.context.evaluatee;
                            var evidencesGrouped = _.groupBy(evidences, 'evaluationId');
                            var evaluationsGrouped = _.groupBy(evaluations, 'evaluationId');
                            var tempCollections = [];

                            for (var i in this.context.evaluatees) {
                                this.context.evaluatee = this.context.evaluatees[i];
                                var collection = this.evidenceCollectionService.newEvidenceCollection(
                                    this.context.evaluatees[i].displayName,
                                    {collectionType: this.enums.EvidenceCollectionType.SUMMATIVE},
                                    {rubricRowEvaluations: evaluationsGrouped[this.context.evaluatees[i].evalData.id],
                                        availableEvidence: evidencesGrouped[this.context.evaluatees[i].evalData.id]});
                                tempCollections.push(collection);

                            }
                            this.context.evaluatee = evaluatee;
                            this.evidenceCollections = tempCollections;
                            this.fullCollection = this.evidenceCollectionService.newEvidenceCollection('Conglomerate Evaluations',
                                {collectionType: this.enums.EvidenceCollectionType.SUMMATIVE},
                                {rubricRowEvaluations: evaluations, availableEvidence: evidences});
                            this.seeWhatsLeft = '0';
                        });
                });
        }

    }

    angular.module('stateeval.core')
        .directive('coverage', coverageDirective)
        .controller('coverageController', CoverageController);

})();
