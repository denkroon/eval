/**
 * Created by anne on 11/9/2015.
 */
(function() {
    'use strict';

    class evaluateeCoverageController {

        evaluatee;

        constructor(private activeUserContextService: any) {

            this.evaluatee = activeUserContextService.context.evaluatee;
        }
    }

    angular.module('stateeval.layout')
        .controller('evaluateeCoverageController', evaluateeCoverageController);

}) ();
