(function () {
    'use strict';

    class EvaluateeListCoverageController {

        singleView: boolean;
        evaluatee: any;
        evaluateeTerm: string;

        constructor(activeUserContextService: any) {

            this.singleView = false;
            this.evaluatee = null;
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
        }

        back = () => {
            this.evaluatee = null;
            this.singleView = false;
        }

    }

    angular.module('stateeval.layout')
        .controller('evaluateeListCoverageController', EvaluateeListCoverageController);
})();