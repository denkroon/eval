(function () {
    'use strict';

    class daDashboardController {
        evaluatee: any;
        evaluateeTerm: string;
        links: [any];

        constructor(private activeUserContextService:any, private utils: any) {
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
            this.links = [
                new utils.Link('Startup', 'da-begin-year'),
            ];
        }
    }

    angular
        .module('stateeval.layout')
        .controller('daDashboardController', daDashboardController);
})();