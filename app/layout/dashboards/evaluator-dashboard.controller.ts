(function () {
    'use strict';

    class evaluatorDashboardController {
        evaluatee: any;
        evaluateeTerm: string;
        links: [any];
        unassignedCount: number;
        assignmentDelegated: boolean;
        showStartup: boolean;

        constructor(
            private activeUserContextService:any,
            private assignmentsService: any,
            private userService: any,
            private enums: any,
            private utils: any) {

            this.showStartup = false;
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();

            this.links = [];

            var workAreaTag = activeUserContextService.context.orientation.workAreaTag;
            if (workAreaTag === 'DTE') {

                var pendingAssignmentRequest = 0;
                assignmentsService.getDTEAssignmentRequestsForDTE(activeUserContextService.user)
                    .then((requests) => {
                        pendingAssignmentRequest =
                            _.filter(requests, {evalRequestStatus:
                                                enums.EvalAssignmentRequestStatus.PENDING}).length;

                        if (pendingAssignmentRequest>0) {
                            this.links.push(new utils.Link('Startup&nbsp;&nbsp; <span class="label label-warning">Incomplete</span>', 'evaluator-begin-year'));
                        }

                        this.setupStandardLinks();
                });
            }
            else if (workAreaTag === 'DE') {
                this.assignmentsService.getEvaluateesForAssignment(
                        activeUserContextService.context.orientation.districtCode, '').then((principals) => {

                    this.unassignedCount = 0;
                    for (var i=0; i<principals.length; ++i) {
                        if (!principals[i].evalData.evaluatorId ||
                            principals[i].evaluateePlanType === this.enums.EvaluationPlanType.UNDEFINED) {
                            this.unassignedCount++;
                        }
                    }

                    if (this.unassignedCount>0) {
                        this.links.push(new utils.Link('Startup&nbsp;&nbsp; <span class="label label-warning">Incomplete</span>', 'evaluator-begin-year'));
                    }

                    this.setupStandardLinks();
                });
            }
            else {
                assignmentsService.getSchoolAssignmentsSummary().then((summary) => {

                    this.unassignedCount = summary.unassignedCount;
                    this.assignmentDelegated = summary.delegated;

                    if (this.unassignedCount>0) {
                        this.links.push(new utils.Link('Startup&nbsp;&nbsp; <span class="label label-warning">Incomplete</span>', 'evaluator-begin-year'));
                    }

                    this.setupStandardLinks();
                });
            }
        }

        setupStandardLinks() {
            this.links.push(new this.utils.Link('Yearly Status', 'evaluatee-list'));
            this.links.push(new this.utils.Link('Coverage', 'evaluatee-list-coverage'));

            var context = this.activeUserContextService.context;
            if (context.evaluatee) {
                this.links.push(new this.utils.Link(context.evaluatee.displayName, 'evaluatee-tor', {id: context.evaluatee.id}));
            }
        }
    }

    angular
        .module('stateeval.layout')
        .controller('evaluatorDashboardController', evaluatorDashboardController);
})();