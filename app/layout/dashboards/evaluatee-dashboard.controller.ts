(function () {
    'use strict';

    class evaluateeDashboardController {

        evaluatee: any;
        evaluateeTerm: string;
        links: [any];

        constructor(
            private activeUserContextService:any,
            private utils: any) {

            this.evaluatee = activeUserContextService.context.evaluatee;
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
            this.links = [
                new utils.Link("Summary", 'evaluatee-summary'),
                new utils.Link('Coverage', 'evaluatee-coverage')
            ];
        }
    }

    angular
        .module('stateeval.layout')
        .controller('evaluateeDashboardController', evaluateeDashboardController);
})();