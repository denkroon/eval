(function () {
    'use strict';

    class saDashboardController {
        evaluatee: any;
        evaluateeTerm: string;
        links: [any];

        constructor(private activeUserContextService:any, private utils: any) {
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
            this.links = [
                new utils.Link('Startup', 'sa-begin-year'),
            ];
        }
    }

    angular
        .module('stateeval.layout')
        .controller('saDashboardController', saDashboardController);
})();