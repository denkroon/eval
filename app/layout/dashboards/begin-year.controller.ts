/**
 * Created by anne on 4/24/2016.
 */
(function() {

    'use strict';

    class beginYearController {

        welcomeMessage: string;
        enums: any;
        frameworkContext: any;
        assignmentDelegated: boolean;
        frameworkIsLoaded: boolean;
        frameworkIsActive: boolean;
        schoolCode: string;
        schoolName: string;
        districtName: string;
        schoolYearDisplayName: string;
        evaluateeTermUC: string;
        evaluateeTermLC: string;
        roleName : string;
        evaluationType: number;
        schoolOrDistrictName: string;
        unassignedCount: number;
        schoolUnassignedCount: [any];
        workAreaTag: string;
        delegatedAssignmentsCount: number;
        schoolCount: number;
        pendingDTEAssignmentRequestCount: number;

        isDA: boolean;
        isSA: boolean;
        isPRH: boolean;

        constructor(
            private frameworkService: any,
            private _: _.LoDashStatic,
            public enums: any,
            private assignmentsService: any,
            private activeUserContextService: any,
            private utils: any,
            private $state: any
        )
        {
            this.schoolUnassignedCount = [];
            var user = activeUserContextService.user;
            var context = activeUserContextService.context;
            this.workAreaTag = activeUserContextService.context.orientation.workAreaTag;
            this.schoolYearDisplayName = utils.getSchoolYear(activeUserContextService.context);
            this.schoolCode = context.orientation.schoolCode;
            this.schoolName = context.orientation.schoolName;
            this.districtName = context.orientation.districtName;
            this.schoolOrDistrictName = this.schoolName?this.schoolName:this.districtName;
            this.roleName = utils.mapRoleNameToFriendlyName(context.orientation.roleName);
            this.evaluationType = context.workArea().evaluationType;
            this.evaluateeTermUC = activeUserContextService.getEvaluateeTermUpperCase();
            this.evaluateeTermLC = activeUserContextService.getEvaluateeTermLowerCase();
            this.enums = enums;
            this.welcomeMessage = 'Welcome ' + user.displayName + '!';
            this.frameworkIsActive = false;

            if (this.workAreaTag === 'DA_TR') {
                assignmentsService.getDTEAssignmentRequestsForDistrict().then((requests) => {
                    this.pendingDTEAssignmentRequestCount =
                        _.filter(requests, {evalRequestStatus: enums.EvalAssignmentRequestStatus.PENDING}).length;
                    });
            }
            else if (this.workAreaTag === 'DTE') {
                assignmentsService.getDTEAssignmentRequestsForDTE(activeUserContextService.user).then((requests) => {
                    this.pendingDTEAssignmentRequestCount =
                        _.filter(requests, {evalRequestStatus: enums.EvalAssignmentRequestStatus.PENDING}).length;
                });
            }

            assignmentsService.getDistrictAssignmentsSummary().then((summaries) => {

                this.unassignedCount = 0;
                this.delegatedAssignmentsCount = 0;
                this.schoolCount = 0;
                for (var i=0; i< summaries.length; ++i) {
                    this.schoolUnassignedCount[summaries[i].schoolCode] = summaries[i].unassignedCount;
                    this.unassignedCount+=summaries[i].unassignedCount;
                    this.schoolCount++;
                    if (summaries[i].delegated) {
                        this.delegatedAssignmentsCount++;
                    }
                }
            });

            frameworkService.getFrameworkContextWithoutActiveContext(context.orientation.schoolYear,
                context.orientation.districtCode,
                context.workArea().evaluationType)
                .then((frameworkContext)=> {
                    this.frameworkContext = frameworkContext;
                    if (this.isLoaded()) {
                        this.frameworkIsLoaded = true;
                        this.frameworkIsActive = this.isActive();
                        if (context.orientation.schoolCode) {
                            return assignmentsService.districtDelegatedPrincipalAssignmentsForSchool(
                                activeUserContextService.context.orientation.schoolCode)
                                .then((delegate) => {
                                    this.assignmentDelegated = delegate;
                                });
                        }
                    }
            });
        }

        gotoDTEAssignmentRequests() {
            this.$state.go('dte-request-assignment');
        }
        gotoAssignments() {
            if (this.workAreaTag == 'DA_TR') {
                this.$state.go('assignments-summary', {evaluationtype: this.evaluationType})
            }
            else {
                this.$state.go('assignments-detail', {evaluationtype: this.evaluationType})
            }
        }

        gotoPromptBank() {
            this.$state.go('prompt-bank', {evaluationtype: this.evaluationType})
        }

        gotoSettings() {
            this.$state.go('da-settings', {evaluationtype: this.evaluationType})
        }

        activateFrameworkContext() {
            this.frameworkContext.isActive = true;
            this.frameworkContext.activationDate = new Date();
            this.frameworkService.updateFrameworkContext(this.frameworkContext).then(() => {
                this.frameworkIsActive = true;
            });
        }

        isLoaded() {
            return this.frameworkContext != null;
        }

        isActive() {
            return this.frameworkContext != null && this.frameworkContext.isActive;
        }

        loadDate() {
            if (!this.frameworkContext || !this.frameworkContext.isActive) {
                return '';
            }
            else {
                return this.frameworkContext.loadDateTime;
            }
        }

        selectedFrameworkDisplayName() {
            if (!this.frameworkContext || !this.frameworkContext.isActive) {
                return '';
            }
            else {
                return this.frameworkContext.name;
            }
        }
    }

    angular.module('stateeval.layout')
        .controller('beginYearController', beginYearController);

})();