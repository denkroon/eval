/**
 * topNavBarController - controller
 */
namespace StateEval.Layout {

    export class TopNavBarController {
        frameworks: any[];
        framework: any;
        frameworkId: string;
        state;

        constructor(
            private _: _.LoDashStatic,
            private activeUserContextService: any,
            private help: StateEval.Core.HelpService,
            $rootScope: ng.IRootScopeService,
            evidenceCollectionService
        ) {
            $('body').removeClass('mini-navbar');

            $rootScope.$on('change-framework-context', () => {
                this.setupFrameworkContext();
            });

            $rootScope.$on('change-framework', () => {
                this.setupFrameworkContext();
            });

            this.setupFrameworkContext();
            this.state = evidenceCollectionService.state;
        }

        helpHidden() {
            return !this.help.helpVisible();
        }

        showHelp() {
            this.help.showHelp();
        }

        changeFramework() {
            this.framework = _.findWhere(this.frameworks, {'id': this.frameworkId});
            this.activeUserContextService.setActiveFramework(this.framework);
        }

        toggleSideBar() {
            $('body').toggleClass('mini-navbar');
        }

        private setupFrameworkContext() {
            if (this.activeUserContextService.getActiveFramework() != null) {
                var frameworkContext = this.activeUserContextService.getFrameworkContext();
                this.frameworks = frameworkContext.frameworks;
                this.framework = this.activeUserContextService.getActiveFramework();
                this.frameworkId = this.framework.id;
            }
        }
    }

    angular
        .module('stateeval.layout')
        .controller('topNavBarController', TopNavBarController);
}