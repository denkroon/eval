/**
 * evaluatingUserProfileController - controller
 */
(function() {
    angular
        .module('stateeval.layout')
        .controller('evaluatingUserProfileController', evaluatingUserProfileController);

    evaluatingUserProfileController.$inject = ['activeUserContextService', 'utils'];

    /* @ngInject */

    function evaluatingUserProfileController(activeUserContextService, utils) {
        var vm = this;
        vm.context = activeUserContextService.context;
        vm.evaluatee = null;
        vm.evaluator = null;
        vm.isEvaluating =  vm.context.isEvaluating();
        vm.isImpersonating = vm.context.isImpersonating();

        if (vm.isEvaluating) {
            vm.evaluatee = vm.context.evaluatee;
            vm.planType = utils.mapEvaluationPlanTypeForUserToString(activeUserContextService, vm.evaluatee, false);
        }
        if (vm.isImpersonating) {
            vm.evaluator = vm.context.impersonateConfig.impersonatee;
        }

        activate();

        function activate() {
        }
    }
})();

