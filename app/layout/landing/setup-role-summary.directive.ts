/**
 * Created by anne on 4/30/2016.
 */
/**
 * Created by anne on 4/25/2016.
 */
(function() {
    'use strict';

    function setupRoleSummaryDirective() {
        return {
            scope: {
                workAreaSetupStatuses: '=',
                evaluationType: '='
            },
            templateUrl: 'app/layout/landing/setup-role-summary.html',
            controller: 'setupRoleSummaryController as vm',
            bindToController: true
        }
    }

    class setupRoleSummaryController {

        evaluationType: number;
        workAreaSetupStatuses: [any];
        workAreaSetupStatusesByEvalType: [any];

        constructor (
            public enums:any,
            private startupService: any
        )
        {
            this.workAreaSetupStatusesByEvalType = _.filter(this.workAreaSetupStatuses, {evaluationType: this.evaluationType});
        }

        gotoWorkArea(workAreaSetupStatus) {
            switch (workAreaSetupStatus.roleName) {
                case this.enums.Roles.SEDistrictAdmin:
                    return this.gotoDAWorkArea(workAreaSetupStatus);
                case this.enums.Roles.SEDistrictViewer:
                    return this.gotoDVWorkArea(workAreaSetupStatus);
                case this.enums.Roles.SEDistrictEvaluator:
                    return this.gotoDEWorkArea(workAreaSetupStatus);
                case this.enums.Roles.SEDistrictAssignmentManager:
                    return this.gotoDAMWorkArea(workAreaSetupStatus);
                case this.enums.Roles.SEDistrictWideTeacherEvaluator:
                    return this.gotoDTEWorkArea(workAreaSetupStatus);
                case this.enums.Roles.SESchoolAdmin:
                    return this.gotoSAWorkArea(workAreaSetupStatus);
                case this.enums.Roles.SESchoolHeadPrincipal:
                    return this.gotoPRHWorkArea(workAreaSetupStatus);
                case this.enums.Roles.SESchoolPrincipal:
                    return this.gotoPRWorkArea(workAreaSetupStatus);
                case this.enums.Roles.SESchoolTeacher:
                    return this.gotoTRWorkArea(workAreaSetupStatus);
            }
        }

        gotoDVWorkArea(workAreaSetupStatus) {
            this.startupService.setupContext('DV')
        }

        gotoDTEWorkArea(workAreaSetupStatus) {
            this.startupService.setupContext('DTE_TR')
        }

        gotoDEWorkArea(workAreaSetupStatus) {
            this.startupService.setupContext('DE_PR')
        }

        gotoDAMWorkArea(workAreaSetupStatus) {
            var tag;
            switch(workAreaSetupStatus.evaluationType) {
                case this.enums.EvaluationType.PRINCIPAL:
                    tag = 'DAM_PR';
                    break;
                case this.enums.EvaluationType.TEACHER:
                    tag = 'DAM_TR';
                    break;
            }
            this.startupService.setupContext(tag)
        }

        gotoDAWorkArea(workAreaSetupStatus) {
            var tag;
            switch(workAreaSetupStatus.evaluationType) {
                case this.enums.EvaluationType.PRINCIPAL:
                    tag = 'DA_PR';
                    break;
                case this.enums.EvaluationType.TEACHER:
                    tag = 'DA_TR';
                    break;
            }
            this.startupService.setupContext(tag)
        }

        gotoSAWorkArea(workAreaSetupStatus) {
            var tag;
            switch(workAreaSetupStatus.evaluationType) {
                case this.enums.EvaluationType.PRINCIPAL:
                    tag = 'SA_PR';
                    break;
                case this.enums.EvaluationType.TEACHER:
                    tag = 'SA_TR';
                    break;
            }
            this.startupService.setupContext(tag)
        }

        gotoPRHWorkArea(workAreaSetupStatus) {
            this.startupService.setupContext('PR_PR')
        }

        gotoPRWorkArea(workAreaSetupStatus) {
            var tag;
            switch(workAreaSetupStatus.evaluationType) {
                case this.enums.EvaluationType.PRINCIPAL:
                    tag = 'PR_ME';
                    break;
                case this.enums.EvaluationType.TEACHER:
                    tag = 'PR_TR';
                    break;
            }
            this.startupService.setupContext(tag)
        }

        gotoTRWorkArea(workAreaSetupStatus) {
            this.startupService.setupContext('TR_ME')
        }
    }

    angular.module('stateeval.layout')
        .directive('setupRoleSummary', setupRoleSummaryDirective)
        .controller('setupRoleSummaryController', setupRoleSummaryController);

})();
