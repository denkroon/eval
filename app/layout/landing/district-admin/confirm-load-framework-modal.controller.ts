/**
 * Created by anne on 4/26/2016.
 */
(function() {
    'use strict';

    class confirmLoadFrameworkModalController {

        selectedFrameworkContext: any;
        evaluationTerm: string;

         constructor(
            public setupStatus: any,
            private $uibModalInstance: any,
            private enums: any
        )
        {
            this.evaluationTerm = this.enums.EvaluationTypes[setupStatus.evaluationType];
        }

        cancel() {
            this.$uibModalInstance.dismiss('cancel');
        }

        ok() {
            this.$uibModalInstance.close({ selectedFrameworkContext: this.selectedFrameworkContext });
        }
    }

    angular.module('stateeval.layout')
    .controller('confirmLoadFrameworkModalController', confirmLoadFrameworkModalController);

})();
