/// <reference path="../layout.module.ts" />
(function () {
    'use strict';

    class landingController {

        evalLogo:string;
        isDA: boolean;
        schoolYearDisplayString: string;
        welcomeMessage: string;

        constructor(private utils:any,
                    private enums: any,
                    private activeUserContextService: any,
                    private config: any
        ) {

            this.welcomeMessage = 'Welcome ' + this.activeUserContextService.user.displayName + '!';
            this.isDA = !!_.find(this.activeUserContextService.user.locationRoles, {'roleName': this.enums.Roles.SEDistrictAdmin});

            this.schoolYearDisplayString = (config.schoolYear-1) + '-' + config.schoolYear;
            this.evalLogo = utils.getEvalLogo();
        }
    }

    angular
        .module('stateeval.layout')
        .controller('landingController', landingController);
})();


