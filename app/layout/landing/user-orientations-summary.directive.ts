/**
 * Created by anne on 4/30/2016.
 */
/**
 * Created by anne on 4/25/2016.
 */
(function() {
    'use strict';

    function userOrientationsSummaryDirective() {
        return {
            scope: {
            },
            templateUrl: 'app/layout/landing/user-orientations-summary.html',
            controller: 'userOrientationsSummaryController as vm',
            bindToController: true
        }
    }

    class userOrientationsSummaryController {

        evalLogo:string;
        isDA: boolean;
        user: any;
        evalTypes: [any];
        districts: [any];
        schoolYearDisplayString: string;
        welcomeMessage: string;

        constructor(private utils:any,
                    private enums: any,
                    private activeUserContextService: any,
                    private config: any,
                    private workAreaService: any,
                    private frameworkService: any,
                    private startupService: any,
                    private $uibModal: any,
                    private userService: any
        ) {

            this.welcomeMessage = 'Welcome ' + this.activeUserContextService.user.displayName + '!';
            this.isDA = !!_.find(this.activeUserContextService.user.locationRoles, {'roleName': this.enums.Roles.SEDistrictAdmin});

            this.schoolYearDisplayString = (config.schoolYear-1) + '-' + config.schoolYear;
            this.evalLogo = utils.getEvalLogo();

            this.districts =
                _.unique(
                    _.map(this.activeUserContextService.user.locationRoles, (locationRole) => {
                        return {
                            districtCode: locationRole.districtCode,
                            districtName: locationRole.districtName,
                        };
                    }), (district) => {
                        return district.districtCode;
                    });

            this.districts = _.map(this.districts, (district) => {
                return {
                    districtCode: district.districtCode,
                    districtName: district.districtName,
                    setupComplete: true,
                    setupStatuses: [],
                    workAreaSetupStatuses: [],
                    isADA: _.find(this.activeUserContextService.user.locationRoles, (locationRole) => {
                        return (locationRole.districtCode === district.districtCode && locationRole.roleName === this.enums.Roles.SEDistrictAdmin);
                    })
                }
            });

            frameworkService.getPrototypeFrameworkContexts(config.schoolYear).then((prototypeFrameworks) => {
                this.evalTypes = _.unique(_.pluck(prototypeFrameworks, 'evaluationType'));
                this.evalTypes = _.reject(this.evalTypes, (evalType) => {
                    return evalType === this.enums.EvaluationType.LIBRARIAN ||
                        !this.includeEvalTypeForUser(evalType);
                });

                for (var d = 0; d < this.districts.length; ++d) {
                    var district = this.districts[d];
                    this.processDistrict(district, prototypeFrameworks);
                }
            });
        }

        includeEvalTypeForUser(evalType) {
            for (var locationRole in this.activeUserContextService.user.locationRoles) {
                var workAreas = this.workAreaService.getWorkAreasForRole(this.activeUserContextService.user.locationRoles[locationRole].roleName, evalType);
                if (workAreas.length>0) {
                    return true;
                }
            }

            return false;
        }

        processDistrict(district, prototypeFrameworks) {
            var districtRoles = _.filter(this.activeUserContextService.user.locationRoles, {districtCode: district.districtCode});

            this.frameworkService.getLoadedFrameworkContexts(this.config.schoolYear, district.districtCode)
                .then((loadedFrameworkContexts)=> {
                    for (var j = 0; j < this.evalTypes.length; ++j) {
                        var evaluationType = this.evalTypes[j];
                        var loadedContext = _.find(loadedFrameworkContexts, {evaluationType: evaluationType});

                        this.createDistrictSetupStatus(prototypeFrameworks, loadedContext, evaluationType, district);
                        for (var r = 0; r < districtRoles.length; ++r) {
                            if (loadedContext) {
                                this.createWorkAreaSetupStatus(prototypeFrameworks, loadedContext, evaluationType, districtRoles[r], district);
                            }
                        }
                    }
                });
        }

        gotoDAWorkArea(districtSetupStatus) {
            var tag = this.workAreaService.getSingleTagOfRole(this.enums.Roles.SEDistrictAdmin, districtSetupStatus.evaluationType);
            this.startupService.setupContext(tag)
        }

        createDistrictSetupStatus(prototypeFrameworks, loadedContext, evaluationType, district) {

            this.frameworkService.getFrameworkContextOptOut(
                this.config.schoolYear, district.districtCode, evaluationType).then((optOut) => {

                var workArea = this.workAreaService.getSingleWorkAreaForRole(
                    this.enums.Roles.SEDistrictAdmin, evaluationType);

                var districtSetupStatus = {
                    districtCode: district.districtCode,
                    evaluationTypeDisplayName: this.enums.EvaluationTypes[evaluationType] +'s',
                    evaluationType: evaluationType,
                    workAreaTitle: workArea.title,
                    loaded: !!loadedContext,
                    optOut: optOut,
                    active: !!loadedContext && loadedContext.isActive,
                    loadedContext: loadedContext,
                    protoTypeFrameworks: _.filter(prototypeFrameworks, {evaluationType: evaluationType})
                };

                if (!districtSetupStatus.active) {
                    district.setupComplete = false;
                }
                district.setupStatuses.push(districtSetupStatus);
            });
        }

        changeOptOut(setupStatus) {
            this.frameworkService.updateFrameworkContextOptOut(
                this.config.schoolYear, setupStatus.districtCode, setupStatus.evaluationType, setupStatus.optOut);
        }

        isWorkOnMyEvalWorkArea(tag) {
            return tag === 'PR_ME' || tag === 'TR_ME';
        }

        createWorkAreaSetupStatus(prototypeFrameworks, loadedContext, evaluationType, locationRole, district) {

            var workArea = this.workAreaService.getSingleWorkAreaForRole(locationRole.roleName, evaluationType);
            if (workArea) {

                var workOnMyEvalWorkArea = this.isWorkOnMyEvalWorkArea(workArea.tag);
                if (workOnMyEvalWorkArea) {

                    var alreadyAdded = _.find(district.workAreaSetupStatuses, (setupStatus) => {
                        return setupStatus.districtCode === locationRole.districtCode &&
                            setupStatus.workAreaTitle === workArea.title;
                    });

                    if (alreadyAdded) {
                        return;
                    }
                }
                var workAreaSetupStatus = {
                    districtCode: locationRole.districtCode,
                    districtName: locationRole.districtName,
                    schoolName: workOnMyEvalWorkArea?'':locationRole.schoolName,
                    schoolCode: workOnMyEvalWorkArea?'':locationRole.schoolCode,
                    roleName: locationRole.roleName,
                    roleDisplayName: this.utils.mapRoleNameToFriendlyName(locationRole.roleName),
                    evaluationType: evaluationType,
                    workAreaTitle: workArea.title,
                    loaded: !!loadedContext,
                    active: !!loadedContext && loadedContext.isActive,
                    loadedContext: loadedContext,
                    protoTypeFrameworks: _.filter(prototypeFrameworks, {evaluationType: evaluationType})
                };

                district.workAreaSetupStatuses.push(workAreaSetupStatus);
            }
        }


        loadFramework(setupStatus) {
            var modalInstance = this.$uibModal.open({
                animation: false,
                templateUrl: 'app/layout/landing/district-admin/_confirm-load-framework.html',
                controller: 'confirmLoadFrameworkModalController as vm',
                resolve: {
                    setupStatus: setupStatus
                }
            });

            modalInstance.result.then((result) => {
                this.frameworkService.loadFrameworkContext(result.selectedFrameworkContext.id,
                    setupStatus.districtCode).then(()=>{
                    setupStatus.loadedContext = result.selectedFrameworkContext;
                    setupStatus.loaded = true;

                    var user = this.activeUserContextService.user;
                    // need to reload user orientations
                    this.userService.getOrientationsForUser(user.id).then((orientations) => {
                        this.activeUserContextService.user.userOrientations = orientations;
                        this.activeUserContextService.save();
                    })
                });
            });
        }
    }

    angular.module('stateeval.layout')
        .directive('userOrientationsSummary', userOrientationsSummaryDirective)
        .controller('userOrientationsSummaryController', userOrientationsSummaryController);

})();
