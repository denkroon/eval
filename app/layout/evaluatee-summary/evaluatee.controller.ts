(function() {
    'use strict';

    class evaluateeController {

        context: any;
        evaluateeTerm: string;
        observationMinutes: number = 0;
        totalAvailableEvidenceCount: number = 0;
        totalUsedEvidenceCount: number = 0;
        totalTeeAvailableEvidenceCount: number = 0;
        totalTeeUsedEvidenceCount: number = 0;
        summativeScore: string;
        evaluateePlanType:string;

        constructor(
            private activeUserContextService: any,
            private evaluatee: any,
            private rubricUtils: any,
            private evidenceCollection: any,
            private utils: any,
            private _,
            private enums: any,
            private $anchorScroll: any,
            private observationService: any,
            public evidenceCollectionService: any) {
            this.context = activeUserContextService.context;
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();

            this.evaluateePlanType = (this.evaluatee.evalData.planType === enums.EvaluationPlanType.UNDEFINED)?
                '<span class="label label-warning"> NOT SET</span>' :
                utils.mapEvaluationPlanTypeForUserToString(activeUserContextService, evaluatee, false);

            this.summativeScore = rubricUtils.mapPerformanceLevelToDisplayString(activeUserContextService.context.evaluatee.evalData.performanceLevel);

            this.totalAvailableEvidenceCount= this.evidenceCollection.availableEvidence.length;
            var used = _.filter(this.evidenceCollection.availableEvidence, {inUse: true});
            if (used) {
                this.totalUsedEvidenceCount = used.length;
            }

            var teeEvidence = _.filter(this.evidenceCollection.availableEvidence, {userId: activeUserContextService.context.evaluatee.id});
            if (teeEvidence) {
                this.totalTeeAvailableEvidenceCount = teeEvidence.length;
            }
            teeEvidence = _.filter(teeEvidence, {inUse: true});
            if (teeEvidence) {
                this.totalTeeUsedEvidenceCount= teeEvidence.length;
            }

            var observations =
                this.evidenceCollection.associatedCollections[this.enums.AssociatedCollectionsAccessor[enums.EvidenceCollectionType.OBSERVATION]];
            for (var i in observations) {
                observationService.setObservationStatusDisplayString(observations[i]);
                if (observations[i].observeDuration) {
                    this.observationMinutes+= observations[i].observeDuration;
                }
            }
        }

        scrollTo (id) {
            this.$anchorScroll(id);
        }
    }

    angular.module('stateeval.layout')
        .controller('evaluateeController', evaluateeController);
}) ();
