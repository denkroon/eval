/**
 * Created by anne on 3/29/2016.
 */

/**
 * Created by anne on 3/24/2016.
 */
/// <reference path="../layout.module.ts" />

(function () {
    'use strict';

    function activitiesSummaryDirective() {
        return {
            restrict: 'E',
            scope: {
                evidenceCollection: '='
            },
            templateUrl: 'app/layout/evaluatee-summary/activities-summary.directive.html',
            controller: 'activitiesSummaryController as vm',
            bindToController: true
        }
    }

    class activitiesSummaryController {
        isAssignedEvaluator: boolean;
        isEvaluatee: boolean;
        isShow: boolean;
        isLimit: boolean;
        notifications: [any];

        constructor(
            private notificationService: any,
            private activeUserContextService: any,
            private $state: any,
            private enums: any,
            private startupService: any,
            private utils: any,
            private _: _.LoDashStatic
        ) {

            this.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();

            var curdate = moment();
            var twoweeks = curdate.subtract(14, 'days').utc();
            this.isShow = false;
            this.isLimit = false;

            const self = this;
            notificationService.getNotificationsForEvaluation()
                .then((notifications) => {


                    _.each(notifications, function(item, index) {

                        var created = moment(item.creationDateTime).utc();
                        if (index <= 9) {
                            item.twoweeks = true;
                        } else {
                            item.twoweeks = false;
                            self.isLimit = true;
                        }
                    });

                    this.notifications = notifications;
                });
        }

        toggle () {
            this.isShow = !this.isShow;
        }

        gotoEvidenceCollection(event) {
            this.utils.gotoEvidenceCollection(event, this.activeUserContextService, this.startupService, this.$state);
        }
    }
    angular
        .module('stateeval.layout')
        .controller('activitiesSummaryController', activitiesSummaryController)
        .directive('activitiesSummaryDirective', activitiesSummaryDirective)
})();



