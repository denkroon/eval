/**
 * Created by anne on 3/29/2016.
 */

/**
 * Created by anne on 3/24/2016.
 */
/// <reference path="../layout.module.ts" />

(function () {
    'use strict';

    function evidenceCollectionsOfTypeSummaryDirective() {
        return {
            restrict: 'E',
            scope: {
                // YTD collection
                evidenceCollection: '=',
                collectionType: '='
            },
            templateUrl: 'app/layout/evaluatee-summary/evidence-collections-of-type-summary.directive.html',
            controller: 'evidenceCollectionsOfTypeSummaryController as vm',
            bindToController: true
        }
    }

    class evidenceCollectionsOfTypeSummaryController {
        evidenceCollection:any;
        collectionType: any;
        collections: any;
        isAssignedEvaluator: boolean;
        isEvaluatee: boolean;
        rubricRowVisitedByRowId: [any];
        rubricRowModifiedByRowId: [any];
        framework: any;
        evidence: [any];
        coverage: [any];

        constructor(
            private evidenceCollectionService: any,
            private activeUserContextService: any,
            private observationService: any,
            private $state: any,
            private enums: any,
            private _: _.LoDashStatic
        ) {

            this.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();
            this.rubricRowVisitedByRowId = [];
            this.rubricRowModifiedByRowId = [];
            this.collections = [];
            this.collapse = [];
            this.coverage = [];

            var collectionsOfType = this.evidenceCollection.associatedCollections[this.enums.AssociatedCollectionsAccessor[this.collectionType]];
            for (var i in collectionsOfType) {
                this.collections.push(collectionsOfType[i]);
            }
          // this.collections = this.evidenceCollection.associatedCollections[this.enums.AssociatedCollectionsAccessor[this.collectionType]];
            this.framework = activeUserContextService.getActiveFramework();
            this.evidence = [];

            evidenceCollectionService.getRubricRowLastVisits()
                .then((lastVisits) => {
                    this.rubricRowVisitedByRowId = _.groupBy(lastVisits, 'rubricRowId');
                    return evidenceCollectionService.getRubricRowLastModifiedDates();
                }).then((modificationDates) =>  {
                    this.rubricRowModifiedByRowId = _.groupBy(modificationDates, 'rubricRowId');
                    this.processCollections();
            });
        }

        processCollections() {
            for(var i in this.collections) {

                this.collapse[i] = false;
                var collection = this.collections[i];
                collection.changed = false;

                if (this.collectionType === this.enums.EvidenceCollectionType.OBSERVATION) {
                    if (collection.wfState !== this.enums.WfState.OBS_LOCKED_SEALED) {
                        this.collapse[i] = true;
                    }
                }

                this.framework.frameworkNodes.forEach((node) => {
                    var nodeHasEvidence = false;
                    if (!node.rowsWithEvidenceCount) {
                        node.rowsWithEvidenceCount = [];
                        node.firstRowWithEvidence = [];
                    }
                    node.rowsWithEvidenceCount[collection.id] = 0;
                    node.rubricRows.forEach((row) => {
                        var rowHasEvidence = this.getDataForCollectionRubricComponent(collection, row.id);
                        if (rowHasEvidence) {
                            node.rowsWithEvidenceCount[collection.id]++;
                            if (!node.firstRowWithEvidence[collection.id]) {
                                node.firstRowWithEvidence[collection.id] = row;
                            }
                            nodeHasEvidence = true;
                        }
                    });

                    if (nodeHasEvidence) {
                        if (this.coverage[collection.id]) {
                            this.coverage[collection.id]+= (', ' + node.shortName);
                        }
                        else {
                            this.coverage[collection.id] = node.shortName;
                        }
                    }
                });
            }
        }

        gotoECNode(collection, node) {

            if (this.collectionType === this.enums.EvidenceCollectionType.OBSERVATION) {
               this.$state.go('observation-align', {collectionId:collection.id, nodeShortName:node.shortName});
            }
            else if (this.collectionType === this.enums.EvidenceCollectionType.SELF_ASSESSMENT) {
                this.$state.go('self-assessment-align', {collectionId:collection.id, nodeShortName:node.shortName});
            }
            else if (this.collectionType === this.enums.EvidenceCollectionType.OTHER_EVIDENCE) {
                this.$state.go('ytd-evidence', {collectionId:collection.id, nodeShortName:node.shortName});
            }
        }

        gotoECRow(collection, node, row) {
            if (this.collectionType === this.enums.EvidenceCollectionType.OBSERVATION) {
                this.$state.go('observation-align', {collectionId:collection.id, nodeShortName:node.shortName, rowId: row.id});
            }
            else if (this.collectionType === this.enums.EvidenceCollectionType.SELF_ASSESSMENT) {
                this.$state.go('self-assessment-align', {collectionId:collection.id, nodeShortName:node.shortName, rowId: row.id});
            }
            else if (this.collectionType === this.enums.EvidenceCollectionType.OTHER_EVIDENCE) {
                this.$state.go('ytd-evidence', {collectionId:collection.id, nodeShortName:node.shortName, rowId: row.id});
            }
        }

        gotoCollection(collection) {
            if (this.collectionType === this.enums.EvidenceCollectionType.OBSERVATION) {
                this.observationService.gotoObservation(this.activeUserContextService, collection);
            }
            else if (this.collectionType === this.enums.EvidenceCollectionType.SELF_ASSESSMENT) {
                this.$state.go('self-assessment-align', {id: collection.id});
            }
            else if (this.collectionType === this.enums.EvidenceCollectionType.OTHER_EVIDENCE) {
                this.$state.go('ytd-evidence');
            }
        }

        getDataForCollectionRubricComponent(collection,rubricRowId) {
            var list = this._.filter(this.evidenceCollection.availableEvidence, (ae) =>{
                return ((ae.evidenceCollectionType === this.collectionType) &&
                ae.evidenceCollectionObjectId === collection.id &&
                ae.rubricRowId == rubricRowId);
            });

            var key = collection.id+'-'+rubricRowId;
            var score = _.find(this.evidenceCollection.getRowById(rubricRowId).scores, (score) => {
                return score.evidenceCollectionType === this.collectionType &&
                        score.evidenceCollectionObjectId === collection.id &&
                        score.rubricRowId === rubricRowId;
            });

            if (score && score.performanceLevel) {
                score = this.enums.PerformanceLevels[score.performanceLevel - 1].substring(0, 3);
            }
            else {
                score = '-';
            }

            this.evidence[key] = {
                avail: list.length,
                used: this._.filter(list, {inUse: true}).length,
                changed:  this.evidenceCollectionService.checkForEvidenceChange(this.rubricRowModifiedByRowId,
                    this.rubricRowVisitedByRowId, collection,
                    this.collectionType, rubricRowId),
                score:score
            };

            if (this.evidence[key].changed) {
                collection.changed = true;
                if (this.collectionType === this.enums.EvidenceCollectionType.OBSERVATION) {
                    this.evidenceCollectionService.state.observationEvidenceChanged = true;
                }
                else if (this.collectionType === this.enums.EvidenceCollectionType.SELF_ASSESSMENT) {
                    this.evidenceCollectionService.state.selfAssessmentEvidenceChanged = true;
                }
                else if (this.collectionType === this.enums.EvidenceCollectionType.OTHER_EVIDENCE) {
                    this.evidenceCollectionService.state.otherEvidenceChanged = true;
                }
            }

            return list.length>0;
        }

        toggle(index) {
            this.collapse[index] = !this.collapse[index];
        }
    }
    angular
        .module('stateeval.layout')
        .controller('evidenceCollectionsOfTypeSummaryController', evidenceCollectionsOfTypeSummaryController)
        .directive('evidenceCollectionsOfTypeSummaryDirective', evidenceCollectionsOfTypeSummaryDirective)
})();



