(function() {
    'use strict';

    angular.module('stateeval.layout')
        .controller('evaluateeListController', evaluateeListController);

    evaluateeListController.$inject = ['activeUserContextService','utils', '_', 'enums',
        'evidenceCollectionService', 'observationService', 'workAreaService', '$uibModal'];

    function evaluateeListController(activeUserContextService, utils,  _, enums,
        evidenceCollectionService, observationService, workAreaService, $uibModal) {
        var vm = this;
        vm.enums = enums;

        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
        vm.evaluatorNames = [];
        vm.evaluateePlanTypes = [];
        vm.otherEvidence = [];
        vm.observations = [];
        vm.observationMinutes = [];
        vm.evidenceChanged = [];
        vm.rubricRowLastVisits = [];
        vm.rubricRowModificationDates = [];
        vm.showOpenOnly = false;
        vm.showNotes = false;

        vm.assignedOnly = activeUserContextService.context.assignedEvaluatees?"true":"false";

        var workAreaTag = activeUserContextService.context.orientation.workAreaTag;
        var userIsDTE = (workAreaTag.indexOf('DTE') === 0);

        if (userIsDTE) {
            vm.noEvaluateeTitle = "There are no teachers approved for you to observe at this location.";
            vm.noEvaluateeDescription = "You must create an Assignment Request to observe teachers as a district-wide teacher evaluator. This can be one in the Setup -> Assignments section.";
        }
        else if (activeUserContextService.context.assignedEvaluatees) {
            vm.noEvaluateeTitle = "There are no " + vm.evaluateeTerm + " assigned to you at this location.";
            vm.noEvaluateeDescription = "You can try changing your settings to include un-assigned " + vm.evaluateeTerm + "s";
        }
        else {
            vm.noEvaluateeTitle = "There are no " + vm.evaluateeTerm + " (assigned or unassigned) available to you at this location.";
            vm.noEvaluateeDescription = "Check with your district administrator to see if assignments have been configured yet.";
        }

        vm.changeEvaluatee = changeEvaluatee;
        vm.gotoObservation = gotoObservation;
        vm.changeAssignmentFilter = changeAssignmentFilter;
        vm.newObservation = newObservation;
        vm.changeShowOpenOnly = changeShowOpenOnly;
        vm.editReminder = editReminder;

        function editReminder(observation) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/layout/evaluatee-summary/edit-observation-reminder.html',
                controller: 'editObservationReminderController as vm',
                windowClass: 'modal-sharing',
                resolve: {
                    observation: observation
                }
            });

            modalInstance.result.then(function (result) {
            });
        }

        function newObservation(evaluatee) {
            observationService.startNewObservation(evaluatee);
        }

        function changeAssignmentFilter() {
            activeUserContextService.context.assignedEvaluatees = (vm.assignedOnly === "true");
            activeUserContextService.save();
            loadEvaluatees();
        }

        ////////////////////////////////////

        activate();

        function activate() {

            evidenceCollectionService.getRubricRowLastVisits()
            .then(function(lastVisits) {
                vm.rubricRowLastVisits = lastVisits;
                return evidenceCollectionService.getRubricRowLastModifiedDates();
            }).then(function(modificationDates) {
                vm.rubricRowModificationDates = modificationDates;
                return observationService.getObservationsForTeeTor();
            }).then(function(observations) {
                vm.observationsByEvaluation = _.groupBy(observations, 'evaluationId');
                loadEvaluatees();
                processChange();
                processObservations();
            });
        }

        function changeShowOpenOnly() {
            processObservations();
        }

        function processChange() {
            vm.evaluatees.forEach(function(evaluatee) {
                // should come back in date sorted order, most recent first
                var rrLastVisits = _.groupBy(vm.rubricRowLastVisits, 'evaluationId');
                var rrLastModified = _.groupBy(vm.rubricRowModificationDates, 'evaluationId');


                var rubricRowModified = rrLastModified[evaluatee.evalData.id] && rrLastModified[evaluatee.evalData.id].length>0;
                var rubricRowVisited = rrLastVisits[evaluatee.evalData.id] && rrLastVisits[evaluatee.evalData.id].length>0;

                if (rubricRowModified) {
                    if (rubricRowVisited) {
                        // go through each rubric row modification and see if the rubric row has been
                        // visited since
                        rrLastModified[evaluatee.evalData.id].forEach(function(nextLastModified) {

                            var rrLastVisit = _.find(rrLastVisits[evaluatee.evalData.id], function(nextLastVisit) {
                                return nextLastVisit.evidenceCollectionType === nextLastModified.evidenceCollectionType &&
                                        nextLastVisit.evidenceCollectionObjectId === nextLastModified.evidenceCollectionObjectId &&
                                        nextLastVisit.rubricRowId === nextLastModified.rubricRowId;
                            });

                            if (!rrLastVisit) {
                                // something new, not visited yet
                                vm.evidenceChanged[evaluatee.evalData.id] = true;
                            }
                            else {
                                if (rrLastVisit.lastVisitDateTime < nextLastModified.lastModifiedDateTime) {
                                    // something new, not visited yet
                                    vm.evidenceChanged[evaluatee.evalData.id] = true;
                                }
                            }
                        });
                    } else {
                        // something new, not visited yet
                        vm.evidenceChanged[evaluatee.evalData.id] = true;
                    }
                }
            })
        }

        function loadEvaluatees() {
            var evaluatees = activeUserContextService.context.evaluatees;
            if (vm.assignedOnly === "true") {
                evaluatees = _.reject(evaluatees, function(evaluatee) {
                    return evaluatee.evalData.evaluatorId != activeUserContextService.user.id;
                })
            }
            evaluatees.forEach(function(evaluatee) {

                vm.observationMinutes[evaluatee.evalData.id] = 0;
                vm.otherEvidence[evaluatee.evalData.id] = {
                    artifactCount: 0,
                    otherCount: 0
                };

                vm.evidenceChanged[evaluatee.evalData.id] = false;

                vm.evaluatorNames[evaluatee.id] = getAssignedEvaluatorDisplayName(evaluatee);

                vm.evaluateePlanTypes[evaluatee.id] = (evaluatee.evalData.planType === enums.EvaluationPlanType.UNDEFINED)?
                    '<span class="label label-warning"> NOT SET</span>' :
                    utils.mapEvaluationPlanTypeForUserToString(activeUserContextService, evaluatee, true);
            });

            vm.evaluatees = evaluatees;
        }

        function processObservations() {
            for (var evaluationId in vm.observationsByEvaluation)  {
                vm.observations[parseInt(evaluationId)] = vm.observationsByEvaluation[evaluationId];

                // need to count minutes before filtering
                vm.observations[parseInt(evaluationId)].forEach(function(observation) {
                    observationService.setObservationStatusDisplayString(observation);
                    if (observation.wfState >= enums.WfState.OBS_LOCKED_SEALED && observation.observeDuration) {
                        vm.observationMinutes[observation.evaluationId]+=observation.observeDuration;
                    }
                });

                if (vm.showOpenOnly) {
                    vm.observations[evaluationId] = _.reject(vm.observations[evaluationId], {wfState: enums.WfState.OBS_LOCKED_SEALED});
                }
            }
        }

        function changeEvaluatee(evaluatee) {
            workAreaService.changeEvaluatee(evaluatee, activeUserContextService)
        }

        function gotoObservation(observation) {
            activeUserContextService.context.evaluatee = _.find(activeUserContextService.context.evaluatees, {id: observation.evaluateeId});
            activeUserContextService.save();

            observationService.gotoObservation(activeUserContextService, observation);
        }

        function getAssignedEvaluatorDisplayName(evaluatee) {
            if (evaluatee.evalData.evaluatorDisplayName === "") {
                return '<span class="label label-warning">NOT SET</span>';
            }
            else {
                return evaluatee.evalData.evaluatorDisplayName;
            }
        }
    }
}) ();
