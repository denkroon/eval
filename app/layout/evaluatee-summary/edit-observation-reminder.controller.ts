(function() {
    'use strict';

    class editObservationReminderController {

        evaluatee: any;

        constructor(
            public observation:any,
            private activeUserContextService: any,
            private observationService: any,
            private $uibModalInstance: any
        ) {

            this.evaluatee = activeUserContextService.context.evaluatee;
        }

        saveNote() {
            this.observationService.saveObserveNotes(this.observation.id, this.observation.reminderNote, 'reminderNote').then(() => {
                this.$uibModalInstance.close({});
            });
        }

        removeNote() {
            this.observation.reminderNote = null;
            this.observationService.saveObserveNotes(this.observation.id, this.observation.reminderNote, 'reminderNote').then(() => {
                this.$uibModalInstance.close({});
            });
        }
    }

    angular.module('stateeval.layout')
    .controller('editObservationReminderController', editObservationReminderController);

})();
