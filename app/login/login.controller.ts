/// <reference path="login.module.ts" />

(function() {

    class loginController {

        districts: [any];
        district: string;
        username: string;
        password: string;
        users: [any];
        user: any;

        constructor(
            private authenticationService: any,
            private activeUserContextService: any,
            private locationService: any,
            private _: _.LoDashStatic,
            private userService: any,
            private startupService: any,
            private $state: any,
            private signalRService: any
        )
        {
            this.password = "";
            authenticationService.clearCredentials();
            activeUserContextService.clear();

            locationService.getDistricts().then((districts) =>
            {
                this.districts = districts;
                this.district = _.find(this.districts, {districtCode: '34003'});
                this.changeDistrict();
            });
        }

        changeDistrict() {

            this.userService.getUsersInDistrict(this.district.districtCode).then((users)=> {
                this.users = users;
                this.user = _.find(this.users, (u) => {
                    return u.userName.match(/PR1/);
                })
            });
        }

        startUserSession(user) {
            this.signalRService.initialize(user.id);
            console.log(user.userName + ' logging in: ', user);
            this.authenticationService.setCredentials(user.userName, user.password);

            if (this.checkForSafariPrivateMode()) {
                this.$state.go('private-mode-error');
            }
            else {
                this.startupService.setUser(user);
               //this.$state.go('landing');
               this.startupService.setupContext();
            }
        }

        login() {
            return this.authenticationService.authenticateUser(this.user.userName, this.user.password)
                .then((user) => {
                    this.startUserSession(user);
                });
        }

        // Safari, in Private Browsing Mode, looks like it supports localStorage but all calls to setItem
        // throw QuotaExceededError. We're going to detect it upfront and send them to an error page.
        checkForSafariPrivateMode() {
            if (typeof localStorage === 'object') {
                try {
                    localStorage.setItem('localStorage', "test");
                    localStorage.removeItem('localStorage');
                    return false;
                } catch (e) {
                    return true;
                }
            }
        }
    }

    angular
        .module('stateeval.login')
        .controller('loginController', loginController);

})();
