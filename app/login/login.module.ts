/// <reference path="../core/core.module.ts" />

/**
 * Created by anne on 6/26/2015.
 */

(function() {
    'use strict';

    angular.module('stateeval.login', ['stateeval.core']);
})();

