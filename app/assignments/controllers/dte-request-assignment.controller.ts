/// <reference path="../assignments.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.assignments')
        .controller('dteRequestAssignmentController', dteRequestAssignmentController);

    dteRequestAssignmentController.$inject = ['assignmentsService', 'userService', 'activeUserContextService', 'enums',
        '$uibModal', 'utils', '$stateParams', 'locationService'];

    /* @ngInject */
    function dteRequestAssignmentController(assignmentsService, userService, activeUserContextService, enums,
                                   $uibModal, utils, $stateParams, locationService) {

        var vm = this;
        vm.districtCode = '';
        vm.teacherSearchInput = '';

        vm.requests = [];
        vm.teachers = [];
        vm.schools = [];
        vm.school = null;
        vm.currentUser = null;

        vm.changeSchool = changeSchool;
        vm.createRequest = createRequest;
        vm.requestStatus = requestStatus;
        vm.requestType = requestType;
        vm.removeRequest = removeRequest;

      ////////////////////////////

        activate();

        function activate() {
            vm.districtCode = activeUserContextService.getActiveDistrictCode();
            vm.currentUser = activeUserContextService.getActiveUser();

            locationService.getSchoolsInDistrict()
                .then(function(schools) {
                    vm.schools = schools;
                    vm.school = schools[0];
                    loadRequests();
                })
        }

        function removeRequest(request) {
            assignmentsService.deleteDTEAssignmentRequest(request).then(function() {
                vm.requests = _.reject(vm.requests, {id: request.id});
                loadTeachers();
            })
        }

        function requestStatus(request) {
           return utils.mapEvalRequestStatusToString(request.evalRequestStatus);
        }

        function requestType(request) {
            if (request.evalRequestStatus === enums.EvalAssignmentRequestStatus.PENDING) {
                return "PENDING";
            }
            else {
                return utils.mapEvalRequestTypeToString(request.evalRequestType);
            }
        }

        function loadRequests() {
            assignmentsService.getDTEAssignmentRequestsForDTE(vm.currentUser).then(function(requests) {
                vm.requests = requests;
                loadTeachers();
            });
        }
        function loadTeachers() {
            vm.teachers = [];
            userService.getUsersInRoleAtSchool(vm.school.schoolCode, enums.Roles.SESchoolTeacher)
                .then(function(teachers) {
                    teachers.forEach(function(teacher) {
                        if (!_.findWhere(vm.requests, {evaluateeId: teacher.id})) {
                            vm.teachers.push(teacher);
                        }
                    })
                })
        }

        function changeSchool() {
           loadTeachers();
        }

        function createRequest(teacher) {
            assignmentsService.createDTEAssignmentRequest(vm.currentUser, teacher).then(function() {
                loadRequests();
            })
        }
    }

})();

