/// <reference path="../assignments.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.assignments')
        .controller('selectFocusModalController', selectFocusModalController);

    selectFocusModalController.$inject = ['$uibModalInstance', 'enums', 'userService',
        'activeUserContextService', 'evaluatee'];

    /* @ngInject */
    function selectFocusModalController($uibModalInstance, enums, userService,
        activeUserContextService, evaluatee) {
        var vm = this;

        vm.focusSelectionMissing = false;
        vm.growthSelectionMissing = false;

        vm.evaluatee = evaluatee;
        vm.showStudentGrowthDropdown = false;
        vm.focusFrameworkNode = null;
        vm.sgFocusFrameworkNode = null;

        vm.frameworkNodes = [];
        vm.sgFrameworkNodes = [];

        vm.save = save;
        vm.cancel = cancel;
        vm.changeFocus = changeFocus;


        /////////////////////////////////

        activate();

        function activate() {

            vm.frameworkNodes = activeUserContextService.getFrameworkContext().stateFramework.frameworkNodes;
            vm.sgFrameworkNodes = activeUserContextService.getFrameworkContext().studentGrowthFrameworkNodes;
            if (vm.evaluatee.evalData.evaluationType === enums.EvaluationType.TEACHER) {
                vm.sgFrameworkNodes = _.reject(vm.sgFrameworkNodes, {'shortName': 'C8'});
            }

            vm.focusFrameworkNode = findFrameworkNode(vm.evaluatee.evalData.focusFrameworkNodeId);
            vm.sgFocusFrameworkNode = findSGFrameworkNode(vm.evaluatee.evalData.focusSGFrameworkNodeId);
            setShowStudentGrowthOption();
        }

        function findSGFrameworkNode(id) {
            return _.find(vm.sgFrameworkNodes, {id: id});
        }

        function findFrameworkNode(id) {
            return _.find(vm.frameworkNodes, {id: id});
        }

        function setShowStudentGrowthOption() {

            if (!vm.focusFrameworkNode) {
                vm.showStudentGrowthDropdown=false;
                return;
            }
            var match = findSGFrameworkNode(vm.focusFrameworkNode.id);
            vm.showStudentGrowthDropdown = match === undefined;
        }

        function changeFocus() {
            setShowStudentGrowthOption();
        }

        function buildFocusDisplayString() {
            var displayString = '<label>Focus:</label>';
            displayString+=(vm.focusFrameworkNode.shortName);
            if (vm.sgFocusFrameworkNode != null) {
                displayString+=('&nbsp;&nbsp;<label>Student Growth:</label>');
                displayString+=(vm.sgFocusFrameworkNode.shortName);
            }
            return displayString;
        }

        function save() {
            vm.growthSelectionMissing = false;
            vm.focusSelectionMissing = false;
            if (!vm.focusFrameworkNode) {
                vm.focusSelectionMissing = true;
                return;
            }

            if (_.find(vm.sgFrameworkNodes, {id: vm.focusFrameworkNode.id})) {
                vm.sgFocusFrameworkNode = vm.focusFrameworkNode;
            }

            if (!vm.sgFocusFrameworkNode) {
                vm.growthSelectionMissing = true;
                return;
            }

            $uibModalInstance.close({
                evaluatee: vm.evaluatee,
                focusFrameworkNodeId: vm.focusFrameworkNode.id,
                sgFocusFrameworkNodeId: vm.sgFocusFrameworkNode.id,
                focusDisplayString: buildFocusDisplayString()
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

