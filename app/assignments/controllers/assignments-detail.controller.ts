/// <reference path="../assignments.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.assignments')
        .controller('assignmentsDetailController', assignmentsDetailController);

    assignmentsDetailController.$inject = ['assignmentsModel', 'activeUserContextService', '$state', '$stateParams'];

    /* @ngInject */
    function assignmentsDetailController(assignmentsModel, activeUserContextService, $state, $stateParams) {

        var vm = this;

        vm.fromSummary = $stateParams.fromSummary === "1";

        vm.back = back;

        vm.assignmentsModel = assignmentsModel.model;

        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();

        ////////////////////////////

        activate();

        function activate() {
        }

        function back() {
            $state.go('assignments-summary', {}, {reload:true});
        }
    }

})();
