(function() {
    'use strict';

    class assignmentsSummaryController {

        evaluateeTerm: string;
        summaries: [any];
        totalCount: number = 0;
        unassignedCount: number = 0;
        assignedCount: number = 0;

        constructor(
            private assignmentsService: any,
            private activeUserContextService: any,
            _: _.LoDashStatic,
            private $state: any,
            private toastr: any
        ) {

            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
            assignmentsService.getDistrictAssignmentsSummary().then((summaries) => {
                this.summaries = summaries;
                for (var i=0; i< this.summaries.length; ++i) {
                    this.totalCount+=this.summaries[i].totalCount;
                    this.unassignedCount+=this.summaries[i].unassignedCount;
                }
                this.assignedCount = this.totalCount - this.unassignedCount;
            })

        }

        delegateToAllSchools(delegate) {
            this.assignmentsService.delegatePrincipalAssignmentsForAllSchools(delegate).then(()=> {
                for (var i=0; i< this.summaries.length; ++i) {
                   this.summaries[i].delegated = true;
                }
                this.toastr.info(delegate?  'Assignments have been delegated to all schools.': 'Assignments delegation has been removed from all schools.');
            });
        }

        toggleDelegateForSchool(summary) {
            this.assignmentsService.delegatePrincipalAssignmentsForSchool(summary.schoolCode, !summary.delegated).then(()=>{
                summary.delegated=!summary.delegated;
            })
        }

        viewSchool(schoolCode, schoolName) {
            this.$state.go('assignments-detail', {fromSummary:1, schoolCode:schoolCode, schoolName: schoolName, evaluationtype:2});
        }
    }

    angular.module('stateeval.core')
    .controller('assignmentsSummaryController', assignmentsSummaryController);

})();