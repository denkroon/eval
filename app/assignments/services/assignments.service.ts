/// <reference path="../assignments.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.assignments')
        .factory('assignmentsService', assignmentsService);

    assignmentsService.$inject = ['$http', 'config', 'activeUserContextService', 'enums',
        'studentGrowthGoalsService', 'utils'];
    function assignmentsService($http, config, activeUserContextService, enums,
        studentGrowthGoalsService, utils) {

        var service = {
            createDTEAssignmentRequest: createDTEAssignmentRequest,
            deleteDTEAssignmentRequest: deleteDTEAssignmentRequest,
            getDTEAssignmentRequestsForDTE: getDTEAssignmentRequestsForDTE,
            updateDTEAssignmentRequest: updateDTEAssignmentRequest,
            getDTEAssignmentRequestsForSchool:getDTEAssignmentRequestsForSchool,
            getDTEAssignmentRequestsForDistrict:getDTEAssignmentRequestsForDistrict,
            getEvaluateesForAssignment: getEvaluateesForAssignment,
            assignComprehensiveEvaluateePlanType: assignComprehensiveEvaluateePlanType,
            assignFocusedEvaluateePlanType: assignFocusedEvaluateePlanType,
            assignUndefinedEvaluateePlanType: assignUndefinedEvaluateePlanType,
            assignEvaluator: assignEvaluator,
            delegatePrincipalAssignmentsForSchool: delegatePrincipalAssignmentsForSchool,
            delegatePrincipalAssignmentsForAllSchools: delegatePrincipalAssignmentsForAllSchools,
            districtDelegatedPrincipalAssignmentsForSchool:districtDelegatedPrincipalAssignmentsForSchool,
            getDistrictViewerSchools:getDistrictViewerSchools,
            setDistrictViewerSchool:setDistrictViewerSchool,
            getDistrictAssignmentsSummary: getDistrictAssignmentsSummary,
            getSchoolAssignmentsSummary: getSchoolAssignmentsSummary,
            getEvaluationsForPreviousYear: getEvaluationsForPreviousYear
        };

        return service;

        function getEvaluationsForPreviousYear() {
            var evalType = activeUserContextService.context.workArea().evaluationType;
            var request = {
                schoolYear: activeUserContextService.context.orientation.schoolYear-1,
                districtCode: activeUserContextService.getActiveDistrictCode(),
                evaluationType: evalType,
                schoolCode: activeUserContextService.getActiveSchoolCode(),
                roleName:utils.mapEvalTypeToEvaluateeRole(evalType)
            };

            var url = config.apiUrl + 'assignments/evaluationsforusersinrole';

            return $http.get(url, {params: request}).then(function(response) {
                return response.data;
            });
        }

        function getDistrictAssignmentsSummary() {
            var frameworkContextId = activeUserContextService.context.frameworkContext.id;
            var url = config.apiUrl + 'assignments/districtsummary/' + frameworkContextId;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function getSchoolAssignmentsSummary() {
            var frameworkContextId = activeUserContextService.context.frameworkContext.id;
            var schoolCode = activeUserContextService.context.orientation.schoolCode;
            var url = config.apiUrl + 'assignments/schoolsummary/' + frameworkContextId + '/' + schoolCode;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function newComprehensiveAssignmentRequest(evaluatee) {
            return {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.getActiveDistrictCode(),
                evaluateePlanType: enums.EvaluationPlanType.COMPREHENSIVE,
                evaluationType: evaluatee.evalData.evaluationType,
                evaluateeId: evaluatee.id,
                FocusFrameworkNodeId: null,
                SGFocusFrameworkNodeId: null
            }
        }

        function newUndefinedAssignmentRequest(evaluatee) {
            return {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.getActiveDistrictCode(),
                evaluateePlanType: enums.EvaluationPlanType.UNDEFINED,
                evaluationType: evaluatee.evalData.evaluationType,
                evaluateeId: evaluatee.id,
                FocusFrameworkNodeId: null,
                SGFocusFrameworkNodeId: null
            }
        }

        function newFocusedAssignmentRequest(evaluatee, focusFrameworkNodeId, sgFocusFrameworkNodeId) {
            return {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.getActiveDistrictCode(),
                evaluateePlanType: enums.EvaluationPlanType.FOCUSED,
                evaluationType: evaluatee.evalData.evaluationType,
                evaluateeId: evaluatee.id,
                FocusFrameworkNodeId: focusFrameworkNodeId,
                SGFocusFrameworkNodeId: sgFocusFrameworkNodeId
            }
        }

        function newEvaluatorAssignmentRequest(evaluatee, evaluator) {
            return {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.getActiveDistrictCode(),
                evaluationType: evaluatee.evalData.evaluationType,
                evaluateeId: evaluatee.id,
                evaluatorId: evaluator?evaluator.id:null
            }
        }

        function newEvalAssignmentRequestForDTE(dte, teacher) {
            return {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.getActiveDistrictCode(),
                evaluatorId: dte.id,
                evaluateeId: teacher.id
            }
        }

        function getDelegateAssignmentsRequest(schoolCode, delegateAssignments) {
            var context = activeUserContextService.context;
            return {
                frameworkContextId: context.frameworkContext.id,
                schoolCode: schoolCode,
                delegateTeacherAssignments: delegateAssignments
            };
        }

        function districtDelegatedPrincipalAssignmentsForSchool(schoolCode) {
            var request = getDelegateAssignmentsRequest(schoolCode, true);
            var url = config.apiUrl + 'delegateassignments';
            return $http.get(url, {params: request}).then(function(response) {
                return response.data;
            });
        }

        function delegatePrincipalAssignmentsForSchool(schoolCode, delegateAssignments) {
            var request = getDelegateAssignmentsRequest(schoolCode, delegateAssignments);
            var url = config.apiUrl + 'delegateassignments';
            return $http.post(url, request).then(function (response) {
            });
        }


        function delegatePrincipalAssignmentsForAllSchools(delegateAssignments) {
            var request = {
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                delegateTeacherAssignments: delegateAssignments
            };
            var url = config.apiUrl + 'delegateassignmentsforframeworkcontext';
            return $http.post(url, request).then(function (response) {
            });
        }

        function deleteDTEAssignmentRequest(request) {
            var url = config.apiUrl + 'dteassignmentrequest/' + request.id;
            return $http.delete(url).then(function(response) {
            });
        }

        function createDTEAssignmentRequest(dte, teacher) {
            var request = newEvalAssignmentRequestForDTE(dte, teacher)
            var url = config.apiUrl + 'dteassignmentrequest';
            return $http.post(url, request).then(function(response) {
            });
        }

        function updateDTEAssignmentRequest(request) {
            var url = config.apiUrl + 'dteassignmentrequest';
            return $http.put(url, request).then(function(response) {
            });
        }

        function getDistrictViewerSchools(districtUserId)
        {
            var url = config.apiUrl + 'districtviewerschools/' +
                activeUserContextService.context.orientation.schoolYear + '/' +
                districtUserId;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function setDistrictViewerSchool(districtUserId, schoolCode, enable)
        {
            var request = {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.context.orientation.districtCode,
                districtUserId: districtUserId,
                schoolCode: schoolCode,
                enable: enable
            };

            var url = config.apiUrl + 'districtviewerrequest';
            return $http.post(url, request).then(function(response) {
                return response.data;
            });
        }

        function getDTEAssignmentRequestsForDTE(dte) {
            var schoolYear = activeUserContextService.context.orientation.schoolYear;
            var districtCode = activeUserContextService.context.orientation.districtCode;
            var url = config.apiUrl + 'dteassignmentrequests/' + schoolYear + '/' +
                                        districtCode + '/' + dte.id;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function getDTEAssignmentRequestsForSchool(schoolCode) {
            var schoolYear = activeUserContextService.context.orientation.schoolYear;
            var url = config.apiUrl + 'dteassignmentrequestsforschool/' + schoolYear + '/' +
                activeUserContextService.getActiveDistrictCode() + '/' + schoolCode;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function getDTEAssignmentRequestsForDistrict() {
            var schoolYear = activeUserContextService.context.orientation.schoolYear;
            var districtCode = activeUserContextService.context.orientation.districtCode;
            var url = config.apiUrl + 'dteassignmentrequestsfordistrict/' + schoolYear + '/' + districtCode;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function assignComprehensiveEvaluateePlanType(evaluatee) {
            var request = newComprehensiveAssignmentRequest(evaluatee)
            var url = config.apiUrl + 'assignplantype/comprehensive';
            return $http.post(url, request).then(function(response) {
                return studentGrowthGoalsService.syncGoalsWithEvalCycle(evaluatee);
            });
        }


        function assignUndefinedEvaluateePlanType(evaluatee) {
            var request = newUndefinedAssignmentRequest(evaluatee)
            var url = config.apiUrl + 'assignplantype/undefined';
            return $http.post(url, request).then(function(response) {
            });
        }

        function assignFocusedEvaluateePlanType(evaluatee, focusFrameworkNodeId, sgFocusFrameworkNodeId) {
            var request = newFocusedAssignmentRequest(evaluatee, focusFrameworkNodeId, sgFocusFrameworkNodeId)
            var url = config.apiUrl + 'assignplantype/focused';
            return $http.post(url, request).then(function(response) {
                return studentGrowthGoalsService.syncGoalsWithEvalCycle(evaluatee);
            });
        }

        function assignEvaluator(evaluatee, evaluator) {
            var request = newEvaluatorAssignmentRequest(evaluatee, evaluator);
            var url = config.apiUrl + 'assignevaluator';
            return $http.post(url, request).then(function(response) {
            });
        }

        function getNewCoreRequest(districtCode, schoolCode) {
            return {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: districtCode,
                schoolCode: schoolCode,
                evaluationType: activeUserContextService.context.workArea().evaluationType,
                roleName: utils.mapEvalTypeToEvaluateeRole(activeUserContextService.context.workArea().evaluationType),
                currentUserId: activeUserContextService.user.id
            };

        }
        function getEvaluateesForAssignment(districtCode, schoolCode) {
            var request = getNewCoreRequest(districtCode, schoolCode);
            var url = config.apiUrl + 'evaluateesforassignment';
            return $http.get(url, {params: request}).then(function(response) {
                return response.data;
            });
        }
    }
})();