/// <reference path="../assignments.module.ts" />

/**
 * Created by anne on 10/5/2015.
 */

(function () {
    'use strict';

    angular
        .module('stateeval.assignments')
        .factory('assignmentsModel', assignmentsModel);

    assignmentsModel.$inject = ['activeUserContextService', 'enums', 'userService', 'utils',
        'locationService', 'assignmentsService', '$q', '$sce', '$uibModal', '$stateParams'];
    function assignmentsModel(activeUserContextService, enums, userService, utils,
        locationService, assignmentsService, $q, $sce, $uibModal, $stateParams) {

        var modalInstance = null;

        var service = {
            activate: activate,
            model: {
                schools : [],
                evaluatees : [],
                principals : [],
                districtWideTeacherEvaluators : [],
                delegates : [],
                dteAssignmentRequests : [],
                acceptedDTEAssignmentRequests : [],
                assignedEvaluators : [],
                evaluatorOptionsForEvaluatee : [],
                planTypes : [],
                lastYearPlanTypeDisplayString: [],
                lastYearSuggestedPlanTypeDisplayString: [],
                frameworkNodes : [],
                sgFrameworkNodes : [],
                focusDisplayStrings : [],
                headPrincipals : [],
                districtEvaluators : [],
                schoolCode : '',
                schoolName: '',
                districtCode : '',
                userIsSchoolAdmin : false,
                userIsDistrictAdmin: false,
                userIsPrincipal: false,
                assignmentGridIsReadOnly : false,
                dteGridIsReadOnly : false,
                evaluationType : null,
                evaluateeTerm : '',
                assignedCount: 0,
                unassignedCount: 0,
                totalCount: 0,

                toggleDelegate: toggleDelegate,
                loadTeachers: loadTeachers,
                evaluatorDisplayName: evaluatorDisplayName,
                planTypeDisplayString: planTypeDisplayString,
                setPlanType: setPlanType,
                modifyFocus: modifyFocus,
                assignEvaluator: assignEvaluator,
                getFocusDisplayString: getFocusDisplayString,
                requestTypeDisplayString: requestTypeDisplayString,
                changeRequestStatus: changeRequestStatus,
                changeRequestType: changeRequestType
            }
        }

        return service;


        function init() {
            service.model.schools = [];
            service.model.evaluatees = [];
            service.model.principals = [];
            service.model.districtWideTeacherEvaluators = [];
            service.model.delegates = [];
            service.model.dteAssignmentRequests = [];
            service.model.acceptedDTEAssignmentRequests = [];
            service.model.assignedEvaluators = [];
            service.model.evaluatorOptionsForEvaluatee = [];
            service.model.planTypes = [];
            service.model.frameworkNodes = [];
            service.model.sgFrameworkNodes = [];
            service.model.focusDisplayStrings = [];
            service.model.headPrincipals = [];
            service.model.districtEvaluators = [];
            service.model.schoolCode = '';
            service.model.schoolName = '';
            service.model.districtCode = '';
            service.model.userIsSchoolAdmin = false;
            service.model.userIsDistrictAdmin = false;
            service.model.userIsPrincipal = false;
            service.model.assignmentGridIsReadOnly = false;
            service.model.dteGridIsReadOnly = false;
            service.model.evaluationType = null;
            service.model.evaluateeTerm = '';
            service.model.assignedCount = 0;
            service.model.unassignedCount = 0;
            service.model.totalCount = 0;
        }

        function activate(evalType, schoolCode, schoolName) {

            init();

            service.model.evaluationType = evalType;
            var workAreaTag = activeUserContextService.context.orientation.workAreaTag;
            service.model.userIsSchoolAdmin = (workAreaTag.indexOf('SA') === 0);
            service.model.userIsDistrictAdmin = (workAreaTag.indexOf('DA') === 0);
            service.model.userIsPrincipal = (workAreaTag.indexOf('PR') === 0);
            service.model.dteGridIsReadOnly = !service.model.userIsDistrictAdmin;

            // the user is either from a school and coming directly to this page,
            // or from a district and drilling down to this page
            if (service.model.userIsSchoolAdmin || service.model.userIsPrincipal) {
                service.model.schoolCode = activeUserContextService.context.orientation.schoolCode;
                service.model.schoolName = activeUserContextService.context.orientation.schoolName;
            }
            else {
                service.model.schoolCode = schoolCode;
                service.model.schoolName = schoolName;
            }

            if (service.model.evaluationType === enums.EvaluationType.PRINCIPAL && !service.model.userIsDistrictAdmin) {
                service.model.assignmentGridIsReadOnly = true;
            }

            service.model.evaluateeTerm = utils.getEvaluateeTermUpperCase(service.model.evaluationType);
            service.model.districtCode = activeUserContextService.getActiveDistrictCode();

            if (service.model.evaluationType === enums.EvaluationType.PRINCIPAL) {
                return activatePrincipalAssignments().then(function() {
                    calculateTotals();
                })
            }
            else {
                return activateTeacherAssignments().then(function() {
                    calculateTotals();
                })
            }
        }

        function evaluatorDisplayName(evaluatee) {
            var evaluatorId = evaluatee.evalData.evaluatorId;
            if (evaluatorId === null) {
                return $sce.trustAsHtml('<span class="label label-warning">NOT SET</span>');
            }
            else {
                var evaluator = service.model.assignedEvaluators[evaluatee.id];
                if (evaluator) {
                    return $sce.trustAsHtml(evaluator.displayName);
                }
                else {
                    return $sce.trustAsHtml('not found')
                }
            }
        }

        function planTypeDisplayString(evaluatee) {
            var retval = utils.mapEvaluationPlanTypeToString(evaluatee.evalData.planType);

            if (service.model.planTypes[evaluatee.id] === enums.EvaluationPlanType.FOCUSED) {
                retval+= (' ' + utils.mapEvaluationFocusForUserToString(activeUserContextService, evaluatee, false));
            }

            return retval;
        }

        function toggleDelegate() {
            service.model.delegates[service.model.schoolCode] = !service.model.delegates[service.model.schoolCode];
            assignmentsService.delegatePrincipalAssignmentsForSchool(service.model.schoolCode, service.model.delegates[service.model.schoolCode]);
        }

        function activateTeacherAssignments() {
            return setDelegateStatusForSchool()
                .then(function () {
                    return userService.getPrincipalsInDistrict();
                })
                .then(function (data) {
                    service.model.principals = data;
                    return userService.getUsersInRoleAtDistrict(enums.Roles.SEDistrictWideTeacherEvaluator);
                })
                .then(function (data) {
                    service.model.districtWideTeacherEvaluators = data;
                    return setupDTEAssignmentRequestsForSchool();
                })
                .then(function () {
                    return loadTeachers();
                })
        }

        function activatePrincipalAssignments() {
            return assignmentsService.getEvaluateesForAssignment(service.model.districtCode, service.model.schoolCode)
                .then(function(principals) {
                    service.model.evaluatees = principals;
                    service.model.evaluatees.forEach(function(principal) {
                        if (principalIsAHeadPrincipal(principal)) {
                            service.model.headPrincipals.push(principal);
                        }
                    })
                })
                .then(function() {
                    return userService.getUsersInRoleAtDistrict(enums.Roles.SEDistrictEvaluator)
                        .then(function(data) {

                            service.model.districtEvaluators = data;

                            service.model.evaluatees.forEach(function (principal) {

                                service.model.evaluatorOptionsForEvaluatee[principal.id] = [];
                                service.model.lastYearPlanTypeDisplayString[principal.id] = 'N/A';
                                service.model.lastYearSuggestedPlanTypeDisplayString[principal.id] = 'N/A';
                                Array.prototype.push.apply(service.model.evaluatorOptionsForEvaluatee[principal.id], service.model.districtEvaluators);
                                var headPrincipals = findHeadPrincipalsForPrincipal(principal);
                                if (!principalIsAHeadPrincipal(principal)) {
                                    Array.prototype.push.apply(service.model.evaluatorOptionsForEvaluatee[principal.id], headPrincipals);
                                }
                                setupAssignedEvaluatorAndPlanTypes(principal);
                            })
                        })
                        .then(function() {
                            setFocusDisplayStrings(enums.EvaluationType.PRINCIPAL);
                            return assignmentsService.getEvaluationsForPreviousYear();
                        }).then (function(lastYearEvaluations) {
                            lastYearEvaluations.forEach(function(evaluation) {
                                setupLastYearPlanType(evaluation);
                                setupLastYearSuggestedPlanType(evaluation);
                            })
                        })
                })
        }

        function loadTeachers() {
            return assignmentsService.getEvaluateesForAssignment(service.model.districtCode, service.model.schoolCode)
                .then(function(teachers) {
                    service.model.evaluatees = teachers;

                    service.model.evaluatees.forEach(function(teacher) {
                        service.model.evaluatorOptionsForEvaluatee[teacher.id] = [];
                        var dte = findDTEForTeacher(teacher);
                        if (dte) {
                            service.model.evaluatorOptionsForEvaluatee[teacher.id].push(dte);
                        }
                        else {
                            var principals = findPrincipalsThatCanEvaluateTeacher(teacher);
                            Array.prototype.push.apply(service.model.evaluatorOptionsForEvaluatee[teacher.id], principals);
                        }
                        service.model.lastYearPlanTypeDisplayString[teacher.id] = 'N/A';
                        service.model.lastYearSuggestedPlanTypeDisplayString[teacher.id] = 'N/A';
                        setupAssignedEvaluatorAndPlanTypes(teacher);
                        setFocusDisplayStrings(enums.EvaluationType.TEACHER);
                    });

                    return assignmentsService.getEvaluationsForPreviousYear();
                }).then (function(lastYearEvaluations) {
                    lastYearEvaluations.forEach(function(evaluation) {
                        setupLastYearPlanType(evaluation);
                        setupLastYearSuggestedPlanType(evaluation);
                    })
                })
        }

        function setDelegateStatusForSchool() {
            return assignmentsService.districtDelegatedPrincipalAssignmentsForSchool(service.model.schoolCode)
                .then(function(delegate) {
                    service.model.delegates[service.model.schoolCode] = delegate;
                    service.model.assignmentGridIsReadOnly = !delegate && !service.model.userIsDistrictAdmin;
                })
        }

        function setupDTEAssignmentRequestsForSchool() {
            return assignmentsService.getDTEAssignmentRequestsForSchool(service.model.schoolCode)
                .then(function(requests) {
                    service.model.dteAssignmentRequests = requests;
                    service.model.dteAssignmentRequests.forEach(function (request) {
                        request.statusAsString = request.evalRequestStatus.toString();
                        request.typeAsString = request.evalRequestType.toString();

                        if (request.evalRequestStatus == enums.EvalAssignmentRequestStatus.ACCEPTED) {
                            service.model.acceptedDTEAssignmentRequests.push(request);
                        }
                    })
                });
        }

        function setupLastYearPlanType(evalData) {
            var displayString = utils.mapEvaluationPlanTypeToString(evalData.planType) + ", ";

            if (evalData.planType === enums.EvaluationPlanType.FOCUSED) {
                displayString+= "<strong>" + buildFocusDisplayString(evalData.focusFrameworkNodeId, evalData.focusSGFrameworkNodeId); + "</strong>";
            }
            else {
                displayString += "<strong>C1-C8</strong>";
            }

            service.model.lastYearPlanTypeDisplayString[evalData.evaluateeId] = displayString;
        }

        function setupLastYearSuggestedPlanType(evalData) {
            var displayString = utils.mapEvaluationPlanTypeToString(evalData.nextYearPlanType);

            if (evalData.nextYearPlanType === enums.EvaluationPlanType.FOCUSED) {
                displayString+= ", <strong>" + buildFocusDisplayString(evalData.nextYearFcusFrameworkNodeId, evalData.nextYearFocusSGFrameworkNodeId); + "</strong>";
            }
            else if (evalData.nextYearPlanType === enums.EvaluationPlanType.COMPREHENSIVE) {
                displayString += ", <strong>C1-C8</strong>";
            }

            service.model.lastYearSuggestedPlanTypeDisplayString[evalData.evaluateeId] = displayString;
        }

        function setFocusDisplayStrings(evalType) {
            service.model.frameworkNodes = activeUserContextService.getFrameworkContext().stateFramework.frameworkNodes;
            service.model.sgFrameworkNodes = activeUserContextService.getFrameworkContext().studentGrowthFrameworkNodes;
            if (evalType === enums.EvaluationType.TEACHER) {
                service.model.sgFrameworkNodes = _.reject(service.model.sgFrameworkNodes, {'shortName': 'C8'});
            }
            service.model.evaluatees.forEach(function(evaluatee) {
                if (evaluatee.evalData.planType === enums.EvaluationPlanType.FOCUSED) {
                    service.model.focusDisplayStrings[evaluatee.id] = buildFocusDisplayString(
                        evaluatee.evalData.focusFrameworkNodeId,
                        evaluatee.evalData.focusSGFrameworkNodeId);
                }
            });
        }

        function setupAssignedEvaluatorAndPlanTypes(evaluatee) {
            if (evaluatee.evalData.evaluatorId!=null) {
                for (var i=0; i<service.model.evaluatorOptionsForEvaluatee[evaluatee.id].length; ++i) {
                    var evaluator = service.model.evaluatorOptionsForEvaluatee[evaluatee.id][i];
                    if (evaluatee.evalData.evaluatorId === evaluator.id) {
                        service.model.assignedEvaluators[evaluatee.id] = evaluator;
                        break;
                    }
                }
            }
            if (evaluatee.evalData.planType!==null) {
                service.model.planTypes[evaluatee.id] = evaluatee.evalData.planType;
            }
        }

        function calculateTotals() {
            service.model.totalCount = 0;
            service.model.assignedCount = 0;
            service.model.evaluatees.forEach(function(evaluatee) {
                service.model.totalCount++;
                if (service.model.assignedEvaluators[evaluatee.id] && service.model.planTypes[evaluatee.id]!= enums.EvaluationPlanType.UNDEFINED) {
                    service.model.assignedCount++;
                }
            });

            service.model.unassignedCount = service.model.totalCount - service.model.assignedCount;
        }

        function buildFocusDisplayString(focusFrameworkNodeId, focusSGFrameworkNodeId) {
            var focusFrameworkNode = findFrameworkNode(focusFrameworkNodeId);
            var sgFocusFrameworkNode = findSGFrameworkNode(focusSGFrameworkNodeId);

            var displayString = '<label>Focus:</label>';
            displayString+=(focusFrameworkNode.shortName);
            if (sgFocusFrameworkNode != null) {
                displayString+=('&nbsp;&nbsp;<label>Student Growth:</label>');
                displayString+=(sgFocusFrameworkNode.shortName);
            }
            return displayString;
        }

        function principalIsAHeadPrincipal(principal) {
            for (var i=0;i<principal.locationRoles.length; ++i) {
                var isHeadPrincipalAtLocation =
                    principal.locationRoles[i].roleName === enums.Roles.SESchoolHeadPrincipal;
                if (isHeadPrincipalAtLocation) {
                    return true;
                }
            }

            return false;
        }

        function findHeadPrincipalsForPrincipal(principal) {
            var matches = [];
            principal.locationRoles.forEach(function(locationRole) {
                service.model.headPrincipals.forEach(function(headPrincipal) {
                    var match = _.findWhere(headPrincipal.locationRoles, {schoolCode: locationRole.schoolCode});
                    var isHeadPrincipalAtLocation = locationRole.roleName === enums.Roles.SESchoolHeadPrincipal;
                    if (match && !isHeadPrincipalAtLocation) {
                        matches.push(headPrincipal);
                    }
                })
            });

            return matches;
        }

        function findSGFrameworkNode(id) {
            return _.find(service.model.sgFrameworkNodes, {id: id});
        }

        function findFrameworkNode(id) {
            return _.find(service.model.frameworkNodes, {id: id});
        }

        function findDTEForTeacher(teacher) {
            return _.find(service.model.districtWideTeacherEvaluators, {id: teacher.evalData.evaluatorId});
        }

        function findPrincipalsThatCanEvaluateTeacher(teacher) {
            var principalsList = [];
            teacher.locationRoles.forEach(function(locationRole) {
                service.model.principals.forEach(function(principal) {
                    var match = _.findWhere(principal.locationRoles, {schoolCode: locationRole.schoolCode});
                    if (match) {
                        match = _.find(principalsList, {id: principal.id});
                        if (!match) {
                            principalsList.push(principal);
                        }
                    }
                })
            });

            return principalsList;
        }

        function requestStatus(request) {
            return utils.mapEvalRequestStatusToString(request.evalRequestStatus);
        }

        function getFocusDisplayString(evaluatee) {
            return service.model.focusDisplayStrings[evaluatee.id];
        }

        function requestTypeDisplayString(request) {
            return utils.mapEvalRequestTypeToString(request.evalRequestType);
        }

        function setPlanType(evaluatee) {
            if (service.model.planTypes[evaluatee.id] === enums.EvaluationPlanType.COMPREHENSIVE) {
                setComprehensivePlanType(evaluatee);
            }
            else if (service.model.planTypes[evaluatee.id] === enums.EvaluationPlanType.FOCUSED) {
                setFocusedPlanType(evaluatee);
            }
            else {
                setUndefinedPlanType(evaluatee);
            }
            calculateTotals();
        }

        function setComprehensivePlanType(evaluatee) {
            assignmentsService.assignComprehensiveEvaluateePlanType(evaluatee)
                .then(function() {
                    evaluatee.evalData.planType = enums.EvaluationPlanType.COMPREHENSIVE;
                    evaluatee.evalData.focusFrameworkNodeId = null;
                    evaluatee.evalData.focusSGFrameworkNodeId = null;
                    activeUserContextService.save();
                })
        }

        function setUndefinedPlanType(evaluatee) {
            assignmentsService.assignUndefinedEvaluateePlanType(evaluatee)
                .then(function() {
                    evaluatee.evalData.planType = enums.EvaluationPlanType.UNDEFINED;
                    evaluatee.evalData.focusFrameworkNodeId = null;
                    evaluatee.evalData.focusSGFrameworkNodeId = null;
                    activeUserContextService.save();
                })
        }

        function setFocusedPlanType(evaluatee) {
            return openSelectFocusModal(evaluatee);
        }

        function assignEvaluator(evaluatee) {
            return assignmentsService.assignEvaluator(evaluatee, service.model.assignedEvaluators[evaluatee.id]).then(() =>
            {
                calculateTotals();
            })
        }

        function modifyFocus(evaluatee) {
            openSelectFocusModal(evaluatee);
        }

        function changeRequestStatus(request) {
            request.evalRequestStatus = parseInt(request.statusAsString);
            request.evalRequestType = parseInt(request.typeAsString);
            assignmentsService.updateDTEAssignmentRequest(request).then(function() {
                loadTeachers();
            })
        }

        function changeRequestType(request) {
            request.evalRequestStatus = parseInt(request.statusAsString);
            request.evalRequestType = parseInt(request.typeAsString);
            assignmentsService.updateDTEAssignmentRequest(request).then(function() {
                loadTeachers();
            })
        }

        function openSelectFocusModal(evaluatee) {

            modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'app/assignments/views/select-focus-modal.html',
                controller: 'selectFocusModalController as vm',
                size: 'lg',
                resolve: {
                    evaluatee: function () {
                        return evaluatee;
                    }
                }
            });

            modalInstance.result.then(function (result) {
                service.model.planTypes[result.evaluatee.id] = enums.EvaluationPlanType.FOCUSED;
                service.model.focusDisplayStrings[result.evaluatee.id] = result.focusDisplayString;
                assignmentsService.assignFocusedEvaluateePlanType(result.evaluatee, result.focusFrameworkNodeId, result.sgFocusFrameworkNodeId)
                    .then(function() {
                        var evaluatee = null;
                        var context = activeUserContextService.context;
                        if(context.evaluatees) {
                            evaluatee = _.find(context.evaluatees, {id: result.evaluatee.id});
                        }
                        result.evaluatee.evalData.planType = enums.EvaluationPlanType.FOCUSED;
                        result.evaluatee.evalData.focusFrameworkNodeId = result.focusFrameworkNodeId;
                        result.evaluatee.evalData.focusSGFrameworkNodeId = result.sgFocusFrameworkNodeId;

                        // if this is a principal doing the assignments then we want to update the
                        // evalData in his list of evaluatees if the evaluatee is there (might not be

                        if (evaluatee) {
                            evaluatee.evalData.planType = enums.EvaluationPlanType.FOCUSED;
                            evaluatee.evalData.focusFrameworkNodeId = result.focusFrameworkNodeId;
                            evaluatee.evalData.focusSGFrameworkNodeId = result.sgFocusFrameworkNodeId;
                            activeUserContextService.save();
                        }
                    })
            },
            function () {
                service.model.planTypes[evaluatee.id] = evaluatee.evalData.planType;
            });
        }
    }

})();
