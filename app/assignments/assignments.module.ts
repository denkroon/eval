/// <reference path="../core/core.module.ts" />

/**
 * Created by anne on 9/27/2015.
 */
(function() {
    'use strict';

    angular.module('stateeval.assignments', ['stateeval.core']);
})();
