/// <reference path="assignments.module.ts" />

(function(){
angular.module('stateeval.assignments')
    .config(configureRoutes);

function configureRoutes($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('assignments-summary', {
            url: '/assignments-summary/:evaluationtype',
            parent: 'default-content',
            views: {
                'content@base': {
                    templateUrl: 'app/assignments/views/assignments-summary.html',
                    controller: 'assignmentsSummaryController as vm'
                }
            },
            data: {
                title: 'Assignments',
            },
            ncyBreadcrumb: {
                label: 'Assignments for {{vm.evaluateeTerm}} Evaluations'
            },
        })
        .state('assignments-detail', {
            url: '/assignments-detail/{fromSummary}/{schoolCode}/{schoolName}/{evaluationtype}',
            parent: 'default-content',
            views: {
                'content@base': {
                    templateUrl: 'app/assignments/views/assignments-detail.html',
                    controller: 'assignmentsDetailController as vm'
                }
            },
            resolve: {
                initModel: ['assignmentsModel', '$stateParams', function (assignmentsModel, $stateParams) {
                    return assignmentsModel.activate(parseInt($stateParams.evaluationtype), $stateParams.schoolCode, $stateParams.schoolName);
                }]
            },
            ncyBreadcrumb: {
                label: 'Assignments for {{vm.evaluateeTerm}} Evaluations'
            },
        })
        .state('dte-request-assignment', {
            url: '/dte-request-assignment/',
            parent: 'da-base',
            views: {
                'content@base': {
                    templateUrl: 'app/assignments/views/dte-request-assignment.html',
                    controller: 'dteRequestAssignmentController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'District-wide Teacher Evaluator Assignment Requests'
            },
        });
}
}());
