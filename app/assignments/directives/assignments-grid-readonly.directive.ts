﻿/// <reference path="../assignments.module.ts" />

(function () {
    'use strict';
    angular.module('stateeval.assignments')
        .directive('assignmentsGridReadOnly', assignmentsGridReadOnly)
        .controller('assignmentsGridReadOnlyController', assignmentsGridReadOnlyController);

    assignmentsGridReadOnly.$inject = [];
    assignmentsGridReadOnlyController.$inject = ['enums'];

    function assignmentsGridReadOnly() {
        return {
            restrict: 'E',
            scope: {
                assignmentsModel: '='
            },
            templateUrl: 'app/assignments/views/assignments-grid-read-only-directive.html',
            controller: 'assignmentsGridReadOnlyController as vm',
            bindToController: true
        }
    }

    function assignmentsGridReadOnlyController(enums) {
        var vm = this;
        vm.enums = enums;
        vm.hideComplete = false;

        ///////////////////////////////

        activate();

        function activate() {
        }
    }
})();


