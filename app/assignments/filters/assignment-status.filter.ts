
(function () {
    'use strict';
     angular.module('stateeval.assignments')
     .filter('assignmentStatus', ['enums', '_', function(enums, _) {
         return function (data, hideComplete) {
             var results = _.filter(data, function(evaluatee) {
                 if (hideComplete) {
                     return !evaluatee.evalData.evaluatorId || evaluatee.evalData.planType === enums.EvaluationPlanType.UNDEFINED;
                 }
                 else {
                    return true;
                 }
             });
             return results;
         }
     }]);
})();
