/// <reference path="observation.module.ts" />

(function() {
    'use strict';

angular.module('stateeval.observation')
    .config(configureRoutes);

configureRoutes.$inject = ['$stateProvider'];

function configureRoutes($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('observation-home', {
            url: '/observation-home',
            parent: 'default-content',
            abstract: true
        })
        .state('observation-list', {
            url: '/observation-list',
            parent: 'observation-home',
            views: {
                'content@base': {
                    templateUrl: 'app/observation/views/observation-list.html',
                    controller: 'observationListController as vm'
                }
            },
            data: {
                evaluateeRequired: true,
                title: 'Observations'
            },
            ncyBreadcrumb: {
                label: 'Observations'
            }
        })
        .state('observation', {
            url: '/observation/:observationId',
            parent: 'observation-list',
            abstract: true,
            views: {
                'content@base': {
                    templateUrl: 'app/observation/details/observation.html',
                    controller: 'observationController as vm'
                }
            },
            resolve: {
                // need to get observation independent of the evidence collection so that it is relative
                // to the current user (to get linked artifacts). evidence collection can be relative to the evaluator even when tee
                // is viewing.
                observation: function(observationService, $stateParams) {
                    return observationService.getObservation($stateParams.observationId)
                        .then(function(observation) {
                            return observation;
                        });
                },
                //todo: we're loading the observation twice
                evidenceCollection: ['evidenceCollectionService', 'observationService', 'activeUserContextService', 'enums', '$stateParams', 'rubricUtils',
                    function (evidenceCollectionService, observationService, activeUserContextService, enums, $stateParams, rubricUtils) {
                        return observationService.getObservation($stateParams.observationId)
                            .then(function(observation) {
                                var userId;
                                if (observation.evaluateeId === activeUserContextService.user.id) {
                                    if (observation.selfEvalShared) {
                                        userId = observation.evaluateeId;
                                    }
                                    else {
                                        userId = observation.evaluatorId;
                                    }
                                }
                                else {
                                    userId = observation.evaluatorId;
                                }

                                return evidenceCollectionService.getEvidenceCollection("OBSERVATION",
                                    enums.EvidenceCollectionType.OBSERVATION,
                                    parseInt($stateParams.observationId), userId)
                                    .then(function (evidenceCollection) {
                                            return evidenceCollection;
                                        }
                                    )
                            });
                }]
            },
            data: {
                title: 'Observation Cycle'
            }
        })
        .state("observation-setup", {
            url: '/observation-setup',
            parent: 'observation',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/setup/setup.html',
                    controller: 'observationSetupController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Observation - {{vm.observation.title}}'
            }
        })
        .state("observation-report", {
            url: '/observation-report',
            parent: 'observation',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/report/report.html',
                    controller: 'observationReportController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Observation - {{vm.observation.title}}'
            }
        })
        .state("observation-notes", {
            url: '/observation-notes',
            parent: 'observation',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/notes/notes.html',
                    controller: 'observationNotesController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Observation - {{vm.observation.title}}'
            },
            params: {
                nodeShortName: null,
                rowId: null
            }
        })
        .state("observation-align", {
            url: '/observation-align',
            parent: 'observation',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/align/align.html',
                    controller: 'observationAlignController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Observation - {{vm.observation.title}}'
            },
            params: {
                nodeShortName: null,
                rowId: null
            }
        })

        .state('preconference', {
            url: '/preconference',
            parent: 'observation',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/conference/conference-tor.html',
                    controller: 'conferenceTorController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Observation - {{vm.observation.title}}'
            }
        })
        .state('preconferencetee', {
            url: '/preconferencetee',
            parent: 'observation',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/conference/conference-tee.html',
                    controller: 'conferenceTeeController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Observation - {{vm.observation.title}}'
            }
        })
        .state('postconference', {
            url: '/postconference',
            parent: 'observation',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/conference/conference-tor.html',
                    controller: 'conferenceTorController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Observation - {{vm.observation.title}}'
            }
        })
        .state('postconferencetee', {
            url: '/postconferencetee',
            parent: 'observation',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/conference/conference-tee.html',
                    controller: 'conferenceTeeController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Observation - {{vm.observation.title}}'
            }
        })
        .state('observation-artifacts', {
            url: '/observation-artifacts',
            parent: 'observation',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/artifacts/observe-artifacts.html',
                    controller: 'observationArtifactsController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Observation - {{vm.observation.title}}'
            }
        })
        .state('observation-artifact-view', {
            url: '/observation-artifact-view/{artifactId}',
            parent: 'observation-artifacts',
            views: {
                'content@observation': {
                    templateUrl: 'app/observation/details/artifacts/observe-artifact-view.html',
                    controller: 'observationArtifactViewController as vm'
                }
            },
            resolve: {
                artifact: ['artifactService', '$stateParams', function (artifactService, $stateParams) {
                    return artifactService.getArtifactById($stateParams.artifactId)
                        .then(function (artifact) {
                            return artifact;
                        });
                }]
            },
            data: {
                title: 'Observation Artifacts'
            },
            ncyBreadcrumb: {
                label: 'View Artifact - {{vm.artifact.title}}'
            }
        })
}

})();
