/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('newObservationModalController', newObservationModalController);


    newObservationModalController.$inject = ['observationService', 'config', '$uibModalInstance', 'enums'];
    function newObservationModalController(observationService, config, $uibModalInstance, enums) {
        var vm = this;
        vm.enums = enums;

        vm.save = save;
        vm.close = close;

        function save(observationType) {
            $uibModalInstance.close({
                observationType: observationType
            });
        }

        function close() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();