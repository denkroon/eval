/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('observationListController', observationListController);

    observationListController.$inject = ['config', 'enums', 'observationService', '$filter',
        'activeUserContextService', '$uibModal', '$state', 'utils', 'rubricUtils', '$rootScope', 'signalRService'];

    /* @ngInject */
    function observationListController(config, enums, observationService, $filter,
                                       activeUserContextService, $uibModal, $state, utils, rubricUtils, $rootScope, signalRService) {

        var vm = this;
        vm.context = activeUserContextService.context;
        vm.observations = null;
        vm.evaluatee = activeUserContextService.context.evaluatee;
        vm.isEvaluatee = activeUserContextService.context.isEvaluatee();
        vm.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();

        vm.showNewObservationBtn = !activeUserContextService.context.evaluationIsReadOnly() &&
                                   !activeUserContextService.context.isEvaluatee();

        vm.editObservation = editObservation;
        vm.openNewObservationWindow = openNewObservationWindow;

        // This is the list of filters available
        // fn is the function doing the actual filtering as explained in https://docs.angularjs.org/api/ng/filter/filter
        // it returns true for items that should be visible
        vm.filters = [
            { name: 'All', selected: true },
            {
                name: 'Formal',
                fn: function(value, index, array) {
                    return value.observationType === enums.ObservationType.FORMAL;
                }
            },
            {
                name: 'Informal',
                fn: function(value, index, array) {
                    return value.observationType === enums.ObservationType.INFORMAL;
                }
            },
            {
                name: 'In-progress',
                fn: function(value, index, array) {
                    return value.wfState === enums.WfState.OBS_IN_PROGRESS_TOR;
                }
            }
        ];

        if (!activeUserContextService.context.isEvaluatee()) {
            var filter = {
                name: 'My Observations',
                fn: function(value, index, array) {
                    return value.evaluatorId === activeUserContextService.user.id;
                }};

                vm.filters.push(filter);
        }

        vm.selectedFilter = function selectedFilter() {
            return _.find(vm.filters, { selected: true }).fn || function() { return true; };
        };


        ////////////////////////////
        activate();

        function activate() {

            observationService.getObservations().then(function (observations) {
                vm.observations = observations;

               addFocusFiltersIfNeeded();
            });
        }

        function addFocusFiltersIfNeeded() {
            if (_.any(vm.observations, {focused: true})) {
                vm.filters.push(
                    {
                        name: 'Comprehensive',
                        fn: function(value, index, array) {
                            return !value.focused;
                        }
                    }
                );

                vm.filters.push(
                    {
                        name: 'Focused',
                        fn: function(value, index, array) {
                            return value.focused;
                        }
                    }
                );
            }
        }
        function editObservation(observationId) {
            if (activeUserContextService.context.isEvaluatee()) {
                $state.go("observation-setup", {observationId: observationId});
            } else {
                $state.go("observation-setup", {observationId: observationId});
            }
        }

        function openNewObservationWindow() {
            observationService.startNewObservation(activeUserContextService.context.evaluatee);
        }
    }

})();