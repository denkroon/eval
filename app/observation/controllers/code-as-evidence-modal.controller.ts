/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('codeAsEvidenceModalController', codeAsEvidenceModalController);

    codeAsEvidenceModalController.$inject = ['$uibModalInstance', 'activeUserContextService'];

    /* @ngInject */
    function codeAsEvidenceModalController($uibModalInstance,  activeUserContextService) {

        var vm = this;

        vm.evaluateeTermUpperCase = activeUserContextService.getEvaluateeTermUpperCase();
        vm.evaluateeTermLowerCase = activeUserContextService.getEvaluateeTermLowerCase();

        vm.save = save;
        vm.cancel = cancel;

        /////////////////////////////////

        function save(showNextTime) {

            $uibModalInstance.close({
                showNextTime: showNextTime
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

