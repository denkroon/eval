/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('shareConfResponsesModalController', shareConfResponsesModalController);

    shareConfResponsesModalController.$inject = ['$uibModalInstance', 'activeUserContextService', 'type'];

    /* @ngInject */
    function shareConfResponsesModalController($uibModalInstance,  activeUserContextService, type) {

        var vm = this;
        vm.type = type;

        vm.evaluateeTermUpperCase = activeUserContextService.getEvaluateeTermUpperCase();
        vm.evaluateeTermLowerCase = activeUserContextService.getEvaluateeTermLowerCase();

        vm.save = save;
        vm.cancel = cancel;

        /////////////////////////////////

        function save(showNextTime) {

            $uibModalInstance.close({
                showNextTime: showNextTime
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

