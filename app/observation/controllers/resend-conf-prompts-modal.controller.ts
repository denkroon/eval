/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('resendConfPromptsModalController', resendConfPromptsModalController);

    resendConfPromptsModalController.$inject = ['$uibModalInstance', 'activeUserContextService'];

    /* @ngInject */
    function resendConfPromptsModalController($uibModalInstance,  activeUserContextService) {

        var vm = this;

        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermLowerCase();
        vm.comment = '';

        vm.save = save;
        vm.cancel = cancel;

        /////////////////////////////////

        function save() {

            $uibModalInstance.close({
                comment: vm.comment
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

