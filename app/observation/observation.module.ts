/// <reference path="../core/core.module.ts" />

(function() {
    'use strict';

    angular.module('stateeval.observation', ['stateeval.core']);
})();
