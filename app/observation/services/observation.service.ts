/// <reference path="../observation.module.ts" />

/**
 * Created by anne on 6/19/2015.
 */
(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .factory('observationService', observationService);

    observationService.$inject = ['$http', 'utils', '_', 'config', 'enums',
        'activeUserContextService', '$uibModal', '$state', '$filter'];

    function observationService($http, utils, _, config, enums,
        activeUserContextService, $uibModal, $state, $filter) {

        var service = {
            getObservations: getObservations,
            getObservation: getObservation,
            getNewObservationForEvaluatee: getNewObservationForEvaluatee,
            saveObservation: saveObservation,
            deleteObservation: deleteObservation,
            updateUserPromptResponse: updateUserPromptResponse,
            saveObserveNotes: saveObserveNotes,
            deleteCodedRubricRowAnnotation: deleteCodedRubricRowAnnotation,
            deleteCodedRubricRowAnnotations: deleteCodedRubricRowAnnotations,
            getObservationsForTeeTor: getObservationsForTeeTor,
            updateShareOptions:updateShareOptions,
            newShareOptions: newShareOptions,
            completeObservation:completeObservation,
            completeSelfEval:completeSelfEval,
            acknowledgeFinalReview: acknowledgeFinalReview,
            sendObservationForFinalReview:sendObservationForFinalReview,
            torSharePrompts: torSharePrompts,
            teeSharePrompts: teeSharePrompts,
            teeShareConfNotes: teeShareConfNotes,
            teeShareFinalNotes: teeShareFinalNotes,
            saveReportFormat: saveReportFormat,
            requestUnlock: requestUnlock,
            acceptUnlockRequest: acceptUnlockRequest,
            declineUnlockRequest: declineUnlockRequest,
            removeUnlockRequest: removeUnlockRequest,
            startNewObservation: startNewObservation,
            setObservationStatusDisplayString: setObservationStatusDisplayString,
            gotoObservation: gotoObservation
        };

        function gotoObservation(activeUserContextService, observation) {

            var state = '';
            var isEvaluatee = activeUserContextService.user.id === observation.evaluateeId;

            if (observation.wfState === enums.WfState.OBS_IN_PROGRESS_TOR) {
                if (isEvaluatee) {
                    state = 'observation-setup';
                }
                else {
                    state = 'observation-align';
                }
            }
            else {
                state = 'observation-report';
            }

            $state.go(state, {observationId: observation.id});
        }

        function setObservationStatusDisplayString(observation) {
            switch(observation.wfState) {
                case enums.WfState.OBS_IN_PROGRESS_TOR:
                    var obsDate = null;
                    if (observation.observeStartDate) {
                        obsDate = $filter('date')(observation.observeStartDate);
                    }
                    observation.statusDisplayString = "In-progress ";
                    if (obsDate) {
                        observation.statusDetailDisplayString = "Obs date: " + obsDate;
                    }
                    observation.statusWarning = false;
                    return;
                case enums.WfState.OBS_LOCKED_TEE_REVIEW:
                    observation.statusDisplayString = "Final Ack";
                    observation.statusDetailDisplayString = 'sent ' + $filter('fromNow')(observation.sendFinalDateTime);
                    observation.statusWarning = true;
                    return;
                case enums.WfState.OBS_LOCKED_SEALED:
                    if (observation.FinalAcknowledgementSentDateTime) {
                        observation.statusDisplayString = "Completed";
                    }
                    else {
                        observation.statusDisplayString = "Completed (manual)";
                    }
                    observation.statusDetailDisplayString = $filter('date')(observation.lockDateTime);
                    observation.statusWarning = true;
                    return;
                case enums.WfState.OBS_UNLOCK_REQUEST_TEE:
                    observation.statusDisplayString = "Completed";
                    observation.statusDetailDisplayString = "Unlock request from " + activeUserContextService.getEvaluateeTermLowerCase();
                    observation.statusWarning = true;
                    return;
                case enums.WfState.OBS_UNLOCK_REQUEST_TOR:
                    observation.statusDisplayString = "Completed";
                    observation.statusDetailDisplayString = "Unlock request from evaluator";
                    observation.statusWarning = true;
                    return;
            }
        }

        function startNewObservation(evaluatee) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/observation/views/new-observation-modal.html',
                controller: 'newObservationModalController as vm'
            });

            modalInstance.result.then(function (result) {
                var observation = getNewObservationForEvaluatee(evaluatee, result.observationType);
                saveObservation(observation).then(function() {
                    $state.go("observation-setup", { observationId: observation.id });
                });
            });
        }
        function removeUnlockRequest(observation) {

            var path = observation.evaluatorId === activeUserContextService.user.id?"removeunlockrequesttor":"removeunlockrequesttee";
            var url = config.apiUrl + 'observation/'  + path + '/';
            return $http.post(url, observation).then(function (response) {
            });
        }

        function acceptUnlockRequest(observation) {

            var path = observation.evaluatorId === activeUserContextService.user.id?"acceptunlockrequesttor":"acceptunlockrequesttee";
            var url = config.apiUrl + 'observation/'  + path + '/';
            return $http.post(url, observation).then(function (response) {
            });
        }

        function declineUnlockRequest(observation) {

            var path = observation.evaluatorId === activeUserContextService.user.id?"declineunlockrequesttor":"declineunlockrequesttee";
            var url = config.apiUrl + 'observation/'  + path + '/';
            return $http.post(url, observation).then(function (response) {
            });
        }

        function requestUnlock(observation) {

            var path = observation.evaluatorId === activeUserContextService.user.id?"unlockrequesttor":"unlockrequesttee";
            var url = config.apiUrl + 'observation/'  + path + '/';
            return $http.post(url, observation).then(function (response) {
            });
        }

        function saveReportFormat(observation) {
            var url = config.apiUrl + 'observation/updatereportformat';
            var requestData = {
                id: observation.id,
                reportFormatType: observation.reportFormatType
            };

            return $http.post(url, requestData).then(function (response) {
            });
        }

        function torSharePrompts(promptType, observation, shared, message) {

            var shareOptions = newShareOptions(observation.id);
            shareOptions.preConfPromptsShared = (!shared && (promptType === enums.PromptType.PreConf));
            shareOptions.postConfPromptsShared = (!shared && (promptType === enums.PromptType.PostConf));
            shareOptions.preConfPromptsUpdated = (shared && (promptType === enums.PromptType.PreConf));
            shareOptions.postConfPromptsUpdated = (shared && (promptType === enums.PromptType.PostConf));

            shareOptions.notifyEvaluatee = true;
            shareOptions.message = message;

            var url = config.apiUrl + 'observation/torshareprompts';
            return $http.post(url, shareOptions).then(function (response) {
                if (promptType === enums.PromptType.PreConf) {
                    observation.preConfQsTorSentDate = new Date();
                }
                else {
                    observation.postConfQsTorSentDate = new Date();
                }
            });
        }

        function teeSharePrompts(observation, promptType) {
            var url = config.apiUrl + 'observation/teeshareprompts/' + observation.id + '/' + promptType;
            return $http.get(url).then(function (response) {
                if (promptType === enums.PromptType.PreConf) {
                    observation.preConfQsTeeSentDate = new Date();
                }
                else {
                    observation.postConfQsTeeSentDate = new Date();
                }
            });
        }

        function teeShareConfNotes(observation, promptType) {
            var url = config.apiUrl + 'observation/teeshareconfnotes/' + observation.id + '/' + promptType;
            return $http.get(url).then(function (response) {
                if (promptType === enums.PromptType.PreConf) {
                    observation.preConfNotesTeeSentDate = new Date();
                }
                else {
                    observation.postConfNotesTeeSentDate = new Date();
                }
            });
        }

        function teeShareFinalNotes(observation, promptType) {
            var url = config.apiUrl + 'observation/teesharefinalnotes/' + observation.id;
            return $http.get(url).then(function (response) {
                observation.evaluateeFinalNotesSentDateTime = new Date();
            });
        }

        function sendObservationForFinalReview(observation) {
            var url = config.apiUrl + 'observation/sendobservationforfinalreview';
            return $http.post(url, observation).then(function (response) {
            });
        }

        function acknowledgeFinalReview(observation) {
            var url = config.apiUrl + 'observation/teeacknowledgefinalreview';
            return $http.post(url, observation).then(function (response) {
            });
        }

        function completeObservation(observation) {
            var url = config.apiUrl + 'observation/torcompleteobservation';
            return $http.post(url, observation).then(function (response) {
            });
        }

        function completeSelfEval(observation) {
            var url = config.apiUrl + 'observation/teecompleteselfeval';
            return $http.post(url, observation).then(function (response) {
            });
        }

        function getObservationsForTeeTor() {

            var request = {
                schoolYear: activeUserContextService.context.orientation.schoolYear,
                districtCode: activeUserContextService.context.orientation.districtCode,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                evaluationType: activeUserContextService.context.workArea().evaluationType,
                evaluatorId: activeUserContextService.user.id,
                roleName: utils.mapEvalTypeToEvaluateeRole(activeUserContextService.context.workArea().evaluationType),
                assignedOnly: activeUserContextService.context.assignedEvaluatees
            };

            var url = config.apiUrl + 'observationstortee';

            return $http.get(url, {params: request}).then(function(response) {
                return response.data;
            });
        }


        function newObservationRequest() {
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var currentUserId = activeUserContextService.user.id;
            return {
                evaluationId: evaluationId,
                currentUserId: currentUserId
            }
        }

        function getObservations() {
            var request = newObservationRequest();
            var url = config.apiUrl + '/observations';
            return $http.get(url, {params: request})
                .then(function (response) {
                    return response.data;
                });
        }

        function getObservation(observationId) {
            var url = config.apiUrl + 'observation/' + observationId + '/' + activeUserContextService.user.id;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function newShareOptions(observationId) {
            return {
                observationId: observationId,
                observationNotesShared: false,
                evaluatorScoresShared: false,
                selfEvalShared: false,
                preConfPromptsShared: false,
                postConfPromptsShare: false,
                preConfPromptsUpdated: false,
                postConfPromptsUpdated: false,
                notifyEvaluatee: true,
                message: ''
            }
        }
        function updateShareOptions(shareOptions) {
            var url = config.apiUrl + 'observation/updateshareoptions';
            return $http.post(url, shareOptions).then(function (response) {
            });
        }

        function getNewObservationForEvaluatee(evaluatee, observationType) {
            var evalData = evaluatee.evalData;
            var focused = evalData.planType === enums.EvaluationPlanType.FOCUSED;
            return {
                id: 0,
                observationType: observationType,
                preConfIsComplete: false,
                obsIsComplete: false,
                postConfIsComplete: false,
                preConfIsActive: observationType === enums.ObservationType.FORMAL,
                postConfIsActive: observationType === enums.ObservationType.FORMAL,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                evaluateeId: evaluatee.id,
                evaluatorId: activeUserContextService.user.id,
                wfState: enums.WfState.OBS_IN_PROGRESS_TOR,
                evaluationId: evalData.id,
                evaluationType: evalData.evaluationType,
                focused: focused,
                focusFrameworkNodeId: focused? evalData.focusFrameworkNodeId:null,
                focusSGFrameworkNodeId: focused? evalData.focusSGFrameworkNodeId:null,
                reportFormatType: 1
            };
        }


        function deleteObservation(observation) {
            return $http.delete(config.apiUrl + 'observations/' + observation.id);
        }

        function saveObservation(observation) {
            var url = config.apiUrl + 'observation/saveobservation';
            return $http.post(url, observation).then(function (response) {
                if (observation.id===0) {
                    observation.id = response.data.id;
                }
                return response.data;
            });
        }

        function deleteCodedRubricRowAnnotation(evidenceCollection, clientId) {
            var url = config.apiUrl + 'rubricrowannotation/' + clientId + '/delete';
            return $http.post(url).then(function (response) {
                var availableEvidenceId = response.data;
                evidenceCollection.deleteUnusedAvailableEvidenceById(availableEvidenceId);
            });
        }

        function deleteCodedRubricRowAnnotations(evidenceCollection, clientIdList) {
            var url = config.apiUrl + 'rubricrowannotations/delete';
            return $http.post(url,  clientIdList).then(function (response) {
                var availableEvidenceIds = response.data;
                availableEvidenceIds.forEach(function(availableEvidenceId) {
                    evidenceCollection.deleteUnusedAvailableEvidenceById(availableEvidenceId);
                });
             });
        }

        function updateUserPromptResponse(observationId, userPromptResponseId, response) {
            var url = config.apiUrl + 'userpromptresponse/savecodedresponse';
            var userPromptResponse = {
                id: userPromptResponseId,
                response: response,
                observationId: observationId
            };

            return $http.post(url, userPromptResponse).then(function(result) {
                return result.data;
            });
        }

        function saveObserveNotes(observationId, note, noteProperty) {
            var url = config.apiUrl + 'observation/updateobservenotesproperty';
            var request = {
                observationId: observationId,
                noteProperty: noteProperty,
                notePropertyValue: note};
            return $http.post(url, request).then(function (response) {
                return response.data;
            });
        }

        return service;

    }
})();

