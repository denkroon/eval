/// <reference path="../../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('observationAlignController', observationAlignController);

    observationAlignController.$inject = ['$state', 'enums', 'rubricUtils',
        'activeUserContextService', 'evidenceCollectionService',
        'evidenceCollection', 'observation', '$timeout'];

    /* @ngInject */
    function observationAlignController($state, enums, rubricUtils,
                                   activeUserContextService, evidenceCollectionService,
                                   evidenceCollection, observation, $timeout) {

        var vm = this;

        vm.observation = observation;
        vm.evidenceCollection = evidenceCollection;
        vm.state = evidenceCollectionService.state;
        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();

        vm.isObservationReadOnly = activeUserContextService.context.observationIsReadOnly(vm.observation);
        vm.isEvaluator = vm.observation.evaluatorId === activeUserContextService.user.id;

        vm.showBothCB = false;
        vm.teeInputNotCompleteToolTip = 'The ' + vm.evaluateeTerm + ' has not shared her input yet.';
        vm.showShowTeeInputPopover = false;

    }


})();