/// <reference path="../observation.module.ts" />

namespace StateEval.Observation {

    export class ObservationController {

        observationTabs: any[];
        isEvaluator: boolean;
        isEvaluatee: boolean;
        observation: any;
        sharing: boolean;
        evaluateeTermUC: string;

        tabsCollapsed = false;

        constructor(
            public enums: any,
            private $state: ng.ui.IStateService,
            private activeUserContextService: any,
            private evidenceCollectionService: any,
            private observationService: any,
            private $uibModal: ng.ui.bootstrap.IModalService,
            private $confirm: any,
            private $scope: ng.IScope,
            $timeout: ng.ITimeoutService,
            observation: any,
            evidenceCollection: any,
            utils: any
        ) {
            $state.current.data.title = "Observation - " + observation.title;

            this.observation = evidenceCollection.observation;
            this.isEvaluator = this.observation.evaluatorId === activeUserContextService.user.id;
            this.isEvaluatee = this.observation.evaluateeId === activeUserContextService.user.id;
            this.evaluateeTermUC = activeUserContextService.getEvaluateeTermUpperCase();

            this.observationTabs = [
                new utils.Link('Setup', 'observation-setup'),
                new utils.Link('Pre Conference', this.isEvaluatee ? 'preconferencetee' : 'preconference', null, null, !this.observation.preConfIsActive),
                new utils.Link('Observe', 'observation-notes', null, null, this.isEvaluatee && !this.observation.observeNotesShared),
                new utils.Link('Post Conference', this.isEvaluatee ? 'postconferencetee' : 'postconference', null, null, !this.observation.postConfIsActive),
                new utils.Link('Align & Score', 'observation-align', null, null, this.isEvaluatee && !this.observation.selfEvalShared && !this.observation.evaluatorScoresShared),
                new utils.Link('Artifacts', 'observation-artifacts'),
                new utils.Link('Report', 'observation-report')
            ];

            $scope.$watch('vm.observation.observeNotesShared || vm.observation.evaluatorScoresShared || vm.observation.selfEvalShared', (val: boolean) => {
                this.sharing = val;
            });

            $(window).on('resize', this.onWindowResize);
            $scope.$on('$destroy', () => { $(window).off('resize', this.onWindowResize) });
            // Ensure the tab bar is collapsed if needed
            $timeout(this.onWindowResize);
        }

        acceptUnlockRequestModal() {
            this.observationService.requestUnlock(this.observation).then(() => {
                this.$state.go(this.$state.current, [], {reload: true});
            })
        }


        openConfigureSharingModal() {

             var modalInstance = this.$uibModal.open({
                animation: false,
                templateUrl: 'app/observation/details/_share-observation-modal.html',
                controller: 'shareObservationModalController as vm',
                windowClass: 'modal-sharing',
                resolve: {
                    observation: this.observation
                }
            });

            modalInstance.result.then(function (result) {
           });
        }

        openSendFinalVersionModal() {

            this.observation.reason = '';
            var modalInstance = this.$uibModal.open({
                animation: false,
                templateUrl: 'app/observation/details/report/_send-final-modal.html',
                controller: 'sendFinalModalController as vm',
                resolve: {
                    observation: this.observation
                }
            });

            modalInstance.result.then((result) => {
                this.observationService.sendObservationForFinalReview(this.observation).then(() => {
                    this.$state.go(this.$state.current, [], {reload: true});
                })
            });
        }

        openSelfEvalCompleteModal() {
            this.$confirm(
                {
                    ok: "Yes, share alignment", cancel: "Cancel"
                }, {templateUrl: 'app/observation/details/_confirm-self-eval-complete.html'})
                .then(() => {
                    this.observationService.completeSelfEval(this.observation).then(() => {
                        this.$state.go(this.$state.current, [], {reload: true});
                    })
                });
        }


        // Collapses the tab bar depending on the with it has access to
        private onWindowResize = () => {
            // 800px is picked based on today's configuration. If tabs are added, removed
            // or their labels are modified, this value will need to be adjusted.
            if ($('#observationTabs').width() < 800) {
                if (!this.tabsCollapsed) this.$scope.$eval('vm.tabsCollapsed=true');
            }
            else if (this.tabsCollapsed) {
                this.$scope.$eval('vm.tabsCollapsed=false');
            }
        };

        markAsComplete() {
            this.observationService.completeObservation(this.observation).then(()=>{
                this.$state.go(this.$state.current, [], {reload: true});
            });
        }
    }

    angular
        .module('stateeval.observation')
        .controller('observationController', ObservationController);

}