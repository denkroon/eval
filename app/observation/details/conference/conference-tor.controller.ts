/// <reference path="../../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('conferenceTorController', conferenceTorController);

    conferenceTorController.$inject = ['$state', 'enums',
        'observationService', 'userPromptService', 'activeUserContextService',
        'observation', 'evidenceCollection', 'utils', '$uibModal', 'userService', '$scope',
        '$timeout'
    ];

    function conferenceTorController($state, enums,
                                     observationService, userPromptService, activeUserContextService,
                                     observation, evidenceCollection, utils, $uibModal, userService, $scope,
                                     $timeout) {

        var vm = this;
        vm.getSafeHtml = utils.getSafeHtml;
        vm.enums = enums;
        vm.showSetup = true;
        vm.showResponses = false;
        vm.evidenceCollection = evidenceCollection;
        vm.observation = evidenceCollection.observation;
        vm.notesCodeMode = false;
        vm.promptType = $state.current.name.indexOf('pre') === 0 ? enums.PromptType.PreConf : enums.PromptType.PostConf;
        vm.isObservationReadOnly = activeUserContextService.context.observationIsReadOnly(vm.observation);
        vm.evaluateeTermUpperCase = activeUserContextService.getEvaluateeTermUpperCase();
        vm.evaluateeTermLowerCase = activeUserContextService.getEvaluateeTermLowerCase();
        $scope.noHelperDefault = true;


        vm.sendPrompts = sendPrompts;
        vm.saveConfNotes = saveConfNotes;
        vm.toggleCodeModeOnPrompt = toggleCodeModeOnPrompt;
        vm.toggleCodeModeOnNotes = toggleCodeModeOnNotes;
        vm.redirect = redirect;
        vm.saveFinalNotes = saveFinalNotes;

        vm.conferenceTitle = vm.promptType === enums.PromptType.PreConf ? "Pre-Conference" : "Post-Conference";

        vm.promptEvidenceType = vm.promptType === enums.PromptType.PreConf ?
            vm.enums.EvidenceType.RR_ANNOTATION_PRECONF_PROMPT :
            vm.enums.EvidenceType.RR_ANNOTATION_POSTCONF_PROMPT;

        vm.evaluateeNotesEvidenceType = vm.promptType === enums.PromptType.PreConf ?
            vm.enums.EvidenceType.RR_ANNOTATION_TEE_PRECONF_SUMMARY :
            vm.enums.EvidenceType.RR_ANNOTATION_TEE_POSTCONF_SUMMARY;

        vm.evaluatorNotesProperty = vm.promptType === enums.PromptType.PreConf ? "evaluatorPreConNotes" : "evaluatorPostConNotes";

        vm.showPromptArea = vm.promptType == enums.PromptType.PreConf?
                vm.observation.preConfQsIsActive:
                vm.observation.postConfQsIsActive;

        vm.showEvaluateeNotes = false;
        vm.showEvaluateeFinalNotes = false;
        vm.locked = false;
        vm.promptsSentByTor = false;
        vm.promptsSentByTee = false;

        activate();

        function redirect(node, row) {
            $state.go('observation-align', {nodeShortName: node.data.shortName, rowId: row && row.data.id});
        }

        function activate() {

            setupInitialStateVars();

            loadUserPromptResponses();
        }

        function loadUserPromptResponses() {
            userPromptService.getObservationUserPromptResponses(vm.observation.id, vm.promptType).then(function (userPromptResponses) {
                vm.userPromptResponses = userPromptResponses;
                vm.userPromptResponses.forEach(function(promptResponse) {
                    if (!vm.promptsSentByTee || !promptResponse.response) {
                        promptResponse.responseWithOutRemoveTag = "(no response yet)";
                    }
                    else {
                        promptResponse.responded = true;
                        promptResponse.responseWithOutRemoveTag = utils.getResponseWithoutRemoveCodeSelection(promptResponse.response);
                        promptResponse.response = utils.getSafeHtml(promptResponse.response);
                    }
                })
            });
        }

        function toggleCodeModeOnPrompt(userPromptResponse) {
            if (!userPromptResponse.codeMode) {
                if (activeUserContextService.user.showConfCodeConfirm) {
                    var modalInstance = $uibModal.open({
                        templateUrl: 'app/observation/views/code-as-evidence-modal.html',
                        controller: 'codeAsEvidenceModalController as vm'
                    });

                    modalInstance.result.then(function (result) {
                        if (!result.showNextTime) {
                            userService.updateShowConfCodeConfirm(activeUserContextService, false).then(function() {
                                userPromptResponse.codeMode = !userPromptResponse.codeMode;
                            });
                        }
                        else {
                            userPromptResponse.codeMode = !userPromptResponse.codeMode;
                        }
                    });
                }
                else {
                    userPromptResponse.codeMode = !userPromptResponse.codeMode;
                }
            }
            else {
                userPromptResponse.codeMode=!userPromptResponse.codeMode;
                loadUserPromptResponses();
            }
        }

        function toggleCodeModeOnNotes() {

            if (!vm.notesCodeMode) {
                if (activeUserContextService.user.showConfCodeConfirm) {
                    var modalInstance = $uibModal.open({
                        templateUrl: 'app/observation/views/code-as-evidence-modal.html',
                        controller: 'codeAsEvidenceModalController as vm'
                    });

                    modalInstance.result.then(function (result) {
                        if (!result.showNextTime) {
                            userService.updateShowConfCodeConfirm(activeUserContextService, false).then(function() {
                                vm.notesCodeMode = !vm.notesCodeMode;
                            });
                        }
                        else {
                            vm.notesCodeMode = !vm.notesCodeMode;
                        }
                    });
                }
                else {
                    vm.notesCodeMode=!vm.notesCodeMode;
                }
            }
            else {
                vm.notesCodeMode=!vm.notesCodeMode;
                loadConfNotes();
            }
        }

        function setupInitialStateVars() {
            vm.showSetup = true;
            vm.showResponses = false;

            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfQsTorSentDate) {
                vm.promptsSentByTor = true;
                vm.confQsTorSentDate = vm.observation.preConfQsTorSentDate;
            }
            else if (vm.promptType === enums.PromptType.PostConf && vm.observation.postConfQsTorSentDate) {
                vm.promptsSentByTor = true;
                vm.confQsTorSentDate = vm.observation.postConfQsTorSentDate;
            }

            if (vm.promptsSentByTor) {
                vm.showSetup = false;
                vm.showResponses = true;
            }
            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfQsTeeSentDate) {
                vm.promptsSentByTee = true;
                vm.confQsTeeSentDate = vm.observation.preConfQsTeeSentDate;
            }
            else if (vm.promptType === enums.PromptType.PostConf && vm.observation.postConfQsTeeSentDate) {
                vm.promptsSentByTee = true;
                vm.confQsTeeSentDate = vm.observation.postConfQsTeeSentDate;
            }

            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfNotesTeeSentDate &&
                vm.observation.evaluateePreConNotes) {
                vm.showEvaluateeNotes = true;
            }
            else if (vm.promptType === enums.PromptType.PostConf && vm.observation.postConfNotesTeeSentDate &&
                vm.observation.evaluateePostConNotes) {
                vm.showEvaluateeNotes = true;
            }

            if (vm.promptType === enums.PromptType.PostConf && vm.observation.evaluateeFinalNotesSentDateTime &&
                vm.observation.evaluateeFinalNotes) {
                vm.showEvaluateeFinalNotes = true;
            }

            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfIsLocked) {
                vm.locked = true;
            }
            else if (vm.promptType === enums.PromptType.PostConf && vm.observation.postConfIsLocked) {
                vm.locked = true;
            }
            vm.lockedForCodingBtnText = vm.locked ? 'Coding ' + vm.evaluateeTermUpperCase +  ' Responses' : 'Code ' + vm.evaluateeTermUpperCase + ' Responses';

            loadConfNotes();

        }

        function loadConfNotes() {
            vm.confNotes = vm.promptType === enums.PromptType.PreConf ? vm.observation.evaluateePreConNotes : vm.observation.evaluateePostConNotes;
            vm.confNotes = utils.getSafeHtml(vm.confNotes);

            vm.confNotesWithoutRemoveCodeSelection = utils.getResponseWithoutRemoveCodeSelection(vm.confNotes);
        }

        function sendPrompts(resend, comment) {

            if (!resend) {
               comment = "Prompts are ready."
            }
            return observationService.torSharePrompts(vm.promptType, vm.observation, resend, comment).then(function () {
                activate();
                vm.showResponses = false;
            });
        }

        var finalNotesSaveTimer = null;

        function saveFinalNotes() {
            vm.observation.saveNotice = 'Saving...';

            if (finalNotesSaveTimer) {
                $timeout.cancel(finalNotesSaveTimer);
            }

            finalNotesSaveTimer = $timeout(function () {
                return observationService.saveObserveNotes(vm.observation.id, vm.observation.evaluatorFinalNotes, 'evaluatorFinalNotes')
                    .then(() => {
                        vm.observation.saveNotice = 'All changes saved';
                    });
            }, 1000);
        }

        var promptSaveNotes = null;

        function saveConfNotes() {
            vm.observation.saveNotice = 'Saving...';

            if (promptSaveNotes) {
                $timeout.cancel(promptSaveNotes);
            }

            promptSaveNotes = $timeout(function () {
                return saveConfNotesInner().then(function() {
                    vm.observation.saveNotice = 'All changes saved';
                });
            }, 1000);
        }

        function saveConfNotesInner() {
            if (vm.promptType === enums.PromptType.PreConf) {
                return observationService.saveObserveNotes(vm.observation.id, vm.observation.evaluateePreConNotes, 'evaluatorPreConNotes');
            }
            else {
                return observationService.saveObserveNotes(vm.observation.id, vm.observation.evaluateePostConNotes, 'evaluatorPostConNotes');
            }
        }


    }
})();
