/// <reference path="../../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('conferenceTeeController', conferenceTeeController);

    conferenceTeeController.$inject = ['$state', 'enums',
        'observationService', 'userPromptService', 'activeUserContextService', 'utils',
        'observation', '$uibModal', 'userService', '$timeout', 'evidenceCollection'
    ];

    /* @ngInject */
    function conferenceTeeController($state, enums,
        observationService, userPromptService, activeUserContextService, utils,
        observation, $uibModal, userService, $timeout, evidenceCollection) {

        var vm = this;

        vm.evidenceCollection = evidenceCollection;
        vm.observation = evidenceCollection.observation;
        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
        vm.userPromptResponses = [];
        vm.isObservationReadOnly = activeUserContextService.context.observationConferenceIsReadOnly(vm.observation);

        vm.promptType = $state.current.name.indexOf('pre') === 0 ? enums.PromptType.PreConf : enums.PromptType.PostConf;
        vm.conferenceTitle = vm.promptType === enums.PromptType.PreConf ? "Pre-Conference" : "Post-Conference";
        vm.evaluatorNotesExist = false;
        vm.locked = false;
        vm.promptsSent = true;

        ////////////////////////////

        vm.saveConfNotes = saveConfNotes;
        vm.sendPrompts = sendPrompts;
        vm.savePrompt = savePrompt;
        vm.getEvaluatorConfNotes = getEvaluatorConfNotes;
        vm.shareConfNotes = shareConfNotes;
        vm.shareFinalNotes = shareFinalNotes;
        vm.saveFinalNotes = saveFinalNotes;

        function activate() {

            var promptsReady = false;
            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfQsTorSentDate ||
                vm.promptType === enums.PromptType.PostConf && vm.observation.postConfQsTorSentDate) {
                promptsReady = true;
            }

            if (promptsReady) {
                userPromptService.getObservationUserPromptResponses(vm.observation.id, vm.promptType).then(function (userPromptResponses) {
                    vm.userPromptResponses = userPromptResponses;
                    vm.userPromptResponses.forEach(function(userPromptResponse) {
                        if (userPromptResponse.coded) {
                            userPromptResponse.response = utils.getTextWithoutCoding(userPromptResponse.response);
                        }
                    })
                });
            }

            setupInitialStateVars();
        }

        function setupInitialStateVars() {
            vm.pre = (vm.promptType === enums.PromptType.PreConf);
            vm.evaluatorNotesExist = vm.promptType === enums.PromptType.PreConf ?
                (vm.observation.evaluatorPreConNotes) :
                (vm.observation.evaluatorPostConNotes);

            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfQsTorSentDate) {
                vm.promptsSentByTor = true;
                vm.confQsTorSentDate = vm.observation.preConfQsTorSentDate;
            }
            else if (vm.promptType === enums.PromptType.PostConf && vm.observation.postConfQsTorSentDate) {
                vm.promptsSentByTor = true;
                vm.confQsTorSentDate = vm.observation.postConfQsTorSentDate;
            }

            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfQsTeeSentDate) {
                vm.promptsSentByTee = true;
                vm.confQsTeeSentDate = vm.observation.preConfQsTeeSentDate;
            }
            else if (vm.promptType === enums.PromptType.PostConf && vm.observation.postConfQsTeeSentDate) {
                vm.promptsSentByTee = true;
                vm.confQsTeeSentDate = vm.observation.postConfQsTeeSentDate;
            }

            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfNotesTeeSentDate) {
                vm.notesSentByTee = true;
                vm.confNotesSentDate = vm.observation.preConfNotesTeeSentDate;
                vm.confNotesCoded = utils.containsCoding(vm.observation.evaluateePreConNotes);
                if (vm.confNotesCoded) {
                    vm.observation.evaluateePreConNotes = utils.getTextWithoutCoding(vm.observation.evaluateePreConNotes);
                }
            }
            else if (vm.promptType === enums.PromptType.PostConf && vm.observation.postConfNotesTeeSentDate) {
                vm.notesSentByTee = true;
                vm.confNotesSentDate = vm.observation.postConfNotesTeeSentDate;
                vm.confNotesCoded = utils.containsCoding(vm.observation.evaluateePostConNotes);
                if (vm.confNotesCoded) {
                    vm.observation.evaluateePostConNotes = utils.getTextWithoutCoding(vm.observation.evaluateePostConNotes);
                }
            }

            if (vm.promptType === enums.PromptType.PostConf && vm.observation.evaluateeFinalNotesSentDateTime) {
                vm.finalNotesSentByTee = true;
                vm.finalNotesSentDate = vm.observation.evaluateeFinalNotesSentDateTime;
            }

            if (vm.promptType == enums.PromptType.PreConf && vm.observation.evaluatorPreConNotes && utils.containsCoding(vm.observation.evaluatorPreConNotes)) {
                vm.observation.evaluatorPreConNotes = utils.getTextWithoutCoding(vm.observation.evaluatorPreConNotes);
            }
            else if (vm.promptType == enums.PromptType.PostConf && vm.observation.evaluatorPostConNotes && utils.containsCoding(vm.observation.evaluatorPostConNotes)) {
                vm.observation.evaluatorPostConNotes = utils.getTextWithoutCoding(vm.observation.evaluatorPostConNotes);
            }
        }

        activate();

        function getEvaluatorConfNotes() {
            return vm.promptType === enums.PromptType.PreConf ? vm.observation.evaluatorPreConNotes : vm.observation.evaluatorPostConNotes;
        }

        function sendPrompts() {

            if (activeUserContextService.user.showConfShareConfirm) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/observation/views/share-conf-responses-modal.html',
                    controller: 'shareConfResponsesModalController as vm',
                    resolve: {
                        type: 1
                    }
                });

                modalInstance.result.then(function (result) {
                    if (!result.showNextTime) {
                        userService.updateShowConfShareConfirm(activeUserContextService, false).then(function() {
                        });
                    }
                    sendPromptsInner();
                });
            }
            else {
                sendPromptsInner();
            }
        }

        function sendPromptsInner() {
            return observationService.teeSharePrompts(vm.observation, vm.promptType).then(function() {
                setupInitialStateVars();
            });
        }

        function shareConfNotes() {

            if (activeUserContextService.user.showConfShareConfirm) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/observation/views/share-conf-responses-modal.html',
                    controller: 'shareConfResponsesModalController as vm',
                    resolve: {
                        type: 2
                    }
                });

                modalInstance.result.then(function (result) {
                    if (!result.showNextTime) {
                        userService.updateShowConfShareConfirm(activeUserContextService, false).then(function() {
                        });
                    }
                    shareConfNotesInner();
                });
            }
            else {
                shareConfNotesInner();
            }
        }

        function shareConfNotesInner() {

            return observationService.teeShareConfNotes(vm.observation, vm.promptType).then(function() {
                setupInitialStateVars();
            });
        }

        function shareFinalNotes() {

            if (activeUserContextService.user.showConfShareConfirm) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/observation/views/share-conf-responses-modal.html',
                    controller: 'shareConfResponsesModalController as vm',
                    resolve: {
                        type: 3
                    }
                });

                modalInstance.result.then(function (result) {
                    if (!result.showNextTime) {
                        userService.updateShowConfShareConfirm(activeUserContextService, false).then(function() {
                        });
                    }
                    shareFinalNotesInner();
                });
            }
            else {
                shareFinalNotesInner();
            }
        }

        function shareFinalNotesInner() {

            return observationService.teeShareFinalNotes(vm.observation, vm.promptType).then(function() {
                setupInitialStateVars();
            });
        }

        var promptSaveTimer = null;

        function savePrompt(prompt) {
            vm.observation.saveNotice = 'Saving...';

            if (promptSaveTimer) {
                $timeout.cancel(promptSaveTimer);
            }

            promptSaveTimer = $timeout(function () {
                return userPromptService.saveObservationUserPromptResponse(prompt).then(function() {
                    vm.observation.saveNotice = 'All changes saved';
                });
            }, 1000);
        }

        var promptSaveNotes = null;

        function saveConfNotes() {
            vm.observation.saveNotice = 'Saving...';

            if (promptSaveNotes) {
                $timeout.cancel(promptSaveNotes);
            }

            promptSaveNotes = $timeout(function () {
                return saveConfNotesInner().then(function() {
                    vm.observation.saveNotice = 'All changes saved';
                });
            }, 1000);
        }


        var finalNotesSaveTimer = null;

        function saveFinalNotes() {
            vm.observation.saveNotice = 'Saving...';

            if (finalNotesSaveTimer) {
                $timeout.cancel(finalNotesSaveTimer);
            }

            finalNotesSaveTimer = $timeout(function () {
                return observationService.saveObserveNotes(vm.observation.id, vm.observation.evaluateeFinalNotes, 'evaluateeFinalNotes')
                    .then(() => {
                        vm.observation.saveNotice = 'All changes saved';
                    });
            }, 1000);
        }

        function saveConfNotesInner() {
            if (vm.promptType === enums.PromptType.PreConf) {
                return observationService.saveObserveNotes(vm.observation.id, vm.observation.evaluateePreConNotes, 'evaluateePreConNotes');
            }
            else {
                return observationService.saveObserveNotes(vm.observation.id, vm.observation.evaluateePostConNotes, 'evaluateePostConNotes');
            }
        }
    }

})();