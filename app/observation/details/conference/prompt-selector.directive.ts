﻿/// <reference path="../observation.module.ts" />

(function () {
    'use strict';
    angular.module('stateeval.observation')
        .directive('promptSelector', promptSelectorDirective)
        .controller('promptSelectorController', promptSelectorController);

    promptSelectorDirective.$inject = [];
    promptSelectorController.$inject = ['activeUserContextService', 'userPromptService',
        'config', '_', '$stateParams', 'enums', '$confirm', '$uibModal'];

    function promptSelectorDirective() {
        return {
            restrict: 'E',
            scope: {
                promptType: '=',
                sendPromptsFcn: '=',
                observation: '='
            },
            templateUrl: 'app/observation/details/conference/prompt-selector.html',
            controller: 'promptSelectorController as vm',
            bindToController:true
        }
    }

    function promptSelectorController(activeUserContextService, userPromptService,
              config, _, $stateParams, enums, $confirm, $uibModal) {

        var vm = this;

        vm.observationId = parseInt($stateParams.observationId);
        vm.assigendUserPromptResponses = [];
        vm.selectedPrompt = {};
        vm.selectedPromptAssigned = {};
        vm.promptsSentByTor = false;
        vm.promptsSentByTee = false;
        vm.prompt = '';
        vm.evaluateeTermUpperCase = activeUserContextService.getEvaluateeTermUpperCase();
        vm.evaluateeTermLowerCase = activeUserContextService.getEvaluateeTermLowerCase();
        vm.addToBank = false;
        vm.observationIsReadOnly = activeUserContextService.context.observationIsReadOnly(vm.observation);

        vm.selectPromptRow = selectPromptRow;
        vm.selectPromptAssignedRow = selectPromptAssignedRow;
        vm.assign = assign;
        vm.unAssign = unAssign;
        vm.sendPrompts = sendPrompts;
        vm.editPrompt = editPrompt;
        vm.addNewPrompt = addNewPrompt;
        vm.savePrompt = savePrompt;
        vm.cancelAddPrompt = cancelAddPrompt;
        vm.isAssigned = isAssigned;
        vm.cancelEditPrompts = cancelEditPrompts;

        activate(true);

        function activate(checkEditMode) {

            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfQsTorSentDate) {
                vm.promptsSentByTor = true;
                vm.confQsTorSentDate = vm.observation.preConfQsTorSentDate;
            }
            else if (vm.promptType === enums.PromptType.PostConf && vm.observation.postConfQsTorSentDate) {
                vm.promptsSentByTor = true;
                vm.confQsTorSentDate = vm.observation.postConfQsTorSentDate;
            }

            if (vm.promptType === enums.PromptType.PreConf && vm.observation.preConfQsTeeSentDate) {
                vm.promptsSentByTee = true;
                vm.confQsTeeSentDate = vm.observation.preConfQsTeeSentDate;
            }

            if (checkEditMode) {
                if (vm.observationIsReadOnly) {
                    vm.editMode = false;
                }
                else {
                    vm.editMode = !vm.promptsSentByTor;
                }
            }

            userPromptService.getAssignableUserPrompts(vm.promptType).then(function (userPrompts) {
                vm.userPrompts = userPrompts;

                userPromptService.getObservationUserPromptResponses(vm.observationId, vm.promptType).then(function (userPromptResponses) {
                    vm.assigendUserPromptResponses = userPromptResponses;
                    vm.assigendUserPromptResponses.forEach(function (promptResponse) {
                        var prompt = _.find(vm.userPrompts, { id: promptResponse.userPromptId });
                        promptResponse.definedBy = prompt ? prompt.definedBy : 'Observation';
                    });
                    vm.userPrompts = _.reject(vm.userPrompts, function(prompt) {
                        return _.any(vm.assigendUserPromptResponses, {userPromptId: prompt.id});
                    });
                });
            });
        }

        function isAssigned(currentPromptId) {
            var out = _.findWhere(vm.assigendUserPromptResponses, { userPromptId: currentPromptId });
            if (out)
                return true;
            else
                return false;
        }

        function assign() {
            var item = vm.selectedPrompt;
            if (_.findWhere(vm.assigendUserPromptResponses, { userPromptId: item.id })) {
                return;
            }

            if (item) {
                userPromptService.assignPrompt(item.id, vm.observationId).then(function (data) {
                    vm.selectedPrompt = {};


                    activate(false);
                });
            }
        }

        function unAssign() {
            var item = vm.selectedPromptAssigned;
            if (item) {
                if (item.private) {
                    userPromptService.deleteUserPrompt(item.userPromptId).then(function() {
                        vm.selectedPromptAssigned = {};
                        activate(false);
                    });
                }
                else {
                    userPromptService.unassignPrompt(item.userPromptId, vm.observationId).then(function (data) {
                        vm.selectedPromptAssigned = {};
                        activate(false);
                    });
                }
            }
        }

        function selectPromptRow(userPrompt) {
            vm.selectedPrompt = userPrompt;
        }

        function selectPromptAssignedRow(assignedPrompt) {
            vm.selectedPromptAssigned = assignedPrompt;
        }

        function savePrompt() {
            if (vm.prompt === '') {
                return;
            }

            // we have a userpromptreponse, not a prompt, so we have to retrieve it first
            // todo: or we could make a special call to just update the prompt, given the id
            if (vm.selectedPromptAssigned) {
                userPromptService.getUserPromptById(vm.selectedPromptAssigned.userPromptId).then(function(prompt) {
                    prompt.prompt = vm.prompt;
                    if (vm.addToBank) {
                        prompt.observationId = null;
                        prompt.private = false;
                    }
                    userPromptService.saveUserPrompt(prompt).then(function() {
                        vm.showNewPromptArea = false;
                        activate(false);
                    })
                })
            }
            else {
                userPromptService.insertNewConfPrompt(vm.addToBank, vm.observationId, vm.promptType, vm.prompt).then(function () {
                    vm.showNewPromptArea = false;
                    activate(false);
                });
            }
        }

        function editPrompt(prompt) {
            vm.selectedPromptAssigned = prompt;
            vm.prompt = prompt.prompt;
            vm.showNewPromptArea = true;
            vm.addToBank = false;
        }

        function cancelAddPrompt() {
            vm.prompt = '';
            vm.showNewPromptArea = false;
        }

        function cancelEditPrompts() {
            vm.editMode = false;
        }

        function addNewPrompt() {
            vm.prompt = '';
            vm.selectedPromptAssigned = null;
            vm.showNewPromptArea = true;
            vm.addToBank = false;
        }

        function sendPrompts() {
            if (vm.promptsSentByTor) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'app/observation/views/resend-conf-prompts-modal.html',
                    controller: 'resendConfPromptsModalController as vm'
                });

                modalInstance.result.then(function (result) {
                    vm.sendPromptsFcn(true,  result.comment).then(function() {
                        activate(true);
                    });
                });
            }
            else {
                vm.sendPromptsFcn(false, '').then(function () {
                    activate(true);
                });
            }
        }
    }
})();


