/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('sendFinalModalController', sendFinalModalController);

    sendFinalModalController.$inject = ['$uibModalInstance', 'enums', 'activeUserContextService',
        'observationService', 'observation'];

    /* @ngInject */
    function sendFinalModalController($uibModalInstance, enums,  activeUserContextService,
         observationService, observation) {

        var vm = this;
        vm.observation = observation;
        vm.readyToSubmit = false;

        activate();

        function activate() {
            vm.evaluateeTermUpperCase = activeUserContextService.getEvaluateeTermUpperCase();
            vm.evaluateeTermLowerCase = activeUserContextService.getEvaluateeTermLowerCase();
            vm.readyToSubmit = observationIsReadyToSubmit();
        }

        function observationIsReadyToSubmit() {
            return vm.observation.observeStartDate && vm.observation.observeDuration;
        }

        vm.ok = ok;
        vm.cancel = cancel;

        /////////////////////////////////

        function ok() {

            $uibModalInstance.close({
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

