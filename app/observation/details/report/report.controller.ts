/// <reference path="../../observation.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.observation')
        .controller('observationReportController', observationReportController);

    observationReportController.$inject = ['enums', 'activeUserContextService', 'evidenceCollection',
        'observationService', 'reportService', '$state'];

    function observationReportController(enums, activeUserContextService, evidenceCollection,
         observationService, reportService, $state) {

        var vm = this;
        vm.evidenceCollection = evidenceCollection;
        vm.observation = evidenceCollection.observation;
        vm.enums = enums;
        vm.context = activeUserContextService.context;
        vm.previewReportFormatType = vm.enums.ReportFormatTypeEnum.SUMMARY;
        vm.finalReportFormatType = vm.enums.ReportFormatTypeEnum.FINAL;
        vm.fileUrl = null;
        vm.pdfLoading = false;

        ///////////////////////////////

        vm.changeReportFormat = changeReportFormat;
        vm.createPDF = createPDF;
        vm.downloadPDF = downloadPDF;
        vm.acknowledgeFinalVersion = acknowledgeFinalVersion;

        activate();

        function activate() {

            vm.evaluateeTermUC = activeUserContextService.getEvaluateeTermUpperCase();
            vm.evaluateeTermLC = activeUserContextService.getEvaluateeTermLowerCase();
            vm.isEvaluatee = activeUserContextService.context.isEvaluatee();
            vm.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();
            vm.obseravtionIsReadOnly = activeUserContextService.context.observationIsReadOnly(vm.observation);
        }

        function acknowledgeFinalVersion() {
            observationService.acknowledgeFinalReview(this.observation).then( () => {
                $state.go(this.$state.current, [], {reload: true});
            });
        }

        function downloadPDF() {
            var fileName = "test.pdf";
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            reportService.getObservationReport(vm.observation).then(function (data) {
                var file = new Blob([data], {type: 'application/pdf'});
                a.href = window.URL.createObjectURL(file);;
                a.download = fileName;
                vm.pdfLoading = false;
                a.click();
            });
        }

        function createPDF() {
            vm.pdfLoading = true;
            var element = angular.element('observation-report-output');
            console.log(element);
            var start =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                    '<title>eVAL</title>' +
                    '<link rel="stylesheet" href="' + window.location.origin + '/css/vendor.css" />' +
                    '<link rel="stylesheet" href="' + window.location.origin + '/css/main.css" />' +
                '</head>' +
                '<body style=\'background-color:white\'>';

            var end =
                '</body>' +
                '</html>';

            var html = start + element.html() + end;

            console.log(html);
            //reportService.saveObservationReport(this.observation, html).then((pdfBytes) => {
            //    downloadPDF();
            //})
        }

        function changeReportFormat()
        {
            observationService.saveReportFormat(vm.observation).then(() => {

            });
        }
    }

})();