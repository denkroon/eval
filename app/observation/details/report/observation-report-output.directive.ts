(function () {
    'use strict';

    function observationReportOutputDirective() {
        return {
            restrict: 'E',
            scope: {
                evidenceCollection: '=',
                reportFormat: '=',
                preview: '='
            },
            templateUrl: 'app/observation/details/report/observation-report-output.directive.html',
            controller: 'observationReportOutputController as vm',
            bindToController: true
        }
    }

    class ObservationReportOutputController {

        observationFocusDisplayName: string;
        planTypeDisplayName: string;
        planType: string;
        evaluatee: any;
        evaluatorDisplayName: string;
        evalFocused: boolean;
        schoolYear: string;
        district: string;
        school: string;
        rawScore: string;
        summativeScore: string;
        evidenceCollection: any;
        observation: any;
        evaluateeTermUC: string;
        evaluateeTermLC: string;
        isEvaluatee: boolean;
        isAssignedEvaluator: boolean;
        stateFramework: any;
        instructionalFramework: any;
        reportTitle: string;
        preview: boolean;
        observationFinalOptionReportFormats: [any];
        reportFormat: any;
        reportOptions: [any];
        includePreConfPrompts: boolean;
        includePostConfPrompts: boolean;
        preConfPromptResponses: [any];
        postConfPromptResponses: [any];
        evaluateePreConfNotes: string;
        evaluateePostConfNotes: string;
        evaluatorPreConfNotes: string;
        evaluatorPostConfNotes: string;

        constructor(private activeUserContextService: any,
                    private utils: any,
                    private evidenceCollectionService: any,
                    private rubricUtils: any,
                    public enums: any,
                    private reportService: any,
                    private _: _.LoDashStatic,
                    private userPromptService: any,
                    $scope: ng.IScope
        ) {

            this.observation = this.evidenceCollection.observation;

            var reportTitle = activeUserContextService.context.frameworkContext.districtConfiguration.observationReportTitle;
            if (this.observation.wfState === this.enums.WfState.OBS_IN_PROGRESS_TOR) {
                this.reportTitle = reportTitle + " (Preview)";
            }
            else {
                this.reportTitle = reportTitle;
            }

            this.evaluateeTermUC = activeUserContextService.getEvaluateeTermUpperCase();
            this.evaluateeTermLC = activeUserContextService.getEvaluateeTermLowerCase();
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();
            this.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();

            this.observationFocusDisplayName = this.observation.focused? this.planTypeDisplayName:"Focus Unlocked";
            this.planType = activeUserContextService.context.evaluatee.evalData.planType;
            this.evaluatee = activeUserContextService.context.evaluatee;

            this.planTypeDisplayName = utils.mapEvaluationPlanTypeForUserToString(activeUserContextService, this.evaluatee, false);
            this.evaluatorDisplayName = activeUserContextService.getEvaluatorDisplayName();
            this.evalFocused = this.planType === enums.EvaluationPlanType.FOCUSED;

            this.schoolYear =  utils.getSchoolYear(activeUserContextService.context);
            this.district = activeUserContextService.context.orientation.districtName;
            // todo: if this is a dte then we have to figure out the school from the dte's request
            this.school = activeUserContextService.context.orientation.schoolName;
            this.rawScore = evidenceCollectionService.getObservationRawScore(this.observation);
            this.summativeScore = rubricUtils.mapPerformanceLevelToDisplayString(this.evidenceCollection.score.performanceLevel);

            $scope.$watch('vm.reportFormat', (val: any) => {
                this.buildReport();
            });
        }

        loadUserPromptResponses(promptType) {
            return this.userPromptService.getObservationUserPromptResponses(this.observation.id, promptType)
                .then((responses) => {
                    var answeredResponses = [];
                    for (var i=0; i<responses.length; ++i) {
                        var response = responses[i];
                        if (response.response) {
                            response.response =
                                this.utils.getTextWithoutCoding(response.response);
                            answeredResponses.push(response);
                        }
                    }
                    return answeredResponses;
            });
        }

        setReportOption(optionName) {
            this.reportOptions[optionName] =  this.reportOptionEnabled(optionName);
        }

        buildReport() {
            this.reportService.getReportOptionReportFormats(this.enums.ReportTypeEnum.OBSERVATION, this.reportFormat)
                .then((observationFinalOptionReportFormats) => {
                    this.observationFinalOptionReportFormats = observationFinalOptionReportFormats;
                    this.reportOptions = [];

                    this.setReportOption(this.enums.ObservationReportOptions.INSTRUCTIONAL_VIEW);
                    this.setReportOption(this.enums.ObservationReportOptions.STATE_VIEW);

                    this.stateFramework = this.activeUserContextService.context.frameworkContext.stateFramework;

                    if (this.reportOptions[this.enums.ObservationReportOptions.INSTRUCTIONAL_VIEW]) {
                        this.instructionalFramework = this.activeUserContextService.context.frameworkContext.instructionalFramework;
                    }

                    this.setReportOption(this.enums.ObservationReportOptions.PRE_CONFERENCE);
                    this.setReportOption(this.enums.ObservationReportOptions.POST_CONFERENCE);
                    this.setReportOption(this.enums.ObservationReportOptions.OBSERVATION_NOTES);
                    this.setReportOption(this.enums.ObservationReportOptions.AVAILABLE_EVIDENCE);
                    this.setReportOption(this.enums.ObservationReportOptions.ALIGNED_EVIDENCE);

                    if (this.reportOptions[this.enums.ObservationReportOptions.PRE_CONFERENCE]) {
                        if (this.observation.evaluateePreConNotes) {
                            this.evaluateePreConfNotes =
                                this.utils.getTextWithoutCoding(this.observation.evaluateePreConNotes);
                        }
                        if (this.observation.evaluatorPreConNotes) {
                            this.evaluatorPreConfNotes =
                                this.utils.getTextWithoutCoding(this.observation.evaluatorPreConNotes);
                        }
                        if (this.observation.preConfQsTorSentDate) {
                            this.includePreConfPrompts = true;
                            this.loadUserPromptResponses(this.enums.PromptType.PreConf).then((responses) => {
                                this.preConfPromptResponses = responses;
                            });
                        }
                    }

                    if (this.reportOptions[this.enums.ObservationReportOptions.POST_CONFERENCE]) {
                        if (this.observation.evaluateePreConNotes) {
                            this.evaluateePostConfNotes =
                                this.utils.getTextWithoutCoding(this.observation.evaluateePostConNotes);
                        }
                        if (this.observation.evaluatorPostConNotes) {
                            this.evaluatorPostConfNotes =
                                this.utils.getTextWithoutCoding(this.observation.evaluatorPostConNotes);
                        }
                        if (this.observation.postConfQsTorSentDate) {
                            this.includePostConfPrompts = true;
                            this.loadUserPromptResponses(this.enums.PromptType.PostConf).then((responses) => {
                                this.postConfPromptResponses = responses;
                            });
                        }
                    }
                });
        }

        reportOptionEnabled(name) {
            var match = _.find(this.observationFinalOptionReportFormats, (optionFormat) => {
                return (optionFormat.reportOption.optionName === name && optionFormat.enabled);
            });

            return match != null;
        }

    }

    angular.module('stateeval.observation')
        .directive('observationReportOutput', observationReportOutputDirective)
        .controller('observationReportOutputController', ObservationReportOutputController);

})();
