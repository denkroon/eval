/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('acknowledgeModalController', acknowledgeModalController);

    acknowledgeModalController.$inject = ['$uibModalInstance'];

    /* @ngInject */
    function acknowledgeModalController($uibModalInstance) {

        var vm = this;
        vm.notes = '';

        vm.ok = ok;
        vm.cancel = cancel;

        /////////////////////////////////

        function ok() {

            $uibModalInstance.close({
                notes: vm.notes
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

