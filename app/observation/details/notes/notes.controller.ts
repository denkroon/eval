/// <reference path="../../observation.module.ts" />

(function () {
    'use strict';

    interface CustomScope extends ng.IScope {
        noHelperDefault: boolean
    }

    class observationNotesController {

        isEvaluator: boolean;
        notes: string = '';
        showHelper: boolean = true;
        observation: any;
        isObservationReadOnly: boolean;

        constructor(
            public evidenceCollection: any,
            public enums: any,
            private activeUserContextService: any,
            private utils: any,
            private $scope: CustomScope,
            private $state: ng.ui.IStateService
        )
        {
            this.observation = evidenceCollection.observation;
            this.isEvaluator = this.observation.evaluatorId === activeUserContextService.user.id;
            $scope.noHelperDefault = true;

            if (this.evidenceCollection.request.collectionType === enums.EvidenceCollectionType.OBSERVATION &&
                activeUserContextService.context.isEvaluatee()) {

                if (!this.evidenceCollection.observation.evaluatorScoresShared &&
                    !this.evidenceCollection.observation.selfEvalShared) {
                    this.showHelper = false;
                }
            }

            this.isObservationReadOnly = activeUserContextService.context.observationIsReadOnly(this.observation);
            if (this.observation.observeNotes) {
                // used when page is read-only
                if (!this.isEvaluator) {
                    this.notes = utils.getTextWithoutCoding(this.observation.observeNotes);
                }
                else {
                    // read-only view for evaluator
                    this.notes = utils.getResponseWithoutRemoveCodeSelection(this.observation.observeNotes);
                }
            }
        }

        redirect(node, row) {
            this.$state.go('observation-align', {nodeShortName: node.data.shortName, rowId: row && row.data.id});
        }
    }

    angular
        .module('stateeval.observation')
        .controller('observationNotesController', observationNotesController);
})();