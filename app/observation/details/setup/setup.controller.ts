/// <reference path="../../observation.module.ts" />

namespace StateEval.Observation {

    export class ObservationSetupController {

        dateRegex = '((19|[2-9][0-9])\\d\\d-(((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-8]))|((0[13578]|1[02])-(29|30|31))|((0[4,6,9]|11)-(29|30))))|((19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)-02-29)';

        isEvaluatee: boolean;
        isAssignedEvaluator: boolean;
        isObservationReadOnly: boolean;

        title: string;

        constructor(
            private $state: ng.ui.IStateService,
            private $confirm: any,
            private observationService: any,
            private _: _.LoDashStatic,
            public observation: any,
            public evidenceCollection: any,
            activeUserContextService: any,
            $scope: ng.IScope,
            private $confirm: any
        ) {
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();
            this.isAssignedEvaluator = activeUserContextService.context.isAssignedEvaluator();
            this.observation = evidenceCollection.observation;
            this.isObservationReadOnly = activeUserContextService.context.observationSetupIsReadOnly(this.observation);

            this.initializeStartTimes();

            $scope.$watch('vm.observation.title', (val: string) => {
                this.title = val;
            });
        }

        titleFocused() {
            this.title = '';
        }

        titleBlurred() {
            if (!this.title.length) {
                this.title = this.observation.title;
            }
        }

        titleChanged() {
            this.observation.title = this.title;
            this.saveObservation();
        }

        addPreConf() {
            this.$confirm({
                    title: "Pre-conference",
                    ok: "Yes, add Pre-conference",
                    cancel: "Cancel"
                },
                {
                    templateUrl: 'app/observation/views/confirm-add-conf.html'
                })
                .then(() => {
                    this.observation.preConfIsActive = true;
                    this.saveObservation().then(() => {
                        this.$state.go(this.$state.current, {}, { reload: true });
                    })
                });
        }

        addPostConf() {
            this.$confirm({
                    title: "Post-conference",
                    ok: "Yes, add Post-conference",
                    cancel: "Cancel"
                },
                {
                    templateUrl: 'app/observation/views/confirm-add-conf.html'
                })
                .then(() => {
                    this.observation.postConfIsActive = true;
                    this.saveObservation().then(() => {
                        this.$state.go(this.$state.current, {}, { reload: true });
                    })
                });
        }

        saveObservation() {
            return this.observationService.saveObservation(this.observation);
         }

        deleteObservation() {
            this.$confirm(
                {
                }, {templateUrl: 'app/observation/details/setup/_delete-observation.html'})
                .then(() => {
                    this.observationService.deleteObservation(this.observation)
                        .then(() => { this.$state.go("observation-list"); });
                })
        }

        private initializeStartTimes() {
            this._.forEach(['observeStartDate', 'preConfStartDate', 'postConfStartDate', 'observeStartTime', 'preConfStartTime', 'postConfStartTime'],
                (prop) => {
                    if (this.observation[prop]) {
                        this.observation[prop] = new Date(this.observation[prop]);
                    }
                });
        }

    }

    angular
        .module('stateeval.observation')
        .controller('observationSetupController', ObservationSetupController);

}