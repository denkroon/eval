(function () {
    'use strict';

    angular.module('stateeval.observation')
        .controller('observationArtifactViewController', observationArtifactViewController);

    observationArtifactViewController.$inject = ['artifact'];
    function observationArtifactViewController(artifact) {
        var vm = this;
        vm.artifact = artifact;
        vm.back = {state: 'observation-artifacts'};

    }
})();