/// <reference path="../../observation.module.ts" />

(function () {
    'use strict';

    class ObservationArtifactsController {
        observation;
        context;

        constructor(public enums,
                    observation,
                    activeUserContextService,
                    public $state) {
            this.observation = observation;
            this.context = activeUserContextService.context;
        }

        redirect = (nodeShortName, rowId) => {
            this.$state.go('observation-align', {nodeShortName: nodeShortName, rowId: rowId});
        };

        viewArtifact = (artifact) => {
            this.$state.go('observation-artifact-view', {artifactId: artifact.id});
        }
    }

    angular.module('stateeval.observation')
        .controller('observationArtifactsController', ObservationArtifactsController);

})();