/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.observation')
        .controller('shareObservationModalController', shareObservationModalController);

    shareObservationModalController.$inject = ['$uibModalInstance', 'activeUserContextService',
        'observationService', 'observation'];

    /* @ngInject */
    function shareObservationModalController($uibModalInstance, activeUserContextService,
         observationService, observation) {

        var vm = this;
        vm.observation = observation;

        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();

        vm.notes = '';
        vm.notify = true;
        vm.shareOptions = observationService.newShareOptions(vm.observation.id);
        vm.shareOptions.observationNotesShared = vm.observation.observeNotesShared;
        vm.shareOptions.evaluatorScoresShared = vm.observation.evaluatorScoresShared;
        vm.shareOptions.selfEvalShared = vm.observation.selfEvalShared;
        vm.shareOptions.notifyEvaluatee = false;
        vm.shareOptions.message = '';

        vm.save = save;
        vm.cancel = cancel;

        /////////////////////////////////

        function save() {

            observationService.updateShareOptions(vm.shareOptions).then(function() {
                vm.observation.observeNotesShared = vm.shareOptions.observationNotesShared;
                vm.observation.evaluatorScoresShared = vm.shareOptions.evaluatorScoresShared;
                vm.observation.selfEvalShared = vm.shareOptions.selfEvalShared;
                $uibModalInstance.close({ });
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();

