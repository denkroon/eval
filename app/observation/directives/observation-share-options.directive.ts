(function () {
    'use strict';

    angular.module('stateeval.observation')
        .directive('observationShareOptions', observationShareOptionsDirective)
        .controller('observationShareOptionsController', observationShareOptionsController)

    function observationShareOptionsDirective() {
        return {
            restrict: 'E',
            scope: {
                observation: '='
            },
            templateUrl: 'app/observation/views/observation-share-options.directive.html',
            controller: 'observationShareOptionsController as vm',
            bindToController: true
        }
    }

    observationShareOptionsController.$inject = ['activeUserContextService'];

    function observationShareOptionsController(activeUserContextService) {
        var vm = this;
        vm.shown = false;
        vm.context = activeUserContextService.context;
        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
        vm.toggle = toggle;
        vm.save = save;

        function toggle() {
            vm.shown = !vm.shown;
        }

        function save() {
             vm.shown = false;
        }
    }

})();
