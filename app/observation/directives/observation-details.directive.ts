/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.observation')
        .directive('observationDetails', observationDetailsDirective)
        .controller('observationDetailsController', observationDetailsController);

    observationDetailsController.$inject = ['activeUserContextService', 'utils', 'enums', '$scope'];

    function observationDetailsDirective() {
        return {
            scope: {
                observation: '='
            },
            templateUrl: 'app/observation/views/observation-details.directive.html',
            controller: 'observationDetailsController as vm',
            bindToController: true
        }
    }

    function observationDetailsController(activeUserContextService, utils, enums, $scope) {
        var vm = this;
        vm.enums = enums;

        $scope.$watch('vm.observation.focused', function(newVal, oldVal) {
            setObservationFocusDisplayName();
        });

        activate();

        function activate() {
            vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
            vm.evaluatee = activeUserContextService.context.evaluatee;
            vm.planType = activeUserContextService.context.evaluatee.evalData.planType;
            vm.planTypeDisplayName = utils.mapEvaluationPlanTypeForUserToString(activeUserContextService, vm.evaluatee, false);
            vm.evaluatorDisplayName = activeUserContextService.getEvaluatorDisplayName();
            vm.isEvaluatee = activeUserContextService.context.isEvaluatee();
            vm.evalFocused = vm.planType === enums.EvaluationPlanType.FOCUSED;
            setObservationFocusDisplayName();
         }

        function setObservationFocusDisplayName() {
            vm.observationFocusDisplayName = vm.observation.focused? vm.planTypeDisplayName:"<span class='label label-warning'>Focus Unlocked</span>";
        }

    }
}) ();