/// <reference path="../observation.module.ts" />

/**
 * Created by anne on 11/30/2015.
 */

(function() {
    'use strict';

    angular.module('stateeval.observation')
        .directive('observationAlignment', observationAlignmentDirective)
        .controller('observationAlignmentController', observationAlignmentController);

    function observationAlignmentDirective() {
        return {
            scope: {
                observation: '=',
                evidenceCollection: '='
            },
            templateUrl: 'app/observation/views/observation-alignment.directive.html',
            controller: 'observationAlignmentController as vm',
            bindToController: true
        }
    }

    function observationAlignmentController(activeUserContextService, $rootScope,
        rubricUtils, $scope, $confirm, utils, _) {
        var vm = this,
            flatRows;

        vm.loaded = false;
        vm.unlocked = unlocked;

        function unlocked() {
            $confirm(
                {
                    title: "Unlock Focus", ok: "Yes, I want to unlock focus", cancel: "Cancel"
                }, { templateUrl: 'app/observation/views/confirm-unlock-focus.html' })
                .then(function () {
                    vm.evidenceCollection.unlockObservationFocus();

                }, function() {
                    vm.observation.focused = true;
                });
        }

        vm.isEvaluatee = activeUserContextService.context.isEvaluatee();

        vm.readOnly = vm.isEvaluatee || activeUserContextService.context.observationIsReadOnly(vm.observation);

        $rootScope.$on('change-framework', function () {
            if (!vm.observation.focused) {
                framework = activeUserContextService.getActiveFramework();
                loadFramework();
            }
        });

        var framework = vm.observation.focused?
                            activeUserContextService.context.frameworkContext.stateFramework:
                            activeUserContextService.getActiveFramework();

        var focusNode = _.find(framework.frameworkNodes, {id: vm.observation.focusFrameworkNodeId});

        loadFramework();

        function loadFramework() {
            flatRows = rubricUtils.getFlatRows(framework);

            // massaging the data to pass it to the categorized selector directive
            vm.categories = _.map(framework.frameworkNodes, function(fn) {
                var list = [];
                for(var i in fn.rubricRows) {
                    if(utils.stripGrowthGoalsRow(fn.rubricRows[i], $scope.vm.evidenceCollection)) {
                        list.push(fn.rubricRows[i]);
                    }
                }

                return {
                    name: formatName(fn),
                    collapsed: true,
                    rows: _.map(list, function(rr) {
                        return {
                            id: rr.id,
                            name: rr.shortName + ' ' + rr.title,
                            selected: _.any(focusNode.rubricRows, { id: rr.id }),
                            inUse: false
                        };
                    })
                };
            });
        }


        function formatName(row) {
            return '<div class="pull-left">' + row.shortName + '</div>' +
                '<div class="row-title">' + row.title + '</div>';
        }

        // called by the categories selector when the selection has changed
        vm.changed = function changed(selectedRows) {
            // does nothing for an observation
        }

        $scope.$watch('vm.observation.focused', function(newVal, oldVal) {
            if (newVal !== oldVal && !newVal) {
             // todo: isn't getting called so had to craete a unlock method callback

            }
        });
    }

})();
