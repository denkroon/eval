/// <reference path="../observation.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.observation')
        .directive('observationState', observationStateDirective)
        .controller('observationStateController', observationStateController);

    observationStateController.$inject = ['activeUserContextService', 'enums', 'observationService', '$state',
    '$uibModal', '$confirm'];

    function observationStateDirective() {
        return {
            scope: {
                observation: '='
            },
            templateUrl: 'app/observation/views/observation-state.directive.html',
            controller: 'observationStateController as vm',
            bindToController: true
        }
    }

    function observationStateController(activeUserContextService, enums, observationService, $state,
        $uibModal, $confirm) {

        var vm = this;
        vm.enums = enums;

        vm.requestUnlock = requestUnlock;
        vm.removeRequest = removeRequest;
        vm.acceptRequest = acceptRequest;
        vm.declineRequest = declineRequest;

        vm.isEvaluator = vm.observation.evaluatorId === activeUserContextService.user.id;
        vm.isEvaluatee = vm.observation.evaluateeId === activeUserContextService.user.id;
        vm.evaluateeTermLC = activeUserContextService.getEvaluateeTermLowerCase();
        vm.isObservationReadOnly = activeUserContextService.context.observationIsReadOnly(vm.observation);

        function acceptRequest() {
            observationService.acceptUnlockRequest(vm.observation).then(() => {
                $state.go($state.current, [], {reload: true});
            });
        }

        function declineRequest() {
            observationService.declineUnlockRequest(vm.observation).then(() => {
                $state.go($state.current, [], {reload: true});
            });
        }

        function removeRequest() {
            observationService.removeUnlockRequest(vm.observation).then(() => {
                $state.go($state.current, [], {reload: true});
            });
        }

        function requestUnlock() {
            $confirm(
                {
                }, {templateUrl: 'app/observation/details/_unlock-request.html'})
                .then(() => {
                    observationService.requestUnlock(vm.observation).then(() => {
                        $state.go($state.current, [], {reload: true});
                    })
                });
        }

        vm.formatLockedMessage = formatLockedMessage;

        function formatLockedMessage() {
            return 'The Observation is complete and locked. Lock Date: ' + moment(vm.observation.lockDateTime).format('l') + '.';
        }
    }
}) ();