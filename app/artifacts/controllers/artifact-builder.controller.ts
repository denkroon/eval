/// <reference path="../artifacts.module.ts" />

namespace StateEval.Artifacts {

    export class ArtifactBuilderController {

        newItem = false;
        itemToEdit: any;
        itemDisplayName: string;
        evaluateeTerm: string;
        isEvaluatee: boolean;
        builderForm: any;
        origin: string;
        fromSG: boolean;
        saveNotice = 'All Changes Saved';
        warning: string;
        timer: ng.IPromise<void>;
        currentStep = 1;
        title: string;

        constructor(
            private artifactService: any,
            private $state: ng.ui.IStateService,
            private $stateParams: ng.ui.IStateParamsService,
            private $confirm: any,
            private $timeout: ng.ITimeoutService,
            private $rootScope: ng.IRootScopeService,
            public enums: any,
            public artifact: any,
            public utils: any,
            public bundle: any,
            activeUserContextService: any,
            _: _.LoDashStatic,
            $stateParams: ng.ui.IStateParamsService,
            $scope: ng.IScope
        ) {
            this.itemDisplayName = artifactService.libItemDisplayName;
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
            this.isEvaluatee = activeUserContextService.context.isEvaluatee();

            this.origin = $stateParams['artifactOrigin'] || 'artifacts';
            this.fromSG = this.origin === 'sg-goal-planning' || this.origin === 'sg-goal-results';

            $scope.$watchGroup(['vm.artifact.alignedRubricRows', 'vm.artifact.linkedStudentGrowthGoals'], () => {
                this.warning = null;
                var goals = [];
                this.artifact.linkedStudentGrowthGoals.forEach((goal: any) => {
                    if (!_.any(this.artifact.alignedRubricRows, { id: goal.processRubricRowId }) &&
                        !_.any(this.artifact.alignedRubricRows, { id: goal.resultsRubricRowId })) {
                        goals.push(goal);
                    }

                });

                if (goals.length > 0) {
                    this.warning = "For an artifact to be available to a student growth goal, it must be aligned to at " +
                        "least one of the student growth rubric components for the goal. This artifact will not be " +
                        "available to the following goals: " + _.map(goals, 'title').join(', ');
                }
            });

            $scope.$watch('vm.artifact.title', (val: string) => {
                this.title = val;
            });

            // The code below determines the current step in the checklist
            // doing this here makes the views simpler
            $scope.$watch('vm.artifact.libItems.length === 0', (val: boolean) => {
                if (val) this.currentStep = 1;
            });

            $scope.$watch('vm.artifact.libItems.length > 0 && vm.artifact.alignedRubricRows.length === 0', (val: boolean) => {
                if (val) this.currentStep = 2;
            });

            $scope.$watch('vm.artifact.alignedRubricRows.length > 0 && (vm.artifact.linkedObservations.length === 0 && vm.artifact.linkedOtherEvidenceCollections.length===0 && vm.artifact.linkedSelfAssessments.length===0 && vm.artifact.linkedStudentGrowthGoals.length===0)', (val: boolean) => {
                if (val) this.currentStep = 3;
            });

            $scope.$watch('vm.artifact.libItems.length>0 && vm.artifact.alignedRubricRows.length>0 && (vm.artifact.linkedObservations.length>0 || vm.artifact.linkedOtherEvidenceCollections.length>0 || vm.artifact.linkedSelfAssessments.length>0 || vm.artifact.linkedStudentGrowthGoals.length>0)', (val: boolean) => {
                if (val) this.currentStep = 4;
            });

            $scope.$watch('vm.artifact.isShared', (val: boolean) => {
                if (val) this.currentStep = 5;
            })

            console.log( this.artifact.title );
        }


        shareArtifact() {
            this.artifactService.shareArtifact(this.artifact).then(() => {
                this.goBack();
            })
        }

        stepClass(step: number) {
            if (step < this.currentStep) {
                return 'step-complete';
            }
            if (step == this.currentStep) {
                return 'step-active';
            }
            return 'step-inactive';
        }

        titleFocused() {
            this.title = '';
        }

        titleBlurred() {
            if (!this.title.length) {
                this.title = this.artifact.title;
            }
        }

        titleChanged() {
            this.artifact.title = this.title;
            this.autoSave();
        }

        cancelArtifact() {
            this.artifactService.deleteArtifact(this.artifact).then(() => {
                this.goBack();
            })
        }

        deleteArtifact() {
            this.$confirm({}, { templateUrl: 'app/artifacts/views/artifacts-confirm-delete.html' })
                .then(() => {
                    this.artifactService.deleteArtifact(this.artifact).then(() => {
                        this.goBack();
                    })
                });
        }

        // Has to be in lambda form to preserve the "this" context when called back by the child directive
        doneItemEdit = (item: any) => {
            if (this.newItem) {
                this.artifact.libItems.push(item);
            }
            this.itemToEdit = null;
            this.save().then(() => {
                this.artifactService.getArtifactById(this.artifact.id).then((artifact: any) => {
                    this.artifact = artifact;
                })
            })
        }

        // Has to be in lambda form to preserve the "this" context when called back by the child directive
        cancelItemEdit = () => {
            this.itemToEdit = null;
        }

        editItem(item: any) {
            this.newItem = false;
            this.itemToEdit = item;
        }

        createItem(itemType: any) {
            this.newItem = true;
            this.itemToEdit = this.artifactService.newLibItem(itemType);
        }

        removeItem(item: any) {
            this.artifact.libItems = _.reject(this.artifact.libItems, { id: item.id });
            this.save();
        }

        viewItem(item: any) {
            this.artifactService.viewItem(item);
        }

        save() {
            this.saveNotice = 'Saving...';
            return this.artifactService.saveArtifact(this.artifact).then(() => {
                this.$timeout(() => {
                    this.saveNotice = 'All changes saved';
                }, 1000);
            })
        }

        autoSave() {
            this.saveNotice = 'Saving...';

            if (this.timer) {
                this.$timeout.cancel(this.timer);
            }

            this.timer = this.$timeout(() => {
                return this.artifactService.saveArtifact(this.artifact).then(() => {
                    this.saveNotice = 'All changes saved';
                });
            }, 1000);
        }

        goBack() {
            if (this.fromSG) {
                this.$state.go(this.origin, {id: parseInt(this.$stateParams.goalId)});
            }
            else {
                this.$state.go(this.origin);
            }
        }

        saveForLater() {
            if (this.builderForm.$valid) {
                this.save().then(() => {
                    this.goBack();
                })
            }
        }

        // called by the categories selector when the selection has changed
        done = () => {
            this.saveNotice = 'Saving...';
            this.$timeout(() => {
                this.saveNotice = 'All changes saved';
            }, 1000);
        }
    }

    angular.module('stateeval.artifact')
        .controller('artifactBuilderController', ArtifactBuilderController);

}