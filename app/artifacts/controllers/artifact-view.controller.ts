/// <reference path="../artifacts.module.ts" />

/**
 * Created by anne on 12/11/2015.
 */

(function() {
    'use strict';

    angular.module('stateeval.artifact')
        .controller('artifactViewController', artifactViewController);

    artifactViewController.$inject = ['artifact', 'bundle'];

    function artifactViewController(artifact, bundle) {
        var vm = this;
        vm.artifact = artifact;
        vm.bundle = bundle;
        vm.back = {state: 'artifacts'};
    }

})();