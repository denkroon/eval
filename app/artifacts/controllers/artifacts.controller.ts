/// <reference path="../artifacts.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.artifact')
        .controller('artifactsController', artifactsController);

    artifactsController.$inject = ['artifactService', 'enums', 'utils', '$state', 'activeUserContextService'];

    function artifactsController(artifactService, enums, utils, $state, activeUserContextService) {
        var vm = this;
        vm.artifactService = artifactService;
        vm.enums = enums;
        vm.utils = utils;
        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
        vm.readOnly = activeUserContextService.context.evaluationIsReadOnly();
        vm.context = activeUserContextService.context;
        vm.artifacts = null;
        vm.alignment = [];
        vm.attachments = [];
        vm.owner = [];
        vm.itemDisplayName = artifactService.libItemDisplayName;
        vm.editingStep1 = false;

        vm.editArtifact = editArtifact;
        vm.newArtifact = newArtifact;
        vm.viewArtifact = viewArtifact;
        vm.viewItem = viewItem;

        function viewItem(item) {
            artifactService.viewItem(item);
        }

        // This is the list of filters available
        // fn is the function doing the actual filtering as explained in https://docs.angularjs.org/api/ng/filter/filter
        // it returns true for items that should be visible
        vm.filters = [
            { name: 'All', selected: true },
            {
                name: 'Evaluator',
                fn: function(value, index, array) {
                    return value.createdByUserId !== activeUserContextService.context.evaluatee.id;
                }
            },
            {
                name: vm.evaluateeTerm,
                fn: function(value, index, array) {
                    return value.createdByUserId === activeUserContextService.context.evaluatee.id;
                }
            },
            {
                name: 'Observations',
                fn: function(value, index, array) {
                    return value.linkedObservations.length>0;
                }
            },
            {
                name: 'Self-assessments',
                fn: function(value, index, array) {
                    return value.linkedSelfAssessments.length>0;
                }
            },
            {
                name: 'Student Growth Goals',
                fn: function(value, index, array) {
                    return value.linkedStudentGrowthGoals.length>0;
                }
            },
            {
                name: 'Other Evidence',
                fn: function(value, index, array) {
                    return value.linkedOtherEvidenceCollections.length>0;
                }
            }
        ];

        vm.selectedFilter = function selectedFilter() {
            return _.find(vm.filters, { selected: true }).fn || function() { return true; };
        }

        ///////////////////////////////

        activate();

        function activate() {
            var request = artifactService.newArtifactBundleRequest(activeUserContextService.user.id);

            artifactService.getArtifactBundlesForEvaluation(request).then(function(artifacts) {
                vm.artifacts = artifacts;

                loadAttachments();
                loadOwner();
            })
        }

        function loadOwner() {
            vm.artifacts.forEach(function (artifact) {
                vm.owner[artifact.id] = artifact.createdByUserId === activeUserContextService.user.id;
            })
        }

        function loadAttachment(title, attachments, linkedItems) {
            if (linkedItems.length>0) {
                if (attachments!=="") {
                    attachments+= "<br/>";
                }

                attachments+= "<b>" + title + "</b><br/>";
                for (var i=0; i<linkedItems.length; ++i) {
                    if (i>0) {
                        attachments+= "<br/>";
                    }

                    attachments += linkedItems[i].title;
                }
                attachments+=  "<br/>";
            }
            return attachments;
        }

        function loadAttachments() {
            vm.artifacts.forEach(function(artifact) {
                var attachments = "";
                attachments = loadAttachment("Observations", attachments, artifact.linkedObservations);
                attachments = loadAttachment("Self-Assessments", attachments, artifact.linkedSelfAssessments);
                attachments = loadAttachment("Student Growth Goals", attachments, artifact.linkedStudentGrowthGoals);
                attachments = loadAttachment("Other Evidence", attachments, artifact.linkedOtherEvidenceCollections);
                vm.attachments[artifact.id] = attachments;
            })
        }

        function newArtifact() {
            var artifact = artifactService.newArtifact();
            artifactService.saveArtifact(artifact).then(function() {
                $state.go('artifact-builder', {artifactId: artifact.id, artifactOrigin: $state.current.name});
            })
        }

        function editArtifact(artifact) {
            $state.go('artifact-builder', {artifactId: artifact.id, artifactOrigin: $state.current.name});
        }

        function viewArtifact(artifact) {
            $state.go('artifact-view', {artifactId: artifact.id});
        }
    }

})();