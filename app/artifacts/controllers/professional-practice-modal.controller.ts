/// <reference path="../artifacts.module.ts" />

(function() {
   'use strict';

   angular.module('stateeval.artifact')
        .controller('ProfessionalPracticeController', ProfessionalPracticeController);

        ProfessionalPracticeController.$inject = ['artifactService', 'libItem', '$uibModalInstance', 'utils'];

        function ProfessionalPracticeController(artifactService, libItem, $uibModalInstance, utils) {
            var vm = this;
            vm.item = libItem;

            vm.close = function () {
                $uibModalInstance.dismiss('cancel');
            }
        }
}) ();