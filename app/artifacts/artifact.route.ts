/// <reference path="artifacts.module.ts" />

(function () {
    'use strict';

    angular.module('stateeval.artifact')
        .config(configureRoutes);

    configureRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configureRoutes($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('artifacts', {
                url: '/artifacts',
                parent: 'default-content',
                views: {
                    'content@base': {
                        templateUrl: 'app/artifacts/views/artifacts.html',
                        controller: 'artifactsController as vm'
                    }
                },
                data: {
                    evaluateeRequired: true
                },
                ncyBreadcrumb: {
                    label: 'Artifacts'
                }
            })
            .state('artifact-builder', {
                url: '/artifact-builder/{artifactId}/{artifactOrigin}/{goalId}',
                parent: 'artifacts',
                views: {
                    'content@base': {
                        templateUrl: 'app/artifacts/views/artifact-builder.html',
                        controller: 'artifactBuilderController as vm'
                    }
                },
                resolve: {
                    artifact: ['artifactService', '$stateParams', function (artifactService, $stateParams) {
                        return artifactService.getArtifactById($stateParams.artifactId)
                            .then( (artifact) => {                               
                                return artifact;
                            });
                    }],
                    bundle: ['studentGrowthGoalsService', function(studentGrowthGoalsService) {
                        return studentGrowthGoalsService.getBundleForEvaluation().then((bundle) => {
                            return bundle;
                        });
                    }]

                },
                ncyBreadcrumb: {
                    label: 'Edit Artifact - {{ vm.artifact.title }}'
                },
               
            })
            .state('artifact-view', {
                url: '/artifact-view/{artifactId}/{artifactOrigin}',
                parent: 'artifacts',
                views: {
                    'content@base': {
                        templateUrl: 'app/artifacts/views/artifact-view.html',
                        controller: 'artifactViewController as vm'
                    }
                },
                resolve: {
                    artifact: ['artifactService', '$stateParams', function (artifactService, $stateParams) {
                        return artifactService.getArtifactById($stateParams.artifactId)
                            .then(function (artifact) {
                                return artifact;
                            });
                    }],
                    bundle: ['studentGrowthGoalsService', function(studentGrowthGoalsService) {
                        return studentGrowthGoalsService.getBundleForEvaluation().then((bundle) => {
                            return bundle;
                        });
                    }]
                },
                ncyBreadcrumb: {
                    label: 'View Artifact - {{vm.artifact.title}}'
                }
            })
    }
})();