/// <reference path="../artifacts.module.ts" />

/**
 * Created by anne on 11/30/2015.
 */

(function() {
    'use strict';

    angular.module('stateeval.artifact')
        .directive('artifactBuilderAlignment', artifactBuilderAlignmentDirective)
        .controller('artifactBuilderAlignmentController', artifactBuilderAlignmentController);

    artifactBuilderAlignmentController.$inject = ['artifactService', 'activeUserContextService',
        '$rootScope', 'rubricUtils', '_', 'enums'];

    function artifactBuilderAlignmentDirective() {
        return {
            scope: {
                artifact: '=',
                goalBundle: '=',
                readOnly: '=',
                stateFrameworkOnly: '=',
                done: '='
            },
            templateUrl: 'app/artifacts/views/artifact-builder-alignment.directive.html',
            controller: 'artifactBuilderAlignmentController as vm',
            bindToController: true
        }
    }

    function artifactBuilderAlignmentController(artifactService, activeUserContextService,
            $rootScope, rubricUtils, _, enums) {

        var vm = this;
        vm.enums = enums;

        var flatRows,
            alignedEvidences;

        var framework = null;
        if (vm.stateFrameworkOnly) {
            framework = activeUserContextService.context.frameworkContext.stateFramework;
        }
        else {
            framework = activeUserContextService.getActiveFramework();
        }

        vm.loaded = false;

        $rootScope.$on('change-framework', function () {
            if (!vm.stateFrameworkOnly) {
                framework = activeUserContextService.getActiveFramework();
                loadFramework();
            }
        });

        artifactService
            .getArtifactAlignedEvidencesForArtifact(vm.artifact.id)
            .then(function(evidences) {
                alignedEvidences = evidences;
                loadFramework();
                vm.loaded = true;
            });

        function loadFramework() {
            // need to always get state rows since we show SG rows in domain view
            flatRows = rubricUtils.getFlatRows(activeUserContextService.context.frameworkContext.stateFramework);

            // massaging the data to pass it to the categorized selector directive
            vm.categories = _.map(framework.frameworkNodes, function(fn) {
                return {
                    name: formatName(fn),
                    collapsed: true,
                    rows: _.map(fn.rubricRows, function(rr) {
                        return createRubricCategoryRow(rr);
                    })
                };
            });

            if (framework.id !== activeUserContextService.getStateFramework().id) {
                vm.categories.push({
                    name: formatName({shortName: 'SG', title: 'Student Growth'}),
                    collapsed: true,
                    rows: createSGRubricCategoryRows()
                })
            }
        }

        function createRubricCategoryRow(rr)
        {
           return {
                id: rr.id,
                name: formatRubricRowName(rr),
                selected: _.any(vm.artifact.alignedRubricRows, { id: rr.id }),
                inUse: _.any(alignedEvidences, { rubricRowId: rr.id }),
                readOnly:  (vm.goalBundle.wfState >= enums.WfState.SGBUNDLE_PROCESS_SHARED) && rr.isStudentGrowthAligned && rr.shortName.match(/.1/)
            };
        }

        function createSGRubricCategoryRows() {
            return _.map(rubricUtils.getStudentGrowthRubricRows(activeUserContextService.getFrameworkContext().studentGrowthFrameworkNodes),
                        function(rr) {
                            return createRubricCategoryRow(rr);
                        })
        }

        // called by the categories selector when the selection has changed
        vm.changed = function changed(selectedRows) {
            vm.artifact.alignedRubricRows = _.map(selectedRows, function(row) { return flatRows[row.id]; });
            artifactService.saveArtifactAlignment(vm.artifact);
            vm.done();
        }

        function formatRubricRowName(rr) {
            if ((vm.goalBundle.wfState >= enums.WfState.SGBUNDLE_PROCESS_SHARED) && rr.isStudentGrowthAligned && rr.shortName.match(/.1/)) {
                return rr.shortName + ' ' + rr.title +  '&nbsp;&nbsp;&nbsp;&nbsp;' + vm.goalBundle.statusDisplayName;
            }
            else {
                return rr.shortName + ' ' + rr.title;
            }
        }

        function formatName(fn) {
            return '<div class="pull-left">' + fn.shortName + '</div>' +
                '<div class="row-title">' + fn.title + '</div>';
        }
    }

})();
