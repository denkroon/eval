/// <reference path="../artifacts.module.ts" />

(function () {
'use strict';

    angular.module('stateeval.artifact')
    .directive('artifactBuilderEditLibItem', artifactBuilderEditLibItem)
        .controller('artifactBuilderEditLibItemController', artifactBuilderEditLibItemController);

    artifactBuilderEditLibItem.$inject = [];
    artifactBuilderEditLibItemController.$inject = ['enums', '$scope', '$stateParams', '$state',
        'utils', 'config'];

    function artifactBuilderEditLibItem() {
        return {
            restrict: 'E',
            scope: {
                item: '=',
                doneFcn: '=',
                cancelFcn: '='
            },
            templateUrl: 'app/artifacts/views/artifact-builder-edit-libitem.directive.html',
            controller: 'artifactBuilderEditLibItemController as vm',
            bindToController: true
        }
    }

    function artifactBuilderEditLibItemController(enums, $scope, $stateParams, $state,
          utils, config) {
        var vm = this;
        vm.utils = utils;
        vm.enums = enums;

        vm.editItemForm = {};
        vm.fileURL = '';
        vm.webUrl = '';
        vm.profPracticeNotes = '';
        vm.title = '';
        vm.fileUUID = null;
        vm.fileName = '';

        vm.uploadFile = uploadFile;
        vm.submitForm = submitForm;
        vm.cancel = cancel;

        activate();

        function cancel() {
            vm.uploadError = null;
            vm.cancelFcn();
        }

        function activate() {

            $scope.$watch('vm.item', function (newValue, oldValue) {
                vm.item = newValue;
                 if (vm.item && vm.item.itemType === enums.ArtifactLibItemType.FILE && vm.item.fileUUID === null) {
                    uploadFile();
                }

                // need to load local copy in case of cancel
                if (vm.item) {
                    vm.title = vm.item.title;
                    switch (vm.item.itemType) {
                        case enums.ArtifactLibItemType.PROFPRACTICE:
                            vm.profPracticeNotes = vm.item.profPracticeNotes;
                            break;
                        case enums.ArtifactLibItemType.WEB:
                            vm.webUrl = vm.item.webUrl;
                            break;
                        case enums.ArtifactLibItemType.FILE:
                            vm.fileName = vm.item.fileName;
                            vm.fileUUID = vm.item.fileUUID;
                            break;
                        default:
                            break;
                    }
                }
            })
        }

        function submitForm(item) {
            if (vm.editItemForm.$valid) {
                item.title = vm.title;
                switch (item.itemType) {
                    case enums.ArtifactLibItemType.PROFPRACTICE:
                        item.profPracticeNotes = vm.profPracticeNotes;
                        break;
                    case enums.ArtifactLibItemType.WEB:
                        var r = /^(ftp|http|https):\/\/[^ "]+$/;
                        if (r.test(vm.webUrl)) {
                            item.webUrl = vm.webUrl;
                        } else {
                            item.webUrl = "http://" + vm.webUrl;
                        }
                        break;
                    case enums.ArtifactLibItemType.FILE:
                        vm.item.fileName = vm.fileName;
                        vm.item.fileUUID = vm.fileUUID;
                        break;
                    default:
                        break;
                }
                vm.doneFcn(item);
            }
        }


        function uploadFile() {
            vm.waiting = true;
            vm.uploadError = null;
            uploadcare.openDialog(null, {
                publicKey: '0c18992f124cb62a6940',
                validators: [
                    function(fileInfo) {
                        if (fileInfo.size && fileInfo.size > 104857600) {
                            throw new Error("fileMaximumSize");
                        }
                        if (fileInfo.name && fileInfo.name.match(/(\.docm|\.dotm|\.xlsm|\.xlam|\.pptm|\.potm|\.ppam|\.ppsm|\.sldm|\.scf|\.lnk|\.inf|\.reg|\.psc1|\.psc2|\.ps1|\.ps1xml|\.ps2|\.ps2xml|\.msh|\.msh1|\.msh2|\.mshxml|\.msh1xml|\.msh2xml|\.vb|\.vbs|\.ws|\.wsh\.pif|\.msi|\.com\.JAR\.jar|\.js|\.sh|\.bat|\.cmd|\.ps1)$/)) {
                            throw new Error('fileExtension');
                        }
                    }
                ]
            }).then(function (file) {
                console.log('done!', file)
                file.then(function (fileInfo) {
                    vm.waiting = false;
                    vm.fileUUID = fileInfo.uuid;
                    vm.fileName = fileInfo.name;
                    $scope.$apply();
                }, function(error) {
                    console.log('error!', error);
                    switch(error) {
                        case 'fileMaximumSize':
                            vm.uploadError = 'The file you attempted to upload is too big. The maximum size is 100Mb.';
                            break;
                        case 'fileExtension':
                            vm.uploadError = 'Uploading of executable files is not permitted.';
                            break;
                    }
                    vm.waiting = false;
                    $scope.$apply();
                })
            }, function () {
                // this is called when the upload dialog is cancelled
                vm.waiting = false;
                $scope.$apply();
            })
        }
    }

}) ();
