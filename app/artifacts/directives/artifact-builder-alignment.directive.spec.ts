/**
 * Created by anne on 2/18/2016.
 */

/// <reference path="../artifacts.module.ts" />


describe('artifact-builder-alignment-directive', function() {

    var element,
        artifact = {
            alignedRubricRows: [
                {
                    id: 109, // must match mock rubric row id
                    shortName: '1a'
                }
            ]
        },
        controller,
        artifactServiceGetAlignedEvidenceStub,
        activeUserContextServiceGetActiveFrameworkStub,
        rubricUtils,
        $rootScope,
        artifactService;

    beforeEach(() => {
        angular.mock.module('stateeval.artifact');
        angular.mock.module('templates');
        angular.mock.module('stateeval.core', function ($provide) {
            $provide.factory('categorizedSelectorDirective', function () {
                return {};
            });
        });
    });

    beforeEach(inject((
            _$rootScope_, $q, $compile, $controller, _artifactService_, activeUserContextService, _rubricUtils_
        ) => {
            $rootScope = _$rootScope_;
            rubricUtils = _rubricUtils_;
            artifactService = _artifactService_

            artifactServiceGetAlignedEvidenceStub = sinon.stub(artifactService, 'getArtifactAlignedEvidencesForArtifact',
                function(id) {
                    return $q.when([ {rubricRowId: 109}]);
                });

            activeUserContextServiceGetActiveFrameworkStub = sinon.stub(activeUserContextService, 'getActiveFramework',
                function() {
                    return TestHelper.TR_FrameworkContext.instructionalFramework;
                });

            element = angular.element('<artifact-builder-alignment artifact="vm.artifact" done="vm.done"></artifact-builder-alignment>');
            var scope = $rootScope;
            scope.vm = {
                artifact: artifact,
                done: function() {}
            };

            $compile(element)(scope);
            scope.$digest();

            controller = element.controller('artifactBuilderAlignment');
        }));

    it('should initialize the artifact scope variable', function() {
        expect(controller.artifact).toEqual(artifact);
    });

    it('should initialize the categories to 4 domains', function() {
        expect(controller.categories.length).toEqual(4);
    });

    it('should set row.selected to false for un-aligned row', function() {
        expect(controller.categories[0].rows[ 1].selected).toBe(false);
    });

    it('should set row.inUse to false for row with un-used evidence', function() {
        expect(controller.categories[0].rows[1].inUse).toBe(false);
    });

    it('should set row.selected to true for aligned row', function() {
        expect(controller.categories[0].rows[0].selected).toBe(true);
    });

    it('should set row.inUse to true for row with used evidence', function() {
        expect(controller.categories[0].rows[0].inUse).toBe(true);
    });

    it ('should save the artifact on change', () => {
        spyOn(artifactService, 'saveArtifactAlignment')
        // ideally we'd pass different values to test the different behaviors
        controller.changed([]);
        // you can use toHaveBeenCalledWith to validate the correct params are passed
        // see http://jasmine.github.io/2.0/introduction.html#section-Spies
        expect(artifactService.saveArtifactAlignment).toHaveBeenCalled();
    })

});

describe('artifact-builder-alignment-directive change-framework', function() {

    var element,
        artifact = {
            alignedRubricRows: [
                {
                    id: 109, // must match mock rubric row id
                    shortName: '1a'
                }
            ]
        },
        controller,
        artifactServiceGetAlignedEvidenceStub,
        activeUserContextServiceGetActiveFrameworkStub,
        rubricUtils,
        $rootScope,
        artifactService;

    beforeEach(() => {
        angular.mock.module('stateeval.artifact');
        angular.mock.module('templates');
        angular.mock.module('stateeval.core', function ($provide) {
            $provide.factory('categorizedSelectorDirective', function () {
                return {};
            });
        });
    });

    beforeEach(inject((
        _$rootScope_, $q, $compile, $controller, _artifactService_, activeUserContextService, _rubricUtils_
    ) => {
        $rootScope = _$rootScope_;
        rubricUtils = _rubricUtils_;
        artifactService = _artifactService_;

        artifactServiceGetAlignedEvidenceStub = sinon.stub(artifactService, 'getArtifactAlignedEvidencesForArtifact',
            function(id) {
                return $q.when([ {rubricRowId: 109}]);
            });

        activeUserContextServiceGetActiveFrameworkStub = sinon.stub(activeUserContextService, 'getActiveFramework',
            function() {
                return TestHelper.TR_FrameworkContext.stateFramework;
            });

        element = angular.element('<artifact-builder-alignment artifact="vm.artifact" done="vm.done"></artifact-builder-alignment>');
        var scope = $rootScope;
        scope.vm = {
            artifact: artifact,
            done: function() {}
        };

        $compile(element)(scope);
        scope.$digest();

        controller = element.controller('artifactBuilderAlignment');
    }));


    it('should initialize the categories to 8 state criteria after change-framework', function() {
        $rootScope.$broadcast('change-framework');
        expect(controller.categories.length).toEqual(8);
    });

});

