/**
 * Created by anne on 2/18/2016.
 */

/// <reference path="../artifacts.module.ts" />


describe('artifact-builder-linked-objects-directive', function() {

    var element,
        artifact,
        controller,
        artifactServiceGetAlignedEvidenceStub,
        artifactServiceGetAttachableObservationsStub,
        artifactServiceGetAttachableSelfAssessmentsStub,
        artifactServiceGetAttachableGoalsStub,
        artifactServiceGgetOtherEvidenceCollectionStub,
        activeUserContextServiceGetActiveFrameworkStub,
        rubricUtils,
        $rootScope;

    beforeEach(() => {
        angular.mock.module('stateeval.artifact');
        angular.mock.module('templates');
        angular.mock.module('stateeval.core', function ($provide) {
            $provide.factory('categorizedSelectorDirective', function () {
                return {};
            });
        });
    });

    beforeEach(inject((
            _$rootScope_, $q, enums, $compile, $controller, artifactService, activeUserContextService, _rubricUtils_
        ) => {
            $rootScope = _$rootScope_;
            rubricUtils = _rubricUtils_;

            artifact = {
                linkedObservations: [
                    {
                        id: 1
                    }
                ],
                linkedSelfAssessments: [
                    {
                        id: 2
                    }
                ],
                linkedStudentGrowthGoals: [
                    {
                        id: 3
                    }
                ],
                linkedOtherEvidenceCollections: [
                    { id: 1}
                ]
            };

            artifactServiceGetAttachableObservationsStub = sinon.stub(artifactService, 'getAttachableObservationsForEvaluation',
                function() {

                    return $q.when([
                        {
                            id: 1,
                            title: 'Observation #1',
                            wfState: enums.WfState.OBS_LOCKED_SEALED,
                            statusDisplayName: 'Complete'
                        },
                        {
                            id: 2,
                            title: 'Observation #2',
                            wfState: enums.WfState.OBS_IN_PROGRESS_TOR,
                            statusDisplayName: 'Open - In Progress'
                        },
                        {
                            id: 3,
                            title: 'Observation #3',
                            wfState: enums.WfState.OBS_LOCKED_TEE_REVIEW,
                            statusDisplayName: 'Open - Final'
                        }
                    ])

                });

            artifactServiceGetAttachableSelfAssessmentsStub = sinon.stub(artifactService, 'getAttachableSelfAssessmentsForEvaluation',
                function() {
                    return $q.when([]);
                });


            artifactServiceGetAttachableGoalsStub = sinon.stub(artifactService, 'getAttachableStudentGrowthGoalsForEvaluation',
                function() {
                    return $q.when([]);
                });

            artifactServiceGgetOtherEvidenceCollectionStub = sinon.stub(artifactService, 'getOtherEvidenceCollectionForEvaluation',
                function() {
                    return $q.when(
                        {
                            id: 1
                        }
                    )
                });

            artifactServiceGetAlignedEvidenceStub = sinon.stub(artifactService, 'getArtifactAlignedEvidencesForArtifact',
                function(id) {
                    return $q.when([
                        {
                            rubricRowId: 109,
                            evidenceCollectionObjectId: 1,
                            evidenceCollectionType: enums.EvidenceCollectionType.OBSERVATION
                        }
                    ]);
                });

            activeUserContextServiceGetActiveFrameworkStub = sinon.stub(activeUserContextService, 'getActiveFramework',
                function() {
                    return TestHelper.TR_FrameworkContext.stateFramework;
                });

            element = angular.element('<artifact-builder-linked-objects artifact="vm.artifact" done="vm.done"></artifact-builder-linked-objects>');
            var scope = $rootScope;
            scope.vm = {
                artifact: artifact,
                done: function() {}
            };

            $compile(element)(scope);
            scope.$digest();

            controller = element.controller('artifactBuilderLinkedObjects');
        }));

    it('should initialize the artifact scope variable', function() {
        expect(controller.artifact).toEqual(artifact);
    });

    it('should have observations category', function() {
        expect(_.find(controller.categories, {name: 'Observations'})).not.toBeNull();
    });

    it('should have self-assessments category', function() {
        expect(_.find(controller.categories, {name: 'Self-Assessments'})).not.toBeNull();
     });

    it('should have goals category', function() {
        expect(_.find(controller.categories, {name: 'Student Growth Goals'})).not.toBeNull();
     });

    it('should have other evidence category', function() {
        expect(_.find(controller.categories, {name: 'Other Evidence'})).not.toBeNull();
     });

    it('should have three attachable observations', function() {
        var category = _.find(controller.categories, {name: 'Observations'});
        expect(category.rows.length).toEqual(3);
    });

    it('should have workstate in the row name for observations', function() {
        var category = _.find(controller.categories, {name: 'Observations'});
        expect(category.rows[0].name).toContain('Complete');
        expect(category.rows[1].name).toContain('Progress');
        expect(category.rows[2].name).toContain('Final');
    });

    it('should have one linked observation', function() {
        var category = _.find(controller.categories, {name: 'Observations'});
        var selectedRow = _.find(category.rows, {selected:true});
        expect(selectedRow).not.toBeNull();
    });

    it('should set row.inUse to false for un-linked/in progress observation with aligned evidence', function() {
        var category = _.find(controller.categories, {name: 'Observations'});
        expect(category.rows[1].inUse).toBe(false);
    });

    it('should set row.readOnly to false for un-linked/in progress observation with aligned evidence', function() {
        var category = _.find(controller.categories, {name: 'Observations'});
        expect(category.rows[1].inUse).toBe(false);
    });

    it('should set row.inUse to true for linked/sealed observation with aligned evidence', function() {
        var category = _.find(controller.categories, {name: 'Observations'});
        expect(category.rows[0].inUse).toBe(true);
    });

    it('should set row.readOnly to true for linked/sealed observation', function() {
        var category = _.find(controller.categories, {name: 'Observations'});
        expect(category.rows[0].readOnly).toBe(true);
    });

    it('should set row.inUse to false for un-linked/final ack observation', function() {
        var category = _.find(controller.categories, {name: 'Observations'});
        expect(category.rows[2].inUse).toBe(false);
    });

    it('should set row.readOnly to true for un-linked/final ack observation', function() {
        var category = _.find(controller.categories, {name: 'Observations'});
        expect(category.rows[2].readOnly).toBe(true);
    });

    it('should have one evidence collection', function() {
        var category = _.find(controller.categories, {name: 'Other Evidence'});
        expect(category.rows.length).toEqual(1);
    });

    it('should have one linked other evidence', function() {
        var category = _.find(controller.categories, {name: 'Other Evidence'});
        var selectedRow = _.find(category.rows, {selected:true});
        expect(selectedRow).not.toBeNull();
    });

});
