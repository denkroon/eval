/// <reference path="../artifacts.module.ts" />

/**
 * Created by anne on 11/30/2015.
 */

(function() {
    'use strict';

    angular.module('stateeval.artifact')
        .directive('artifactBuilderLinkedObjects', artifactBuilderLinkedObjectsDirective)
        .controller('artifactBuilderLinkedObjectsController', artifactBuilderLinkedObjectsController);

    artifactBuilderLinkedObjectsController.$inject = ['enums', 'artifactService', '_', 'studentGrowthGoalsService'];

    function artifactBuilderLinkedObjectsDirective() {
        return {
            scope: {
                artifact: '=',
                goalBundle: '=',
                readOnly: '=',
                done: '='
            },
            templateUrl: 'app/artifacts/views/artifact-builder-linked-objects.directive.html',
            controller: 'artifactBuilderLinkedObjectsController as vm',
            bindToController: true
        }
    }

    function artifactBuilderLinkedObjectsController(enums, artifactService, _, studentGrowthGoalsService) {
        var vm = this;
        vm.loaded = false;
        vm.categories = [];

        var types = enums.EvidenceCollectionType;

        categoryLoader(
            artifactService.getAttachableObservationsForEvaluation,
            'Observations',
            vm.artifact.linkedObservations,
            types.OBSERVATION)()
            .then(categoryLoader(
                artifactService.getAttachableSelfAssessmentsForEvaluation,
                'Self Assessments',
                vm.artifact.linkedSelfAssessments,
                types.SELF_ASSESSMENT))
            .then(categoryLoader(
                artifactService.getAttachableStudentGrowthGoalsForEvaluation,
                'Student Growth Goals',
                vm.artifact.linkedStudentGrowthGoals,
                types.STUDENT_GROWTH_GOALS))
            .then(function() {
                // The other evidence collection is a bit special because there can only ever be one
                return artifactService.getOtherEvidenceCollectionForEvaluation().then(function(evidenceCollection) {
                    vm.categories.push({
                        name: 'Other Evidence',
                        collapsed: true,
                        rows: [{
                            item: evidenceCollection,
                            type: types.OTHER_EVIDENCE,
                            selected: _.any(vm.artifact.linkedOtherEvidenceCollections, { id: evidenceCollection.id }),
                            name: "Year-to-date Evidence Collection"
                        }]
                    });
                })
            })
            .then(loadAlignedEvidences)
            .then(function() {
                vm.loaded = true;
            })

        // called when the user clicks on Done in the categorized selector and changes are detected
        vm.changed = function changed(selectedRows) {
            var categories = _.groupBy(selectedRows, 'type');

            vm.artifact.linkedObservations = getItems(categories[types.OBSERVATION]);
            vm.artifact.linkedSelfAssessments = getItems(categories[types.SELF_ASSESSMENT]);
            vm.artifact.linkedStudentGrowthGoals = getItems(categories[types.STUDENT_GROWTH_GOALS]);
            vm.artifact.linkedOtherEvidenceCollections = getItems(categories[types.OTHER_EVIDENCE]);
            artifactService.saveArtifactBundleLinkages(vm.artifact);
            vm.done();
        }

        // helper used by the changed function
        function getItems(collection) {
            return collection ? _.pluck(collection, 'item') : [];
        }

        // retrieves the evidences aligned to the current artifact and uses that to flag
        // the categories rows as in use
        function loadAlignedEvidences() {
            return artifactService.getArtifactAlignedEvidencesForArtifact(vm.artifact.id).then(function(alignedEvidences) {
                _(vm.categories)
                    .pluck('rows')
                    .flatten()
                    .forEach(function(row) {
                        row.readOnly = false;
                        row.inUse = false;

                        if (row.type === enums.EvidenceCollectionType.OBSERVATION && row.item.wfState >= enums.WfState.OBS_LOCKED_TEE_REVIEW) {
                            row.readOnly = true;
                        }

                        if (row.type === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS &&
                            vm.goalBundle.wfState>= enums.WfState.SGBUNDLE_PROCESS_SHARED) {
                            if (row.item.linkedArtifacts.length>0) {
                                row.readOnly = true;
                            }
                        }

                        row.inUse = _.any(alignedEvidences, {
                            evidenceCollectionObjectId: row.item.id,
                            evidenceCollectionType: row.type
                        });
                    }).value();
            })
        }

        // Helper function that retrieves a collection of objects using the provided function
        // and massages the data in a way the categorized selector directive can use
        function categoryLoader(fn, name, selected, type) {
            return function() {
                return fn().then(function(items) {
                    vm.categories.push({
                        name: name,
                        collapsed: true,
                        rows: _.map(items, function(i) {
                            return {
                                item: i,
                                type: type,
                                selected: _.any(selected, { id: i.id }),
                                name: formatName(i, type, i.creationDateTime, i.title)
                            };
                        })
                    });
                });
            }
        }

        // Helper that formats the name of a row
        function formatName(item, type, date, title) {
            if (type === enums.EvidenceCollectionType.OBSERVATION) {
                return '<span class="linked-object-date">' + moment(date).format('l') + '</span> ' + title + ' (' +  item.statusDisplayName + ')';
            }
            if (type === enums.EvidenceCollectionType.STUDENT_GROWTH_GOALS) {
                return '<span class="linked-object-date">' + moment(date).format('l') + '</span> ' + title +  ' (' +  vm.goalBundle.statusDisplayName + ')';
            }
            else {
                return '<span class="linked-object-date">' + moment(date).format('l') + '</span> ' + title;
            }
        }
    }

})();
