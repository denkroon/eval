(function () {
    'use strict';

    angular.module('stateeval.artifact')
        .factory('artifactService', artifactService);

    artifactService.$inject = ['$http', 'config', 'enums', '_', '$sce', '$window', '$uibModal',
        'activeUserContextService', '$q'];

    function artifactService($http, config, enums, _, $sce, $window, $uibModal,
         activeUserContextService, $q) {

        var FILE = enums.ArtifactLibItemType.FILE;
        var PROFPRACTICE = enums.ArtifactLibItemType.PROFPRACTICE;
        var WEB = enums.ArtifactLibItemType.WEB;

        return {
            newArtifact: newArtifact,
            newLibItem: newLibItem,
            newArtifactBundleRequest: newArtifactBundleRequest,

            getArtifactBundlesForEvaluation: getArtifactBundlesForEvaluation,
            getOtherEvidenceCollectionForEvaluation: getOtherEvidenceCollectionForEvaluation,
            getAttachableObservationsForEvaluation: getAttachableObservationsForEvaluation,
            getAttachableStudentGrowthGoalsForEvaluation: getAttachableStudentGrowthGoalsForEvaluation,
            getAttachableSelfAssessmentsForEvaluation: getAttachableSelfAssessmentsForEvaluation,
            getArtifactAlignedEvidencesForArtifact: getArtifactAlignedEvidencesForArtifact,
            saveArtifact: saveArtifact,
            shareArtifact: shareArtifact,
            saveArtifactAlignment: saveArtifactAlignment,
            saveArtifactBundleLinkages: saveArtifactBundleLinkages,
            deleteArtifact: deleteArtifact,
            getArtifactById: getArtifactById,
            alignmentToString: alignmentToString,
            libItemDisplayName: libItemDisplayName
            trustAsHtml: trustAsHtml,
            viewItem: viewItem
        };

        function getArtifactAlignedEvidencesForArtifact(artifactId) {
            var url = config.apiUrl + 'artifactalignedevidence/' + artifactId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        // evaluators only see self-assessments that have been shared
        function getAttachableSelfAssessmentsForEvaluation() {
            var currentUserId = activeUserContextService.user.id;
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + evaluationId + '/attachableselfassessments/' + currentUserId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getAttachableStudentGrowthGoalsForEvaluation() {
            var currentUserId = activeUserContextService.user.id;
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + evaluationId + '/attachablesggoals/' + currentUserId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getAttachableObservationsForEvaluation() {
            var currentUserId = activeUserContextService.user.id;
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + evaluationId + '/attachableobservations/' + currentUserId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getArtifactById(id) {
            var url = config.apiUrl + 'artifactbundles/' + id + '/' + activeUserContextService.user.id;
            return $http.get(url).then(function(response) {
                return response.data;
            })
        }

        function getArtifactBundlesForEvaluation(request) {
            var url = config.apiUrl + '/artifactbundles';
            return $http.get(url, {params: request})
                .then(function (response) {
                    return response.data;
                });
        }

        function getOtherEvidenceCollectionForEvaluation(evaluationId) {
            var evaluationId = evaluationId || activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + '/otherevidencecollections/' + evaluationId;
            return $http.get(url)
                .then(function (response) {
                    return response.data;
                });
        }


        function saveArtifact(artifact) {
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + evaluationId + '/artifactbundles';
            if (artifact.id != 0) {
                return $http.put(url, artifact);
            } else {
                return $http.post(url, artifact).then(function(response) {
                    artifact.id = response.data.id;
                });
            }
        }

        function getAlignmentLinkageData(artifact) {
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            return {
                id: artifact.id,
                title: artifact.title,
                isShared: artifact.isShared,
                createdByUserId: artifact.createdByUserId,
                evaluationId: evaluationId,
                alignedRubricRows: artifact.alignedRubricRows || [],
                linkedObservations: artifact.linkedObservations,
                linkedSelfAssessments: artifact.linkedSelfAssessments,
                linkedOtherEvidenceCollections: artifact.linkedOtherEvidenceCollections,
                linkedStudentGrowthGoals: artifact.linkedStudentGrowthGoals
            };
        }

        function shareArtifact(artifact) {
            var url = config.apiUrl + 'artifactbundles/share';
             return $http.put(url, artifact);
        }

        function saveArtifactAlignment(artifact) {
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + evaluationId + '/saveartifactalignment';
            var data = getAlignmentLinkageData(artifact);
            return $http.put(url, data);
        }

        function saveArtifactBundleLinkages(artifact) {
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var url = config.apiUrl + evaluationId + '/saveartifactlinkages';
            var data = getAlignmentLinkageData(artifact);
            return $http.put(url, data).then(function() {

            });
        }


        function deleteArtifact(artifact) {
            return $http.delete(config.apiUrl + 'artifactbundles/' + artifact.id);
        }


        function alignmentToString(artifact) {
            var list = '' ;
            for (var i in artifact.alignedRubricRows) {
                if (list != '')
                {
                    list += ', ';
                }
                list+= (artifact.alignedRubricRows[i].shortName);
            }
            return list;
        }

        function trustAsHtml(text) {
            return $sce.trustAsHtml(text);
        }

        function viewItem(item) {
            switch (item.itemType) {
                case (FILE):
                    var url = 'http://www.ucarecdn.com/' + item.fileUUID + '/';
                    $window.open(url, '_blank');
                    break;
                case (WEB):
                     $window.open(item.webUrl, '_blank');
                    break;
                case (PROFPRACTICE):
                    $uibModal.open({
                        templateUrl: 'app/artifacts/views/professional-practice-modal.html',
                        controller: 'ProfessionalPracticeController as vm',
                        resolve: {
                            libItem: function () {
                                return item;
                            }
                        }
                    });
                    break;
            }
        }

        function libItemDisplayName(item) {
            var displayName = '';
            switch (item.itemType) {
                case (FILE):
                    displayName = item.title || item.fileName;
                    break;
                case (WEB):
                    displayName = item.title || item.webUrl;
                    break;
                case (PROFPRACTICE):
                    displayName = item.title || item.profPracticeNotes;
                    break;
                default:
                    displayName = 'libItemDisplayName: unknown type: ' + item.itemType;
                    break;
            }

            if (displayName.length>30) {
                displayName = displayName.substr(0, 30) + '...';
            }
            return displayName;
        }

        function newArtifactBundleRequest(createdByUserId) {
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            var currentUserId = activeUserContextService.user.id;
            return {
                evaluationId: evaluationId,
                evaluateeId: activeUserContextService.context.evaluatee.id,
                createdByUserId: createdByUserId,
                currentUserId: currentUserId,
                rubricRowId: 0,
                isShared: false
            }
        }

        function newArtifact() {
            var evaluationId = activeUserContextService.context.evaluatee.evalData.id;
            return {
                creationDateTime: new Date(),
                evaluationId: evaluationId,
                id: 0,
                libItems: [],
                alignedRubricRows: [],
                linkedObservations: [],
                linkedStudentGrowthGoals: [],
                linkedSelfAssessments: [],
                linkedOtherEvidenceCollections: [],
                evidence: '',
                title: '',
                isShared: false,
                shareDateTime: null,
                createdByUserId: activeUserContextService.user.id
            }
        }

        function newLibItem(type) {
            // this is used for resources, which do not have an evaluationI
            var evaluationId = activeUserContextService.context.evaluatee?activeUserContextService.context.evaluatee.evalData.id:null;
            if (type) {
                return {
                    comments: '',
                    creationDateTime: new Date(),
                    evaluationId: evaluationId,
                    fileUUID: null,
                    id: 0,
                    itemType: type || -1,
                    profPracticeNotes: '',
                    title: '',
                    webUrl: '',
                    fileName: '',
                    tags: [],
                    createdByUserId: activeUserContextService.user.id
                }
            } else {
                console.log('Cannot build Item without an typeType');
            }

        }
    }
})
();