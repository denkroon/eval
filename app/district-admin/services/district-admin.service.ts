/// <reference path="../district-admin.module.ts" />

/**
 * Created by anne on 7/30/2015.
 */

(function () {
    'use strict';

    angular
        .module('stateeval.district-admin')
        .factory('districtAdminService', districtAdminService);

    districtAdminService.$inject = ['$http', 'config', 'activeUserContextService'];
    function districtAdminService($http, config, activeUserContextService) {


        var service = {
            getDistrictConfiguration: getDistrictConfiguration,
            saveSettings: saveSettings
        };

        return service;

        function saveSettings(districtConfiguration) {
            var url = config.apiUrl + '/districtConfigurations';
            return $http.put(url, districtConfiguration);
        }

        function getDistrictConfiguration() {
            var frameworkContextId = activeUserContextService.context.frameworkContext.id;
            var url = config.apiUrl + '/districtconfigurations/' + frameworkContextId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }
    }
})();