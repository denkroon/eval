(function() {
    'use strict';

    class DASettingsController {

        districtConfiguration: any;
        evaluateeTerm: string;
        settingsForm: any;
        isDanielsonFramework: boolean;
        observationFinalOptionReportFormats: [any];
        evaluationType: number;

        constructor(
            private districtAdminService: any,
            private $stateParams: any,
            private activeUserContextService: any,
            private toastr: any,
            private reportService: any,
            public enums: any
        ) {
            this.evaluationType = parseInt($stateParams.evaluationtype);
            this.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();
            this.districtConfiguration = activeUserContextService.context.frameworkContext.districtConfiguration;
            this.isDanielsonFramework = activeUserContextService.getActiveFramework().name.indexOf('Danielson') != -1;

            this.reportService.getReportOptionReportFormats(this.enums.ReportTypeEnum.OBSERVATION, this.enums.ReportFormatTypeEnum.FINAL)
                .then((observationFinalOptionReportFormats) => {
                    this.observationFinalOptionReportFormats = observationFinalOptionReportFormats;
                })
        }

        saveObservationReportOptions() {
            var optionFormats = [];
            var that = this;
            this.observationFinalOptionReportFormats.forEach((optionFormat) => {
                var newOptionFormat = {
                    id: optionFormat.id,
                    enabled: optionFormat.enabled
                };
                optionFormats.push(newOptionFormat);
            });

            that.reportService.saveReportOptionReportFormats(optionFormats).then(()=> {
                this.districtAdminService.saveSettings(this.districtConfiguration).then(() => {
                    this.activeUserContextService.save();
                    that.toastr.info('Observation Report settings have been saved');
                });
            })
        }

        saveFinalReportOptions() {
            this.districtAdminService.saveSettings(this.districtConfiguration).then(() => {
                this.activeUserContextService.save();
                this.toastr.info('Final Report settings have been saved');
            });
        }

        saveSettings() {
            if (this.settingsForm.$valid) {
                this.districtAdminService.saveSettings(this.districtConfiguration).then(() => {
                    this.activeUserContextService.save();
                    this.toastr.info('Your settings have been saved');
                });
            }
        }
    }

    angular.module('stateeval.core')
        .controller('daSettingsController', DASettingsController);

})();