/// <reference path="../district-admin.module.ts" />


(function () {
    'use strict';


    class DistrictViewerSetupController {

        viewers: [any];
        viewer: any;
        schools: [any];

        constructor(
            private userService: any,
            private enums: any,
            private locationService: any,
            public assignmentsService: any,
            _: _.LoDashStatic
        ) {
            var that = this;
            userService.getUsersInRoleAtDistrict(enums.Roles.SEDistrictViewer)
                .then(function(viewers) {
                    that.viewers = viewers;
                    if (viewers.length>0) {
                        that.viewer = viewers[0];
                        locationService.getSchoolsInDistrict().then(function(schools) {
                            that.schools = schools;
                            that.loadDistrictViewer();
                        })
                    }
                })
        };

        setDistrictViewerSchool(school)
        {
            this.assignmentsService.setDistrictViewerSchool(this.viewer.id, school.schoolCode, school.selected);
        }

        loadDistrictViewer()
        {
            this.schools.forEach(function(school) {
                school.selected = false;
            });

            var that = this;
            this.assignmentsService.getDistrictViewerSchools(this.viewer.id).then(function(schools) {
                schools.forEach(function(school) {
                    var match = _.find(that.schools, {schoolCode: school.schoolCode});
                    match.selected = true;
                })
            })
        }
    }

    angular.module('stateeval.district-admin')
        .controller('districtViewerSetupController', DistrictViewerSetupController);

})();