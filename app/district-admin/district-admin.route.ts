/// <reference path="district-admin.module.ts" />

/**
 * Created by anne on 6/19/2015.
 */
(function () {
    'use strict';
    angular.module('stateeval.district-admin')
        .config(configureRoutes);

    configureRoutes.$inject = ['$stateProvider'];

    function configureRoutes($stateProvider) {

        $stateProvider
            .state('da-base', {
                abstract: true,
                url: '/da-base',
                parent: 'default-content'
            })
            .state('district-viewer-setup', {
                url: '/ditrict-viewer-setup',
                parent: 'da-base',
                views: {
                    'content@base': {
                        controller: 'districtViewerSetupController as vm',
                        templateUrl: 'app/district-admin/views/district-viewer-setup.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'District Viewer Setup'
                }
            })

            .state('da-settings', {
                url: '/da-settings/:evaluationtype',
                parent: 'da-base',
                views: {
                    'content@base': {
                        controller: 'daSettingsController as vm',
                        templateUrl: 'app/district-admin/views/da-settings.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Select Settings for {{vm.evaluateeTerm}} Evaluations'
                }
            });

    }
}) ();

