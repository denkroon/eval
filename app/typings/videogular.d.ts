declare module Videogular {

    interface IMediaSource {
        /**
         * must contains a trustful url resource
         */
        src: string;
        type: string;
    }

    interface API {
        /**
         * Plays media.
         */
        play();

        /**
         * Pause media.
         */
        pause();

        /**
         * Stops media
         */
        stop();

        /**
         * Toggles play and pause.
         */
        playPause();

        /**
         * Updates current media source.
         */
        changeSource(sources: IMediaSource[]);

        /**
         * Seeks to a specified time position. Param value must be an integer representing the target position in seconds or a percentage. By default seekTime seeks by seconds, if you want to seek by percentage just pass byPercent to true.
         */
        seekTime(value: number, byPercent?: boolean);

        /**
         * Sets volume.
         */
        setVolume(volume: number);

        /**
         * Toggles between fullscreen and normal mode.
         */
        toggleFullScreen();

        /**
         * Removes previous CSS theme and sets a new one.
         */
        updateTheme(url: string);

        /**
         * Cleans the current media file.
         */
        clearMedia();

        /**
         * String with a url to JSON config file.
         */
        config: string

        /**
         * Boolean value with current player initialization state.
         */
        isReady: boolean

        /**
         * Boolean value to know if player is buffering media.
         */
        isBuffering: boolean

        /**
         * Boolean value to know if current media file has been completed.
         */
        isCompleted: boolean

        /**
         * Boolean value to know if current media file is a Live Streaming.
         */
        isLive: boolean

        /**
         * Boolean value to know if Videogular if fullscreen mode will use native mode or inline playing.
         */
        playsInline: boolean

        /**
         * Reference to video/audio object.
         */
        mediaElement: any

        /**
         * Reference to videogular tag.
         */
        videogularElement: any

        /**
         * Array with current sources.
         */
        sources: IMediaSource[]

        /**
         * Array with current tracks.
         */
        tracks: any[]

        /**
         * Boolean value to know if we’re in fullscreen mode.
         */
        isFullScreen: boolean

        /**
         * String value with “play”, “pause” or “stop”.
         */
        currentState: string

        /**
         * Number value with current media time progress.
         */
        currentTime: number

        /**
         * Number value with total media time.
         */
        totalTime: number

        /**
         * Number value with current media time left.
         */
        timeLeft: number

        /**
         * Number value with current volume between 0 and 1.
         */
        volume: number
    }
}