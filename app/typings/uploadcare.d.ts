declare module uploadcare {

    interface IDialog {
        then(doneFn: (file: IFile) => void, errorFn: (error: any) => void): IDialog

        /**
         * Dialog closed and file or file group chosen.
         */
        done(fn: (file: IFile) => void): IDialog

        /**
         * Dialog closed and no file or file group was selected. The result argument is either null, or the last selected file.
         */
        fail(fn: (error?: any) => void)

        /**
         * Handle dialog closing, irregardless of file selection.
         */
        always(fn: () => void)

        /**
         * tabName is selected
         */
        progress(fn: (tabName: string) => void)

        /**
         * Resolve dialog with current selected files.
         */
        resolve()

        /**
         * Close the dialog and discard file selection.
         */
        reject()

        /**
         * Add array of files to dialog result set.
         */
        addFiles(files: [IFileInfo])

        /**
         * Switch dialog to tab with given name. Name should be present in settings.tabs array.
         */
        switchTab(tabName: string)

        /**
         * 	Collection of selected files. Use it for subsribe to change events.
         */
        fileColl: [IFileInfo]

        /**
         * Hide tab with given name.
         */
        hideTab(tabName: string)

        /**
         * Show tab with given name.
         */
        showTab(tabName: string)

        /**
         * Is tab with given name visible.
         */
        isTabVisible(tabName: string): boolean

        /**
         * Register callback which will be called when a tab visibility is changed. The first argument will be tab name, the second is boolean: is tab visible.
         */
        onTabVisinility(fn: (tabName: string, visible: boolean) => void)
    }

    interface IFileInfo {
        /**
         * File UUID.
         */
        uuid: string,

        /**
         * Original name of the uploaded file.
         */
        name: string,

        /**
         * File size in bytes.
         */
        size: number,

        /**
         * true, if the file is stored in our storage, false otherwise.
         */
        isStored: boolean,

        /**
         * true, if the file is an image, false otherwise.
         */
        isImage: boolean,

        /**
         * Public file CDN URL, may contain CDN operations.
         */
        cdnUrl: string,

        /**
         * URL part with applied CDN operations or null. Appear after user crops image, for example.
         */
        cdnUrlModifiers: string,

        /**
         * Public file CDN URL without any operations.
         */
        originalUrl: string,

        /**
         * Object with original image width and height if file is image, null otherwise.
         */
        originalImageInfo?: {
            width: number,
            height: number
        },

        /**
         * Object with information about file source. For example this can be name of social network, public link if any, owner's user name etc.
         */
        sourceInfo: any
    }

    interface IUploadInfo {
        /**
         * Current readiness state: 'uploading', 'uploaded', or 'ready'
         */
        state: string,

        /**
         * Upload progress as a value from 0 to 1.
         */
        uploadProgress: number,

        /**
         * File readiness (sum of upload progress and info preparation progress) as a value from 0 to 1.
         */
        progress: number
    }

    interface IFile {
        then(doneFn: (fileInfo: IFileInfo) => void, errorFn: (error: any, fileInfo: IFileInfo) => void): IFile

        done(doneFn: (fileInfo: IFileInfo) => void): IFile

        fail(errorFn: (error: any, fileInfo: IFileInfo) => void)

        progress(progressFn: (uploadInfo: IUploadInfo) => void);
    }

    interface IDialogSettings {
        publicKey?: string,
        multiple?: boolean,
        multipleMax?: number,
        multipleMin?: number,
        imagesOnly?: boolean,
        previewStep?: boolean,
        crop?: string,
        imageShrink?: string,
        clearable?: boolean,
        tabs?: string,
        inputAcceptTypes?: string,
        preferredTypes?: string,
        cdnBase?: string,
        doNotStore?: boolean,
        validators?: [(fileInfo: IFileInfo) => void]
    }

    function openDialog(files: any, tab: string, settings: IDialogSettings): IDialog
    function openDialog(files: any, settings: IDialogSettings): IDialog
}