/// <reference path="../core/core.module.ts" />

(function() {
    'use strict';

    angular.module('stateeval.prompt', ['stateeval.core']);
})();
