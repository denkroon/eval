/// <reference path="../prompt.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.prompt')
        .controller('promptBankController', promptBankController);

    promptBankController.$inject = ['enums', 'activeUserContextService', 'userPromptService',
        'utils', '$stateParams'];

    function promptBankController(enums, activeUserContextService, userPromptService,
          utils, $stateParams) {
        var vm = this;

        vm.showEvaluatorPrompts = false;
        vm.showSchoolPrompts = false;

        vm.currentTab = 0;

        if ($stateParams.tab) {
            vm.currentTab = parseInt($stateParams.tab);
        }

        var workAreaTag = activeUserContextService.context.orientation.workAreaTag;
        var userIsASchoolAdmin = (workAreaTag.indexOf('SA') === 0);
        var userIsADistrictAdmin = (workAreaTag.indexOf('DA') === 0);
        var userIsDistrictEvaluator = (workAreaTag === 'DE' || workAreaTag === 'DTE');

        vm.showSchoolPrompts = !userIsADistrictAdmin && !userIsDistrictEvaluator;
        vm.showEvaluatorPrompts = !userIsADistrictAdmin && !userIsASchoolAdmin;

        var role = utils.mapEvalTypeToFriendlyRoleName(activeUserContextService.context.evaluationType()) + " Evaluations";
        if (userIsADistrictAdmin) {
            vm.pageTitle = "District Prompt Bank for " + role;
        }
        else if (userIsASchoolAdmin) {
            vm.pageTitle = "School Prompt Bank for " + role;
        }
        else {
            vm.pageTitle = "My Prompt Bank for " + role;
        }

        vm.preConfGridOption = newGridOption(enums.PromptType.PreConf);
        vm.postConfGridOption = newGridOption(enums.PromptType.PostConf);
        vm.sgGoalsGridOption = newGridOption(enums.PromptType.StudentGrowthGoal);

        function newGridOption(promptType) {
            return {
                promptType: promptType,
                evaluator: vm.showEvaluatorPrompts,
                schoolAdmin: userIsASchoolAdmin,
                districtAdmin: userIsADistrictAdmin,
                title: utils.mapPromptTypeToString(promptType),

                getPromptsFcn: function () {
                    return userPromptService.promptBankGetPrompts(promptType);
                },

                getGroupsFcn: function () {
                    return userPromptService.promptBankGetPromptGroups(promptType);
                },

                getLibPromptsFcn: function () {
                    return geLibItemPrompts(promptType);
                }
            }
        }

        function geLibItemPrompts(promptType) {
            var prompts = [];

            return userPromptService.getUserPromptLibItems(promptType)
                .then(function(districtPrompts) {
                    userPromptService.addPrompts(prompts, districtPrompts, userIsADistrictAdmin);
                    return prompts;
                });
        }


    }

})();