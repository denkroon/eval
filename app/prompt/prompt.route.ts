/// <reference path="prompt.module.ts" />

(function(){
angular.module('stateeval.prompt')
    .config(configureRoutes);

function configureRoutes($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('prompt-home', {
            url: '/prompt-home',
            abstract: true,
            parent: 'default-content'
        })
        .state('prompt-bank', {
            url: '/prompt-bank/:evaluationtype?tab',
            parent: 'prompt-home',
            views: {
                'content@base': {
                    templateUrl: 'app/prompt/views/prompt-bank.html',
                    controller: 'promptBankController as vm'
                }
            },
            data: {
                title: 'Prompt Bank',
                displayName: '{{displayName}}'
            },
            ncyBreadcrumb: {
                label: '{{vm.pageTitle}}'
            },
            resolve: {
                displayName: ['$stateParams', 'enums', function($stateParams, enums) {
                    var evalType = parseInt($stateParams.evaluationtype);
                    return (evalType===enums.EvaluationType.PRINCIPAL)?"Principal Prompt Bank":"Teacher Prompt Bank";
                }]
            }
        })
}

})();