/// <reference path="../prompt.module.ts" />

(function () {
    'use strict';

    angular
        .module('stateeval.prompt')
        .directive('libPrompts', libPromptsDirective)
        .controller('libPromptsController', libPromptsController);

    libPromptsDirective.$inject = [];
    libPromptsController.$inject = ['utils', 'userPromptService', '_'];


    function libPromptsDirective() {
        return {
            restrict: 'E',
            scope: {
                gridOption: '=',
                closeFcn: '=',
                bankPrompts: '='
            },
            templateUrl: 'app/prompt/views/lib-prompts.directive.html',
            controller: 'libPromptsController as vm',
            bindToController: true
        }
    }

    /* @ngInject */
    function libPromptsController(utils, userPromptService, _) {

        var vm = this;
        vm.getSafeHtml = utils.getSafeHtml;

        vm.save = save;
        vm.cancel = cancel;

        vm.prompts = [];
        vm.availablePrompts = false;

        /////////////////////////////////

        activate();

        function activate() {

            vm.gridOption.getLibPromptsFcn().then(function(prompts) {
                vm.prompts = prompts;
                vm.prompts.forEach(function(prompt) {
                    prompt.alreadySelected = _.any(vm.bankPrompts, {userPromptLibItemId: prompt.id});
                });

                vm.availablePrompts = _.any(vm.prompts, {alreadySelected: false});

            });
        }

        function save() {

            var prompts = _.filter(vm.prompts, {selected:true});
            userPromptService.addUserPromptLibItems(prompts).then(function() {
                vm.closeFcn();
            });
        }

        function cancel() {
            vm.closeFcn();
        }
    }

})();

