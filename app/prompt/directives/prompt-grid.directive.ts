﻿/// <reference path="../prompt.module.ts" />

(function () {
    'use strict';
    angular.module('stateeval.prompt')
        .directive('promptGrid', promptGridDirective)
        .controller('promptGridController', promptGridController);

    promptGridDirective.$inject = [];
    promptGridController.$inject = ['$state', 'enums', 'userPromptService', 'config', '$uibModal', '_',
        'activeUserContextService', 'frameworkService', '$confirm'];

    function promptGridDirective() {
        return {
            restrict: 'E',
            scope: {
                gridOption: '='
            },
            templateUrl: 'app/prompt/views/prompt-grid.html',
            controller: 'promptGridController as vm',
            bindToController: true
        }
    }

    function promptGridController($state, enums, userPromptService, config, $uibModal, _,
        activeUserContextService, frameworkService, $confirm) {

        var vm = this;
        vm.enums = enums;

        vm.groups = [];
        vm.studentGrowth = false;
        vm.isEvaluator = vm.gridOption.evaluator;
        vm.evaluateeTerm = activeUserContextService.getEvaluateeTermUpperCase();

        vm.readOnly = activeUserContextService.context.evaluationIsReadOnly();

        vm.promptInGroup = [];
        vm.inDefaultGroup = [];
        vm.inDistrictDefaultGroup = [];
        vm.inSchoolDefaultGroup = [];
        vm.inEvaluatorDefaultGroup = [];
        vm.assignableToDefaultGroup = [];
        vm.removableFromDefaultGroup = [];
        vm.requiredPrompt = [];
        vm.editPromptInGroup = [];
        vm.defaultGroup = null;

        vm.showNewPromptArea = false;
         vm.editPromptModel = '';
        vm.showLibraryPromptArea = false;

        if (vm.gridOption.promptType === enums.PromptType.StudentGrowthGoal) {
            vm.studentGrowth = true;
            vm.studentGrowthFrameworkNodes = activeUserContextService.getFrameworkContext().studentGrowthFrameworkNodes;
        }

        vm.editPrompt = editPrompt;
        vm.cancelAddPrompt = cancelAddPrompt;
        vm.showLibItemsModal = showLibItemsModal;
        vm.savePrompt = savePrompt;
        vm.deletePrompt = deletePrompt;
        vm.showLibraryPrompts = showLibraryPrompts;
        vm.closeLibraryPromptsArea = closeLibraryPromptsArea;
        vm.removeFromDefaultGroup = removeFromDefaultGroup;
        vm.assignToDefaultGroup = assignToDefaultGroup;

        /////////////////////////////////

        activate();

        function activate() {

            loadData().then(function() {

            })
        }

        function loadData() {
            return loadPrompts().then(function (prompts) {
                    vm.gridOption.getGroupsFcn().then(function(groups) {
                        vm.groups = groups;
                        if (vm.studentGrowth) {
                            vm.groups.forEach(function (group) {
                                setGroupFrameworkNodeTitle(group);
                            });

                            prompts.forEach(function(prompt) {
                                updatePromptInStudentGrowthGroups(prompt);
                            });
                        }
                        else {
                            updatePromptInDefaultGroups();
                        }
                    });
            });
        }

        function updatePromptInStudentGrowthGroups(prompt) {
            prompt.editable = !prompt.assigned && prompt.isOwner;
            vm.groups.forEach(function(group) {
                vm.promptInGroup[group.id + "-" + prompt.id] = promptInGroup(prompt, group);
            });
        }

        function promptInGroup(prompt, group) {
            return _.any(group.prompts, {id: prompt.id});
        }


        function updatePromptInDefaultGroups() {

            vm.prompts.forEach(function(prompt) {
                vm.inDefaultGroup[prompt.id] = false;
                vm.inDistrictDefaultGroup[prompt.id] = false;
                vm.inSchoolDefaultGroup[prompt.id] = false;
                vm.inEvaluatorDefaultGroup[prompt.id] = false;
            });

            vm.groups.forEach(function(group) {
                group.prompts.forEach(function(prompt) {
                    vm.inDefaultGroup[prompt.id] = true;
                    if (group.createdAsAdmin && group.schoolCode!=='') {
                        vm.inSchoolDefaultGroup[prompt.id] = true;
                    }
                    else if (group.createdAsAdmin) {
                        vm.inDistrictDefaultGroup[prompt.id] = true;
                    }
                    else {
                        vm.inEvaluatorDefaultGroup[prompt.id] = true;
                    }
                });
            });

            vm.prompts.forEach(function(prompt) {
                vm.assignableToDefaultGroup[prompt.id] = !vm.inDefaultGroup[prompt.id];

                vm.removableFromDefaultGroup[prompt.id] =
                        ((vm.inDistrictDefaultGroup[prompt.id] && vm.gridOption.districtAdmin) ||
                         (vm.inSchoolDefaultGroup[prompt.id] && vm.gridOption.schoolAdmin) ||
                         (vm.inEvaluatorDefaultGroup[prompt.id] && vm.gridOption.evaluator));

                vm.requiredPrompt[prompt.id] = vm.inDefaultGroup[prompt.id] &&
                                                !vm.removableFromDefaultGroup[prompt.id];

                prompt.editable = !prompt.assigned && prompt.isOwner;
            });
        }

        function loadPrompts() {
            return vm.gridOption.getPromptsFcn().then(function (prompts) {
                vm.prompts = prompts;
                return prompts;
            });
        }

        function setGroupFrameworkNodeTitle(group) {
            var fn = _.findWhere(activeUserContextService.context.frameworkContext.stateFramework.frameworkNodes,
                {id: group.sgFrameworkNodeId});
            group.frameworkNodeShortName = fn.shortName;
            group.frameworkNodeTitle = fn.title;
        }

        function findDefaultGroup() {
            var group = _.find(vm.groups, {isOwner: true});
            if (!group) {
                group = addNewDefaultsPromptGroup();
            }

            return group;
        }

        function assignToDefaultGroup(prompt) {
            var group = findDefaultGroup();
            group.prompts.push(prompt);

            savePromptGroup(group, vm.prompt).then(function() {
                updatePromptInDefaultGroups();
            })
        }

        function removeFromDefaultGroup(prompt) {
            var group = findDefaultGroup();
            if (vm.gridOption.districtAdmin) {
                vm.inDistrictDefaultGroup[prompt.id] = false;
            }
            else if (vm.gridOption.schoolAdmin) {
                vm.inSchoolDefaultGroup[prompt.id] = false;
            }
            else {
                vm.inEvaluatorDefaultGroup[prompt.id] = false;
            }
            group.prompts = _.reject(group.prompts, {id: prompt.id});
            userPromptService.saveUserPromptGroup(group).then(function() {
                updatePromptInDefaultGroups();
            })

        }

        function editPrompt(prompt) {
            vm.showNewPromptArea = true;
            vm.editPromptInGroup = [];

            if (!prompt) {
                vm.prompt = userPromptService.getNewUserPrompt(vm.gridOption.promptType, false);
                vm.editPromptModel='';
                if (!vm.studentGrowth) {
                    vm.defaultGroup = findDefaultGroup();
                    vm.editPromptInGroup[vm.defaultGroup.id] = promptInGroup(vm.prompt, vm.defaultGroup);
                }
            }
            else {
                vm.prompt = prompt;
                vm.editPromptModel = vm.prompt.prompt;
                if (vm.studentGrowth) {
                     vm.groups.forEach(function(group) {
                        vm.editPromptInGroup[group.id] = promptInGroup(prompt, group);
                    });
                }
                else {
                    vm.defaultGroup = findDefaultGroup();
                    vm.editPromptInGroup[vm.defaultGroup.id] = promptInGroup(prompt, vm.defaultGroup);
                }
            }
        }

        function cancelAddPrompt() {
            vm.showNewPromptArea = false;
            vm.prompt = null;
        }

        function savePromptGroup(group, prompt) {
            return userPromptService.saveUserPromptGroup(group).then(function(group) {
                if (vm.gridOption.promptType === enums.PromptType.StudentGrowthGoal) {
                    updatePromptInStudentGrowthGroups(prompt);
                }
                else {
                    updatePromptInDefaultGroups();
                }
            })
        }


        function togglePromptInGroup(prompt, group) {
            if (_.find(group.prompts, {id: prompt.id})) {
                if (!vm.editPromptInGroup[group.id]) {
                    group.prompts = _.reject(group.prompts, {id: prompt.id});
                }
            }
            else {
                if (vm.editPromptInGroup[group.id]) {
                    group.prompts.push(prompt);
                }
            }
        }

        function savePrompt(newPrompt) {

            if (vm.editPromptModel === '') {
                return;
            }

            vm.prompt.prompt = vm.editPromptModel;
            vm.showNewPromptArea = false;
            userPromptService.saveUserPrompt(vm.prompt).then(function() {
                if (newPrompt) {
                    vm.prompts.push(vm.prompt);
                }

                // for student growth prompts we only allow them to be set at the district level
                // so there are only district groups
                if (vm.studentGrowth) {
                    vm.groups.forEach(function(group) {
                        togglePromptInGroup(vm.prompt, group);
                        savePromptGroup(group, vm.prompt).then(function() {
                            updatePromptInStudentGrowthGroups(vm.prompt);
                        })
                    })
                }
                else {
                   togglePromptInGroup(vm.prompt, vm.defaultGroup);
                    savePromptGroup(vm.defaultGroup, vm.prompt).then(function() {
                        updatePromptInDefaultGroups();
                    })
                }
            });
        }

        function addNewDefaultsPromptGroup() {
            var name = vm.gridOption.title + ' Defaults';
            var group = userPromptService.getNewUserPromptGroup(vm.gridOption.promptType, name);
            vm.groups.push(group);
            return group;
        }

        function closeLibraryPromptsArea() {
            vm.showLibraryPromptArea = false;
            loadPrompts().then(function() {

            });
        }

        function showLibraryPrompts() {
            vm.showLibraryPromptArea = true;
        }

        function deletePrompt(prompt) {
            userPromptService.deleteUserPrompt(prompt.id).then(function() {
                vm.prompts = _.reject(vm.prompts, {id: prompt.id});
            });
        }

        function showLibItemsModal() {
            vm.gridOption.alreadySelectedPrompts = _.filter(vm.prompts, function(prompt) { return prompt.userPromptLibItemId!=null});;
            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'app/prompt/views/lib-prompts.directive.html',
                controller: 'viewLibPromptsModalController as vm',
                size: 'lg',
                resolve: {
                    gridOption: vm.gridOption
                }
            });

            modalInstance.result.then(function (result) {
                var prompts = _.filter(result.prompts, {selected:true});
                userPromptService.addUserPromptLibItems(prompts).then(function() {
                    loadData();
                })
            });
        }
    }
})();


