/// <reference path="../prompt.module.ts" />

(function () {

    angular
        .module('stateeval.prompt')
        .factory('userPromptService', userPromptService);

    userPromptService.$inject = ['_', 'enums', '$http', '$q', 'logger', 'config', 'activeUserContextService'];

    /* @ngInject */
    function userPromptService(_, enums, $http, $q, logger, config, activeUserContextService) {
        var service = {
            getUserPromptById: getUserPromptById,
            getUserPromptResponseById: getUserPromptResponseById,
            getNewUserPrompt: getNewUserPrompt,
            saveUserPrompt: saveUserPrompt,
            getAssignableUserPrompts: getAssignableUserPrompts,
            insertNewConfPrompt: insertNewConfPrompt,
            getEvaluatorDefinedUserPrompts: getEvaluatorDefinedUserPrompts,
            getSchoolAdminDefinedUserPrompts: getSchoolAdminDefinedUserPrompts,
            getDistrictAdminDefinedUserPrompts: getDistrictAdminDefinedUserPrompts,
            deleteUserPrompt: deleteUserPrompt,
            assignPrompt: assignPrompt,
            unassignPrompt: unassignPrompt,
            getObservationUserPromptResponses: getObservationUserPromptResponses,
            saveObservationUserPromptResponse: saveObservationUserPromptResponse,
            getStudentGrowthUserPromptResponses: getStudentGrowthUserPromptResponses,
            saveStudentGrowthUserPromptResponse: saveStudentGrowthUserPromptResponse,
            saveUserPromptGroup: saveUserPromptGroup,
            getNewUserPromptGroup: getNewUserPromptGroup,
            getPromptGroupDefaultsForEvaluator: getPromptGroupDefaultsForEvaluator,
            getUserPromptLibItems: getUserPromptLibItems,
            addUserPromptLibItems: addUserPromptLibItems,
            getDistrictAdminDefinedUserPromptGroups: getDistrictAdminDefinedUserPromptGroups,
            getSchoolAdminDefinedUserPromptGroups: getSchoolAdminDefinedUserPromptGroups,
            getEvaluatorDefinedUserPromptGroups: getEvaluatorDefinedUserPromptGroups,
            getUserPromptGroupById: getUserPromptGroupById,
            promptBankGetPrompts: promptBankGetPrompts,
            promptBankGetPromptGroups: promptBankGetPromptGroups,
            addPrompts: addPrompts
        };

        return service;

        function getNewUserPrompt(promptType, isPrivate) {

            var workAreaTag = activeUserContextService.context.orientation.workAreaTag;
            var userIsASchoolAdmin = (workAreaTag.indexOf('SA') === 0);
            var userIsADistrictAdmin = (workAreaTag.indexOf('DA') === 0);

            return {
                id: 0,
                isOwner: true,
                assigend: false,
                promptType: promptType,
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                createdByUserId: activeUserContextService.user.id,
                private: isPrivate,
                createdAsAdmin: userIsASchoolAdmin || userIsADistrictAdmin,
                prompt: ''
            }
        }

        function saveUserPrompt(userPrompt) {
            var url = config.apiUrl + 'userprompt/save';
            return $http.post(url, userPrompt).then(function (response) {
                userPrompt.id = response.data.id;
                return response.data;
            });
        }

        function getUserPromptRequestParam(promptType) {
            return {
                promptType: promptType,
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                createdByUserId: activeUserContextService.user.id,
                currentUserId: activeUserContextService.user.id,
                roleName: activeUserContextService.context.orientation.roleName
            }
        }

        function getEvaluatorDefinedUserPrompts(promptType) {
            var requestParam = getUserPromptRequestParam(promptType);
            var url = config.apiUrl + 'toruserprompts/promptbank';
            return $http.get(url, { params: requestParam }).then(function (response) {
                return response.data;
            });
        }

        function getSchoolAdminDefinedUserPrompts(promptType) {
            var requestParam = getUserPromptRequestParam(promptType);
            var url = config.apiUrl + 'sauserprompts/promptbank';
            return $http.get(url, { params: requestParam }).then(function (response) {
                return response.data;
            });
        }

        function addUserPromptLibItems(prompts) {
            var newPrompts = [];
            prompts.forEach(function(prompt) {
                var newPrompt = getNewUserPrompt(prompt.promptType, false);
                newPrompt.userPromptLibItemId = prompt.id;
                newPrompt.prompt = prompt.prompt;
                newPrompts.push(newPrompt);
            });
            var requestParam = {
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                createdByUserId: activeUserContextService.user.id,
                newPrompts: newPrompts
            };
            var url = config.apiUrl + 'libitemuserprompts/promptbank';
            return $http.post(url, requestParam);
        }

        function getUserPromptLibItems(promptType) {
            var requestParam = {
                evaluationType: activeUserContextService.context.workArea().evaluationType,
                promptType: promptType
            };
            var url = config.apiUrl + 'libitemuserprompts/promptbank';
            return $http.get(url, { params: requestParam }).then(function (response) {
                return response.data;
            });
        }

        function getDistrictAdminDefinedUserPrompts(promptType) {
            var requestParam = getUserPromptRequestParam(promptType);
            var url = config.apiUrl + 'dauserprompts/promptbank';
            return $http.get(url, { params: requestParam }).then(function (response) {
                return response.data;
            });
        }

        function getNewUserPromptGroup(promptType, name) {

            var workAreaTag = activeUserContextService.context.orientation.workAreaTag;
            var userIsASchoolAdmin = (workAreaTag.indexOf('SA') === 0);
            var userIsADistrictAdmin = (workAreaTag.indexOf('DA') === 0);

            return {
                id: 0,
                isOwner: true,
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                promptType: promptType,
                name: name,
                createdByUserId: activeUserContextService.user.id,
                createdAsAdmin: userIsASchoolAdmin || userIsADistrictAdmin,
                prompts: []
            }
        }

        function getUserPromptGroupRequestParam(promptType) {
            return {
                promptType: promptType,
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                schoolCode: activeUserContextService.context.orientation.schoolCode,
                createdByUserId: activeUserContextService.user.id,
                currentUserId: activeUserContextService.user.id,
                roleName: activeUserContextService.context.orientation.roleName
            }
        }


        function getDistrictAdminDefinedUserPromptGroups(promptType) {
            var requestParam = getUserPromptGroupRequestParam(promptType);
            var url = config.apiUrl + 'userpromptgroups/da';
            return $http.get(url, {params: requestParam}).then(function (response) {
                return response.data;
            });
        }

        function getSchoolAdminDefinedUserPromptGroups(promptType) {
            var requestParam = getUserPromptGroupRequestParam(promptType);
            var url = config.apiUrl + 'userpromptgroups/sa';
            return $http.get(url, {params: requestParam}).then(function (response) {
                return response.data;
            });
        }

        function getEvaluatorDefinedUserPromptGroups(promptType) {
            var requestParam = getUserPromptGroupRequestParam(promptType);
            var url = config.apiUrl + 'userpromptgroups/tor';
            return $http.get(url, {params: requestParam}).then(function (response) {
                return response.data;
            });
        }

        function getPromptGroupDefaultsForEvaluator(promptType) {
            var requestParam = {
                frameworkContextId: activeUserContextService.context.frameworkContext.id,
                createdByUserId: activeUserContextService.user.id,
                promptType: promptType
            };
            var url = config.apiUrl + 'userpromptgroupdefaults/';
            return $http.get(url, {params: requestParam}).then(function (response) {
                return response.data;
            });
        }

        function saveUserPromptGroup(userPromptGroup) {
            var url = config.apiUrl + 'userpromptgroup/save';
            return $http.post(url, userPromptGroup).then(function (response) {
                userPromptGroup.id = response.data.id;
                return response.data;
            });
        }

        function deleteUserPrompt(userPromptId) {
            return $http.delete(config.apiUrl + 'userprompt/delete/' + userPromptId).then(function () {
            });
        }

        function getUserPromptById(userPromptId) {
            var url = config.apiUrl + 'userPrompt/' + userPromptId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getUserPromptResponseById(userPromptResponseId) {
            var url = config.apiUrl + 'userPromptResponses/' + userPromptResponseId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getUserPromptGroupById(userPromptGroupId) {
            var url = config.apiUrl + 'userpromptgroups/' + userPromptGroupId;
            return $http.get(url).then(function (response) {
                return response.data;
            });
        }

        function getAssignableUserPrompts(promptType) {
            var userPromptParam = getUserPromptRequestParam(promptType);
            var url = config.apiUrl + 'userprompts/assignable';
            return $http.get(url, { params: userPromptParam }).then(function (response) {
                return response.data;
            });
        }

        function getObservationUserPromptResponses(observationId, promptType) {
            var url = config.apiUrl + "userpromptresponsesobservation/" + observationId + '/' + promptType;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function saveObservationUserPromptResponse(userPromptResponse) {
            var url = config.apiUrl + "userpromptresponseobservation/save";

            return $http.post(url, userPromptResponse).then(function(response) {
                return response.data;
            });
        }

        function getStudentGrowthUserPromptResponses(goal) {
            var url = config.apiUrl + "userpromptresponsessg/" + goal.id;
            return $http.get(url).then(function(response) {
                return response.data;
            });
        }

        function saveStudentGrowthUserPromptResponse(userPromptResponse) {
            var url = config.apiUrl + "userpromptresponsesg/save";

            return $http.post(url, userPromptResponse).then(function(response) {
                return response.data;
            });
        }

        function insertNewConfPrompt(addToBank, observationId, promptType, prompt) {
            var newPrompt = getNewUserPrompt(promptType, !addToBank);
            newPrompt.observationId = addToBank?null:observationId;
            newPrompt.prompt = prompt;
            return saveUserPrompt(newPrompt).then(function() {
                return assignPrompt(newPrompt.id, observationId);
            })
        }

        function assignPrompt(userPromptId, observationId) {
            var url = config.apiUrl + 'userprompt/assign';
            return $http.post(url, {
                userPromptId: userPromptId,
                observationId: observationId
            });
        }

        function unassignPrompt(userPromptId, observationId) {
            var url = config.apiUrl + 'userprompt/unassign';
            return $http.post(url, {
                userPromptId: userPromptId,
                observationId: observationId
            });
        }

        function promptBankGetPrompts(promptType) {

            var workAreaTag = activeUserContextService.context.orientation.workAreaTag;
            var userIsASchoolAdmin = (workAreaTag.indexOf('SA') === 0);
            var userIsADistrictAdmin = (workAreaTag.indexOf('DA') === 0);
            var userIsDistrictEvaluator = (workAreaTag === 'DE' || workAreaTag === 'DTE');
            var userIsEvaluator = !userIsADistrictAdmin && !userIsASchoolAdmin;

            var showSchoolPrompts = !userIsADistrictAdmin && !userIsDistrictEvaluator;
            var showEvaluatorPrompts = !userIsADistrictAdmin && !userIsASchoolAdmin;

            var prompts = [];

            return getDistrictAdminDefinedUserPrompts(promptType)
                .then(function(districtPrompts) {
                    addPrompts(prompts, districtPrompts, userIsADistrictAdmin);
                    if (showSchoolPrompts) {
                        return getSchoolAdminDefinedUserPrompts(promptType);
                    }
                    else {
                        return [];
                    }
                }).then(function (schoolPrompts) {
                    addPrompts(prompts, schoolPrompts, userIsASchoolAdmin);
                    if (showEvaluatorPrompts) {
                        return getEvaluatorDefinedUserPrompts(promptType);
                    }
                    else {
                        return [];
                    }
                }).then(function (evaluatorPrompts) {
                    addPrompts(prompts, evaluatorPrompts, true);
                    return prompts;
                });
        }

        function addPrompts(allPrompts, newPrompts, isOwner) {
            newPrompts.forEach(function(prompt) {
                prompt.isOwner = isOwner;
                allPrompts.push(prompt);
            });
        }

        function promptBankGetPromptGroups(promptType) {

            var workAreaTag = activeUserContextService.context.orientation.workAreaTag;
            var userIsASchoolAdmin = (workAreaTag.indexOf('SA') === 0);
            var userIsADistrictAdmin = (workAreaTag.indexOf('DA') === 0);
            var userIsDistrictEvaluator = (workAreaTag === 'DE' || workAreaTag === 'DTE');

            var showSchoolPrompts = !userIsADistrictAdmin && !userIsDistrictEvaluator;
            var showEvaluatorPrompts = !userIsADistrictAdmin && !userIsASchoolAdmin;

            var promptGroups = [];

            return getDistrictAdminDefinedUserPromptGroups(promptType)
                .then(function(districtPromptGroups) {
                    addPromptGroups(promptGroups, districtPromptGroups, userIsADistrictAdmin);
                    if (showSchoolPrompts) {
                        return getSchoolAdminDefinedUserPromptGroups(promptType);
                    }
                    else {
                        return [];
                    }
                }).then(function (schoolPromptGroups) {
                    addPromptGroups(promptGroups, schoolPromptGroups, userIsASchoolAdmin);
                    if (showEvaluatorPrompts) {
                        return getEvaluatorDefinedUserPromptGroups(promptType);
                    }
                    else {
                        return [];
                    }
                }).then(function (evaluatorPromptGroups) {
                    addPromptGroups(promptGroups, evaluatorPromptGroups, true);
                    return promptGroups;
                });
        }

        function addPromptGroups(allPromptGroups, newPromptGroups, isOwner) {
            newPromptGroups.forEach(function(promptGroup) {
                promptGroup.isOwner = isOwner;
                allPromptGroups.push(promptGroup);
            });
        }
    }
})();

