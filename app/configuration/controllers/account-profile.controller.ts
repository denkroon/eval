(function() {
    'use strict';

    class AccountProfileController {

        uploading = false;
        uploadError: string;
        imageUrl: string;

        constructor(
            private user: any,
            private userService: any,
            private $scope: ng.IScope
        ) {
            this.imageUrl = user.profileImageUrl || '/images/blank-profile-48x48.png';
        }

        uploadFile() {
            this.uploading = true;
            this.uploadError = null;
            uploadcare.openDialog(null, {
                publicKey: '0c18992f124cb62a6940',
                imagesOnly: true,
                previewStep: true,
                crop: "48x48"
            }).then(file => {
                file.then(fileInfo => {
                    this.userService.updateProfileImageUrl(fileInfo.cdnUrl);
                    this.user.profileImageUrl = this.imageUrl = fileInfo.cdnUrl;
                    this.uploading = false;
                    this.$scope.$digest();
                }, error => {
                    if (error == 'image') {
                        this.uploadError = 'You may only upload an image'
                    }

                    this.uploading = false;
                    this.$scope.$digest();
                })
            }, () => {
                // this is called when the upload dialog is cancelled
                this.uploading = false;
                this.$scope.$digest();
            })
        }
    }

    angular
        .module('stateeval.configuration')
        .controller('accountProfileController', AccountProfileController);

})();
