(function () {
    'use strict';

    angular
        .module('stateeval.configuration')
        .controller('accountController', accountController);

    accountController.$inject = ['utils'];

    /* @ngInject */
    function accountController(utils) {

        var vm = this;

        vm.accountTabs = [
            new utils.Link('Profile', 'account-profile'),
            new utils.Link('Mobile Access', 'account-mobile'),
            new utils.Link('Activity Log', 'account-activity'),
            new utils.Link('User Location/Roles', 'account-orientations')
        ];

        activate();

        function activate() {
        }
    }

})();
