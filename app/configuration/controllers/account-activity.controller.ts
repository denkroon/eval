/**
 * Created by anne on 5/3/2016.
 */

(function() {
    'use strict';

    class accountActivityController {

        notifications: [any];
        constructor(
            private notificationService: any,
            private utils: any,
            private $state: any,
            private activeUserContextService: any,
            private startupService: any
        ) {

            this.notificationService.getNotificationsForUser()
                .then((notifications) => {
                    this.notifications = notifications;
                });
        }

        gotoEvidenceCollection(event) {
            this.utils.gotoEvidenceCollection(event, this.activeUserContextService, this.startupService, this.$state);
        }
    }
    angular.module('stateeval')
    .controller('accountActivityController', accountActivityController);
})();
