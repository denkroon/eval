(function () {
    'use strict';

    angular
        .module('stateeval.configuration')
        .controller('accountMobileController', accountMobileController);

    accountMobileController.$inject = ['utils', 'userService', 'user'];

    /* @ngInject */
    function accountMobileController(utils, userService, user) {

        var vm = this;
        vm.user = user;

        vm.getNewKey = getNewKey;

        activate();

        function activate() {
        }

        function getNewKey() {
            // note: getNewMobileAccessKey generates a new key and saves it in the db
            // and then returns it here so don't need to save.
            userService.getNewMobileAccessKey().then(function(key) {
                vm.user.mobileAccessKey = key;
            })
        }
    }

})();
