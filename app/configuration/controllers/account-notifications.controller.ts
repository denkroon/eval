(function () {
    'use strict';

    angular
        .module('stateeval.configuration')
        .controller('accountNotificationsController', accountNotificationsController);

    accountNotificationsController.$inject = ['activeUserContextService', '_', 'emailRecipientService'];

    /* @ngInject */
    function accountNotificationsController(activeUserContextService, _, emailRecipientService) {

        var vm = this;
        vm.mailRecipientConfigs = [];

        var userId = activeUserContextService.user.id;

        vm.saveEmailDeliveryConfigs = saveEmailDeliveryConfigs;

        activate();

        function activate() {
            emailRecipientService.getMailDeliveryTypes()
                .then(function (mailDeliveryTypes) {
                    vm.mailDeliveryTypes = mailDeliveryTypes;
                    return emailRecipientService.getEventTypes();
                })
                .then(function (eventTypes) {
                    vm.eventTypes = eventTypes;
                    return emailRecipientService.getEmailRecipientConfigs(userId);
                })
                .then(function (emailReceipientConfigs) {
                    vm.eventTypes.forEach(function(eventType) {
                        var config = _.find(emailReceipientConfigs, { eventTypeID: eventType.eventTypeId });
                        if (config) {
                            vm.mailRecipientConfigs.push(config);
                        }
                        else {
                            vm.mailRecipientConfigs.push(
                                {
                                    eventTypeName: eventType.name,
                                    id: 0,
                                    recipientId: userId,
                                    eventTypeID: eventType.eventTypeId,
                                    emailDeliveryTypeID: 1
                                });
                            }
                    })
                });
        }

        function saveEmailDeliveryConfigs() {
            emailRecipientService.saveEmailRecipientConfigs(vm.mailRecipientConfigs);
        }
    }

})();
