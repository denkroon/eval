/// <reference path="configuration.module.ts" />

(function() {
angular.module('stateeval.configuration')
    .config(configureRoutes);

configureRoutes.$inject = ['$stateProvider'];

function configureRoutes($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('configuration', {
            url: '/configuration',
            parent: 'default-content',
            abstract: true
        })
        .state('account', {
            url: '/account',
            parent: 'default-content',
            abstract: true,
            resolve: {
                user: function(userService, activeUserContextService) {
                    return userService.getUserById(activeUserContextService.user.id)
                        .then(function(user) {
                            return user;
                        });
                },
            },
            views: {
                'content@base': {
                    templateUrl: 'app/configuration/views/account.html',
                    controller: 'accountController as vm'
                }
            },
        })
        .state("account-profile", {
            url: '/account-profile',
            parent: 'account',
            views: {
                'content@account': {
                    templateUrl: 'app/configuration/views/account-profile.html',
                    controller: 'accountProfileController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Account - Profile'
            }
        })
        .state("account-activity", {
            url: '/account-activity',
            parent: 'account',
            views: {
                'content@account': {
                    templateUrl: 'app/configuration/views/account-activity.html',
                    controller: 'accountActivityController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Account - Activity Log'
            }
        })
        .state("account-orientations", {
            url: '/account-orientations',
            parent: 'account',
            views: {
                'content@account': {
                    templateUrl: 'app/configuration/views/account-orientations.html'
                }
            },
            ncyBreadcrumb: {
                label: 'Account - Location/Roles'
            }
        })
        .state("account-mobile", {
            url: '/account-mobile',
            parent: 'account',
            views: {
                'content@account': {
                    templateUrl: 'app/configuration/views/account-mobile.html',
                    controller: 'accountMobileController as vm'
                }
            },
            ncyBreadcrumb: {
                label: 'Account - Mobile Access'
            }
        });
}
})();