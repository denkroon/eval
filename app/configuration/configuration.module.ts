/// <reference path="../core/core.module.ts" />

(function() {
    'use strict';

    angular.module('stateeval.configuration', ['stateeval.core', 'monospaced.qrcode']);
})();
